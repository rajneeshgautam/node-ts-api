import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_faq_categories_ls} from "./gur_faq_categories_ls";
import {gur_faq_lists} from "./gur_faq_lists";


@Entity("gur_faq_categories" ,{schema:"sportsmatik_local" } )
export class gur_faq_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fac_id"
        })
    fac_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fac_photo"
        })
    fac_photo:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fac_slug"
        })
    fac_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fac_status"
        })
    fac_status:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fac_createdby"
        })
    fac_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"fac_modifiedby"
        })
    fac_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fac_modifiedtime"
        })
    fac_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_faq_categories_ls, (gur_faq_categories_ls: gur_faq_categories_ls)=>gur_faq_categories_ls.falFac,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFaqCategoriesLss:gur_faq_categories_ls[];
    

   
    @ManyToMany(()=>gur_faq_lists, (gur_faq_lists: gur_faq_lists)=>gur_faq_lists.gurFaqCategoriess,{  nullable:false, })
    @JoinTable({ name:'gur_faq_lists_categories'})
    gurFaqListss:gur_faq_lists[];
    
}
