import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_order_items} from "./gur_order_items";
import {gur_plan_features} from "./gur_plan_features";
import {gur_plans} from "./gur_plans";
import {gur_user_plans} from "./gur_user_plans";
import {gur_user_plans_details} from "./gur_user_plans_details";


@Entity("gur_plan_categories" ,{schema:"sportsmatik_local" } )
@Index("plc_slug",["plc_slug",],{unique:true})
export class gur_plan_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"plc_id"
        })
    plc_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"plc_name"
        })
    plc_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"plc_slug"
        })
    plc_slug:string;
        

    @Column("text",{ 
        nullable:true,
        name:"plc_description"
        })
    plc_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"plc_image"
        })
    plc_image:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"plc_createdby"
        })
    plc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"plc_modifiedby"
        })
    plc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"plc_modifiedtime"
        })
    plc_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_order_items, (gur_order_items: gur_order_items)=>gur_order_items.oriPlc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrderItemss:gur_order_items[];
    

   
    @OneToMany(()=>gur_plan_features, (gur_plan_features: gur_plan_features)=>gur_plan_features.plfPlc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanFeaturess:gur_plan_features[];
    

   
    @OneToMany(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.plnPlc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanss:gur_plans[];
    

   
    @OneToMany(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.pchPlc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlanss:gur_user_plans[];
    

   
    @OneToMany(()=>gur_user_plans_details, (gur_user_plans_details: gur_user_plans_details)=>gur_user_plans_details.updCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansDetailss:gur_user_plans_details[];
    
}
