import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_roller_derby_profile" ,{schema:"sportsmatik_local" } )
@Index("rod_usr_id",["rodUsr",])
@Index("rod_positions",["rodPositions",])
@Index("rod_createdby_user",["rodCreatedbyUser",])
@Index("rod_positions2",["rodPositions2",])
export class gur_user_roller_derby_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"rod_id"
        })
    rod_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRollerDerbyProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rod_usr_id'})
    rodUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRollerDerbyProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rod_positions1'})
    rodPositions:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRollerDerbyProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rod_positions2'})
    rodPositions2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rod_modifiedtime"
        })
    rod_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRollerDerbyProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rod_createdby_user'})
    rodCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rod_modifiedby"
        })
    rod_modifiedby:string | null;
        
}
