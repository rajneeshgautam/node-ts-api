import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_inline_freestyle_skating_profile" ,{schema:"sportsmatik_local" } )
@Index("ifs_usr_id",["ifsUsr",])
@Index("ifs_createdby_user",["ifsCreatedbyUser",])
export class gur_user_inline_freestyle_skating_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ifs_id"
        })
    ifs_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineFreestyleSkatingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ifs_usr_id'})
    ifsUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"ifs_disciplines"
        })
    ifs_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ifs_modifiedtime"
        })
    ifs_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineFreestyleSkatingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ifs_createdby_user'})
    ifsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ifs_modifiedby"
        })
    ifs_modifiedby:string | null;
        
}
