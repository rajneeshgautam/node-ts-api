import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_pickleball_profile" ,{schema:"sportsmatik_local" } )
@Index("pic_usr_id",["picUsr",])
@Index("pic_createdby_user",["picCreatedbyUser",])
export class gur_user_pickleball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pic_id"
        })
    pic_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPickleballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pic_usr_id'})
    picUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pic_hand"
        })
    pic_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pic_modifiedtime"
        })
    pic_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPickleballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pic_createdby_user'})
    picCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pic_modifiedby"
        })
    pic_modifiedby:string | null;
        
}
