import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_user_media_gallery} from "./gur_user_media_gallery";


@Entity("gur_user_albums" ,{schema:"sportsmatik_local" } )
@Index("alb_uty_id",["albUty",])
@Index("alb_createdby_user",["albCreatedbyUser",])
export class gur_user_albums {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"alb_id"
        })
    alb_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserAlbumss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'alb_uty_id'})
    albUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"alb_user_id"
        })
    alb_user_id:number;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"alb_sort_order"
        })
    alb_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"alb_title"
        })
    alb_title:string;
        

    @Column("date",{ 
        nullable:false,
        name:"alb_date"
        })
    alb_date:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"alb_cover_pic"
        })
    alb_cover_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"alb_status"
        })
    alb_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"alb_admin_review"
        })
    alb_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAlbumss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'alb_createdby_user'})
    albCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"alb_createdby"
        })
    alb_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"alb_modifiedby"
        })
    alb_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"alb_modifiedtime"
        })
    alb_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_media_gallery, (gur_user_media_gallery: gur_user_media_gallery)=>gur_user_media_gallery.umgAlb,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserMediaGallerys:gur_user_media_gallery[];
    
}
