import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_profile_completeness" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["upc_uty_id",])
@Index("upc_sub_user_id",["upc_sub_user_id",])
export class gur_profile_completeness {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"upc_id"
        })
    upc_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"upc_uty_id"
        })
    upc_uty_id:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"upc_user_id"
        })
    upc_user_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"upc_sub_user_id"
        })
    upc_sub_user_id:number | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"upc_profile_completeness"
        })
    upc_profile_completeness:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_email_verify"
        })
    upc_user_email_verify:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_mobile_verify"
        })
    upc_user_mobile_verify:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_basic_profile"
        })
    upc_user_basic_profile:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_intro"
        })
    upc_user_intro:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_phy_stats"
        })
    upc_user_phy_stats:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_education"
        })
    upc_user_education:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_user_employment"
        })
    upc_user_employment:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_coach_history"
        })
    upc_coach_history:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_profile_pic"
        })
    upc_profile_pic:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_inst_email_verify"
        })
    upc_inst_email_verify:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_inst_mobile_verify"
        })
    upc_inst_mobile_verify:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_inst_basic_profile"
        })
    upc_inst_basic_profile:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_inst_intro"
        })
    upc_inst_intro:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_inst_tas"
        })
    upc_inst_tas:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_inst_trophies"
        })
    upc_inst_trophies:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_sports"
        })
    upc_sports:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_competition"
        })
    upc_competition:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_stats"
        })
    upc_stats:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_portfolio"
        })
    upc_portfolio:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_professional_details"
        })
    upc_professional_details:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_services"
        })
    upc_services:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_product_services"
        })
    upc_product_services:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_sponsorship"
        })
    upc_sponsorship:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_events"
        })
    upc_events:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_facilities"
        })
    upc_facilities:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_membership"
        })
    upc_membership:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_recruitment_history"
        })
    upc_recruitment_history:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_gallery"
        })
    upc_gallery:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upc_is_user_verified"
        })
    upc_is_user_verified:boolean;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"upc_modifiedtime"
        })
    upc_modifiedtime:Date;
        
}
