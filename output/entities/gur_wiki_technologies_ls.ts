import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_technologies} from "./gur_wiki_technologies";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_technologies_ls" ,{schema:"sportsmatik_local" } )
@Index("tec_title",["tel_title",])
@Index("tel_tec_id",["telTec",])
@Index("tec_lng_code",["telLngCode",])
export class gur_wiki_technologies_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"tel_id"
        })
    tel_id:string;
        

   
    @ManyToOne(()=>gur_wiki_technologies, (gur_wiki_technologies: gur_wiki_technologies)=>gur_wiki_technologies.gurWikiTechnologiesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tel_tec_id'})
    telTec:gur_wiki_technologies | null;


    @Column("varchar",{ 
        nullable:true,
        name:"tel_title"
        })
    tel_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"tel_desc"
        })
    tel_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tel_meta_title"
        })
    tel_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tel_meta_desc"
        })
    tel_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiTechnologiesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tel_lng_code'})
    telLngCode:gur_languages | null;

}
