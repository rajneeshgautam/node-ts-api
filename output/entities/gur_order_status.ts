import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_orders_old} from "./gur_orders_old";
import {gur_plan_orders} from "./gur_plan_orders";


@Entity("gur_order_status" ,{schema:"sportsmatik_local" } )
export class gur_order_status {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ors_id"
        })
    ors_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"ors_value"
        })
    ors_value:string;
        

   
    @OneToMany(()=>gur_orders_old, (gur_orders_old: gur_orders_old)=>gur_orders_old.ordStatus,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrdersOlds:gur_orders_old[];
    

   
    @OneToMany(()=>gur_plan_orders, (gur_plan_orders: gur_plan_orders)=>gur_plan_orders.ordStatus,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurPlanOrderss:gur_plan_orders[];
    
}
