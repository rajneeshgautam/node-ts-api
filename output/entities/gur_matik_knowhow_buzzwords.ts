import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_matik_knowhow_buzzwords_ls} from "./gur_matik_knowhow_buzzwords_ls";


@Entity("gur_matik_knowhow_buzzwords" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_buzzwords_ibfk_1",["khbMkh",])
@Index("gur_matik_knowhow_buzzwords_ibfk_2",["khbKnc",])
export class gur_matik_knowhow_buzzwords {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khb_id"
        })
    khb_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowBuzzwordss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khb_mkh_id'})
    khbMkh:gur_matik_knowhow | null;


   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowBuzzwordss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khb_knc_id'})
    khbKnc:gur_matik_knowhow_tab_categories | null;


    @Column("varchar",{ 
        nullable:false,
        name:"khb_createdby"
        })
    khb_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khb_modifiedby"
        })
    khb_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khb_modifiedtime"
        })
    khb_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_buzzwords_ls, (gur_matik_knowhow_buzzwords_ls: gur_matik_knowhow_buzzwords_ls)=>gur_matik_knowhow_buzzwords_ls.kblKhb,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowBuzzwordsLss:gur_matik_knowhow_buzzwords_ls[];
    
}
