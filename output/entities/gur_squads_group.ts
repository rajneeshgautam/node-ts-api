import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_users} from "./gur_users";
import {gur_squads_list} from "./gur_squads_list";


@Entity("gur_squads_group" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["sqdUty",])
@Index("sqd_createdby_user",["sqdCreatedbyUser",])
@Index("sqd_spo_id",["sqdSpo",])
export class gur_squads_group {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sqd_id"
        })
    sqd_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSquadsGroups,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqd_uty_id'})
    sqdUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"sqd_user_id"
        })
    sqd_user_id:number;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurSquadsGroups,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqd_spo_id'})
    sqdSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"sqd_name"
        })
    sqd_name:string;
        

    @Column("text",{ 
        nullable:true,
        name:"sqd_desc"
        })
    sqd_desc:string | null;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSquadsGroups,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqd_createdby_user'})
    sqdCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"sqd_createdby"
        })
    sqd_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sqd_modifiedby"
        })
    sqd_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sqd_modifiedtime"
        })
    sqd_modifiedtime:Date;
        

    @Column("date",{ 
        nullable:true,
        name:"sqd_valid_till"
        })
    sqd_valid_till:string | null;
        

   
    @OneToMany(()=>gur_squads_list, (gur_squads_list: gur_squads_list)=>gur_squads_list.sqlSqd,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSquadsLists:gur_squads_list[];
    
}
