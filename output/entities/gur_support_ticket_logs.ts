import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_support_ticket} from "./gur_support_ticket";
import {gur_support_ticket_comments} from "./gur_support_ticket_comments";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_support_ticket_logs" ,{schema:"sportsmatik_local" } )
@Index("stl_stk_id",["stlStk",])
@Index("stl_str_id",["stlStr",])
@Index("stl_usr_id",["stl_usr_id",])
@Index("stl_usr_type",["stlUsrType",])
export class gur_support_ticket_logs {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"stl_id"
        })
    stl_id:number;
        

   
    @ManyToOne(()=>gur_support_ticket, (gur_support_ticket: gur_support_ticket)=>gur_support_ticket.gurSupportTicketLogss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stl_stk_id'})
    stlStk:gur_support_ticket | null;


   
    @ManyToOne(()=>gur_support_ticket_comments, (gur_support_ticket_comments: gur_support_ticket_comments)=>gur_support_ticket_comments.gurSupportTicketLogss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stl_str_id'})
    stlStr:gur_support_ticket_comments | null;


    @Column("int",{ 
        nullable:false,
        name:"stl_usr_id"
        })
    stl_usr_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSupportTicketLogss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stl_usr_type'})
    stlUsrType:gur_user_types | null;


    @Column("varchar",{ 
        nullable:false,
        name:"stl_log_msg"
        })
    stl_log_msg:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stl_createdby"
        })
    stl_createdby:string | null;
        
}
