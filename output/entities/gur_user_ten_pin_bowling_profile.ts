import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_ten_pin_bowling_profile" ,{schema:"sportsmatik_local" } )
@Index("tpb_usr_id",["tpbUsr",])
@Index("tpb_createdby_user",["tpbCreatedbyUser",])
export class gur_user_ten_pin_bowling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"tpb_id"
        })
    tpb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTenPinBowlingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tpb_usr_id'})
    tpbUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"tpb_hand"
        })
    tpb_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tpb_modifiedtime"
        })
    tpb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTenPinBowlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tpb_createdby_user'})
    tpbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"tpb_modifiedby"
        })
    tpb_modifiedby:string | null;
        
}
