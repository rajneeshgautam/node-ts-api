import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_wiki_awards_list} from "./gur_wiki_awards_list";
import {gur_wiki_awards_sports_wise_ls} from "./gur_wiki_awards_sports_wise_ls";


@Entity("gur_wiki_awards_sports_wise" ,{schema:"sportsmatik_local" } )
@Index("was_spo_id",["wasSpo",])
export class gur_wiki_awards_sports_wise {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"was_id"
        })
    was_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiAwardsSportsWises,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'was_spo_id'})
    wasSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"was_int_federation"
        })
    was_int_federation:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"was_other_federation"
        })
    was_other_federation:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"was_photo"
        })
    was_photo:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["url","file"],
        name:"was_video_type"
        })
    was_video_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"was_video"
        })
    was_video:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"was_createdby"
        })
    was_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"was_modifiedby"
        })
    was_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"was_modifiedtime"
        })
    was_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_awards_list, (gur_wiki_awards_list: gur_wiki_awards_list)=>gur_wiki_awards_list.walWas,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsLists:gur_wiki_awards_list[];
    

   
    @OneToMany(()=>gur_wiki_awards_sports_wise_ls, (gur_wiki_awards_sports_wise_ls: gur_wiki_awards_sports_wise_ls)=>gur_wiki_awards_sports_wise_ls.wslWas,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsSportsWiseLss:gur_wiki_awards_sports_wise_ls[];
    
}
