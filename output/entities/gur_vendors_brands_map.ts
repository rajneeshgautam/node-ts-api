import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";
import {gur_brands} from "./gur_brands";


@Entity("gur_vendors_brands_map" ,{schema:"sportsmatik_local" } )
@Index("vbm_vcm_id",["vbmVen","vbmBrd",],{unique:true})
@Index("vbm_brd_id",["vbmBrd",])
export class gur_vendors_brands_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"vbm_id"
        })
    vbm_id:string;
        

   
    @ManyToOne(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorsBrandsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vbm_ven_id'})
    vbmVen:gur_vendors | null;


   
    @ManyToOne(()=>gur_brands, (gur_brands: gur_brands)=>gur_brands.gurVendorsBrandsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vbm_brd_id'})
    vbmBrd:gur_brands | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vbm_status"
        })
    vbm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vbm_admin_review"
        })
    vbm_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"vbm_modifiedby"
        })
    vbm_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vbm_modifiedtime"
        })
    vbm_modifiedtime:Date;
        
}
