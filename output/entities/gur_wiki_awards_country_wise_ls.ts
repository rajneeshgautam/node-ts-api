import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_awards_country_wise} from "./gur_wiki_awards_country_wise";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_awards_country_wise_ls" ,{schema:"sportsmatik_local" } )
@Index("wcl_wac_id",["wclWac",])
@Index("wcl_lng_code",["wclLngCode",])
export class gur_wiki_awards_country_wise_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wcl_id"
        })
    wcl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_awards_country_wise, (gur_wiki_awards_country_wise: gur_wiki_awards_country_wise)=>gur_wiki_awards_country_wise.gurWikiAwardsCountryWiseLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcl_wac_id'})
    wclWac:gur_wiki_awards_country_wise | null;


    @Column("text",{ 
        nullable:true,
        name:"wcl_desc"
        })
    wcl_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcl_meta_title"
        })
    wcl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcl_meta_desc"
        })
    wcl_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiAwardsCountryWiseLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcl_lng_code'})
    wclLngCode:gur_languages | null;

}
