import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_floorball_profile" ,{schema:"sportsmatik_local" } )
@Index("flr_usr_id",["flrUsr",])
@Index("flr_position1",["flrPosition",])
@Index("flr_position2",["flrPosition2",])
@Index("flr_createdby_user",["flrCreatedbyUser",])
export class gur_user_floorball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"flr_id"
        })
    flr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFloorballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'flr_usr_id'})
    flrUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFloorballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'flr_position1'})
    flrPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFloorballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'flr_position2'})
    flrPosition2:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"flr_hand"
        })
    flr_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"flr_modifiedtime"
        })
    flr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFloorballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'flr_createdby_user'})
    flrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"flr_modifiedby"
        })
    flr_modifiedby:string | null;
        
}
