import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_events} from "./gur_events";


@Entity("gur_events_media" ,{schema:"sportsmatik_local" } )
@Index("psm_stp_id",["evmEvt",])
export class gur_events_media {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"evm_id"
        })
    evm_id:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["photo","video","url"],
        name:"evm_type"
        })
    evm_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"evm_file"
        })
    evm_file:string;
        

   
    @ManyToOne(()=>gur_events, (gur_events: gur_events)=>gur_events.gurEventsMedias,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evm_evt_id'})
    evmEvt:gur_events | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"evm_admin_review"
        })
    evm_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"evm_status"
        })
    evm_status:boolean;
        
}
