import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_address_book" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["adbUty",])
@Index("evt_createdby_user",["adb_createdby_user",])
@Index("adb_user_id",["adb_user_id",])
@Index("evt_dist_city",["adb_dist_city",])
@Index("evt_country",["adb_country",])
@Index("adb_cnt_id",["adb_cnt_id",])
@Index("adb_dis_id",["adb_dis_id",])
@Index("adb_sts_id",["adb_sts_id",])
export class gur_address_book {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"adb_id"
        })
    adb_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurAddressBooks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adb_uty_id'})
    adbUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"adb_user_id"
        })
    adb_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"adb_name"
        })
    adb_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"adb_street_address1"
        })
    adb_street_address1:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"adb_street_address2"
        })
    adb_street_address2:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"adb_dist_city"
        })
    adb_dist_city:string;
        

    @Column("int",{ 
        nullable:true,
        name:"adb_dis_id"
        })
    adb_dis_id:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"adb_state"
        })
    adb_state:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"adb_sts_id"
        })
    adb_sts_id:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"adb_country"
        })
    adb_country:string;
        

    @Column("int",{ 
        nullable:true,
        name:"adb_cnt_id"
        })
    adb_cnt_id:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"adb_pincode"
        })
    adb_pincode:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"adb_email"
        })
    adb_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"adb_phone"
        })
    adb_phone:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"adb_gstin"
        })
    adb_gstin:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"adb_primary"
        })
    adb_primary:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"adb_createdby_user"
        })
    adb_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"adb_createdby"
        })
    adb_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"adb_modifiedby"
        })
    adb_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"adb_modifiedtime"
        })
    adb_modifiedtime:Date;
        
}
