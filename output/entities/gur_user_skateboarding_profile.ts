import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_skateboarding_profile" ,{schema:"sportsmatik_local" } )
@Index("skb_usr_id",["skbUsr",])
@Index("skb_createdby_user",["skbCreatedbyUser",])
export class gur_user_skateboarding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"skb_id"
        })
    skb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkateboardingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'skb_usr_id'})
    skbUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"skb_styles"
        })
    skb_styles:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"skb_modifiedtime"
        })
    skb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkateboardingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'skb_createdby_user'})
    skbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"skb_modifiedby"
        })
    skb_modifiedby:string | null;
        
}
