import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_ads} from "./gur_matik_ads";
import {gur_vendor_categories_map} from "./gur_vendor_categories_map";
import {gur_vendors_products} from "./gur_vendors_products";


@Entity("gur_ads_categories" ,{schema:"sportsmatik_local" } )
@Index("cat_name",["cat_name",],{unique:true})
@Index("cat_slug",["cat_slug",],{unique:true})
export class gur_ads_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cat_id"
        })
    cat_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"cat_name"
        })
    cat_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:100,
        name:"cat_slug"
        })
    cat_slug:string | null;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"cat_parent"
        })
    cat_parent:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"cat_sort_order"
        })
    cat_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"cat_for_merchants"
        })
    cat_for_merchants:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"cat_for_ads"
        })
    cat_for_ads:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"cat_status"
        })
    cat_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cat_createdby"
        })
    cat_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cat_modifiedby"
        })
    cat_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cat_modifiedtime"
        })
    cat_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_ads, (gur_matik_ads: gur_matik_ads)=>gur_matik_ads.adsCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikAdss:gur_matik_ads[];
    

   
    @OneToMany(()=>gur_vendor_categories_map, (gur_vendor_categories_map: gur_vendor_categories_map)=>gur_vendor_categories_map.vcmCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorCategoriesMaps:gur_vendor_categories_map[];
    

   
    @OneToMany(()=>gur_vendor_categories_map, (gur_vendor_categories_map: gur_vendor_categories_map)=>gur_vendor_categories_map.vcmCatParent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorCategoriesMaps2:gur_vendor_categories_map[];
    

   
    @OneToMany(()=>gur_vendors_products, (gur_vendors_products: gur_vendors_products)=>gur_vendors_products.vprCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsProductss:gur_vendors_products[];
    
}
