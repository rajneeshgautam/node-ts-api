import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_netball_stats" ,{schema:"sportsmatik_local" } )
@Index("nts_usr_id",["ntsUsr",])
@Index("nts_ucp_id",["ntsUcp",])
export class gur_user_netball_stats {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"nts_id"
        })
    nts_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserNetballStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'nts_usr_id'})
    ntsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserNetballStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'nts_ucp_id'})
    ntsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"nts_matches_played"
        })
    nts_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"nts_matches_started"
        })
    nts_matches_started:number;
        

    @Column("int",{ 
        nullable:false,
        name:"nts_total_minutes_played"
        })
    nts_total_minutes_played:number;
        

    @Column("int",{ 
        nullable:true,
        name:"nts_matches_won"
        })
    nts_matches_won:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"nts_matches_draw"
        })
    nts_matches_draw:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"nts_matches_lose"
        })
    nts_matches_lose:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_goals_made"
        })
    nts_goals_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_goals_attempted"
        })
    nts_goals_attempted:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_assists"
        })
    nts_assists:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_rebounds"
        })
    nts_rebounds:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_cpr"
        })
    nts_cpr:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_intercepts"
        })
    nts_intercepts:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_deflection"
        })
    nts_deflection:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_penalties"
        })
    nts_penalties:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"nts_turnovers"
        })
    nts_turnovers:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"nts_modifiedtime"
        })
    nts_modifiedtime:Date;
        
}
