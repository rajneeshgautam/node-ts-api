import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_matik_play_user_registration" ,{schema:"sportsmatik_local" } )
@Index("only_one_winner_for_contest",["mpw_mpu_id",],{unique:true})
@Index("mpw_token",["mpw_token",],{unique:true})
@Index("mpw_mpu_id",["mpw_mpu_id",])
export class gur_matik_play_user_registration {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mpw_id"
        })
    mpw_id:string;
        

    @Column("bigint",{ 
        nullable:false,
        unique: true,
        unsigned: true,
        name:"mpw_mpu_id"
        })
    mpw_mpu_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"mpw_token"
        })
    mpw_token:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpw_datetime"
        })
    mpw_datetime:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpw_prize_collected"
        })
    mpw_prize_collected:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"mpw_prize_collected_time"
        })
    mpw_prize_collected_time:Date | null;
        
}
