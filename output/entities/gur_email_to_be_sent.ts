import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_email_template} from "./gur_email_template";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_email_to_be_sent" ,{schema:"sportsmatik_local" } )
@Index("ets_created_by",["ets_created_by",])
@Index("ets_updated_by",["ets_updated_by",])
@Index("ets_uty_id",["etsUty",])
export class gur_email_to_be_sent {

   
    @ManyToOne(()=>gur_email_template, (gur_email_template: gur_email_template)=>gur_email_template.gurEmailToBeSents,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ets_emt_id'})
    etsEmt:gur_email_template | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurEmailToBeSents,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ets_uty_id'})
    etsUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        primary:true,
        name:"ets_usr_id"
        })
    ets_usr_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"ets_from"
        })
    ets_from:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"ets_to"
        })
    ets_to:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ets_subject"
        })
    ets_subject:string;
        

    @Column("mediumtext",{ 
        nullable:false,
        name:"ets_body"
        })
    ets_body:string;
        

    @Column("tinytext",{ 
        nullable:true,
        name:"ets_attachments"
        })
    ets_attachments:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ets_priority"
        })
    ets_priority:boolean;
        

    @Column("enum",{ 
        nullable:false,
        enum:["sendinblue","own-server","mailjet"],
        name:"ets_send_via"
        })
    ets_send_via:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ets_is_sent"
        })
    ets_is_sent:boolean;
        

    @Column("tinytext",{ 
        nullable:true,
        name:"ets_response"
        })
    ets_response:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ets_is_deleted_after_sent"
        })
    ets_is_deleted_after_sent:boolean;
        

    @Column("datetime",{ 
        nullable:false,
        name:"ets_created_at"
        })
    ets_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ets_updated_at"
        })
    ets_updated_at:Date | null;
        

    @Column("int",{ 
        nullable:false,
        name:"ets_created_by"
        })
    ets_created_by:number;
        

    @Column("int",{ 
        nullable:true,
        name:"ets_updated_by"
        })
    ets_updated_by:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ets_is_active"
        })
    ets_is_active:boolean;
        
}
