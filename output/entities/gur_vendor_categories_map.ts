import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";
import {gur_ads_categories} from "./gur_ads_categories";
import {gur_plans} from "./gur_plans";


@Entity("gur_vendor_categories_map" ,{schema:"sportsmatik_local" } )
@Index("cats",["vcmVen","vcmCat",],{unique:true})
@Index("gur_merchant_categories_map_ibfk_2",["vcmCat",])
@Index("gur_vendor_categories_map_ibfk_3",["vcmCatParent",])
@Index("gur_vendor_categories_map_ibfk_4",["vcmPln",])
export class gur_vendor_categories_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"vcm_id"
        })
    vcm_id:string;
        

   
    @ManyToOne(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorCategoriesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vcm_ven_id'})
    vcmVen:gur_vendors | null;


   
    @ManyToOne(()=>gur_ads_categories, (gur_ads_categories: gur_ads_categories)=>gur_ads_categories.gurVendorCategoriesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vcm_cat_id'})
    vcmCat:gur_ads_categories | null;


   
    @ManyToOne(()=>gur_ads_categories, (gur_ads_categories: gur_ads_categories)=>gur_ads_categories.gurVendorCategoriesMaps2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vcm_cat_parent'})
    vcmCatParent:gur_ads_categories | null;


    @Column("varchar",{ 
        nullable:true,
        name:"vcm_cat_desc"
        })
    vcm_cat_desc:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"vcm_status"
        })
    vcm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vcm_admin_review"
        })
    vcm_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vcm_is_deleted"
        })
    vcm_is_deleted:boolean;
        

   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurVendorCategoriesMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vcm_pln_id'})
    vcmPln:gur_plans | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vcm_mens"
        })
    vcm_mens:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vcm_womens"
        })
    vcm_womens:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vcm_boys"
        })
    vcm_boys:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vcm_girls"
        })
    vcm_girls:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"vcm_createdby"
        })
    vcm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vcm_modifiedby"
        })
    vcm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vcm_modifiedtime"
        })
    vcm_modifiedtime:Date;
        
}
