import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events_schedules} from "./gur_world_events_schedules";


@Entity("gur_world_events_results" ,{schema:"sportsmatik_local" } )
@Index("gur_world_events_results_ibfk_1",["werEvs",])
export class gur_world_events_results {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wer_id"
        })
    wer_id:string;
        

   
    @ManyToOne(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.gurWorldEventsResultss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wer_evs_id'})
    werEvs:gur_world_events_schedules | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wer_stage"
        })
    wer_stage:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"wer_result"
        })
    wer_result:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"wer_createdby"
        })
    wer_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wer_modifiedby"
        })
    wer_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wer_modifiedtime"
        })
    wer_modifiedtime:Date;
        
}
