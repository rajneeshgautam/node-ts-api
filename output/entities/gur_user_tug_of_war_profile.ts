import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_tug_of_war_profile" ,{schema:"sportsmatik_local" } )
@Index("tow_usr_id",["towUsr",])
@Index("tow_createdby_user",["towCreatedbyUser",])
export class gur_user_tug_of_war_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"tow_id"
        })
    tow_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTugOfWarProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tow_usr_id'})
    towUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tow_modifiedtime"
        })
    tow_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTugOfWarProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tow_createdby_user'})
    towCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"tow_modifiedby"
        })
    tow_modifiedby:string | null;
        
}
