import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_site_menu} from "./gur_site_menu";
import {gur_languages} from "./gur_languages";


@Entity("gur_site_menu_ls" ,{schema:"sportsmatik_local" } )
@Index("stl_stm_id",["stlStm",])
@Index("stl_lng_code",["stlLngCode",])
export class gur_site_menu_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"stl_id"
        })
    stl_id:number;
        

   
    @ManyToOne(()=>gur_site_menu, (gur_site_menu: gur_site_menu)=>gur_site_menu.gurSiteMenuLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stl_stm_id'})
    stlStm:gur_site_menu | null;


    @Column("varchar",{ 
        nullable:true,
        name:"stl_value"
        })
    stl_value:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"stl_desc"
        })
    stl_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurSiteMenuLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stl_lng_code'})
    stlLngCode:gur_languages | null;

}
