import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_roller_hockey_quad_profile" ,{schema:"sportsmatik_local" } )
@Index("rhq_usr_id",["rhqUsr",])
@Index("rhq_position1",["rhqPosition",])
@Index("rhq_position2",["rhqPosition2",])
@Index("rhq_createdby_user",["rhqCreatedbyUser",])
export class gur_user_roller_hockey_quad_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"rhq_id"
        })
    rhq_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRollerHockeyQuadProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhq_usr_id'})
    rhqUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"rhq_shoots"
        })
    rhq_shoots:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRollerHockeyQuadProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhq_position1'})
    rhqPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRollerHockeyQuadProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhq_position2'})
    rhqPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rhq_modifiedtime"
        })
    rhq_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRollerHockeyQuadProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhq_createdby_user'})
    rhqCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rhq_modifiedby"
        })
    rhq_modifiedby:string | null;
        
}
