import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_world_events_sports" ,{schema:"sportsmatik_local" } )
@Index("wes_evt_id",["wes_evt_id",])
@Index("wes_schedule",["wes_schedule",])
@Index("wes_spo_id",["wes_spo_id",])
export class gur_world_events_sports {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wes_id"
        })
    wes_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wes_spo_id"
        })
    wes_spo_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wes_events"
        })
    wes_events:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wes_participants"
        })
    wes_participants:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wes_schedule"
        })
    wes_schedule:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wes_evt_id"
        })
    wes_evt_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"wes_createdby"
        })
    wes_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wes_modifiedby"
        })
    wes_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wes_modifiedtime"
        })
    wes_modifiedtime:Date;
        
}
