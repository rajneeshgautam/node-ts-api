import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_user_awards_achievements" ,{schema:"sportsmatik_local" } )
@Index("uaa_uty_id",["uaaUty",])
@Index("uaa_createdby_user",["uaaCreatedbyUser",])
export class gur_user_awards_achievements {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uaa_id"
        })
    uaa_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserAwardsAchievementss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uaa_uty_id'})
    uaaUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"uaa_user_id"
        })
    uaa_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"uaa_name"
        })
    uaa_name:string;
        

    @Column("date",{ 
        nullable:false,
        name:"uaa_date"
        })
    uaa_date:string;
        

    @Column("text",{ 
        nullable:true,
        name:"uaa_description"
        })
    uaa_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uaa_image"
        })
    uaa_image:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uaa_status"
        })
    uaa_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uaa_admin_review"
        })
    uaa_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAwardsAchievementss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uaa_createdby_user'})
    uaaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"uaa_createdby"
        })
    uaa_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"uaa_modifiedby"
        })
    uaa_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uaa_modifiedtime"
        })
    uaa_modifiedtime:Date;
        
}
