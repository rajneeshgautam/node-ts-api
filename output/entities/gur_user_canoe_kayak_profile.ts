import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_canoe_kayak_profile" ,{schema:"sportsmatik_local" } )
@Index("ckp_usr_id",["ckpUsr",])
@Index("ckp_createdby_user",["ckpCreatedbyUser",])
export class gur_user_canoe_kayak_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ckp_id"
        })
    ckp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCanoeKayakProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ckp_usr_id'})
    ckpUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"ckp_disciplines"
        })
    ckp_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ckp_modifiedtime"
        })
    ckp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCanoeKayakProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ckp_createdby_user'})
    ckpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ckp_modifiedby"
        })
    ckp_modifiedby:string | null;
        
}
