import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";


@Entity("gur_institute_branches" ,{schema:"sportsmatik_local" } )
@Index("vbr_country",["ibrCnt",])
@Index("vbr_dis_id",["ibrCit",])
@Index("vbr_ven_id",["ibr_user_id",])
@Index("gur_institute_branches_ibfk_1",["ibrUty",])
@Index("gur_institute_branches_ibfk_5",["ibrCreatedbyAdmin",])
@Index("gur_institute_branches_ibfk_6",["ibrCreatedbyUser",])
export class gur_institute_branches {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ibr_id"
        })
    ibr_id:number;
        

    @Column("bigint",{ 
        nullable:false,
        unsigned: true,
        name:"ibr_user_id"
        })
    ibr_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInstituteBranchess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ibr_uty_id'})
    ibrUty:gur_user_types | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ibr_name"
        })
    ibr_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ibr_address"
        })
    ibr_address:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ibr_address2"
        })
    ibr_address2:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurInstituteBranchess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ibr_cnt_id'})
    ibrCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurInstituteBranchess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ibr_cit_id'})
    ibrCit:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"ibr_pincode"
        })
    ibr_pincode:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ibr_email"
        })
    ibr_email:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ibr_email_verified"
        })
    ibr_email_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"ibr_phone1"
        })
    ibr_phone1:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ibr_phone1_verified"
        })
    ibr_phone1_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"ibr_phone2"
        })
    ibr_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ibr_phone2_verified"
        })
    ibr_phone2_verified:boolean | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ibr_description"
        })
    ibr_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ibr_status"
        })
    ibr_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ibr_admin_review"
        })
    ibr_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInstituteBranchess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ibr_createdby_user'})
    ibrCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurInstituteBranchess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ibr_createdby_admin'})
    ibrCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ibr_createdby"
        })
    ibr_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ibr_modifiedby"
        })
    ibr_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ibr_modifiedtime"
        })
    ibr_modifiedtime:Date;
        
}
