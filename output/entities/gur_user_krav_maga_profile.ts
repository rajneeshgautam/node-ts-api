import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_krav_maga_profile" ,{schema:"sportsmatik_local" } )
@Index("tri_usr_id",["krvUsr",])
@Index("tri_createdby_user",["krvCreatedbyUser",])
export class gur_user_krav_maga_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"krv_id"
        })
    krv_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKravMagaProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'krv_usr_id'})
    krvUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"krv_modifiedtime"
        })
    krv_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKravMagaProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'krv_createdby_user'})
    krvCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"krv_modifiedby"
        })
    krv_modifiedby:string | null;
        
}
