import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_vendors_backup" ,{schema:"sportsmatik_local" } )
@Index("ven_email",["ven_email",],{unique:true})
@Index("ven_gst_number",["ven_gst_number",],{unique:true})
@Index("ven_phone1",["ven_phone1",],{unique:true})
@Index("ven_phone2",["ven_phone2",],{unique:true})
@Index("ven_slug",["ven_slug",],{unique:true})
@Index("ven_bussiness_name",["ven_bussiness_name",])
@Index("ven_city",["ven_dis_id",])
@Index("ven_country",["ven_country",])
@Index("ven_fname",["ven_fname","ven_lname",])
@Index("ven_salutation",["ven_salutation",])
@Index("ven_state",["ven_sts_id",])
export class gur_vendors_backup {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ven_id"
        })
    ven_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"ven_slug"
        })
    ven_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ven_pin"
        })
    ven_pin:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_salutation"
        })
    ven_salutation:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ven_fname"
        })
    ven_fname:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_mname"
        })
    ven_mname:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_lname"
        })
    ven_lname:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["f","m","o"],
        name:"ven_sex"
        })
    ven_sex:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"ven_birthday_old"
        })
    ven_birthday_old:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ven_bussiness_name"
        })
    ven_bussiness_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ven_password"
        })
    ven_password:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"ven_email"
        })
    ven_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_email_verified"
        })
    ven_email_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"ven_phone1"
        })
    ven_phone1:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_phone1_otp"
        })
    ven_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_phone1_otp_time"
        })
    ven_phone1_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_phone1_verified"
        })
    ven_phone1_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"ven_phone2"
        })
    ven_phone2:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_phone2_otp"
        })
    ven_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_phone2_otp_time"
        })
    ven_phone2_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_phone2_verified"
        })
    ven_phone2_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_address"
        })
    ven_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_address2"
        })
    ven_address2:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"ven_country"
        })
    ven_country:number;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_sts_id"
        })
    ven_sts_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_dis_id"
        })
    ven_dis_id:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ven_pincode"
        })
    ven_pincode:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ven_address_google_map"
        })
    ven_address_google_map:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Unverified'",
        enum:["Active","Inactive","Deleted","Unverified","Deactivated"],
        name:"ven_status"
        })
    ven_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_profile_pic"
        })
    ven_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_timeline_pic"
        })
    ven_timeline_pic:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ven_profile_summary"
        })
    ven_profile_summary:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_email_verify_time"
        })
    ven_email_verify_time:Date | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ven_profile_completeness"
        })
    ven_profile_completeness:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_basic_profile_fill"
        })
    ven_basic_profile_fill:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"ven_gst_number"
        })
    ven_gst_number:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_last_login_date"
        })
    ven_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ven_last_login_ip"
        })
    ven_last_login_ip:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"ven_register_date"
        })
    ven_register_date:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_profile_pic_modifiedtime"
        })
    ven_profile_pic_modifiedtime:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ven_password_reset_pin"
        })
    ven_password_reset_pin:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_password_reset_time"
        })
    ven_password_reset_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ven_viewable"
        })
    ven_viewable:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_google_id"
        })
    ven_google_id:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'2'",
        name:"ven_dob_change"
        })
    ven_dob_change:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_is_user_verified"
        })
    ven_is_user_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_createdby"
        })
    ven_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"ven_modifiedby"
        })
    ven_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ven_modifiedtime"
        })
    ven_modifiedtime:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_show_in_knowhow"
        })
    ven_show_in_knowhow:boolean;
        
}
