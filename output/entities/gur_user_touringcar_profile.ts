import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_touringcar_profile" ,{schema:"sportsmatik_local" } )
@Index("tcr_usr_id",["tcrUsr",])
@Index("tcr_createdby_user",["tcrCreatedbyUser",])
export class gur_user_touringcar_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"tcr_id"
        })
    tcr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTouringcarProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcr_usr_id'})
    tcrUsr:gur_users | null;


    @Column("year",{ 
        nullable:true,
        name:"tcr_debut_date"
        })
    tcr_debut_date:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tcr_debut_competition"
        })
    tcr_debut_competition:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tcr_current_team"
        })
    tcr_current_team:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tcr_modifiedtime"
        })
    tcr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTouringcarProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcr_createdby_user'})
    tcrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"tcr_modifiedby"
        })
    tcr_modifiedby:string | null;
        
}
