import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports_logistics_venues} from "./gur_sports_logistics_venues";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_sports_logistics_portfolio" ,{schema:"sportsmatik_local" } )
@Index("slp_venue",["slpVenue",])
@Index("slp_city",["slpCity",])
@Index("slp_cnt_id",["slpCnt",])
@Index("slp_type",["slp_type",])
@Index("slp_user_type",["slpUserType",])
export class gur_sports_logistics_portfolio {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"slp_id"
        })
    slp_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"slp_user_id"
        })
    slp_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSportsLogisticsPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'slp_user_type'})
    slpUserType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        name:"slp_type"
        })
    slp_type:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"slp_client"
        })
    slp_client:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slp_event"
        })
    slp_event:string | null;
        

   
    @ManyToOne(()=>gur_sports_logistics_venues, (gur_sports_logistics_venues: gur_sports_logistics_venues)=>gur_sports_logistics_venues.gurSportsLogisticsPortfolios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'slp_venue'})
    slpVenue:gur_sports_logistics_venues | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsLogisticsPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'slp_cnt_id'})
    slpCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurSportsLogisticsPortfolios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'slp_city'})
    slpCity:gur_all_cities | null;


    @Column("int",{ 
        nullable:true,
        name:"slp_month"
        })
    slp_month:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"slp_year"
        })
    slp_year:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"slp_desc"
        })
    slp_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slp_image"
        })
    slp_image:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"slp_status"
        })
    slp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"slp_admin_review"
        })
    slp_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"slp_createdby"
        })
    slp_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slp_modifiedby"
        })
    slp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"slp_modified_time"
        })
    slp_modified_time:Date;
        
}
