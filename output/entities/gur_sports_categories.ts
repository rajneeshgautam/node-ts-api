import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports} from "./gur_sports";


@Entity("gur_sports_categories" ,{schema:"sportsmatik_local" } )
export class gur_sports_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sct_id"
        })
    sct_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"sct_name"
        })
    sct_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sct_createdby"
        })
    sct_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sct_modifiedby"
        })
    sct_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sct_modifiedtime"
        })
    sct_modifiedtime:Date;
        

   
    @ManyToMany(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurSportsCategoriess,{  nullable:false, })
    @JoinTable({ name:'gur_sports_categories_map'})
    gurSportss:gur_sports[];
    
}
