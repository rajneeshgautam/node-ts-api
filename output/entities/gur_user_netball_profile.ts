import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_netball_profile" ,{schema:"sportsmatik_local" } )
@Index("net_usr_id",["netUsr",])
@Index("net_primary_position",["netPosition",])
@Index("net_alternate_position",["netPosition2",])
@Index("net_createdby_user",["netCreatedbyUser",])
export class gur_user_netball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"net_id"
        })
    net_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserNetballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'net_usr_id'})
    netUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserNetballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'net_position1'})
    netPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserNetballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'net_position2'})
    netPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"net_modifiedtime"
        })
    net_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserNetballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'net_createdby_user'})
    netCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"net_modifiedby"
        })
    net_modifiedby:string | null;
        
}
