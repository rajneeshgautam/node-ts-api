import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_wiki_awards_country_wise_ls} from "./gur_wiki_awards_country_wise_ls";
import {gur_wiki_awards_list} from "./gur_wiki_awards_list";


@Entity("gur_wiki_awards_country_wise" ,{schema:"sportsmatik_local" } )
@Index("uniq",["wacCnt","wac_presented_by",],{unique:true})
export class gur_wiki_awards_country_wise {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wac_id"
        })
    wac_id:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWikiAwardsCountryWises,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wac_cnt_id'})
    wacCnt:gur_countries | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wac_presented_by"
        })
    wac_presented_by:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wac_createdby"
        })
    wac_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wac_modifiedby"
        })
    wac_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wac_modifiedtime"
        })
    wac_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_awards_country_wise_ls, (gur_wiki_awards_country_wise_ls: gur_wiki_awards_country_wise_ls)=>gur_wiki_awards_country_wise_ls.wclWac,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsCountryWiseLss:gur_wiki_awards_country_wise_ls[];
    

   
    @OneToMany(()=>gur_wiki_awards_list, (gur_wiki_awards_list: gur_wiki_awards_list)=>gur_wiki_awards_list.walWac,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsLists:gur_wiki_awards_list[];
    
}
