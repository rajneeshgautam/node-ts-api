import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_kiteboarding_profile" ,{schema:"sportsmatik_local" } )
@Index("kbr_usr_id",["kbrUsr",])
@Index("kbr_createdby_user",["kbrCreatedbyUser",])
@Index("kbr_stance",["kbrStance",])
export class gur_user_kiteboarding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"kbr_id"
        })
    kbr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKiteboardingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbr_usr_id'})
    kbrUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKiteboardingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbr_stance'})
    kbrStance:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"kbr_disciplines"
        })
    kbr_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kbr_modifiedtime"
        })
    kbr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKiteboardingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbr_createdby_user'})
    kbrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kbr_modifiedby"
        })
    kbr_modifiedby:string | null;
        
}
