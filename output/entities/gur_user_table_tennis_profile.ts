import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_table_tennis_profile" ,{schema:"sportsmatik_local" } )
@Index("tat_usr_id",["tatUsr",])
@Index("tat_play_style",["tatPlayStyle",])
@Index("tat_createdby_user",["tatCreatedbyUser",])
export class gur_user_table_tennis_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"tat_id"
        })
    tat_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTableTennisProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tat_usr_id'})
    tatUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"tat_primary_hand"
        })
    tat_primary_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTableTennisProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tat_play_style'})
    tatPlayStyle:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tat_modifiedtime"
        })
    tat_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTableTennisProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tat_createdby_user'})
    tatCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"tat_modifiedby"
        })
    tat_modifiedby:string | null;
        
}
