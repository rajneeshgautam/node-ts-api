import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_rules} from "./gur_matik_knowhow_rules";


@Entity("gur_matik_knowhow_rules_ls" ,{schema:"sportsmatik_local" } )
@Index("khl_lng_code",["krl_lng_code",])
@Index("khl_wkh_id",["krlKhr",])
export class gur_matik_knowhow_rules_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"krl_id"
        })
    krl_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_rules, (gur_matik_knowhow_rules: gur_matik_knowhow_rules)=>gur_matik_knowhow_rules.gurMatikKnowhowRulesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'krl_khr_id'})
    krlKhr:gur_matik_knowhow_rules | null;


    @Column("varchar",{ 
        nullable:true,
        name:"krl_title"
        })
    krl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"krl_description"
        })
    krl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"krl_lng_code"
        })
    krl_lng_code:string;
        
}
