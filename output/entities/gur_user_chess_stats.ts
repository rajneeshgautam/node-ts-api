import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_chess_stats" ,{schema:"sportsmatik_local" } )
@Index("ucs_usr_id",["ucsUsr",])
@Index("ucs_ucp_id",["ucsUcp",])
@Index("ucs_rank",["ucsRank",])
export class gur_user_chess_stats {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ucs_id"
        })
    ucs_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserChessStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucs_usr_id'})
    ucsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserChessStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucs_ucp_id'})
    ucsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"ucs_matches_played"
        })
    ucs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_white_wins"
        })
    ucs_white_wins:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_white_lost"
        })
    ucs_white_lost:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_white_draw"
        })
    ucs_white_draw:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_black_wins"
        })
    ucs_black_wins:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_black_lost"
        })
    ucs_black_lost:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_black_draw"
        })
    ucs_black_draw:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserChessStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucs_rank'})
    ucsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ucs_modifiedtime"
        })
    ucs_modifiedtime:Date;
        
}
