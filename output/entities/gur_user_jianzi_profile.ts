import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_jianzi_profile" ,{schema:"sportsmatik_local" } )
@Index("jia_usr_id",["jiaUsr",])
@Index("jia_createdby_user",["jiaCreatedbyUser",])
export class gur_user_jianzi_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"jia_id"
        })
    jia_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJianziProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jia_usr_id'})
    jiaUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"jia_modifiedtime"
        })
    jia_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJianziProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jia_createdby_user'})
    jiaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"jia_modifiedby"
        })
    jia_modifiedby:string | null;
        
}
