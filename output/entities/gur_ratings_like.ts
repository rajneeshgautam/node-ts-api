import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_ratings_like" ,{schema:"sportsmatik_local" } )
@Index("rtl_rat_id",["rtl_rat_id","rtl_usr_id","rtlUty",],{unique:true})
@Index("rtl_usr_id",["rtl_usr_id",])
@Index("rtl_uty_id",["rtlUty",])
export class gur_ratings_like {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"rtl_id"
        })
    rtl_id:number;
        

    @Column("bigint",{ 
        nullable:false,
        name:"rtl_rat_id"
        })
    rtl_rat_id:string;
        

    @Column("bigint",{ 
        nullable:false,
        name:"rtl_usr_id"
        })
    rtl_usr_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurRatingsLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'rtl_uty_id'})
    rtlUty:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"rtl_is_liked"
        })
    rtl_is_liked:boolean;
        
}
