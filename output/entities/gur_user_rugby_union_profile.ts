import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_rugby_union_profile" ,{schema:"sportsmatik_local" } )
@Index("rgu_usr_id",["rguUsr",])
@Index("rgu_primary_position",["rguPosition",])
@Index("rgu_secondary_event",["rguPosition2",])
@Index("rgu_createdby_user",["rguCreatedbyUser",])
export class gur_user_rugby_union_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"rgu_id"
        })
    rgu_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbyUnionProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgu_usr_id'})
    rguUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRugbyUnionProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgu_position1'})
    rguPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRugbyUnionProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgu_position2'})
    rguPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rgu_modifiedtime"
        })
    rgu_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbyUnionProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgu_createdby_user'})
    rguCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rgu_modifiedby"
        })
    rgu_modifiedby:string | null;
        
}
