import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_sports_logistics" ,{schema:"sportsmatik_local" } )
@Index("clg_gst_number",["slg_gst_number",],{unique:true})
@Index("clg_usr_id",["slgUsr",])
@Index("clg_createdby_user",["slg_createdby_user",])
@Index("clg_country",["slg_country",])
@Index("clg_sts_id",["slg_sts_id",])
@Index("clg_dis_id",["slg_dis_id",])
@Index("clg_name",["slg_name",])
@Index("clg_name_2",["slg_name",])
export class gur_sports_logistics {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"slg_id"
        })
    slg_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slg_slug"
        })
    slg_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"slg_name"
        })
    slg_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsLogisticss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'slg_usr_id'})
    slgUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"slg_address"
        })
    slg_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slg_address2"
        })
    slg_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"slg_pincode"
        })
    slg_pincode:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"slg_country"
        })
    slg_country:number;
        

    @Column("int",{ 
        nullable:true,
        name:"slg_dis_id"
        })
    slg_dis_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"slg_sts_id"
        })
    slg_sts_id:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"slg_address_google_map"
        })
    slg_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slg_email"
        })
    slg_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"slg_email_verified"
        })
    slg_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"slg_email_verify_time"
        })
    slg_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"slg_phone1"
        })
    slg_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"slg_phone1_verified"
        })
    slg_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"slg_phone1_otp"
        })
    slg_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"slg_phone1_otp_time"
        })
    slg_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"slg_phone2"
        })
    slg_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"slg_phone2_verified"
        })
    slg_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"slg_phone2_otp"
        })
    slg_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"slg_phone2_otp_time"
        })
    slg_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"slg_status"
        })
    slg_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slg_profile_pic"
        })
    slg_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"slg_intro_status"
        })
    slg_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"slg_profile_summary"
        })
    slg_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"slg_founded_year"
        })
    slg_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"slg_founded_month"
        })
    slg_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"slg_last_login_date"
        })
    slg_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"slg_last_login_details"
        })
    slg_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"slg_intro_modifiedtime"
        })
    slg_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"slg_profile_pic_modifiedtime"
        })
    slg_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"slg_free_sports"
        })
    slg_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"slg_free_sub_users"
        })
    slg_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"slg_gst_number"
        })
    slg_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"slg_viewable"
        })
    slg_viewable:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"slg_createdby_user"
        })
    slg_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"slg_createdby"
        })
    slg_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"slg_modifiedby"
        })
    slg_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"slg_modifiedtime"
        })
    slg_modifiedtime:Date;
        
}
