import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";
import {gur_wiki_sports_equipage_desc} from "./gur_wiki_sports_equipage_desc";
import {gur_wiki_sports_equipage_ls} from "./gur_wiki_sports_equipage_ls";


@Entity("gur_wiki_sports_equipage" ,{schema:"sportsmatik_local" } )
@Index("uae_slug",["uae_slug",],{unique:true})
@Index("uae_spo_id",["uaeSpo",])
@Index("gur_wiki_sports_equipage_ibfk_2",["uaeKnc",])
export class gur_wiki_sports_equipage {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uae_id"
        })
    uae_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiSportsEquipages,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uae_spo_id'})
    uaeSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"uae_slug"
        })
    uae_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uae_photo"
        })
    uae_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uae_photo_data"
        })
    uae_photo_data:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uae_timeline_image_data"
        })
    uae_timeline_image_data:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'pg'",
        enum:["uniform","pg","equipment"],
        name:"uae_type"
        })
    uae_type:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWikiSportsEquipages,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uae_knc_id'})
    uaeKnc:gur_matik_knowhow_tab_categories | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"uae_sort_order"
        })
    uae_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uae_for_health"
        })
    uae_for_health:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uae_is_featured"
        })
    uae_is_featured:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uae_createdby"
        })
    uae_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"uae_modifiedby"
        })
    uae_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uae_modifiedtime"
        })
    uae_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgUae,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    

   
    @OneToMany(()=>gur_wiki_sports_equipage_desc, (gur_wiki_sports_equipage_desc: gur_wiki_sports_equipage_desc)=>gur_wiki_sports_equipage_desc.uedUae,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsEquipageDescs:gur_wiki_sports_equipage_desc[];
    

   
    @OneToMany(()=>gur_wiki_sports_equipage_ls, (gur_wiki_sports_equipage_ls: gur_wiki_sports_equipage_ls)=>gur_wiki_sports_equipage_ls.uelUae,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsEquipageLss:gur_wiki_sports_equipage_ls[];
    
}
