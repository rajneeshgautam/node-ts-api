import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_admin_users} from "./gur_admin_users";
import {gur_vendors_brands_map} from "./gur_vendors_brands_map";


@Entity("gur_brands" ,{schema:"sportsmatik_local" } )
@Index("brd_name",["brd_name",],{unique:true})
@Index("brd_createdby_user",["brd_createdby_user",])
@Index("brd_createdby_admin",["brdCreatedbyAdmin",])
export class gur_brands {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"brd_id"
        })
    brd_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"brd_name"
        })
    brd_name:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"brd_sort_order"
        })
    brd_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"brd_status"
        })
    brd_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"brd_authentic"
        })
    brd_authentic:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"brd_popular"
        })
    brd_popular:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"brd_is_international"
        })
    brd_is_international:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"brd_createdby_user"
        })
    brd_createdby_user:number | null;
        

   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurBrandss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'brd_createdby_admin'})
    brdCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"brd_createdby"
        })
    brd_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"brd_modifiedby"
        })
    brd_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"brd_modifiedtime"
        })
    brd_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_vendors_brands_map, (gur_vendors_brands_map: gur_vendors_brands_map)=>gur_vendors_brands_map.vbmBrd,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsBrandsMaps:gur_vendors_brands_map[];
    
}
