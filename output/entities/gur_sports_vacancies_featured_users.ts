import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_sports_vacancies_featured_users" ,{schema:"sportsmatik_local" } )
@Index("svu_user_id",["svu_user_id",])
@Index("svu_uty_id",["svuUty",])
export class gur_sports_vacancies_featured_users {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"svu_id"
        })
    svu_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"svu_user_id"
        })
    svu_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSportsVacanciesFeaturedUserss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'svu_uty_id'})
    svuUty:gur_user_types | null;


    @Column("datetime",{ 
        nullable:false,
        name:"svu_start"
        })
    svu_start:Date;
        

    @Column("datetime",{ 
        nullable:false,
        name:"svu_end"
        })
    svu_end:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"svu_is_candidate"
        })
    svu_is_candidate:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"svu_status"
        })
    svu_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"svu_createdby"
        })
    svu_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"svu_modifiedby"
        })
    svu_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"svu_modifiedtime"
        })
    svu_modifiedtime:Date;
        
}
