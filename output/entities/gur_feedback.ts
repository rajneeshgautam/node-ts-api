import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_value_list} from "./gur_value_list";
import {gur_feedback_types} from "./gur_feedback_types";


@Entity("gur_feedback" ,{schema:"sportsmatik_local" } )
@Index("fed_type",["fedType",])
@Index("fed_mode",["fedMode",])
@Index("fed_usr_id",["fed_usr_id",])
@Index("fed_usr_type",["fedUsrType",])
export class gur_feedback {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"fed_id"
        })
    fed_id:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"fed_usr_id"
        })
    fed_usr_id:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurFeedbacks,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fed_usr_type'})
    fedUsrType:gur_user_types | null;


    @Column("varchar",{ 
        nullable:false,
        name:"fed_section"
        })
    fed_section:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurFeedbacks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fed_mode'})
    fedMode:gur_value_list | null;


    @Column("text",{ 
        nullable:true,
        name:"fed_extra_id"
        })
    fed_extra_id:string | null;
        

   
    @ManyToOne(()=>gur_feedback_types, (gur_feedback_types: gur_feedback_types)=>gur_feedback_types.gurFeedbacks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fed_type'})
    fedType:gur_feedback_types | null;


    @Column("varchar",{ 
        nullable:false,
        name:"fed_username"
        })
    fed_username:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fed_useremail"
        })
    fed_useremail:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"fed_phone"
        })
    fed_phone:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"fed_message"
        })
    fed_message:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fed_attachment1"
        })
    fed_attachment1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fed_attachment2"
        })
    fed_attachment2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fed_attachment3"
        })
    fed_attachment3:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fed_ip_address"
        })
    fed_ip_address:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fed_sent_on"
        })
    fed_sent_on:Date;
        
}
