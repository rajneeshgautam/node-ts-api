import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_ski_jumping_profile" ,{schema:"sportsmatik_local" } )
@Index("skj_usr_id",["skjUsr",])
@Index("skj_createdby_user",["skjCreatedbyUser",])
export class gur_user_ski_jumping_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"skj_id"
        })
    skj_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkiJumpingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'skj_usr_id'})
    skjUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"skj_modifiedtime"
        })
    skj_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkiJumpingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'skj_createdby_user'})
    skjCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"skj_modifiedby"
        })
    skj_modifiedby:string | null;
        
}
