import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_blog_posts_comments} from "./gur_blog_posts_comments";
import {gur_blog_posts_ls} from "./gur_blog_posts_ls";
import {gur_blog_categories} from "./gur_blog_categories";


@Entity("gur_blog_posts" ,{schema:"sportsmatik_local" } )
export class gur_blog_posts {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bpt_id"
        })
    bpt_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bpt_photo"
        })
    bpt_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"bpt_photo_data"
        })
    bpt_photo_data:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"bpt_date_posted"
        })
    bpt_date_posted:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bpt_slug"
        })
    bpt_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bpt_status"
        })
    bpt_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bpt_featured"
        })
    bpt_featured:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"bpt_is_top5"
        })
    bpt_is_top5:number;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"bpt_is_top5_interview"
        })
    bpt_is_top5_interview:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bpt_sport"
        })
    bpt_sport:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bpt_for_knowhow"
        })
    bpt_for_knowhow:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bpt_createdby"
        })
    bpt_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"bpt_modifiedby"
        })
    bpt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bpt_modifiedtime"
        })
    bpt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_blog_posts_comments, (gur_blog_posts_comments: gur_blog_posts_comments)=>gur_blog_posts_comments.bcoBpt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlogPostsCommentss:gur_blog_posts_comments[];
    

   
    @OneToMany(()=>gur_blog_posts_ls, (gur_blog_posts_ls: gur_blog_posts_ls)=>gur_blog_posts_ls.bplBpt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlogPostsLss:gur_blog_posts_ls[];
    

   
    @ManyToMany(()=>gur_blog_categories, (gur_blog_categories: gur_blog_categories)=>gur_blog_categories.gurBlogPostss,{  nullable:false, })
    @JoinTable({ name:'gur_blog_posts_categories'})
    gurBlogCategoriess:gur_blog_categories[];
    
}
