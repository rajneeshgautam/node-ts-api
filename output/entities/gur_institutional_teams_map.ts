import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_institutional_staff} from "./gur_institutional_staff";


@Entity("gur_institutional_teams_map" ,{schema:"sportsmatik_local" } )
@Index("itm_itn_id",["itm_itn_id",])
@Index("itm_member",["itmMember",])
export class gur_institutional_teams_map {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"itm_id"
        })
    itm_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"itm_type"
        })
    itm_type:boolean;
        

   
    @ManyToOne(()=>gur_institutional_staff, (gur_institutional_staff: gur_institutional_staff)=>gur_institutional_staff.gurInstitutionalTeamsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'itm_member'})
    itmMember:gur_institutional_staff | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"itm_role"
        })
    itm_role:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"itm_itn_id"
        })
    itm_itn_id:number;
        
}
