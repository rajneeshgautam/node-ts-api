import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_terminologies} from "./gur_wiki_terminologies";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_terminologies_ls" ,{schema:"sportsmatik_local" } )
@Index("trl_name",["trl_name",])
@Index("trl_ter_id",["trlTer",])
@Index("trl_lng_code",["trlLngCode",])
export class gur_wiki_terminologies_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"trl_id"
        })
    trl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_terminologies, (gur_wiki_terminologies: gur_wiki_terminologies)=>gur_wiki_terminologies.gurWikiTerminologiesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'trl_ter_id'})
    trlTer:gur_wiki_terminologies | null;


    @Column("varchar",{ 
        nullable:true,
        name:"trl_name"
        })
    trl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"trl_intro_desc"
        })
    trl_intro_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiTerminologiesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'trl_lng_code'})
    trlLngCode:gur_languages | null;

}
