import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow_rules} from "./gur_wiki_knowhow_rules";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_knowhow_rules_ls" ,{schema:"sportsmatik_local" } )
@Index("rul_wrl_id",["rulWrl",])
@Index("rul_lng_code",["rulLngCode",])
export class gur_wiki_knowhow_rules_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"rul_id"
        })
    rul_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow_rules, (gur_wiki_knowhow_rules: gur_wiki_knowhow_rules)=>gur_wiki_knowhow_rules.gurWikiKnowhowRulesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rul_wrl_id'})
    rulWrl:gur_wiki_knowhow_rules | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rul_name"
        })
    rul_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"rul_desc"
        })
    rul_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiKnowhowRulesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rul_lng_code'})
    rulLngCode:gur_languages | null;

}
