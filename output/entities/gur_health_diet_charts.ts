import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_health_diet_charts" ,{schema:"sportsmatik_local" } )
@Index("htc_sports",["htc_sports",])
export class gur_health_diet_charts {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"htc_id"
        })
    htc_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"htc_sports"
        })
    htc_sports:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"htc_createdby"
        })
    htc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"htc_modifiedby"
        })
    htc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"htc_modifiedtime"
        })
    htc_modifiedtime:Date;
        
}
