import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_soft_tennis_profile" ,{schema:"sportsmatik_local" } )
@Index("sot_usr_id",["sotUsr",])
@Index("sot_createdby_user",["sotCreatedbyUser",])
export class gur_user_soft_tennis_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sot_id"
        })
    sot_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSoftTennisProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sot_usr_id'})
    sotUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"sot_primary_hand"
        })
    sot_primary_hand:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["one-handed","two-handed"],
        name:"sot_back_hand"
        })
    sot_back_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sot_modifiedtime"
        })
    sot_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSoftTennisProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sot_createdby_user'})
    sotCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sot_modifiedby"
        })
    sot_modifiedby:string | null;
        
}
