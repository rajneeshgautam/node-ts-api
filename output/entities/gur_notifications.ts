import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list} from "./gur_value_list";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_notifications" ,{schema:"sportsmatik_local" } )
@Index("not_category",["notCategory",])
@Index("not_by_type",["notByType",])
export class gur_notifications {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"not_id"
        })
    not_id:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurNotificationss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'not_category'})
    notCategory:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"not_mode"
        })
    not_mode:boolean;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurNotificationss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'not_by_type'})
    notByType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"not_user_id"
        })
    not_user_id:number;
        

    @Column("text",{ 
        nullable:true,
        name:"not_message"
        })
    not_message:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:150,
        name:"not_extra_ids"
        })
    not_extra_ids:string | null;
        

    @Column("bigint",{ 
        nullable:true,
        name:"nt_mod_id"
        })
    nt_mod_id:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"not_read"
        })
    not_read:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"not_seen"
        })
    not_seen:boolean;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"not_modifiedtime"
        })
    not_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"not_createdby"
        })
    not_createdby:string | null;
        
}
