import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_wushu_profile" ,{schema:"sportsmatik_local" } )
@Index("wus_usr_id",["wusUsr",])
@Index("wus_event",["wusSanshouEvent",])
@Index("wus_createdby_user",["wusCreatedbyUser",])
export class gur_user_wushu_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"wus_id"
        })
    wus_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWushuProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wus_usr_id'})
    wusUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wus_taolu"
        })
    wus_taolu:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"wus_taolu_events"
        })
    wus_taolu_events:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wus_sanshou"
        })
    wus_sanshou:boolean;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWushuProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wus_sanshou_event'})
    wusSanshouEvent:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wus_modifiedtime"
        })
    wus_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWushuProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wus_createdby_user'})
    wusCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"wus_modifiedby"
        })
    wus_modifiedby:string | null;
        
}
