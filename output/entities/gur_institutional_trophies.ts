import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_users} from "./gur_users";


@Entity("gur_institutional_trophies" ,{schema:"sportsmatik_local" } )
@Index("act_name",["int_name",])
@Index("int_uty_id",["intUty",])
@Index("int_spo_id",["intSpo",])
@Index("int_createdby_user",["intCreatedbyUser",])
export class gur_institutional_trophies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"int_id"
        })
    int_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInstitutionalTrophiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'int_uty_id'})
    intUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"int_user_id"
        })
    int_user_id:number;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurInstitutionalTrophiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'int_spo_id'})
    intSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"int_name"
        })
    int_name:string;
        

    @Column("text",{ 
        nullable:false,
        name:"int_year"
        })
    int_year:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"int_status"
        })
    int_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"int_admin_review"
        })
    int_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInstitutionalTrophiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'int_createdby_user'})
    intCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"int_createdby"
        })
    int_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"int_modifiedby"
        })
    int_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"int_modifiedtime"
        })
    int_modifiedtime:Date;
        
}
