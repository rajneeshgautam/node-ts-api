import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_squash_profile" ,{schema:"sportsmatik_local" } )
@Index("sqa_usr_id",["sqaUsr",])
@Index("sqa_playing_style",["sqaPlayingStyle",])
@Index("sqa_createdby_user",["sqaCreatedbyUser",])
export class gur_user_squash_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sqa_id"
        })
    sqa_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSquashProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqa_usr_id'})
    sqaUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"sqa_primary_hand"
        })
    sqa_primary_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSquashProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqa_playing_style'})
    sqaPlayingStyle:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sqa_modifiedtime"
        })
    sqa_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSquashProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqa_createdby_user'})
    sqaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sqa_modifiedby"
        })
    sqa_modifiedby:string | null;
        
}
