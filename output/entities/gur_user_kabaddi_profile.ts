import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_kabaddi_profile" ,{schema:"sportsmatik_local" } )
@Index("kab_usr_id",["kabUsr",])
@Index("kab_primary_position",["kabPosition",])
@Index("kab_alternate_position",["kabPosition2",])
@Index("kab_createdby_user",["kabCreatedbyUser",])
export class gur_user_kabaddi_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"kab_id"
        })
    kab_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKabaddiProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kab_usr_id'})
    kabUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKabaddiProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kab_position1'})
    kabPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKabaddiProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kab_position2'})
    kabPosition2:gur_value_list | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"kab_disciplines"
        })
    kab_disciplines:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kab_modifiedtime"
        })
    kab_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKabaddiProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kab_createdby_user'})
    kabCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kab_modifiedby"
        })
    kab_modifiedby:string | null;
        
}
