import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_real_tennis_profile" ,{schema:"sportsmatik_local" } )
@Index("ret_usr_id",["retUsr",])
@Index("ret_createdby_user",["retCreatedbyUser",])
export class gur_user_real_tennis_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ret_id"
        })
    ret_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRealTennisProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ret_usr_id'})
    retUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"ret_primary_hand"
        })
    ret_primary_hand:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["one-handed","two-handed"],
        name:"ret_back_hand"
        })
    ret_back_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ret_modifiedtime"
        })
    ret_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRealTennisProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ret_createdby_user'})
    retCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ret_modifiedby"
        })
    ret_modifiedby:string | null;
        
}
