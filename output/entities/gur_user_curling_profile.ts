import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_curling_profile" ,{schema:"sportsmatik_local" } )
@Index("cur_usr_id",["curUsr",])
@Index("cur_position1",["curPosition",])
@Index("cur_position2",["curPosition2",])
@Index("cur_createdby_user",["curCreatedbyUser",])
export class gur_user_curling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cur_id"
        })
    cur_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCurlingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cur_usr_id'})
    curUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCurlingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cur_position1'})
    curPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCurlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cur_position2'})
    curPosition2:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"cur_hand"
        })
    cur_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cur_modifiedtime"
        })
    cur_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCurlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cur_createdby_user'})
    curCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"cur_modifiedby"
        })
    cur_modifiedby:string | null;
        
}
