import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_badminton_stats" ,{schema:"sportsmatik_local" } )
@Index("bds_usr_id",["bdsUsr",])
@Index("bds_ucp_id",["bdsUcp",])
@Index("bds_event",["bdsEvent",])
@Index("bds_rank",["bdsRank",])
export class gur_user_badminton_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"bds_id"
        })
    bds_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBadmintonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bds_usr_id'})
    bdsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserBadmintonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bds_ucp_id'})
    bdsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserBadmintonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bds_event'})
    bdsEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"bds_matches_played"
        })
    bds_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bds_matches_won"
        })
    bds_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bds_matches_loss"
        })
    bds_matches_loss:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bds_balance"
        })
    bds_balance:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserBadmintonStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bds_rank'})
    bdsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bds_modifiedtime"
        })
    bds_modifiedtime:Date;
        
}
