import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_awards_sports_wise} from "./gur_wiki_awards_sports_wise";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_awards_sports_wise_ls" ,{schema:"sportsmatik_local" } )
@Index("wsl_was_id",["wslWas",])
@Index("wsl_lng_code",["wslLngCode",])
export class gur_wiki_awards_sports_wise_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wsl_id"
        })
    wsl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_awards_sports_wise, (gur_wiki_awards_sports_wise: gur_wiki_awards_sports_wise)=>gur_wiki_awards_sports_wise.gurWikiAwardsSportsWiseLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsl_was_id'})
    wslWas:gur_wiki_awards_sports_wise | null;


    @Column("text",{ 
        nullable:true,
        name:"wsl_desc"
        })
    wsl_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wsl_meta_title"
        })
    wsl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wsl_meta_desc"
        })
    wsl_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiAwardsSportsWiseLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsl_lng_code'})
    wslLngCode:gur_languages | null;

}
