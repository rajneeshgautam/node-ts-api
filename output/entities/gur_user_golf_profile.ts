import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_golf_profile" ,{schema:"sportsmatik_local" } )
@Index("glf_usr_id",["glfUsr",])
@Index("glf_createdby_user",["glfCreatedbyUser",])
export class gur_user_golf_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"glf_id"
        })
    glf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGolfProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'glf_usr_id'})
    glfUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"glf_primary_hand"
        })
    glf_primary_hand:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"glf_modifiedtime"
        })
    glf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGolfProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'glf_createdby_user'})
    glfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"glf_modifiedby"
        })
    glf_modifiedby:string | null;
        
}
