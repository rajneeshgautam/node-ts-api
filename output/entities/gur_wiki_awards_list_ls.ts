import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_awards_list} from "./gur_wiki_awards_list";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_awards_list_ls" ,{schema:"sportsmatik_local" } )
@Index("wll_wal_id",["wllWal",])
@Index("wll_lng_code",["wllLngCode",])
export class gur_wiki_awards_list_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wll_id"
        })
    wll_id:string;
        

   
    @ManyToOne(()=>gur_wiki_awards_list, (gur_wiki_awards_list: gur_wiki_awards_list)=>gur_wiki_awards_list.gurWikiAwardsListLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wll_wal_id'})
    wllWal:gur_wiki_awards_list | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wll_name"
        })
    wll_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wll_desc"
        })
    wll_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiAwardsListLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wll_lng_code'})
    wllLngCode:gur_languages | null;

}
