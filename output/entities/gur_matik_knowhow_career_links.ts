import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";


@Entity("gur_matik_knowhow_career_links" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_career_links_ibfk_1",["khlSpo",])
export class gur_matik_knowhow_career_links {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"khl_id"
        })
    khl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCareerLinkss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khl_spo_id'})
    khlSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"khl_title"
        })
    khl_title:string;
        

    @Column("text",{ 
        nullable:false,
        name:"khl_url"
        })
    khl_url:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"khl_description"
        })
    khl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khl_createdby"
        })
    khl_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khl_modifiedby"
        })
    khl_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khl_modifiedtime"
        })
    khl_modifiedtime:Date;
        
}
