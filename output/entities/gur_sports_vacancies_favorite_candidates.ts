import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_sports_vacancies_favorite_candidates" ,{schema:"sportsmatik_local" } )
@Index("svc_createdby_user",["svc_createdby_user",])
export class gur_sports_vacancies_favorite_candidates {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"svc_id"
        })
    svc_id:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"svc_favorite"
        })
    svc_favorite:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"svc_status"
        })
    svc_status:boolean;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"svc_usr_id"
        })
    svc_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"svc_user_id"
        })
    svc_user_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"svc_uty_id"
        })
    svc_uty_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"svc_createdby_user"
        })
    svc_createdby_user:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"svc_createdby"
        })
    svc_createdby:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"svc_modifiedby"
        })
    svc_modifiedby:string;
        
}
