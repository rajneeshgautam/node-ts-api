import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_cricket_stats" ,{schema:"sportsmatik_local" } )
@Index("ucs_usr_id",["ucsUsr",])
@Index("ucs_ucp_id",["ucsUcp",])
export class gur_user_cricket_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"ucs_id"
        })
    ucs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCricketStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucs_usr_id'})
    ucsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserCricketStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucs_ucp_id'})
    ucsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"ucs_matches_played"
        })
    ucs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_batting_innings_played"
        })
    ucs_batting_innings_played:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_not_out"
        })
    ucs_not_out:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_batting_runs_scored"
        })
    ucs_batting_runs_scored:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_batting_highest_score"
        })
    ucs_batting_highest_score:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ucs_batting_highest_score_not_out"
        })
    ucs_batting_highest_score_not_out:boolean | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"ucs_batting_ball_faced"
        })
    ucs_batting_ball_faced:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"ucs_batting_average"
        })
    ucs_batting_average:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"ucs_batting_strike_rate"
        })
    ucs_batting_strike_rate:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_batting_fours_scored"
        })
    ucs_batting_fours_scored:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_batting_sixes_scored"
        })
    ucs_batting_sixes_scored:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"ucs_batting_fifties_scored"
        })
    ucs_batting_fifties_scored:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"ucs_batting_hundred_scored"
        })
    ucs_batting_hundred_scored:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_catches_taken"
        })
    ucs_catches_taken:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_stumping"
        })
    ucs_stumping:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"ucs_run_out_done"
        })
    ucs_run_out_done:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ucs_bowling_innings_played"
        })
    ucs_bowling_innings_played:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_bowling_balls_bowled"
        })
    ucs_bowling_balls_bowled:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_bowling_runs_scored"
        })
    ucs_bowling_runs_scored:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_bowling_wickets_taken"
        })
    ucs_bowling_wickets_taken:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"ucs_bowling_average"
        })
    ucs_bowling_average:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"ucs_bowling_strike_rate"
        })
    ucs_bowling_strike_rate:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"ucs_bowling_economy_rate"
        })
    ucs_bowling_economy_rate:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_bowling_4wicket"
        })
    ucs_bowling_4wicket:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_bowling_5wicket"
        })
    ucs_bowling_5wicket:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"ucs_bowling_10wicket"
        })
    ucs_bowling_10wicket:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ucs_bowling_bbi_wickets"
        })
    ucs_bowling_bbi_wickets:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ucs_bowling_bbi_runs"
        })
    ucs_bowling_bbi_runs:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ucs_bowling_bbm_wickets"
        })
    ucs_bowling_bbm_wickets:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ucs_bowling_bbm_runs"
        })
    ucs_bowling_bbm_runs:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ucs_modifiedtime"
        })
    ucs_modifiedtime:Date;
        
}
