import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_triathlon_profile" ,{schema:"sportsmatik_local" } )
@Index("tri_usr_id",["triUsr",])
@Index("tri_createdby_user",["triCreatedbyUser",])
export class gur_user_triathlon_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"tri_id"
        })
    tri_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTriathlonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tri_usr_id'})
    triUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tri_modifiedtime"
        })
    tri_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTriathlonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tri_createdby_user'})
    triCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"tri_modifiedby"
        })
    tri_modifiedby:string | null;
        
}
