import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_contact" ,{schema:"sportsmatik_local" } )
@Index("con_name",["con_name",])
export class gur_contact {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"con_id"
        })
    con_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"con_name"
        })
    con_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"con_email"
        })
    con_email:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"con_phone"
        })
    con_phone:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"con_subject"
        })
    con_subject:string;
        

    @Column("text",{ 
        nullable:false,
        name:"con_message"
        })
    con_message:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"con_ip_address"
        })
    con_ip_address:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"con_sent_on"
        })
    con_sent_on:Date;
        
}
