import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports} from "./gur_sports";
import {gur_did_you_know_ls} from "./gur_did_you_know_ls";


@Entity("gur_did_you_know" ,{schema:"sportsmatik_local" } )
@Index("dyk_spo_id",["dykSpo",])
export class gur_did_you_know {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"dyk_id"
        })
    dyk_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"dyk_status"
        })
    dyk_status:boolean;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurDidYouKnows,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dyk_spo_id'})
    dykSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"dyk_createdby"
        })
    dyk_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"dyk_modifiedby"
        })
    dyk_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"dyk_modifiedtime"
        })
    dyk_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_did_you_know_ls, (gur_did_you_know_ls: gur_did_you_know_ls)=>gur_did_you_know_ls.dylDyk,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurDidYouKnowLss:gur_did_you_know_ls[];
    
}
