import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_all_cities} from "./gur_all_cities";
import {gur_countries} from "./gur_countries";


@Entity("gur_matik_knowhow_career_guides" ,{schema:"sportsmatik_local" } )
@Index("khv_job_city",["khgCity",])
@Index("khv_job_cnt",["khgCnt",])
@Index("khv_spo_id",["khgSpo",])
export class gur_matik_knowhow_career_guides {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"khg_id"
        })
    khg_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCareerGuidess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khg_spo_id'})
    khgSpo:gur_wiki_sports | null;

    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCareerGuidess2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khg_spo_id'})
    khgSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"khg_status"
        })
    khg_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khg_name"
        })
    khg_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khg_pic"
        })
    khg_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khg_address"
        })
    khg_address:string | null;
        

   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurMatikKnowhowCareerGuidess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khg_city'})
    khgCity:gur_all_cities | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowCareerGuidess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khg_cnt'})
    khgCnt:gur_countries | null;


    @Column("text",{ 
        nullable:false,
        name:"khg_description"
        })
    khg_description:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khg_createdby"
        })
    khg_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khg_modifiedby"
        })
    khg_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khg_modifiedtime"
        })
    khg_modifiedtime:Date;
        
}
