import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_water_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("wsk_usr_id",["wskUsr",])
@Index("wsk_createdby_user",["wskCreatedbyUser",])
export class gur_user_water_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wsk_id"
        })
    wsk_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWaterSkiingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsk_usr_id'})
    wskUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"wsk_discipline"
        })
    wsk_discipline:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wsk_modifiedtime"
        })
    wsk_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWaterSkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsk_createdby_user'})
    wskCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"wsk_modifiedby"
        })
    wsk_modifiedby:string | null;
        
}
