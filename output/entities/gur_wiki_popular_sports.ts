import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_wiki_popular_sports_ls} from "./gur_wiki_popular_sports_ls";
import {gur_wiki_sports} from "./gur_wiki_sports";


@Entity("gur_wiki_popular_sports" ,{schema:"sportsmatik_local" } )
@Index("wns_cnt_id",["wnsCnt",],{unique:true})
export class gur_wiki_popular_sports {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wns_id"
        })
    wns_id:string;
        

   
    @OneToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWikiPopularSports,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wns_cnt_id'})
    wnsCnt:gur_countries | null;


    @Column("text",{ 
        nullable:true,
        name:"wns_sports_int"
        })
    wns_sports_int:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wns_sports_trd"
        })
    wns_sports_trd:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wns_createdby"
        })
    wns_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wns_modifiedby"
        })
    wns_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wns_modifiedtime"
        })
    wns_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_popular_sports_ls, (gur_wiki_popular_sports_ls: gur_wiki_popular_sports_ls)=>gur_wiki_popular_sports_ls.nslWns,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiPopularSportsLss:gur_wiki_popular_sports_ls[];
    

   
    @ManyToMany(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiPopularSportss,{  nullable:false, })
    @JoinTable({ name:'gur_wiki_popular_sports_map'})
    gurWikiSportss:gur_wiki_sports[];
    
}
