import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_shooting_profile" ,{schema:"sportsmatik_local" } )
@Index("sho_usr_id",["shoUsr",])
@Index("sho_createdby_user",["shoCreatedbyUser",])
export class gur_user_shooting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sho_id"
        })
    sho_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserShootingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sho_usr_id'})
    shoUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"sho_master_eye"
        })
    sho_master_eye:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sho_events"
        })
    sho_events:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sho_modifiedtime"
        })
    sho_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserShootingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sho_createdby_user'})
    shoCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sho_modifiedby"
        })
    sho_modifiedby:string | null;
        
}
