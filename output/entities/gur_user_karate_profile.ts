import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_karate_profile" ,{schema:"sportsmatik_local" } )
@Index("kar_usr_id",["karUsr",])
@Index("kar_kumite_event",["karKumiteEvent",])
@Index("kar_createdby_user",["karCreatedbyUser",])
export class gur_user_karate_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"kar_id"
        })
    kar_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKarateProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kar_usr_id'})
    karUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"kar_style_kata"
        })
    kar_style_kata:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"kar_style_kumite"
        })
    kar_style_kumite:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKarateProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kar_kumite_event'})
    karKumiteEvent:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kar_modifiedtime"
        })
    kar_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKarateProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kar_createdby_user'})
    karCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kar_modifiedby"
        })
    kar_modifiedby:string | null;
        
}
