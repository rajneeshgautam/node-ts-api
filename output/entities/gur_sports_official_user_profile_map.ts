import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_official_user_profile} from "./gur_sports_official_user_profile";
import {gur_sports_official_types} from "./gur_sports_official_types";


@Entity("gur_sports_official_user_profile_map" ,{schema:"sportsmatik_local" } )
@Index("unique_sport_official",["spmSom","spmSot",],{unique:true})
@Index("som_sot_id",["spmSot",])
@Index("spm_som_id",["spmSom",])
export class gur_sports_official_user_profile_map {

   
    @ManyToOne(()=>gur_sports_official_user_profile, (gur_sports_official_user_profile: gur_sports_official_user_profile)=>gur_sports_official_user_profile.gurSportsOfficialUserProfileMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'spm_som_id'})
    spmSom:gur_sports_official_user_profile | null;


   
    @ManyToOne(()=>gur_sports_official_types, (gur_sports_official_types: gur_sports_official_types)=>gur_sports_official_types.gurSportsOfficialUserProfileMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'spm_sot_id'})
    spmSot:gur_sports_official_types | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"spm_comment"
        })
    spm_comment:string | null;
        
}
