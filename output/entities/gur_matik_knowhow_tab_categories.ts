import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthdays_photos} from "./gur_birthdays_photos";
import {gur_health_injuries_titles} from "./gur_health_injuries_titles";
import {gur_matik_birthdays} from "./gur_matik_birthdays";
import {gur_matik_knowhow_academies} from "./gur_matik_knowhow_academies";
import {gur_matik_knowhow_buzzwords} from "./gur_matik_knowhow_buzzwords";
import {gur_matik_knowhow_calendar} from "./gur_matik_knowhow_calendar";
import {gur_matik_knowhow_fun_facts} from "./gur_matik_knowhow_fun_facts";
import {gur_matik_knowhow_gallery} from "./gur_matik_knowhow_gallery";
import {gur_matik_knowhow_reference_links} from "./gur_matik_knowhow_reference_links";
import {gur_matik_knowhow_teams} from "./gur_matik_knowhow_teams";
import {gur_sports_personalities} from "./gur_sports_personalities";
import {gur_wiki_competitions_sports_map} from "./gur_wiki_competitions_sports_map";
import {gur_wiki_sports_equipage} from "./gur_wiki_sports_equipage";
import {gur_wiki_technologies} from "./gur_wiki_technologies";
import {gur_wiki_venues_sports_map} from "./gur_wiki_venues_sports_map";
import {gur_world_events} from "./gur_world_events";
import {gur_world_events_players_bkup} from "./gur_world_events_players_bkup";


@Entity("gur_matik_knowhow_tab_categories" ,{schema:"sportsmatik_local" } )
export class gur_matik_knowhow_tab_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"knc_id"
        })
    knc_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"knc_name"
        })
    knc_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"knc_slug"
        })
    knc_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"knc_type"
        })
    knc_type:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"knc_sort_order"
        })
    knc_sort_order:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"knc_createdby"
        })
    knc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"knc_modifiedby"
        })
    knc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"knc_modifiedtime"
        })
    knc_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_birthdays_photos, (gur_birthdays_photos: gur_birthdays_photos)=>gur_birthdays_photos.brpType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysPhotoss:gur_birthdays_photos[];
    

   
    @OneToMany(()=>gur_health_injuries_titles, (gur_health_injuries_titles: gur_health_injuries_titles)=>gur_health_injuries_titles.hwtTab,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthInjuriesTitless:gur_health_injuries_titles[];
    

   
    @OneToMany(()=>gur_matik_birthdays, (gur_matik_birthdays: gur_matik_birthdays)=>gur_matik_birthdays.birKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikBirthdayss:gur_matik_birthdays[];
    

   
    @OneToMany(()=>gur_matik_knowhow_academies, (gur_matik_knowhow_academies: gur_matik_knowhow_academies)=>gur_matik_knowhow_academies.kacKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAcademiess:gur_matik_knowhow_academies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_buzzwords, (gur_matik_knowhow_buzzwords: gur_matik_knowhow_buzzwords)=>gur_matik_knowhow_buzzwords.khbKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowBuzzwordss:gur_matik_knowhow_buzzwords[];
    

   
    @OneToMany(()=>gur_matik_knowhow_calendar, (gur_matik_knowhow_calendar: gur_matik_knowhow_calendar)=>gur_matik_knowhow_calendar.khcType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCalendars:gur_matik_knowhow_calendar[];
    

   
    @OneToMany(()=>gur_matik_knowhow_fun_facts, (gur_matik_knowhow_fun_facts: gur_matik_knowhow_fun_facts)=>gur_matik_knowhow_fun_facts.khfKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowFunFactss:gur_matik_knowhow_fun_facts[];
    

   
    @OneToMany(()=>gur_matik_knowhow_gallery, (gur_matik_knowhow_gallery: gur_matik_knowhow_gallery)=>gur_matik_knowhow_gallery.khgCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowGallerys:gur_matik_knowhow_gallery[];
    

   
    @OneToMany(()=>gur_matik_knowhow_reference_links, (gur_matik_knowhow_reference_links: gur_matik_knowhow_reference_links)=>gur_matik_knowhow_reference_links.krlKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowReferenceLinkss:gur_matik_knowhow_reference_links[];
    

   
    @OneToMany(()=>gur_matik_knowhow_teams, (gur_matik_knowhow_teams: gur_matik_knowhow_teams)=>gur_matik_knowhow_teams.khtKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowTeamss:gur_matik_knowhow_teams[];
    

   
    @OneToMany(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.wepHeroCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiess:gur_sports_personalities[];
    

   
    @OneToMany(()=>gur_wiki_competitions_sports_map, (gur_wiki_competitions_sports_map: gur_wiki_competitions_sports_map)=>gur_wiki_competitions_sports_map.wsmKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiCompetitionsSportsMaps:gur_wiki_competitions_sports_map[];
    

   
    @OneToMany(()=>gur_wiki_sports_equipage, (gur_wiki_sports_equipage: gur_wiki_sports_equipage)=>gur_wiki_sports_equipage.uaeKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsEquipages:gur_wiki_sports_equipage[];
    

   
    @OneToMany(()=>gur_wiki_technologies, (gur_wiki_technologies: gur_wiki_technologies)=>gur_wiki_technologies.tecKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiTechnologiess:gur_wiki_technologies[];
    

   
    @OneToMany(()=>gur_wiki_venues_sports_map, (gur_wiki_venues_sports_map: gur_wiki_venues_sports_map)=>gur_wiki_venues_sports_map.wsmCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurWikiVenuesSportsMaps:gur_wiki_venues_sports_map[];
    

   
    @OneToMany(()=>gur_world_events, (gur_world_events: gur_world_events)=>gur_world_events.evtKnc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventss:gur_world_events[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepBdayCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups:gur_world_events_players_bkup[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepHeroCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups2:gur_world_events_players_bkup[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepPlayerCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups3:gur_world_events_players_bkup[];
    
}
