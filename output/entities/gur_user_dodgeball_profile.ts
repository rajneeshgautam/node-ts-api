import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_dodgeball_profile" ,{schema:"sportsmatik_local" } )
@Index("dod_usr_id",["dodUsr",])
@Index("dod_createdby_user",["dodCreatedbyUser",])
export class gur_user_dodgeball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"dod_id"
        })
    dod_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDodgeballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dod_usr_id'})
    dodUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"dod_hand"
        })
    dod_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"dod_modifiedtime"
        })
    dod_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDodgeballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dod_createdby_user'})
    dodCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"dod_modifiedby"
        })
    dod_modifiedby:string | null;
        
}
