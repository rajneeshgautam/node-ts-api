import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_matik_play_user_activity_log" ,{schema:"sportsmatik_local" } )
@Index("mul_mpu_id",["mul_mpu_id",])
export class gur_matik_play_user_activity_log {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mul_id"
        })
    mul_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mul_mpu_id"
        })
    mul_mpu_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mul_msg"
        })
    mul_msg:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mul_createdby"
        })
    mul_createdby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mul_created_time"
        })
    mul_created_time:Date;
        
}
