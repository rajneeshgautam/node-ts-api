import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_contest} from "./gur_matik_play_contest";
import {gur_matik_play_contest_questions} from "./gur_matik_play_contest_questions";


@Entity("gur_matik_play_contest_user_answers" ,{schema:"sportsmatik_local" } )
@Index("mpa_mpq_id",["mpaMpq",])
@Index("mpa_mpu_id",["mpa_usr_id",])
@Index("mpa_mpc_id",["mpaMpc",])
export class gur_matik_play_contest_user_answers {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mpa_id"
        })
    mpa_id:string;
        

   
    @ManyToOne(()=>gur_matik_play_contest, (gur_matik_play_contest: gur_matik_play_contest)=>gur_matik_play_contest.gurMatikPlayContestUserAnswerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mpa_mpc_id'})
    mpaMpc:gur_matik_play_contest | null;


   
    @ManyToOne(()=>gur_matik_play_contest_questions, (gur_matik_play_contest_questions: gur_matik_play_contest_questions)=>gur_matik_play_contest_questions.gurMatikPlayContestUserAnswerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mpa_mpq_id'})
    mpaMpq:gur_matik_play_contest_questions | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mpa_usr_id"
        })
    mpa_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'100'",
        name:"mpa_uty_id"
        })
    mpa_uty_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"mpa_answer"
        })
    mpa_answer:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpa_answeredby"
        })
    mpa_answeredby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpa_answered_time"
        })
    mpa_answered_time:Date;
        
}
