import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_history_ls} from "./gur_matik_knowhow_history_ls";


@Entity("gur_matik_knowhow_history" ,{schema:"sportsmatik_local" } )
@Index("khl_wkh_id",["khhMkh",])
export class gur_matik_knowhow_history {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khh_id"
        })
    khh_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khh_mkh_id'})
    khhMkh:gur_matik_knowhow | null;


    @Column("text",{ 
        nullable:true,
        name:"khh_photo_data"
        })
    khh_photo_data:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"khh_sort_order"
        })
    khh_sort_order:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khh_createdby"
        })
    khh_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khh_modifiedby"
        })
    khh_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khh_modifiedtime"
        })
    khh_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_history_ls, (gur_matik_knowhow_history_ls: gur_matik_knowhow_history_ls)=>gur_matik_knowhow_history_ls.hhlKhh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowHistoryLss:gur_matik_knowhow_history_ls[];
    
}
