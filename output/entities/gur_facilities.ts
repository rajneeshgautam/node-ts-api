import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_facilities" ,{schema:"sportsmatik_local" } )
@Index("fac_uty_id",["facUty",])
@Index("fac_createdby_user",["facCreatedbyUser",])
export class gur_facilities {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fac_id"
        })
    fac_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurFacilitiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fac_uty_id'})
    facUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"fac_user_id"
        })
    fac_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fac_value"
        })
    fac_value:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fac_others"
        })
    fac_others:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"fac_status"
        })
    fac_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fac_admin_review"
        })
    fac_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurFacilitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fac_createdby_user'})
    facCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"fac_createdby"
        })
    fac_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"fac_modifiedby"
        })
    fac_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fac_modifiedtime"
        })
    fac_modifiedtime:Date;
        
}
