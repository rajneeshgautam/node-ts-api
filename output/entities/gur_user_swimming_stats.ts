import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_swimming_stats" ,{schema:"sportsmatik_local" } )
@Index("sps_usr_id",["spsUsr",])
@Index("sps_ucp_id",["spsUcp",])
@Index("sps_event",["spsEvent",])
@Index("sps_rank",["spsRank",])
export class gur_user_swimming_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"sps_id"
        })
    sps_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSwimmingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sps_usr_id'})
    spsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserSwimmingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sps_ucp_id'})
    spsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSwimmingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sps_event'})
    spsEvent:gur_value_list | null;


    @Column("time",{ 
        nullable:false,
        name:"sps_value"
        })
    sps_value:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSwimmingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sps_rank'})
    spsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sps_modifiedtime"
        })
    sps_modifiedtime:Date;
        
}
