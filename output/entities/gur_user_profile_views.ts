import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_user_profile_views" ,{schema:"sportsmatik_local" } )
@Index("upv_by_utype",["upvByUtype",])
@Index("upv_for_utype",["upvForUtype",])
@Index("upv_by_user",["upvByUser",])
export class gur_user_profile_views {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"upv_id"
        })
    upv_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserProfileViewss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upv_for_utype'})
    upvForUtype:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"upv_for_uid"
        })
    upv_for_uid:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserProfileViewss2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upv_by_utype'})
    upvByUtype:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"upv_by_uid"
        })
    upv_by_uid:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserProfileViewss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upv_by_user'})
    upvByUser:gur_users | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"upv_view_count"
        })
    upv_view_count:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"upv_createdby"
        })
    upv_createdby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"upv_createdtime"
        })
    upv_createdtime:Date;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"upv_last_viewed_time"
        })
    upv_last_viewed_time:Date | null;
        
}
