import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";
import {gur_educational_institutions} from "./gur_educational_institutions";
import {gur_educational_courses} from "./gur_educational_courses";
import {gur_educational_courses_specialization} from "./gur_educational_courses_specialization";
import {gur_educational_boards_types} from "./gur_educational_boards_types";
import {gur_educational_language_medium} from "./gur_educational_language_medium";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_user_qualifications" ,{schema:"sportsmatik_local" } )
@Index("uqf_usr_id",["uqfUsr",])
@Index("uqf_course_name",["uqfCourseName",])
@Index("uqf_zone",["uqfZone",])
@Index("uqf_type",["uqfType",])
@Index("uqf_study_mode",["uqf_study_mode",])
@Index("uqf_specialization_name",["uqfSpecializationName",])
@Index("uqf_board_name",["uqfBoardName",])
@Index("uqf_medium_name",["uqfMediumName",])
@Index("uqf_country",["uqfCountry",])
@Index("uqf_institute",["uqfInstituteName",])
@Index("uqf_createdby_user",["uqfCreatedbyUser",])
export class gur_user_qualifications {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"uqf_id"
        })
    uqf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_usr_id'})
    uqfUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserQualificationss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_type'})
    uqfType:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["full-time","part-time","distance"],
        name:"uqf_study_mode"
        })
    uqf_study_mode:string | null;
        

   
    @ManyToOne(()=>gur_educational_institutions, (gur_educational_institutions: gur_educational_institutions)=>gur_educational_institutions.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_institute_name'})
    uqfInstituteName:gur_educational_institutions | null;


   
    @ManyToOne(()=>gur_educational_courses, (gur_educational_courses: gur_educational_courses)=>gur_educational_courses.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_course_name'})
    uqfCourseName:gur_educational_courses | null;


   
    @ManyToOne(()=>gur_educational_courses_specialization, (gur_educational_courses_specialization: gur_educational_courses_specialization)=>gur_educational_courses_specialization.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_specialization_name'})
    uqfSpecializationName:gur_educational_courses_specialization | null;


   
    @ManyToOne(()=>gur_educational_boards_types, (gur_educational_boards_types: gur_educational_boards_types)=>gur_educational_boards_types.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_board_name'})
    uqfBoardName:gur_educational_boards_types | null;


   
    @ManyToOne(()=>gur_educational_language_medium, (gur_educational_language_medium: gur_educational_language_medium)=>gur_educational_language_medium.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_medium_name'})
    uqfMediumName:gur_educational_language_medium | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uqf_certification_name"
        })
    uqf_certification_name:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"uqf_start"
        })
    uqf_start:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"uqf_end"
        })
    uqf_end:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurUserQualificationss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_country'})
    uqfCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurUserQualificationss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_zone'})
    uqfZone:gur_all_cities | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uqf_highest"
        })
    uqf_highest:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"uqf_description"
        })
    uqf_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uqf_status"
        })
    uqf_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uqf_admin_review"
        })
    uqf_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserQualificationss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uqf_createdby_user'})
    uqfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"uqf_createdby"
        })
    uqf_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"uqf_modifiedby"
        })
    uqf_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uqf_modifiedtime"
        })
    uqf_modifiedtime:Date;
        
}
