import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_cities" ,{schema:"sportsmatik_local" } )
@Index("cit_cnt_id_2",["cit_cnt_id","cit_name2",],{unique:true})
@Index("cit_cnt_id",["cit_cnt_id",])
@Index("cit_name",["cit_name2",])
export class gur_cities {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"cit_id"
        })
    cit_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"cit_cnt_id"
        })
    cit_cnt_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cit_name2"
        })
    cit_name2:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cit_lat"
        })
    cit_lat:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cit_long"
        })
    cit_long:string | null;
        
}
