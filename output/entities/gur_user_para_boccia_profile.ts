import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_boccia_profile" ,{schema:"sportsmatik_local" } )
@Index("boc_usr_id",["bocUsr",])
@Index("boc_disability_classes",["bocDisabilityClasses",])
@Index("boc_createdby_user",["bocCreatedbyUser",])
export class gur_user_para_boccia_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"boc_id"
        })
    boc_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBocciaProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'boc_usr_id'})
    bocUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaBocciaProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'boc_disability_classes'})
    bocDisabilityClasses:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"boc_modifiedtime"
        })
    boc_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBocciaProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'boc_createdby_user'})
    bocCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"boc_modifiedby"
        })
    boc_modifiedby:string | null;
        
}
