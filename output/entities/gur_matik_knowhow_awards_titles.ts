import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_awards_titles_ls} from "./gur_matik_knowhow_awards_titles_ls";
import {gur_matik_knowhow_awards_winners} from "./gur_matik_knowhow_awards_winners";


@Entity("gur_matik_knowhow_awards_titles" ,{schema:"sportsmatik_local" } )
export class gur_matik_knowhow_awards_titles {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"awt_id"
        })
    awt_id:number;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"awt_type"
        })
    awt_type:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awt_pic"
        })
    awt_pic:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"awt_pic_data"
        })
    awt_pic_data:string | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["m","f","b"],
        name:"awt_gender"
        })
    awt_gender:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["player","official"],
        name:"awt_awardee_type"
        })
    awt_awardee_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"awt_createdby"
        })
    awt_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awt_modifiedby"
        })
    awt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"awt_modifiedtime"
        })
    awt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_awards_titles_ls, (gur_matik_knowhow_awards_titles_ls: gur_matik_knowhow_awards_titles_ls)=>gur_matik_knowhow_awards_titles_ls.wtlAwt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsTitlesLss:gur_matik_knowhow_awards_titles_ls[];
    

   
    @OneToMany(()=>gur_matik_knowhow_awards_winners, (gur_matik_knowhow_awards_winners: gur_matik_knowhow_awards_winners)=>gur_matik_knowhow_awards_winners.awwAwt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsWinnerss:gur_matik_knowhow_awards_winners[];
    
}
