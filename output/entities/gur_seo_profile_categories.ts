import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_seo_profile_categories" ,{schema:"sportsmatik_local" } )
export class gur_seo_profile_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"spc_id"
        })
    spc_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"spc_menu"
        })
    spc_menu:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spc_sub_menu"
        })
    spc_sub_menu:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spc_section"
        })
    spc_section:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spc_filter"
        })
    spc_filter:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"spc_url"
        })
    spc_url:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spc_meta_title"
        })
    spc_meta_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"spc_meta_dec"
        })
    spc_meta_dec:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"spc_meta_key"
        })
    spc_meta_key:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spc_status"
        })
    spc_status:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"spc_page_status"
        })
    spc_page_status:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"spc_createdby"
        })
    spc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"spc_modifiedby"
        })
    spc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"spc_modifiedtime"
        })
    spc_modifiedtime:Date;
        
}
