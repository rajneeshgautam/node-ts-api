import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_faq_lists_ls} from "./gur_faq_lists_ls";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";
import {gur_faq_categories} from "./gur_faq_categories";


@Entity("gur_faq_lists" ,{schema:"sportsmatik_local" } )
export class gur_faq_lists {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fll_id"
        })
    fll_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fll_status"
        })
    fll_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"fll_sort_order"
        })
    fll_sort_order:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fll_createdby"
        })
    fll_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"fll_modifiedby"
        })
    fll_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fll_modifiedtime"
        })
    fll_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_faq_lists_ls, (gur_faq_lists_ls: gur_faq_lists_ls)=>gur_faq_lists_ls.flsFll,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFaqListsLss:gur_faq_lists_ls[];
    

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgFll,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    

   
    @ManyToMany(()=>gur_faq_categories, (gur_faq_categories: gur_faq_categories)=>gur_faq_categories.gurFaqListss)
    gurFaqCategoriess:gur_faq_categories[];
    
}
