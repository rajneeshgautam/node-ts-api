import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_users} from "./gur_users";
import {gur_sports_logistics_portfolio} from "./gur_sports_logistics_portfolio";


@Entity("gur_sports_logistics_venues" ,{schema:"sportsmatik_local" } )
@Index("lvn_createdby_user",["lvnCreatedbyUser",])
@Index("lvn_country",["lvnCountry",])
@Index("lvn_city",["lvnCity",])
export class gur_sports_logistics_venues {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"lvn_id"
        })
    lvn_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"lvn_name"
        })
    lvn_name:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsLogisticsVenuess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lvn_country'})
    lvnCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurSportsLogisticsVenuess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lvn_city'})
    lvnCity:gur_all_cities | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lvn_status"
        })
    lvn_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lvn_authentic"
        })
    lvn_authentic:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"lvn_createdby"
        })
    lvn_createdby:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsLogisticsVenuess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lvn_createdby_user'})
    lvnCreatedbyUser:gur_users | null;


    @Column("int",{ 
        nullable:true,
        name:"lvn_createdby_admin"
        })
    lvn_createdby_admin:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lvn_modifiedby"
        })
    lvn_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lvn_modified_time"
        })
    lvn_modified_time:Date;
        

   
    @OneToMany(()=>gur_sports_logistics_portfolio, (gur_sports_logistics_portfolio: gur_sports_logistics_portfolio)=>gur_sports_logistics_portfolio.slpVenue,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsPortfolios:gur_sports_logistics_portfolio[];
    
}
