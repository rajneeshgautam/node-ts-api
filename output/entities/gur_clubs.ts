import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_clubs" ,{schema:"sportsmatik_local" } )
@Index("clb_phone1",["clb_phone1",],{unique:true})
@Index("clb_phone2",["clb_phone2",],{unique:true})
@Index("clb_slug",["clb_slug",],{unique:true})
@Index("clb_gst_number",["clb_gst_number",],{unique:true})
@Index("clb_createdby_user",["clbCreatedbyUser",])
@Index("clb_country",["clbCountry",])
@Index("clb_usr_id",["clbUsr",])
@Index("clb_dis_id",["clbDis",])
@Index("clb_sts_id",["clbSts",])
@Index("clb_name",["clb_name",])
export class gur_clubs {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"clb_id"
        })
    clb_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"clb_slug"
        })
    clb_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"clb_name"
        })
    clb_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurClubss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clb_usr_id'})
    clbUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"clb_address"
        })
    clb_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clb_address2"
        })
    clb_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"clb_pincode"
        })
    clb_pincode:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurClubss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clb_country'})
    clbCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurClubss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clb_dis_id'})
    clbDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurClubss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clb_sts_id'})
    clbSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"clb_address_google_map"
        })
    clb_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clb_email"
        })
    clb_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clb_email_verified"
        })
    clb_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clb_email_verify_time"
        })
    clb_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"clb_phone1"
        })
    clb_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clb_phone1_verified"
        })
    clb_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"clb_phone1_otp"
        })
    clb_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clb_phone1_otp_time"
        })
    clb_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"clb_phone2"
        })
    clb_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clb_phone2_verified"
        })
    clb_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"clb_phone2_otp"
        })
    clb_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clb_phone2_otp_time"
        })
    clb_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"clb_status"
        })
    clb_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clb_profile_pic"
        })
    clb_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clb_timeline_pic"
        })
    clb_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clb_intro_status"
        })
    clb_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"clb_profile_summary"
        })
    clb_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"clb_founded_year"
        })
    clb_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"clb_founded_month"
        })
    clb_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clb_last_login_date"
        })
    clb_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clb_last_login_details"
        })
    clb_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"clb_intro_modifiedtime"
        })
    clb_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clb_profile_pic_modifiedtime"
        })
    clb_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"clb_free_sports"
        })
    clb_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"clb_free_sub_users"
        })
    clb_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"clb_gst_number"
        })
    clb_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clb_viewable"
        })
    clb_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurClubss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clb_createdby_user'})
    clbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"clb_createdby"
        })
    clb_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"clb_modifiedby"
        })
    clb_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"clb_modifiedtime"
        })
    clb_modifiedtime:Date;
        
}
