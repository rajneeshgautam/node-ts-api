import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_job_application} from "./gur_job_application";
import {gur_countries} from "./gur_countries";
import {gur_states} from "./gur_states";
import {gur_districts} from "./gur_districts";


@Entity("gur_job_education" ,{schema:"sportsmatik_local" } )
@Index("jed_joa_id",["jedJoa",])
@Index("joe_cnt_id",["jedCnt",])
@Index("joe_sts_id",["jedSts",])
@Index("joe_dis_id",["jedDis",])
export class gur_job_education {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"jed_id"
        })
    jed_id:number;
        

   
    @ManyToOne(()=>gur_job_application, (gur_job_application: gur_job_application)=>gur_job_application.gurJobEducations,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jed_joa_id'})
    jedJoa:gur_job_application | null;


    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"jed_institution"
        })
    jed_institution:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"jed_degree"
        })
    jed_degree:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"jed_major"
        })
    jed_major:string | null;
        

    @Column("date",{ 
        nullable:false,
        name:"jed_from"
        })
    jed_from:string;
        

    @Column("date",{ 
        nullable:true,
        name:"jed_to"
        })
    jed_to:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurJobEducations,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jed_cnt_id'})
    jedCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurJobEducations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jed_sts_id'})
    jedSts:gur_states | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"jed_state_name"
        })
    jed_state_name:string;
        

   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurJobEducations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jed_dis_id'})
    jedDis:gur_districts | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"jed_district_name"
        })
    jed_district_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"jed_is_pursuing"
        })
    jed_is_pursuing:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"jed_description"
        })
    jed_description:string | null;
        
}
