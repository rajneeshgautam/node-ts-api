import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_skeleton_profile" ,{schema:"sportsmatik_local" } )
@Index("ske_usr_id",["skeUsr",])
@Index("ske_createdby_user",["skeCreatedbyUser",])
export class gur_user_skeleton_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ske_id"
        })
    ske_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkeletonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ske_usr_id'})
    skeUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ske_modifiedtime"
        })
    ske_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkeletonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ske_createdby_user'})
    skeCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ske_modifiedby"
        })
    ske_modifiedby:string | null;
        
}
