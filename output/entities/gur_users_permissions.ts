import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_modules} from "./gur_user_modules";


@Entity("gur_users_permissions" ,{schema:"sportsmatik_local" } )
@Index("adr_moduleid",["adrModule",])
export class gur_users_permissions {

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUsersPermissionss,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adr_userid'})
    adrUser:gur_users | null;


   
    @ManyToOne(()=>gur_user_modules, (gur_user_modules: gur_user_modules)=>gur_user_modules.gurUsersPermissionss,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adr_moduleid'})
    adrModule:gur_user_modules | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_create"
        })
    adr_create:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_edit"
        })
    adr_edit:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_delete"
        })
    adr_delete:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_view"
        })
    adr_view:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"adr_createdby"
        })
    adr_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"adr_modifiedby"
        })
    adr_modifiedby:string | null;
        
}
