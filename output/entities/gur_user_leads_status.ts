import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_leads_status" ,{schema:"sportsmatik_local" } )
export class gur_user_leads_status {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"uls_id"
        })
    uls_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"uls_name"
        })
    uls_name:string;
        

    @Column("int",{ 
        nullable:false,
        name:"uls_sort_order"
        })
    uls_sort_order:number;
        
}
