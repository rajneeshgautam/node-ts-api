import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_matik_knowhow_records_no_use" ,{schema:"sportsmatik_local" } )
export class gur_matik_knowhow_records_no_use {

    @Column("bigint",{ 
        nullable:true,
        name:"khr_id"
        })
    khr_id:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"khr_description"
        })
    khr_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khr_createdby"
        })
    khr_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khr_modifiedby"
        })
    khr_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khr_modifiedtime"
        })
    khr_modifiedtime:Date;
        
}
