import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_powerlifting_stats" ,{schema:"sportsmatik_local" } )
@Index("ups_usr_id",["upsUsr",])
@Index("ups_ucp_id",["upsUcp",])
@Index("ups_event_weight",["upsEventWeight",])
@Index("ups_rank",["upsRank",])
export class gur_user_powerlifting_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"ups_id"
        })
    ups_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPowerliftingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ups_usr_id'})
    upsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserPowerliftingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ups_ucp_id'})
    upsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserPowerliftingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ups_event_weight'})
    upsEventWeight:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["raw","equipped"],
        name:"ups_division"
        })
    ups_division:string | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ups_squat"
        })
    ups_squat:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ups_bench_press"
        })
    ups_bench_press:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ups_deadlift"
        })
    ups_deadlift:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ups_bench_press_single_lift"
        })
    ups_bench_press_single_lift:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserPowerliftingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ups_rank'})
    upsRank:gur_value_list | null;


    @Column("text",{ 
        nullable:true,
        name:"ups_achivement"
        })
    ups_achivement:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ups_modifiedtime"
        })
    ups_modifiedtime:Date;
        
}
