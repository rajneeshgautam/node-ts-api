import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_types} from "./gur_user_types";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_contact_view_history" ,{schema:"sportsmatik_local" } )
@Index("rat_rated_by",["chrByUser",])
@Index("rat_rated_by_type",["chrByType",])
@Index("mtg_for_type",["chrForType",])
@Index("chr_pch_id",["chr_pch_id",])
@Index("gur_contact_view_history_ibfk_4",["chrContactVia",])
export class gur_contact_view_history {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"chr_id"
        })
    chr_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurContactViewHistorys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'chr_by_user'})
    chrByUser:gur_users | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurContactViewHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'chr_by_type'})
    chrByType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"chr_by"
        })
    chr_by:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurContactViewHistorys2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'chr_for_type'})
    chrForType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"chr_for"
        })
    chr_for:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurContactViewHistorys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'chr_contact_via'})
    chrContactVia:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"chr_credits_used"
        })
    chr_credits_used:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"chr_status"
        })
    chr_status:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"chr_pch_id"
        })
    chr_pch_id:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"chr_viewed_on"
        })
    chr_viewed_on:Date;
        
}
