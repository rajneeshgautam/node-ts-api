import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_sumo_wrestling_profile" ,{schema:"sportsmatik_local" } )
@Index("sum_usr_id",["sumUsr",])
@Index("sum_weight_class",["sumWeightClass",])
@Index("sum_createdby_user",["sumCreatedbyUser",])
export class gur_user_sumo_wrestling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sum_id"
        })
    sum_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSumoWrestlingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sum_usr_id'})
    sumUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSumoWrestlingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sum_weight_class'})
    sumWeightClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sum_modifiedtime"
        })
    sum_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSumoWrestlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sum_createdby_user'})
    sumCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sum_modifiedby"
        })
    sum_modifiedby:string | null;
        
}
