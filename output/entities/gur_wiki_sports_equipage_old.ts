import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_wiki_sports_equipage_old" ,{schema:"sportsmatik_local" } )
@Index("uae_slug",["uae_slug",],{unique:true})
@Index("uae_spo_id",["uae_spo_id",])
@Index("gur_wiki_sports_equipage_ibfk_2",["uae_knc_id",])
export class gur_wiki_sports_equipage_old {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uae_id"
        })
    uae_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"uae_spo_id"
        })
    uae_spo_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"uae_slug"
        })
    uae_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uae_photo"
        })
    uae_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uae_photo_data"
        })
    uae_photo_data:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uae_timeline_image_data"
        })
    uae_timeline_image_data:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'pg'",
        enum:["uniform","pg","equipment"],
        name:"uae_type"
        })
    uae_type:string;
        

    @Column("int",{ 
        nullable:false,
        name:"uae_knc_id"
        })
    uae_knc_id:number;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"uae_sort_order"
        })
    uae_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uae_for_health"
        })
    uae_for_health:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uae_is_featured"
        })
    uae_is_featured:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uae_createdby"
        })
    uae_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"uae_modifiedby"
        })
    uae_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uae_modifiedtime"
        })
    uae_modifiedtime:Date;
        
}
