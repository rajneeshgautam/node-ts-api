import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_diving_stats" ,{schema:"sportsmatik_local" } )
@Index("dvs_usr_id",["dvsUsr",])
@Index("dvs_ucp_id",["dvsUcp",])
@Index("dvs_event",["dvsEvent",])
@Index("dvs_rank",["dvs_rank",])
export class gur_user_diving_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"dvs_id"
        })
    dvs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDivingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dvs_usr_id'})
    dvsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserDivingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dvs_ucp_id'})
    dvsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserDivingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dvs_event'})
    dvsEvent:gur_value_list | null;


    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"dvs_points"
        })
    dvs_points:number;
        

    @Column("int",{ 
        nullable:true,
        name:"dvs_rank"
        })
    dvs_rank:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"dvs_modifiedtime"
        })
    dvs_modifiedtime:Date;
        
}
