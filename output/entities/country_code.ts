import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("country_code" ,{schema:"sportsmatik_local" } )
export class country_code {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:44,
        name:"country"
        })
    country:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:11,
        name:"iso2"
        })
    iso2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:39,
        name:"currency"
        })
    currency:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:4,
        name:"currency_code"
        })
    currency_code:string | null;
        
}
