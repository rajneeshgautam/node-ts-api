import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list} from "./gur_value_list";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_user_qualifications} from "./gur_user_qualifications";
import {gur_educational_courses_specialization} from "./gur_educational_courses_specialization";


@Entity("gur_educational_courses" ,{schema:"sportsmatik_local" } )
@Index("cor_name",["cor_name",])
@Index("cor_edu_type",["corEduType",])
@Index("cor_createdby_user",["corCreatedbyUser",])
@Index("cor_createdby_admin",["corCreatedbyAdmin",])
export class gur_educational_courses {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cor_id"
        })
    cor_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cor_name"
        })
    cor_name:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurEducationalCoursess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_edu_type'})
    corEduType:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_status"
        })
    cor_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_authentic"
        })
    cor_authentic:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurEducationalCoursess,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cor_createdby_user'})
    corCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurEducationalCoursess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_createdby_admin'})
    corCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"cor_createdby"
        })
    cor_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cor_modifiedby"
        })
    cor_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cor_modifiedtime"
        })
    cor_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfCourseName,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    

   
    @ManyToMany(()=>gur_educational_courses_specialization, (gur_educational_courses_specialization: gur_educational_courses_specialization)=>gur_educational_courses_specialization.gurEducationalCoursess,{  nullable:false, })
    @JoinTable({ name:'gur_educational_courses_specialization_map'})
    gurEducationalCoursesSpecializations:gur_educational_courses_specialization[];
    
}
