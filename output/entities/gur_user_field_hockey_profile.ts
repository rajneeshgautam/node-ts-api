import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_field_hockey_profile" ,{schema:"sportsmatik_local" } )
@Index("fhk_usr_id",["fhkUsr",])
@Index("fhk_primary_position",["fhkPrimaryPosition",])
@Index("fhk_alternate_position",["fhkAlternatePosition",])
@Index("fhk_createdby_user",["fhkCreatedbyUser",])
export class gur_user_field_hockey_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fhk_id"
        })
    fhk_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFieldHockeyProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fhk_usr_id'})
    fhkUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFieldHockeyProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fhk_primary_position'})
    fhkPrimaryPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFieldHockeyProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fhk_alternate_position'})
    fhkAlternatePosition:gur_value_list | null;


    @Column("int",{ 
        nullable:true,
        name:"fhk_temperament"
        })
    fhk_temperament:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fhk_modifiedtime"
        })
    fhk_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFieldHockeyProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fhk_createdby_user'})
    fhkCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"fhk_modifiedby"
        })
    fhk_modifiedby:string | null;
        
}
