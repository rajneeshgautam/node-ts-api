import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list} from "./gur_value_list";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_lawyer_ca_portfolio} from "./gur_lawyer_ca_portfolio";
import {gur_lawyer_ca_services} from "./gur_lawyer_ca_services";


@Entity("gur_law_ca_services_categories" ,{schema:"sportsmatik_local" } )
@Index("lcs_createdby_user",["lcsCreatedbyUser",])
@Index("lcs_type",["lcsType",])
@Index("lcs_createdby_admin",["lcsCreatedbyAdmin",])
export class gur_law_ca_services_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"lcs_id"
        })
    lcs_id:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurLawCaServicesCategoriess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lcs_type'})
    lcsType:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        name:"lcs_name"
        })
    lcs_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lcs_status"
        })
    lcs_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lcs_authentic"
        })
    lcs_authentic:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"lcs_createdby"
        })
    lcs_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lcs_modifiedby"
        })
    lcs_modifiedby:string | null;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurLawCaServicesCategoriess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lcs_createdby_user'})
    lcsCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurLawCaServicesCategoriess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lcs_createdby_admin'})
    lcsCreatedbyAdmin:gur_admin_users | null;

    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurLawCaServicesCategoriess2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lcs_createdby_admin'})
    lcsCreatedbyAdmin:gur_admin_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lcs_modified_time"
        })
    lcs_modified_time:Date;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"lcs_reviewed_on"
        })
    lcs_reviewed_on:Date | null;
        

   
    @OneToMany(()=>gur_lawyer_ca_portfolio, (gur_lawyer_ca_portfolio: gur_lawyer_ca_portfolio)=>gur_lawyer_ca_portfolio.lwpService,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawyerCaPortfolios:gur_lawyer_ca_portfolio[];
    

   
    @OneToMany(()=>gur_lawyer_ca_services, (gur_lawyer_ca_services: gur_lawyer_ca_services)=>gur_lawyer_ca_services.lwcService,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawyerCaServicess:gur_lawyer_ca_services[];
    
}
