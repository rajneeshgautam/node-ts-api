import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_sports_marketing_company" ,{schema:"sportsmatik_local" } )
@Index("clb_phone1",["smc_phone1",],{unique:true})
@Index("clb_phone2",["smc_phone2",],{unique:true})
@Index("smc_gst_number",["smc_gst_number",],{unique:true})
@Index("clb_createdby_user",["smcCreatedbyUser",])
@Index("clb_country",["smcCountry",])
@Index("clb_usr_id",["smcUsr",])
@Index("clb_dis_id",["smcDis",])
@Index("clb_sts_id",["smcSts",])
@Index("smc_name",["smc_name",])
export class gur_sports_marketing_company {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"smc_id"
        })
    smc_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"smc_slug"
        })
    smc_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"smc_name"
        })
    smc_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsMarketingCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'smc_usr_id'})
    smcUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"smc_address"
        })
    smc_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"smc_address2"
        })
    smc_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"smc_pincode"
        })
    smc_pincode:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsMarketingCompanys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'smc_country'})
    smcCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurSportsMarketingCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'smc_dis_id'})
    smcDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurSportsMarketingCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'smc_sts_id'})
    smcSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"smc_address_google_map"
        })
    smc_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"smc_email"
        })
    smc_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"smc_email_verified"
        })
    smc_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"smc_email_verify_time"
        })
    smc_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"smc_phone1"
        })
    smc_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"smc_phone1_verified"
        })
    smc_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"smc_phone1_otp"
        })
    smc_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"smc_phone1_otp_time"
        })
    smc_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"smc_phone2"
        })
    smc_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"smc_phone2_verified"
        })
    smc_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"smc_phone2_otp"
        })
    smc_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"smc_phone2_otp_time"
        })
    smc_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"smc_status"
        })
    smc_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"smc_profile_pic"
        })
    smc_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"smc_timeline_pic"
        })
    smc_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"smc_intro_status"
        })
    smc_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"smc_profile_summary"
        })
    smc_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"smc_founded_year"
        })
    smc_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"smc_founded_month"
        })
    smc_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"smc_last_login_date"
        })
    smc_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"smc_last_login_details"
        })
    smc_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"smc_intro_modifiedtime"
        })
    smc_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"smc_profile_pic_modifiedtime"
        })
    smc_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"smc_free_sports"
        })
    smc_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"smc_free_sub_users"
        })
    smc_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"smc_gst_number"
        })
    smc_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"smc_viewable"
        })
    smc_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsMarketingCompanys2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'smc_createdby_user'})
    smcCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"smc_createdby"
        })
    smc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"smc_modifiedby"
        })
    smc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"smc_modifiedtime"
        })
    smc_modifiedtime:Date;
        
}
