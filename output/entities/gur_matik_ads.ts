import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_ads_categories} from "./gur_ads_categories";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_plans} from "./gur_plans";
import {gur_matik_ads_media} from "./gur_matik_ads_media";


@Entity("gur_matik_ads" ,{schema:"sportsmatik_local" } )
@Index("ads_title",["ads_title",])
@Index("ads_usr_id",["adsUsr",])
@Index("ads_cnt_id",["adsCnt",])
@Index("ads_cat_id",["adsCat",])
@Index("ads_createdby_user",["ads_createdby_user",])
@Index("gur_matik_ads_ibfk_7",["adsPln",])
@Index("gur_matik_ads_ibfk_8",["adsCit",])
export class gur_matik_ads {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ads_id"
        })
    ads_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurMatikAdss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ads_usr_id'})
    adsUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"ads_title"
        })
    ads_title:string;
        

    @Column("text",{ 
        nullable:true,
        name:"ads_descriptions"
        })
    ads_descriptions:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ads_negotiable"
        })
    ads_negotiable:boolean;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"ads_price"
        })
    ads_price:number | null;
        

   
    @ManyToOne(()=>gur_ads_categories, (gur_ads_categories: gur_ads_categories)=>gur_ads_categories.gurMatikAdss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ads_cat_id'})
    adsCat:gur_ads_categories | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ads_book_author"
        })
    ads_book_author:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'2'",
        name:"ads_status"
        })
    ads_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ads_admin_review"
        })
    ads_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ads_admin_note"
        })
    ads_admin_note:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ads_reason"
        })
    ads_reason:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikAdss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ads_cnt_id'})
    adsCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurMatikAdss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ads_cit_id'})
    adsCit:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"ads_area1"
        })
    ads_area1:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ads_area2"
        })
    ads_area2:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"ads_pincode"
        })
    ads_pincode:string;
        

    @Column("text",{ 
        nullable:true,
        name:"ads_address_google_map"
        })
    ads_address_google_map:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"ads_posted_date"
        })
    ads_posted_date:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ads_deactivate_date"
        })
    ads_deactivate_date:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'both'",
        enum:["email","phone","both"],
        name:"ads_contact_mode"
        })
    ads_contact_mode:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ads_createdby_user"
        })
    ads_createdby_user:number | null;
        

   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurMatikAdss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ads_pln_id'})
    adsPln:gur_plans | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"ads_modifiedby"
        })
    ads_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"ads_modifiedtime"
        })
    ads_modifiedtime:Date | null;
        

   
    @OneToMany(()=>gur_matik_ads_media, (gur_matik_ads_media: gur_matik_ads_media)=>gur_matik_ads_media.admAds,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikAdsMedias:gur_matik_ads_media[];
    
}
