import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_activity_log" ,{schema:"sportsmatik_local" } )
@Index("stl_stk_id",["ual_uty_id",])
@Index("stl_str_id",["ual_usr_id",])
@Index("stl_usr_id",["ual_type",])
@Index("stl_usr_type",["ual_created_time",])
export class gur_user_activity_log {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ual_id"
        })
    ual_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ual_uty_id"
        })
    ual_uty_id:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ual_usr_id"
        })
    ual_usr_id:number | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["i","e"],
        name:"ual_type"
        })
    ual_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ual_log_msg"
        })
    ual_log_msg:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ual_createdby"
        })
    ual_createdby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ual_created_time"
        })
    ual_created_time:Date;
        
}
