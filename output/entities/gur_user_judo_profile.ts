import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_judo_profile" ,{schema:"sportsmatik_local" } )
@Index("jud_usr_id",["judUsr",])
@Index("jud_weight_event",["judWeightEvent",])
@Index("jud_createdby_user",["judCreatedbyUser",])
export class gur_user_judo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"jud_id"
        })
    jud_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJudoProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jud_usr_id'})
    judUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserJudoProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jud_weight_event'})
    judWeightEvent:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"jud_modifiedtime"
        })
    jud_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJudoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jud_createdby_user'})
    judCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"jud_modifiedby"
        })
    jud_modifiedby:string | null;
        
}
