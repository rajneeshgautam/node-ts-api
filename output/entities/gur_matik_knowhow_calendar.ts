import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_matik_knowhow_calendar" ,{schema:"sportsmatik_local" } )
@Index("khc_country",["khc_countries",])
@Index("khc_sport",["khcSpo",])
@Index("gur_matik_knowhow_calendar_ibfk_1",["khcType",])
@Index("gur_matik_knowhow_calendar_ibfk_2",["khcVenue",])
@Index("gur_matik_knowhow_calendar_ibfk_5",["khcCity",])
export class gur_matik_knowhow_calendar {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"khc_id"
        })
    khc_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowCalendars,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khc_type'})
    khcType:gur_matik_knowhow_tab_categories | null;


   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCalendars,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khc_spo_id'})
    khcSpo:gur_wiki_sports | null;

    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCalendars2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khc_spo_id'})
    khcSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"khc_title"
        })
    khc_title:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"khc_description"
        })
    khc_description:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"khc_day_start"
        })
    khc_day_start:number;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'00'",
        name:"khc_month_start"
        })
    khc_month_start:number;
        

    @Column("int",{ 
        nullable:false,
        name:"khc_year_start"
        })
    khc_year_start:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"khc_day_end"
        })
    khc_day_end:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"khc_month_end"
        })
    khc_month_end:number;
        

    @Column("int",{ 
        nullable:true,
        name:"khc_year_end"
        })
    khc_year_end:number | null;
        

   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurMatikKnowhowCalendars,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khc_venue'})
    khcVenue:gur_wiki_venues | null;


    @Column("varchar",{ 
        nullable:true,
        name:"khc_location"
        })
    khc_location:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khc_countries"
        })
    khc_countries:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"khc_multi_country"
        })
    khc_multi_country:boolean;
        

   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurMatikKnowhowCalendars,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khc_city'})
    khcCity:gur_all_cities | null;


    @Column("time",{ 
        nullable:true,
        name:"khc_time"
        })
    khc_time:string | null;
        

    @Column("bigint",{ 
        nullable:true,
        name:"khc_series"
        })
    khc_series:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khc_createdby"
        })
    khc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khc_modifiedby"
        })
    khc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khc_modifiedtime"
        })
    khc_modifiedtime:Date;
        
}
