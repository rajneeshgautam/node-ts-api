import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_sports_official_user_profile_map} from "./gur_sports_official_user_profile_map";


@Entity("gur_sports_official_types" ,{schema:"sportsmatik_local" } )
@Index("sot_name",["sot_name",],{unique:true})
@Index("sot_createdby_user",["sotCreatedbyUser",])
@Index("sot_createdby_admin",["sotCreatedbyAdmin",])
export class gur_sports_official_types {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sot_id"
        })
    sot_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"sot_name"
        })
    sot_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsOfficialTypess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sot_createdby_user'})
    sotCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurSportsOfficialTypess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sot_createdby_admin'})
    sotCreatedbyAdmin:gur_admin_users | null;


    @Column("text",{ 
        nullable:true,
        name:"sot_desc"
        })
    sot_desc:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'2'",
        name:"sot_status"
        })
    sot_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sot_authentic"
        })
    sot_authentic:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sot_createdby"
        })
    sot_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sot_modifiedby"
        })
    sot_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sot_modifiedtime"
        })
    sot_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_sports_official_user_profile_map, (gur_sports_official_user_profile_map: gur_sports_official_user_profile_map)=>gur_sports_official_user_profile_map.spmSot,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialUserProfileMaps:gur_sports_official_user_profile_map[];
    
}
