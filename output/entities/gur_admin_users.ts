import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_admin_users_rights} from "./gur_admin_users_rights";
import {gur_brands} from "./gur_brands";
import {gur_competition_sports_map} from "./gur_competition_sports_map";
import {gur_competitions} from "./gur_competitions";
import {gur_educational_courses} from "./gur_educational_courses";
import {gur_educational_courses_specialization} from "./gur_educational_courses_specialization";
import {gur_educational_institutions} from "./gur_educational_institutions";
import {gur_institute_branches} from "./gur_institute_branches";
import {gur_institute_course_and_fees} from "./gur_institute_course_and_fees";
import {gur_job} from "./gur_job";
import {gur_law_ca_services_categories} from "./gur_law_ca_services_categories";
import {gur_plan_change_log} from "./gur_plan_change_log";
import {gur_sports_attributes} from "./gur_sports_attributes";
import {gur_sports_official_types} from "./gur_sports_official_types";
import {gur_support_ticket} from "./gur_support_ticket";
import {gur_support_ticket_comments} from "./gur_support_ticket_comments";
import {gur_teams} from "./gur_teams";


@Entity("gur_admin_users" ,{schema:"sportsmatik_local" } )
@Index("adm_email",["adm_email",],{unique:true})
@Index("adm_loginid",["adm_loginid",],{unique:true})
@Index("adm_mobile1",["adm_mobile1",],{unique:true})
@Index("adm_screen_name",["adm_screen_name",],{unique:true})
@Index("adm_mobile2",["adm_mobile2",],{unique:true})
@Index("adm_name",["adm_name",])
@Index("adm_status",["adm_status",])
export class gur_admin_users {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"adm_id"
        })
    adm_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"adm_name"
        })
    adm_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"adm_email"
        })
    adm_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"adm_loginid"
        })
    adm_loginid:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"adm_password"
        })
    adm_password:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"adm_screen_name"
        })
    adm_screen_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"adm_departments"
        })
    adm_departments:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:20,
        name:"adm_mobile1"
        })
    adm_mobile1:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:20,
        name:"adm_mobile2"
        })
    adm_mobile2:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adm_status"
        })
    adm_status:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"adm_photo"
        })
    adm_photo:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"adm_createdby"
        })
    adm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"adm_modifiedby"
        })
    adm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"adm_modifiedtime"
        })
    adm_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_admin_users_rights, (gur_admin_users_rights: gur_admin_users_rights)=>gur_admin_users_rights.adrUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAdminUsersRightss:gur_admin_users_rights[];
    

   
    @OneToMany(()=>gur_brands, (gur_brands: gur_brands)=>gur_brands.brdCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBrandss:gur_brands[];
    

   
    @OneToMany(()=>gur_competition_sports_map, (gur_competition_sports_map: gur_competition_sports_map)=>gur_competition_sports_map.csmCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionSportsMaps:gur_competition_sports_map[];
    

   
    @OneToMany(()=>gur_competitions, (gur_competitions: gur_competitions)=>gur_competitions.comCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionss:gur_competitions[];
    

   
    @OneToMany(()=>gur_educational_courses, (gur_educational_courses: gur_educational_courses)=>gur_educational_courses.corCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEducationalCoursess:gur_educational_courses[];
    

   
    @OneToMany(()=>gur_educational_courses_specialization, (gur_educational_courses_specialization: gur_educational_courses_specialization)=>gur_educational_courses_specialization.cspCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEducationalCoursesSpecializations:gur_educational_courses_specialization[];
    

   
    @OneToMany(()=>gur_educational_institutions, (gur_educational_institutions: gur_educational_institutions)=>gur_educational_institutions.insCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEducationalInstitutionss:gur_educational_institutions[];
    

   
    @OneToMany(()=>gur_institute_branches, (gur_institute_branches: gur_institute_branches)=>gur_institute_branches.ibrCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteBranchess:gur_institute_branches[];
    

   
    @OneToMany(()=>gur_institute_course_and_fees, (gur_institute_course_and_fees: gur_institute_course_and_fees)=>gur_institute_course_and_fees.icfCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteCourseAndFeess:gur_institute_course_and_fees[];
    

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobCreatedBy,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobs:gur_job[];
    

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobUpdatedBy,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobs2:gur_job[];
    

   
    @OneToMany(()=>gur_law_ca_services_categories, (gur_law_ca_services_categories: gur_law_ca_services_categories)=>gur_law_ca_services_categories.lcsCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawCaServicesCategoriess:gur_law_ca_services_categories[];
    

   
    @OneToMany(()=>gur_law_ca_services_categories, (gur_law_ca_services_categories: gur_law_ca_services_categories)=>gur_law_ca_services_categories.lcsCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawCaServicesCategoriess2:gur_law_ca_services_categories[];
    

   
    @OneToMany(()=>gur_plan_change_log, (gur_plan_change_log: gur_plan_change_log)=>gur_plan_change_log.pclAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanChangeLogs:gur_plan_change_log[];
    

   
    @OneToMany(()=>gur_sports_attributes, (gur_sports_attributes: gur_sports_attributes)=>gur_sports_attributes.sabCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsAttributess:gur_sports_attributes[];
    

   
    @OneToMany(()=>gur_sports_official_types, (gur_sports_official_types: gur_sports_official_types)=>gur_sports_official_types.sotCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialTypess:gur_sports_official_types[];
    

   
    @OneToMany(()=>gur_support_ticket, (gur_support_ticket: gur_support_ticket)=>gur_support_ticket.stkAdm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTickets:gur_support_ticket[];
    

   
    @OneToMany(()=>gur_support_ticket_comments, (gur_support_ticket_comments: gur_support_ticket_comments)=>gur_support_ticket_comments.strAdm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTicketCommentss:gur_support_ticket_comments[];
    

   
    @OneToMany(()=>gur_teams, (gur_teams: gur_teams)=>gur_teams.tmsCreatedbyAdmin,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTeamss:gur_teams[];
    
}
