import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_advertiser_ads_hits} from "./gur_advertiser_ads_hits";


@Entity("gur_advertiser_ads_placements" ,{schema:"sportsmatik_local" } )
export class gur_advertiser_ads_placements {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"apl_id"
        })
    apl_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"apl_value"
        })
    apl_value:string;
        

   
    @OneToMany(()=>gur_advertiser_ads_hits, (gur_advertiser_ads_hits: gur_advertiser_ads_hits)=>gur_advertiser_ads_hits.adhApl,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAdvertiserAdsHitss:gur_advertiser_ads_hits[];
    
}
