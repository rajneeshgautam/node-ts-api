import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_bobsleigh_profile" ,{schema:"sportsmatik_local" } )
@Index("bob_usr_id",["bobUsr",])
@Index("bob_createdby_user",["bobCreatedbyUser",])
export class gur_user_bobsleigh_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"bob_id"
        })
    bob_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBobsleighProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bob_usr_id'})
    bobUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bob_modifiedtime"
        })
    bob_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBobsleighProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bob_createdby_user'})
    bobCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bob_modifiedby"
        })
    bob_modifiedby:string | null;
        
}
