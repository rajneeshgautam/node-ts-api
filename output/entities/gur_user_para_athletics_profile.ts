import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_athletics_profile" ,{schema:"sportsmatik_local" } )
@Index("uap_usr_id",["uapUsr",])
@Index("uap_disability_class",["uapDisabilityClass",])
@Index("uap_createdby_user",["uapCreatedbyUser",])
export class gur_user_para_athletics_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"uap_id"
        })
    uap_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaAthleticsProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uap_usr_id'})
    uapUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaAthleticsProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uap_disability_class'})
    uapDisabilityClass:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_sprint"
        })
    uap_sprint:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_middle_running"
        })
    uap_middle_running:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uap_long_running"
        })
    uap_long_running:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_road_running"
        })
    uap_road_running:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_high_jump"
        })
    uap_high_jump:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_long_jump"
        })
    uap_long_jump:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_triple_jump"
        })
    uap_triple_jump:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_shot_put"
        })
    uap_shot_put:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_discus_throw"
        })
    uap_discus_throw:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_club"
        })
    uap_club:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_javelin_throw"
        })
    uap_javelin_throw:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uap_pentathlon"
        })
    uap_pentathlon:boolean | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uap_modifiedtime"
        })
    uap_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaAthleticsProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uap_createdby_user'})
    uapCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"uap_modifiedby"
        })
    uap_modifiedby:string | null;
        
}
