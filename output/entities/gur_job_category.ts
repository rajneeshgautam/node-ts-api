import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_job} from "./gur_job";
import {gur_job_profile} from "./gur_job_profile";


@Entity("gur_job_category" ,{schema:"sportsmatik_local" } )
@Index("job_slug",["joc_slug",],{unique:true})
export class gur_job_category {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"joc_id"
        })
    joc_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"joc_title"
        })
    joc_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"joc_slug"
        })
    joc_slug:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"joc_icon_class"
        })
    joc_icon_class:string | null;
        

    @Column("tinytext",{ 
        nullable:true,
        name:"joc_description"
        })
    joc_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"joc_is_active"
        })
    joc_is_active:boolean;
        

    @Column("datetime",{ 
        nullable:false,
        name:"joc_created_at"
        })
    joc_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"joc_updated_at"
        })
    joc_updated_at:Date | null;
        

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobJoc,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    gurJobs:gur_job[];
    

   
    @OneToMany(()=>gur_job_profile, (gur_job_profile: gur_job_profile)=>gur_job_profile.jopJoc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobProfiles:gur_job_profile[];
    
}
