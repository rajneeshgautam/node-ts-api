import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_community_answer} from "./gur_community_answer";
import {gur_community_follow} from "./gur_community_follow";
import {gur_community_question_like} from "./gur_community_question_like";
import {gur_community_question_view} from "./gur_community_question_view";


@Entity("gur_community_question" ,{schema:"sportsmatik_local" } )
@Index("com_slug",["com_slug",],{unique:true})
@Index("com_uty_id",["comUty",])
export class gur_community_question {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"com_id"
        })
    com_id:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"com_question_type"
        })
    com_question_type:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"com_objective_type"
        })
    com_objective_type:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:5,
        name:"com_duration"
        })
    com_duration:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"com_start_date"
        })
    com_start_date:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"com_end_date"
        })
    com_end_date:Date | null;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:260,
        name:"com_slug"
        })
    com_slug:string;
        

    @Column("mediumtext",{ 
        nullable:false,
        name:"com_description"
        })
    com_description:string;
        

    @Column("int",{ 
        nullable:false,
        name:"com_user_id"
        })
    com_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCommunityQuestions,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'com_uty_id'})
    comUty:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"com_is_solved"
        })
    com_is_solved:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"com_is_reported"
        })
    com_is_reported:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"com_is_featured"
        })
    com_is_featured:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"com_status"
        })
    com_status:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"com_is_active"
        })
    com_is_active:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"com_admin_review"
        })
    com_admin_review:boolean;
        

    @Column("datetime",{ 
        nullable:false,
        name:"com_created_at"
        })
    com_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"com_updated_at"
        })
    com_updated_at:Date | null;
        

   
    @OneToMany(()=>gur_community_answer, (gur_community_answer: gur_community_answer)=>gur_community_answer.cmaCom,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityAnswers:gur_community_answer[];
    

   
    @OneToMany(()=>gur_community_follow, (gur_community_follow: gur_community_follow)=>gur_community_follow.cfoCom,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityFollows:gur_community_follow[];
    

   
    @OneToMany(()=>gur_community_question_like, (gur_community_question_like: gur_community_question_like)=>gur_community_question_like.cqlCom,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityQuestionLikes:gur_community_question_like[];
    

   
    @OneToMany(()=>gur_community_question_view, (gur_community_question_view: gur_community_question_view)=>gur_community_question_view.cvuCom,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityQuestionViews:gur_community_question_view[];
    
}
