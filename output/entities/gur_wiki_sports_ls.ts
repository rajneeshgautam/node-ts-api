import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_sports_ls" ,{schema:"sportsmatik_local" } )
@Index("uniq_name",["splLngCode","spl_name",],{unique:true})
@Index("spl_spo_id",["splSpo",])
export class gur_wiki_sports_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"spl_id"
        })
    spl_id:number;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiSportsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'spl_spo_id'})
    splSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:true,
        name:"spl_name"
        })
    spl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"spl_desc"
        })
    spl_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spl_meta_title"
        })
    spl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spl_meta_desc"
        })
    spl_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiSportsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'spl_lng_code'})
    splLngCode:gur_languages | null;

}
