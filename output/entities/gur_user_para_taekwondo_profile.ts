import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_taekwondo_profile" ,{schema:"sportsmatik_local" } )
@Index("ptk_usr_id",["ptkUsr",])
@Index("ptk_weight_event",["ptkWeightEvent",])
@Index("ptk_disablity_class",["ptkDisablityClass",])
@Index("ptk_createdby_user",["ptkCreatedbyUser",])
export class gur_user_para_taekwondo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ptk_id"
        })
    ptk_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaTaekwondoProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptk_usr_id'})
    ptkUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaTaekwondoProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptk_weight_event'})
    ptkWeightEvent:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaTaekwondoProfiles2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptk_disablity_class'})
    ptkDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ptk_modifiedtime"
        })
    ptk_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaTaekwondoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptk_createdby_user'})
    ptkCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ptk_modifiedby"
        })
    ptk_modifiedby:string | null;
        
}
