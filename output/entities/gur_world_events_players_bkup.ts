import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events_sports_categories} from "./gur_world_events_sports_categories";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_value_list} from "./gur_value_list";
import {gur_world_events_players_ls_bakup} from "./gur_world_events_players_ls_bakup";


@Entity("gur_world_events_players_bkup" ,{schema:"sportsmatik_local" } )
@Index("wep_cit_id",["wepBirthCity",])
@Index("wep_format",["wep_format",])
@Index("wep_spo_id",["wep_sports",])
@Index("wep_team",["wep_team",])
@Index("gur_world_events_players_bkup_ibfk_10",["wepBirthCountry",])
@Index("gur_world_events_players_bkup_ibfk_13",["wepGrade",])
@Index("gur_world_events_players_bkup_ibfk_14",["wepBdayCat",])
@Index("gur_world_events_players_bkup_ibfk_4",["wepCat",])
@Index("gur_world_events_players_bkup_ibfk_5",["wepCnt",])
@Index("gur_world_events_players_bkup_ibfk_6",["wepHeroCat",])
@Index("gur_world_events_players_bkup_ibfk_9",["wepPlayerCat",])
export class gur_world_events_players_bkup {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wep_id"
        })
    wep_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_name"
        })
    wep_name:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"wep_dob"
        })
    wep_dob:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","o","x"],
        name:"wep_gender"
        })
    wep_gender:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_image"
        })
    wep_image:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wep_image_data"
        })
    wep_image_data:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_sports"
        })
    wep_sports:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_profile"
        })
    wep_profile:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wep_profile_duration"
        })
    wep_profile_duration:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wep_career_start"
        })
    wep_career_start:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wep_career_end"
        })
    wep_career_end:number | null;
        

   
    @ManyToOne(()=>gur_world_events_sports_categories, (gur_world_events_sports_categories: gur_world_events_sports_categories)=>gur_world_events_sports_categories.gurWorldEventsPlayersBkups,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_cat_id'})
    wepCat:gur_world_events_sports_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_is_player"
        })
    wep_is_player:boolean;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWorldEventsPlayersBkups3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_player_cat'})
    wepPlayerCat:gur_matik_knowhow_tab_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_is_hero"
        })
    wep_is_hero:boolean;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWorldEventsPlayersBkups2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_hero_cat'})
    wepHeroCat:gur_matik_knowhow_tab_categories | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWorldEventsPlayersBkups2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_cnt_id'})
    wepCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurWorldEventsPlayersBkups,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_birth_city'})
    wepBirthCity:gur_all_cities | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWorldEventsPlayersBkups,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_birth_country'})
    wepBirthCountry:gur_countries | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wep_createdby"
        })
    wep_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_modifiedby"
        })
    wep_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wep_modifiedtime"
        })
    wep_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_format"
        })
    wep_format:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_team"
        })
    wep_team:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurWorldEventsPlayersBkups,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_grade'})
    wepGrade:gur_value_list | null;


    @Column("text",{ 
        nullable:true,
        name:"wep_social_links"
        })
    wep_social_links:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_is_birthday"
        })
    wep_is_birthday:boolean;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWorldEventsPlayersBkups,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_bday_cat'})
    wepBdayCat:gur_matik_knowhow_tab_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_only_bday_knowhow"
        })
    wep_only_bday_knowhow:boolean;
        

   
    @OneToMany(()=>gur_world_events_players_ls_bakup, (gur_world_events_players_ls_bakup: gur_world_events_players_ls_bakup)=>gur_world_events_players_ls_bakup.wplWep,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersLsBakups:gur_world_events_players_ls_bakup[];
    
}
