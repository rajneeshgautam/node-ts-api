import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_knowledge_base_categories} from "./gur_knowledge_base_categories";


@Entity("gur_knowledge_base_categories_ls" ,{schema:"sportsmatik_local" } )
@Index("kbl_kbc_id",["kblKbc",])
export class gur_knowledge_base_categories_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kbl_id"
        })
    kbl_id:number;
        

   
    @ManyToOne(()=>gur_knowledge_base_categories, (gur_knowledge_base_categories: gur_knowledge_base_categories)=>gur_knowledge_base_categories.gurKnowledgeBaseCategoriesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbl_kbc_id'})
    kblKbc:gur_knowledge_base_categories | null;


    @Column("varchar",{ 
        nullable:false,
        name:"kbl_title"
        })
    kbl_title:string;
        

    @Column("mediumtext",{ 
        nullable:true,
        name:"kbl_description"
        })
    kbl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"kbl_lng_code"
        })
    kbl_lng_code:string;
        
}
