import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_matik_knowhow_buzzwords} from "./gur_matik_knowhow_buzzwords";
import {gur_matik_knowhow_fun_facts} from "./gur_matik_knowhow_fun_facts";
import {gur_matik_knowhow_gallery} from "./gur_matik_knowhow_gallery";
import {gur_matik_knowhow_history} from "./gur_matik_knowhow_history";
import {gur_matik_knowhow_history_timeline} from "./gur_matik_knowhow_history_timeline";
import {gur_matik_knowhow_ls} from "./gur_matik_knowhow_ls";
import {gur_matik_knowhow_rules} from "./gur_matik_knowhow_rules";
import {gur_matik_knowhow_variants} from "./gur_matik_knowhow_variants";


@Entity("gur_matik_knowhow" ,{schema:"sportsmatik_local" } )
@Index("mkh_spo_id",["mkhSpo",],{unique:true})
export class gur_matik_knowhow {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mkh_id"
        })
    mkh_id:string;
        

   
    @OneToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhow,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mkh_spo_id'})
    mkhSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"mkh_sort_order"
        })
    mkh_sort_order:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"mkh_photo_data"
        })
    mkh_photo_data:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mkh_meta_title"
        })
    mkh_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"mkh_meta_description"
        })
    mkh_meta_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mkh_createdby"
        })
    mkh_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"mkh_modifiedby"
        })
    mkh_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mkh_modifiedtime"
        })
    mkh_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_buzzwords, (gur_matik_knowhow_buzzwords: gur_matik_knowhow_buzzwords)=>gur_matik_knowhow_buzzwords.khbMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowBuzzwordss:gur_matik_knowhow_buzzwords[];
    

   
    @OneToMany(()=>gur_matik_knowhow_fun_facts, (gur_matik_knowhow_fun_facts: gur_matik_knowhow_fun_facts)=>gur_matik_knowhow_fun_facts.khfMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowFunFactss:gur_matik_knowhow_fun_facts[];
    

   
    @OneToMany(()=>gur_matik_knowhow_gallery, (gur_matik_knowhow_gallery: gur_matik_knowhow_gallery)=>gur_matik_knowhow_gallery.khgMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowGallerys:gur_matik_knowhow_gallery[];
    

   
    @OneToMany(()=>gur_matik_knowhow_history, (gur_matik_knowhow_history: gur_matik_knowhow_history)=>gur_matik_knowhow_history.khhMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowHistorys:gur_matik_knowhow_history[];
    

   
    @OneToMany(()=>gur_matik_knowhow_history_timeline, (gur_matik_knowhow_history_timeline: gur_matik_knowhow_history_timeline)=>gur_matik_knowhow_history_timeline.khtMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowHistoryTimelines:gur_matik_knowhow_history_timeline[];
    

   
    @OneToMany(()=>gur_matik_knowhow_ls, (gur_matik_knowhow_ls: gur_matik_knowhow_ls)=>gur_matik_knowhow_ls.khlMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowLss:gur_matik_knowhow_ls[];
    

   
    @OneToMany(()=>gur_matik_knowhow_rules, (gur_matik_knowhow_rules: gur_matik_knowhow_rules)=>gur_matik_knowhow_rules.khrMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowRuless:gur_matik_knowhow_rules[];
    

   
    @OneToMany(()=>gur_matik_knowhow_variants, (gur_matik_knowhow_variants: gur_matik_knowhow_variants)=>gur_matik_knowhow_variants.khvMkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowVariantss:gur_matik_knowhow_variants[];
    
}
