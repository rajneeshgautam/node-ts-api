import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_featured_matik_users" ,{schema:"sportsmatik_local" } )
@Index("fmu_user_type",["fmuUserType",])
export class gur_featured_matik_users {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fmu_id"
        })
    fmu_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"fmu_user_id"
        })
    fmu_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurFeaturedMatikUserss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fmu_user_type'})
    fmuUserType:gur_user_types | null;


    @Column("date",{ 
        nullable:false,
        name:"fmu_from_date"
        })
    fmu_from_date:string;
        

    @Column("date",{ 
        nullable:false,
        name:"fmu_to_date"
        })
    fmu_to_date:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fmu_createdby"
        })
    fmu_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fmu_modifiedby"
        })
    fmu_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fmu_modifiedtime"
        })
    fmu_modifiedtime:Date;
        
}
