import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_sports_event_results} from "./gur_sports_event_results";


@Entity("gur_sports_event_schedule" ,{schema:"sportsmatik_local" } )
@Index("evs_evt_id",["evs_evt_id",])
@Index("gur_sports_event_schedule_ibfk_2",["evsSpo",])
@Index("gur_sports_event_schedule_ibfk_3",["evsCnt",])
@Index("gur_sports_event_schedule_ibfk_4",["evsCit",])
@Index("gur_sports_event_schedule_ibfk_5",["evsVenue",])
export class gur_sports_event_schedule {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"evs_id"
        })
    evs_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evs_title"
        })
    evs_title:string | null;
        

    @Column("int",{ 
        nullable:false,
        name:"evs_evt_id"
        })
    evs_evt_id:number;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurSportsEventSchedules,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_spo_id'})
    evsSpo:gur_wiki_sports | null;


    @Column("datetime",{ 
        nullable:true,
        name:"evs_start"
        })
    evs_start:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"evs_end"
        })
    evs_end:Date | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"evs_show_time"
        })
    evs_show_time:boolean | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["countries","custom","domestic"],
        name:"evs_type"
        })
    evs_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"evs_cnt_participating"
        })
    evs_cnt_participating:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"evs_other_participating"
        })
    evs_other_participating:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"evs_createdby"
        })
    evs_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evs_modifiedby"
        })
    evs_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"evs_modifiedtime"
        })
    evs_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evs_address"
        })
    evs_address:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsEventSchedules,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_cnt_id'})
    evsCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurSportsEventSchedules,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_cit_id'})
    evsCit:gur_all_cities | null;


   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurSportsEventSchedules,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_venue'})
    evsVenue:gur_wiki_venues | null;


   
    @OneToMany(()=>gur_sports_event_results, (gur_sports_event_results: gur_sports_event_results)=>gur_sports_event_results.evrEvs,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsEventResultss:gur_sports_event_results[];
    
}
