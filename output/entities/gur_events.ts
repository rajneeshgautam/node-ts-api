import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_events_media} from "./gur_events_media";


@Entity("gur_events" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["evtUty",])
@Index("evt_createdby_user",["evtCreatedbyUser",])
export class gur_events {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"evt_id"
        })
    evt_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurEventss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evt_uty_id'})
    evtUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"evt_user_id"
        })
    evt_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"evt_name"
        })
    evt_name:string;
        

    @Column("datetime",{ 
        nullable:false,
        name:"evt_date_start"
        })
    evt_date_start:Date;
        

    @Column("datetime",{ 
        nullable:false,
        name:"evt_date_end"
        })
    evt_date_end:Date;
        

    @Column("text",{ 
        nullable:true,
        name:"evt_desc"
        })
    evt_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_photo"
        })
    evt_photo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_web_url"
        })
    evt_web_url:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"evt_status"
        })
    evt_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"evt_admin_review"
        })
    evt_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurEventss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evt_createdby_user'})
    evtCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"evt_createdby"
        })
    evt_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"evt_modifiedby"
        })
    evt_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"evt_modifiedtime"
        })
    evt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_events_media, (gur_events_media: gur_events_media)=>gur_events_media.evmEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEventsMedias:gur_events_media[];
    
}
