import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_paragliding_profile" ,{schema:"sportsmatik_local" } )
@Index("pgl_usr_id",["pglUsr",])
@Index("pgl_createdby_user",["pglCreatedbyUser",])
export class gur_user_paragliding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pgl_id"
        })
    pgl_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaglidingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgl_usr_id'})
    pglUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"pgl_discipline"
        })
    pgl_discipline:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pgl_modifiedtime"
        })
    pgl_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaglidingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgl_createdby_user'})
    pglCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pgl_modifiedby"
        })
    pgl_modifiedby:string | null;
        
}
