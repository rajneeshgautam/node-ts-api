import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_gallery} from "./gur_matik_knowhow_gallery";
import {gur_all_cities} from "./gur_all_cities";
import {gur_countries} from "./gur_countries";
import {gur_world_events} from "./gur_world_events";


@Entity("gur_matik_knowhow_gallery_media" ,{schema:"sportsmatik_local" } )
@Index("khg_cit_id",["kgmCit",])
@Index("khg_cnt_id",["kgmCnt",])
@Index("khg_evt_id",["kgmEvt",])
@Index("khg_kha_id",["kgmKhg",])
export class gur_matik_knowhow_gallery_media {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"kgm_id"
        })
    kgm_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_gallery, (gur_matik_knowhow_gallery: gur_matik_knowhow_gallery)=>gur_matik_knowhow_gallery.gurMatikKnowhowGalleryMedias,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kgm_khg_id'})
    kgmKhg:gur_matik_knowhow_gallery | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"kgm_sort_order"
        })
    kgm_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kgm_file"
        })
    kgm_file:string;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'image'",
        enum:["audio","video","image","url"],
        name:"kgm_type"
        })
    kgm_type:string;
        

    @Column("text",{ 
        nullable:true,
        name:"kgm_media_source_details"
        })
    kgm_media_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kgm_title"
        })
    kgm_title:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"kgm_date_day"
        })
    kgm_date_day:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"kgm_date_month"
        })
    kgm_date_month:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"kgm_date_year"
        })
    kgm_date_year:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kgm_place"
        })
    kgm_place:string | null;
        

   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurMatikKnowhowGalleryMedias,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kgm_cit_id'})
    kgmCit:gur_all_cities | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowGalleryMedias,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kgm_cnt_id'})
    kgmCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_world_events, (gur_world_events: gur_world_events)=>gur_world_events.gurMatikKnowhowGalleryMedias,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kgm_evt_id'})
    kgmEvt:gur_world_events | null;


    @Column("text",{ 
        nullable:true,
        name:"kgm_description"
        })
    kgm_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kgm_createdby"
        })
    kgm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kgm_modifiedby"
        })
    kgm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kgm_modifiedtime"
        })
    kgm_modifiedtime:Date;
        
}
