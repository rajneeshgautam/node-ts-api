import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow_players_officials} from "./gur_wiki_knowhow_players_officials";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_knowhow_players_officials_ls" ,{schema:"sportsmatik_local" } )
@Index("ppl_pmp_id",["pplPmp",])
@Index("ppl_lng_code",["pplLngCode",])
export class gur_wiki_knowhow_players_officials_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ppl_id"
        })
    ppl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow_players_officials, (gur_wiki_knowhow_players_officials: gur_wiki_knowhow_players_officials)=>gur_wiki_knowhow_players_officials.gurWikiKnowhowPlayersOfficialsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_pmp_id'})
    pplPmp:gur_wiki_knowhow_players_officials | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ppl_name"
        })
    ppl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ppl_desc"
        })
    ppl_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiKnowhowPlayersOfficialsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_lng_code'})
    pplLngCode:gur_languages | null;

}
