import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_professional_designations" ,{schema:"sportsmatik_local" } )
@Index("des_createdby_user",["des_createdby_user",])
export class gur_professional_designations {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"des_id"
        })
    des_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"des_name"
        })
    des_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"des_status"
        })
    des_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"des_authentic"
        })
    des_authentic:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"des_createdby"
        })
    des_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"des_modifiedby"
        })
    des_modifiedby:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"des_modifiedtime"
        })
    des_modifiedtime:Date | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"des_createdby_user"
        })
    des_createdby_user:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"des_craetedby_admin"
        })
    des_craetedby_admin:number | null;
        
}
