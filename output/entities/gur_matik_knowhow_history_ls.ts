import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_history} from "./gur_matik_knowhow_history";


@Entity("gur_matik_knowhow_history_ls" ,{schema:"sportsmatik_local" } )
@Index("khl_lng_code",["hhl_lng_code",])
@Index("khl_wkh_id",["hhlKhh",])
export class gur_matik_knowhow_history_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"hhl_id"
        })
    hhl_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_history, (gur_matik_knowhow_history: gur_matik_knowhow_history)=>gur_matik_knowhow_history.gurMatikKnowhowHistoryLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hhl_khh_id'})
    hhlKhh:gur_matik_knowhow_history | null;


    @Column("varchar",{ 
        nullable:true,
        name:"hhl_title"
        })
    hhl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"hhl_description"
        })
    hhl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"hhl_lng_code"
        })
    hhl_lng_code:string;
        
}
