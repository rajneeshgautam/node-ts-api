import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_advertiser_ads_hits} from "./gur_advertiser_ads_hits";


@Entity("gur_advertiser_ads" ,{schema:"sportsmatik_local" } )
export class gur_advertiser_ads {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ads_id"
        })
    ads_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ads_value"
        })
    ads_value:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"ads_redirect_to"
        })
    ads_redirect_to:string;
        

   
    @OneToMany(()=>gur_advertiser_ads_hits, (gur_advertiser_ads_hits: gur_advertiser_ads_hits)=>gur_advertiser_ads_hits.adhAds,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAdvertiserAdsHitss:gur_advertiser_ads_hits[];
    
}
