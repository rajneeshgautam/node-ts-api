import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_leads" ,{schema:"sportsmatik_local" } )
@Index("uls_usr_type",["uls_usr_type",])
export class gur_user_leads {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"uls_id"
        })
    uls_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uls_usr_type"
        })
    uls_usr_type:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"uls_name"
        })
    uls_name:string;
        

    @Column("enum",{ 
        nullable:true,
        enum:["p","a","l"],
        name:"uls_type"
        })
    uls_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_email"
        })
    uls_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_email"
        })
    uls_send_email:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"uls_send_email1_last_sent"
        })
    uls_send_email1_last_sent:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_email2"
        })
    uls_email2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_email3"
        })
    uls_email3:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_email4"
        })
    uls_email4:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_email5"
        })
    uls_email5:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_email2"
        })
    uls_send_email2:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_email3"
        })
    uls_send_email3:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_email4"
        })
    uls_send_email4:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_email5"
        })
    uls_send_email5:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uls_contact"
        })
    uls_contact:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_sms"
        })
    uls_send_sms:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uls_contact2"
        })
    uls_contact2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_send_sms2"
        })
    uls_send_sms2:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uls_contact3"
        })
    uls_contact3:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uls_contact4"
        })
    uls_contact4:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uls_contact5"
        })
    uls_contact5:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"uls_country"
        })
    uls_country:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"uls_state"
        })
    uls_state:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uls_city"
        })
    uls_city:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_address"
        })
    uls_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_address2"
        })
    uls_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"uls_pincode"
        })
    uls_pincode:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uls_remarks"
        })
    uls_remarks:string | null;
        

    @Column("int",{ 
        nullable:false,
        name:"uls_status"
        })
    uls_status:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_contact_name1"
        })
    uls_contact_name1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_contact_name2"
        })
    uls_contact_name2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_contact_name3"
        })
    uls_contact_name3:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_contact_name4"
        })
    uls_contact_name4:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_contact_name5"
        })
    uls_contact_name5:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uls_register_site"
        })
    uls_register_site:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_website_name"
        })
    uls_website_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uls_sport"
        })
    uls_sport:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uls_status_modifiedtime"
        })
    uls_status_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uls_createdby"
        })
    uls_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"uls_modifiedby"
        })
    uls_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uls_modifiedtime"
        })
    uls_modifiedtime:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"uls_send_email2_last_sent"
        })
    uls_send_email2_last_sent:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"uls_send_email3_last_sent"
        })
    uls_send_email3_last_sent:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"uls_send_email4_last_sent"
        })
    uls_send_email4_last_sent:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"uls_send_email5_last_sent"
        })
    uls_send_email5_last_sent:Date | null;
        
}
