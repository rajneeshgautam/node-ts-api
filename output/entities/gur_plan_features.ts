import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_plan_categories} from "./gur_plan_categories";
import {gur_plan_change_log} from "./gur_plan_change_log";
import {gur_plan_features_map} from "./gur_plan_features_map";


@Entity("gur_plan_features" ,{schema:"sportsmatik_local" } )
@Index("feature_code",["plf_code",],{unique:true})
@Index("feature_name",["plf_name",],{unique:true})
@Index("gur_plan_features_ibfk_1",["plfPlc",])
export class gur_plan_features {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"plf_id"
        })
    plf_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:30,
        name:"plf_code"
        })
    plf_code:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"plf_name"
        })
    plf_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"plf_status"
        })
    plf_status:boolean;
        

   
    @ManyToOne(()=>gur_plan_categories, (gur_plan_categories: gur_plan_categories)=>gur_plan_categories.gurPlanFeaturess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'plf_plc_id'})
    plfPlc:gur_plan_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plf_for_unlist_user"
        })
    plf_for_unlist_user:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plf_show_count"
        })
    plf_show_count:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plf_for_duration"
        })
    plf_for_duration:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"plf_duration"
        })
    plf_duration:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"plf_createdby"
        })
    plf_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"plf_modifiedby"
        })
    plf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"plf_modifiedtime"
        })
    plf_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_plan_change_log, (gur_plan_change_log: gur_plan_change_log)=>gur_plan_change_log.pclPlf,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanChangeLogs:gur_plan_change_log[];
    

   
    @OneToMany(()=>gur_plan_features_map, (gur_plan_features_map: gur_plan_features_map)=>gur_plan_features_map.pfmPlf,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanFeaturesMaps:gur_plan_features_map[];
    
}
