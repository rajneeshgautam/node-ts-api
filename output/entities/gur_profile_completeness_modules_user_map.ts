import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_profile_completeness_modules} from "./gur_profile_completeness_modules";


@Entity("gur_profile_completeness_modules_user_map" ,{schema:"sportsmatik_local" } )
@Index("pum_pcm_id",["pumPcm",])
@Index("pum_uty_id",["pumUty",])
export class gur_profile_completeness_modules_user_map {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pum_id"
        })
    pum_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurProfileCompletenessModulesUserMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pum_uty_id'})
    pumUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_profile_completeness_modules, (gur_profile_completeness_modules: gur_profile_completeness_modules)=>gur_profile_completeness_modules.gurProfileCompletenessModulesUserMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pum_pcm_id'})
    pumPcm:gur_profile_completeness_modules | null;


    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        name:"pum_sort_order"
        })
    pum_sort_order:number;
        
}
