import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz_question_mapping} from "./gur_quiz_question_mapping";
import {gur_quiz_question_option} from "./gur_quiz_question_option";
import {gur_quiz_result} from "./gur_quiz_result";
import {gur_quiz_result_detail} from "./gur_quiz_result_detail";


@Entity("gur_quiz_question_bank" ,{schema:"sportsmatik_local" } )
export class gur_quiz_question_bank {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"qbk_id"
        })
    qbk_id:string;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"qbk_difficulty_level"
        })
    qbk_difficulty_level:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["boolean","mcq","msq"],
        name:"qbk_type"
        })
    qbk_type:string;
        

    @Column("text",{ 
        nullable:false,
        name:"qbk_question"
        })
    qbk_question:string;
        

    @Column("text",{ 
        nullable:true,
        name:"qbk_question_hint"
        })
    qbk_question_hint:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"qbk_is_active"
        })
    qbk_is_active:boolean;
        

    @Column("datetime",{ 
        nullable:false,
        name:"qbk_created_at"
        })
    qbk_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"qbk_updated_at"
        })
    qbk_updated_at:Date | null;
        

    @Column("int",{ 
        nullable:false,
        name:"qbk_created_by"
        })
    qbk_created_by:number;
        

    @Column("int",{ 
        nullable:true,
        name:"qbk_updated_by"
        })
    qbk_updated_by:number | null;
        

   
    @OneToMany(()=>gur_quiz_question_mapping, (gur_quiz_question_mapping: gur_quiz_question_mapping)=>gur_quiz_question_mapping.qqmQbk,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizQuestionMappings:gur_quiz_question_mapping[];
    

   
    @OneToMany(()=>gur_quiz_question_option, (gur_quiz_question_option: gur_quiz_question_option)=>gur_quiz_question_option.qboQbk,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurQuizQuestionOptions:gur_quiz_question_option[];
    

   
    @OneToMany(()=>gur_quiz_result, (gur_quiz_result: gur_quiz_result)=>gur_quiz_result.qrsActiveQbk,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResults:gur_quiz_result[];
    

   
    @OneToMany(()=>gur_quiz_result_detail, (gur_quiz_result_detail: gur_quiz_result_detail)=>gur_quiz_result_detail.qrdQbk,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResultDetails:gur_quiz_result_detail[];
    
}
