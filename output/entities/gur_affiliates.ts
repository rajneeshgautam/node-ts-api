import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_affiliates" ,{schema:"sportsmatik_local" } )
@Index("ven_phone1",["aff_phone1",],{unique:true})
@Index("aff_code",["aff_code",],{unique:true})
@Index("ven_email",["aff_email",],{unique:true})
@Index("ven_phone2",["aff_phone2",],{unique:true})
@Index("ven_bussiness_name",["aff_company",])
@Index("ven_country",["aff_country",])
@Index("ven_state",["aff_state",])
@Index("ven_city",["aff_city",])
@Index("ven_fname",["aff_fname","aff_lname",])
export class gur_affiliates {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"aff_id"
        })
    aff_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"aff_fname"
        })
    aff_fname:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aff_mname"
        })
    aff_mname:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aff_lname"
        })
    aff_lname:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"aff_company"
        })
    aff_company:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"aff_password"
        })
    aff_password:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"aff_code"
        })
    aff_code:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"aff_email"
        })
    aff_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aff_email_verified"
        })
    aff_email_verified:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:25,
        name:"aff_phone1"
        })
    aff_phone1:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aff_phone1_verified"
        })
    aff_phone1_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"aff_phone2"
        })
    aff_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aff_phone2_verified"
        })
    aff_phone2_verified:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"aff_address"
        })
    aff_address:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"aff_country"
        })
    aff_country:number;
        

    @Column("int",{ 
        nullable:false,
        name:"aff_state"
        })
    aff_state:number;
        

    @Column("int",{ 
        nullable:false,
        name:"aff_city"
        })
    aff_city:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"aff_pincode"
        })
    aff_pincode:string;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Inactive'",
        enum:["Active","Inactive","Suspended"],
        name:"aff_status"
        })
    aff_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aff_profile_pic"
        })
    aff_profile_pic:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"aff_profile_summary"
        })
    aff_profile_summary:string | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"aff_profile_completeness"
        })
    aff_profile_completeness:number;
        

    @Column("datetime",{ 
        nullable:true,
        name:"aff_last_login_date"
        })
    aff_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"aff_last_login_ip"
        })
    aff_last_login_ip:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"aff_modifiedtime"
        })
    aff_modifiedtime:Date;
        
}
