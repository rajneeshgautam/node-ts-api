import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_lawyer_professional_details" ,{schema:"sportsmatik_local" } )
@Index("lpf_usr_id",["lpfUsr",])
export class gur_lawyer_professional_details {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"lpf_id"
        })
    lpf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurLawyerProfessionalDetailss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lpf_usr_id'})
    lpfUsr:gur_users | null;


    @Column("text",{ 
        nullable:false,
        name:"lpf_law_categories"
        })
    lpf_law_categories:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lpf_council"
        })
    lpf_council:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"lpf_reg_no"
        })
    lpf_reg_no:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"lpf_license_till"
        })
    lpf_license_till:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"lpf_status"
        })
    lpf_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lpf_admin_review"
        })
    lpf_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"lpf_createdby"
        })
    lpf_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lpf_modifiedby"
        })
    lpf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lpf_modified_time"
        })
    lpf_modified_time:Date;
        
}
