import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_sports_tech_product_services_media" ,{schema:"sportsmatik_local" } )
@Index("psm_stp_id",["psm_stp_id",])
export class gur_sports_tech_product_services_media {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"psm_id"
        })
    psm_id:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["photo","video","url"],
        name:"psm_type"
        })
    psm_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"psm_file"
        })
    psm_file:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"psm_stp_id"
        })
    psm_stp_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"psm_admin_review"
        })
    psm_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"psm_status"
        })
    psm_status:boolean;
        
}
