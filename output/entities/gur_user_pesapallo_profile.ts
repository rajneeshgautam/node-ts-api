import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_pesapallo_profile" ,{schema:"sportsmatik_local" } )
@Index("pes_usr_id",["pesUsr",])
@Index("pes_position",["pesPosition",])
@Index("pes_createdby_user",["pesCreatedbyUser",])
@Index("pes_position2",["pesPosition2",])
export class gur_user_pesapallo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pes_id"
        })
    pes_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPesapalloProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pes_usr_id'})
    pesUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserPesapalloProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pes_position1'})
    pesPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserPesapalloProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pes_position2'})
    pesPosition2:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pes_bat_hand"
        })
    pes_bat_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pes_modifiedtime"
        })
    pes_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPesapalloProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pes_createdby_user'})
    pesCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pes_modifiedby"
        })
    pes_modifiedby:string | null;
        
}
