import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_quiz_categories} from "./gur_matik_play_quiz_categories";


@Entity("gur_matik_play_quiz_questions_bank" ,{schema:"sportsmatik_local" } )
@Index("qqb_cat_id",["qqbCat",])
export class gur_matik_play_quiz_questions_bank {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"qqb_id"
        })
    qqb_id:number;
        

   
    @ManyToOne(()=>gur_matik_play_quiz_categories, (gur_matik_play_quiz_categories: gur_matik_play_quiz_categories)=>gur_matik_play_quiz_categories.gurMatikPlayQuizQuestionsBanks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'qqb_cat_id'})
    qqbCat:gur_matik_play_quiz_categories | null;


    @Column("text",{ 
        nullable:false,
        name:"qqb_question"
        })
    qqb_question:string;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        name:"qqb_question_opt_count"
        })
    qqb_question_opt_count:number;
        

    @Column("text",{ 
        nullable:false,
        name:"qtb_options"
        })
    qtb_options:string;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        name:"qqb_answer"
        })
    qqb_answer:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"qqb_sort_order"
        })
    qqb_sort_order:number;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"qqb_createdby"
        })
    qqb_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"qqb_modifiedby"
        })
    qqb_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"qqb_modifiedtime"
        })
    qqb_modifiedtime:Date;
        
}
