import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_swimming_profile" ,{schema:"sportsmatik_local" } )
@Index("psw_usr_id",["pswUsr",])
@Index("psw_disablity_class",["pswSDisablityClass",])
@Index("psw_createdby_user",["pswCreatedbyUser",])
@Index("psw_breaststroke_disablity_class",["pswSbDisablityClass",])
export class gur_user_para_swimming_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"psw_id"
        })
    psw_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSwimmingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psw_usr_id'})
    pswUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"psw_freestyle"
        })
    psw_freestyle:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaSwimmingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psw_s_disablity_class'})
    pswSDisablityClass:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"psw_backstroke"
        })
    psw_backstroke:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"psw_butterfly"
        })
    psw_butterfly:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"psw_breaststroke"
        })
    psw_breaststroke:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaSwimmingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psw_sb_disablity_class'})
    pswSbDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"psw_modifiedtime"
        })
    psw_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSwimmingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psw_createdby_user'})
    pswCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"psw_modifiedby"
        })
    psw_modifiedby:string | null;
        
}
