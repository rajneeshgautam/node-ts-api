import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_skydiving_profile" ,{schema:"sportsmatik_local" } )
@Index("sky_usr_id",["skyUsr",])
@Index("sky_createdby_user",["skyCreatedbyUser",])
export class gur_user_skydiving_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sky_id"
        })
    sky_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkydivingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sky_usr_id'})
    skyUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"sky_discipline"
        })
    sky_discipline:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sky_modifiedtime"
        })
    sky_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkydivingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sky_createdby_user'})
    skyCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sky_modifiedby"
        })
    sky_modifiedby:string | null;
        
}
