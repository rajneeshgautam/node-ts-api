import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_ultra_running_profile" ,{schema:"sportsmatik_local" } )
@Index("utr_usr_id",["utrUsr",])
@Index("utr_createdby_user",["utrCreatedbyUser",])
export class gur_user_ultra_running_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"utr_id"
        })
    utr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserUltraRunningProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'utr_usr_id'})
    utrUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"utr_modifiedtime"
        })
    utr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserUltraRunningProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'utr_createdby_user'})
    utrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"utr_modifiedby"
        })
    utr_modifiedby:string | null;
        
}
