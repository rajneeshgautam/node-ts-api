import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_academies} from "./gur_academies";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";


@Entity("gur_matik_knowhow_academies" ,{schema:"sportsmatik_local" } )
@Index("kac_spo_id",["kacSpo","kacAca",],{unique:true})
@Index("gur_matik_knowhow_academies_ibfk_1",["kacAca",])
@Index("gur_matik_knowhow_academies_ibfk_3",["kacKnc",])
export class gur_matik_knowhow_academies {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kac_id"
        })
    kac_id:number;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowAcademiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kac_spo_id'})
    kacSpo:gur_wiki_sports | null;


   
    @ManyToOne(()=>gur_academies, (gur_academies: gur_academies)=>gur_academies.gurMatikKnowhowAcademiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kac_aca_id'})
    kacAca:gur_academies | null;


   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowAcademiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kac_knc_id'})
    kacKnc:gur_matik_knowhow_tab_categories | null;


    @Column("enum",{ 
        nullable:false,
        enum:["player","umpire","coach"],
        name:"kac_type"
        })
    kac_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kac_createdby"
        })
    kac_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kac_modifiedby"
        })
    kac_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kac_modifiedtime"
        })
    kac_modifiedtime:Date;
        
}
