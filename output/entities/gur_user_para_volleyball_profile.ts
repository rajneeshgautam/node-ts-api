import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_volleyball_profile" ,{schema:"sportsmatik_local" } )
@Index("prw_usr_id",["prwUsr",])
@Index("prw_disablity_class",["prwDisablityClass",])
@Index("prw_createdby_user",["prwCreatedbyUser",])
export class gur_user_para_volleyball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"prw_id"
        })
    prw_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaVolleyballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prw_usr_id'})
    prwUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaVolleyballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prw_disablity_class'})
    prwDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"prw_modifiedtime"
        })
    prw_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaVolleyballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prw_createdby_user'})
    prwCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"prw_modifiedby"
        })
    prw_modifiedby:string | null;
        
}
