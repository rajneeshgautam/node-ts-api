import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_pricing_plans} from "./gur_pricing_plans";
import {gur_user_plans_check} from "./gur_user_plans_check";
import {gur_user_plans_check_not_used} from "./gur_user_plans_check_not_used";


@Entity("gur_pricing_plan_categories" ,{schema:"sportsmatik_local" } )
@Index("plc_name",["plc_name",],{unique:true})
@Index("plc_slug",["plc_slug",],{unique:true})
export class gur_pricing_plan_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"plc_id"
        })
    plc_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:100,
        name:"plc_slug"
        })
    plc_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"plc_name"
        })
    plc_name:string;
        

   
    @OneToMany(()=>gur_pricing_plans, (gur_pricing_plans: gur_pricing_plans)=>gur_pricing_plans.prpCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPricingPlanss:gur_pricing_plans[];
    

   
    @OneToMany(()=>gur_user_plans_check, (gur_user_plans_check: gur_user_plans_check)=>gur_user_plans_check.pchCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansChecks:gur_user_plans_check[];
    

   
    @OneToMany(()=>gur_user_plans_check_not_used, (gur_user_plans_check_not_used: gur_user_plans_check_not_used)=>gur_user_plans_check_not_used.pchCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansCheckNotUseds:gur_user_plans_check_not_used[];
    
}
