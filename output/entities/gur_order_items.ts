import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_plan_orders} from "./gur_plan_orders";
import {gur_plans} from "./gur_plans";
import {gur_plan_categories} from "./gur_plan_categories";
import {gur_plans_price} from "./gur_plans_price";
import {gur_coupons} from "./gur_coupons";


@Entity("gur_order_items" ,{schema:"sportsmatik_local" } )
@Index("gur_order_items_ibfk_2",["oriOrd",])
@Index("gur_order_items_ibfk_3",["oriCoupon",])
@Index("gur_order_items_ibfk_4",["oriPln",])
@Index("gur_order_items_ibfk_5",["oriPlc",])
@Index("gur_order_items_ibfk_6",["oriPlp",])
export class gur_order_items {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ori_id"
        })
    ori_id:number;
        

   
    @ManyToOne(()=>gur_plan_orders, (gur_plan_orders: gur_plan_orders)=>gur_plan_orders.gurOrderItemss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_ord_id'})
    oriOrd:gur_plan_orders | null;


    @Column("varchar",{ 
        nullable:false,
        name:"ori_name"
        })
    ori_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"ori_hsn"
        })
    ori_hsn:string | null;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ori_mrp"
        })
    ori_mrp:number;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ori_sale_price"
        })
    ori_sale_price:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"ori_qty"
        })
    ori_qty:number;
        

    @Column("date",{ 
        nullable:true,
        name:"ori_expiry_date"
        })
    ori_expiry_date:string | null;
        

   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurOrderItemss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_pln_id'})
    oriPln:gur_plans | null;


   
    @ManyToOne(()=>gur_plan_categories, (gur_plan_categories: gur_plan_categories)=>gur_plan_categories.gurOrderItemss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_plc_id'})
    oriPlc:gur_plan_categories | null;


   
    @ManyToOne(()=>gur_plans_price, (gur_plans_price: gur_plans_price)=>gur_plans_price.gurOrderItemss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_plp_id'})
    oriPlp:gur_plans_price | null;


    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"ori_coupon_code"
        })
    ori_coupon_code:string | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"ori_coupon_discount"
        })
    ori_coupon_discount:number | null;
        

   
    @ManyToOne(()=>gur_coupons, (gur_coupons: gur_coupons)=>gur_coupons.gurOrderItemss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_coupon_id'})
    oriCoupon:gur_coupons | null;

}
