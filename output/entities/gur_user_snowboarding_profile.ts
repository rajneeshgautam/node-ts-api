import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_snowboarding_profile" ,{schema:"sportsmatik_local" } )
@Index("sno_usr_id",["snoUsr",])
@Index("sno_createdby_user",["snoCreatedbyUser",])
export class gur_user_snowboarding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sno_id"
        })
    sno_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSnowboardingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sno_usr_id'})
    snoUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"sno_discipline"
        })
    sno_discipline:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sno_modifiedtime"
        })
    sno_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSnowboardingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sno_createdby_user'})
    snoCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sno_modifiedby"
        })
    sno_modifiedby:string | null;
        
}
