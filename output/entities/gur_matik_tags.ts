import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_matik_tags" ,{schema:"sportsmatik_local" } )
@Index("rat_rated_by_type",["mtgByType",])
@Index("mtg_for_type",["mtgForType",])
@Index("rat_rated_by",["mtgByUser",])
export class gur_matik_tags {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mtg_id"
        })
    mtg_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurMatikTagss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mtg_by_user'})
    mtgByUser:gur_users | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMatikTagss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mtg_by_type'})
    mtgByType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mtg_by"
        })
    mtg_by:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMatikTagss2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mtg_for_type'})
    mtgForType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mtg_for"
        })
    mtg_for:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mtg_comments"
        })
    mtg_comments:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"mtg_valid_till"
        })
    mtg_valid_till:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        default: () => "'-'",
        name:"mtg_modifiedby"
        })
    mtg_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mtg_modifiedtime"
        })
    mtg_modifiedtime:Date;
        
}
