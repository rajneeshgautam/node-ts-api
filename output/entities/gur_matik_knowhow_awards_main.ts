import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_matik_knowhow_awards_main_ls} from "./gur_matik_knowhow_awards_main_ls";
import {gur_matik_knowhow_awards_yearly} from "./gur_matik_knowhow_awards_yearly";


@Entity("gur_matik_knowhow_awards_main" ,{schema:"sportsmatik_local" } )
@Index("awm_slug",["awm_slug",],{unique:true})
@Index("khm_cnt_id",["awmCnt",])
export class gur_matik_knowhow_awards_main {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"awm_id"
        })
    awm_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"awm_slug"
        })
    awm_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"awm_type"
        })
    awm_type:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"awm_sports"
        })
    awm_sports:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"awm_multi_spo"
        })
    awm_multi_spo:boolean;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowAwardsMains,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'awm_cnt_id'})
    awmCnt:gur_countries | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"awm_for_country"
        })
    awm_for_country:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"awm_sponsored"
        })
    awm_sponsored:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awm_photo"
        })
    awm_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"awm_photo_data"
        })
    awm_photo_data:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"awm_timeline_image_data"
        })
    awm_timeline_image_data:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"awm_is_featured"
        })
    awm_is_featured:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"awm_award_level"
        })
    awm_award_level:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awm_temp_title"
        })
    awm_temp_title:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"awm_createdby"
        })
    awm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awm_modifiedby"
        })
    awm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"awm_modifiedtime"
        })
    awm_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_awards_main_ls, (gur_matik_knowhow_awards_main_ls: gur_matik_knowhow_awards_main_ls)=>gur_matik_knowhow_awards_main_ls.wmlAwm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsMainLss:gur_matik_knowhow_awards_main_ls[];
    

   
    @OneToMany(()=>gur_matik_knowhow_awards_yearly, (gur_matik_knowhow_awards_yearly: gur_matik_knowhow_awards_yearly)=>gur_matik_knowhow_awards_yearly.awyAwm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsYearlys:gur_matik_knowhow_awards_yearly[];
    
}
