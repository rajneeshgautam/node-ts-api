import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_world_events_teams} from "./gur_world_events_teams";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_wiki_competitions} from "./gur_wiki_competitions";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";


@Entity("gur_matik_knowhow_teams" ,{schema:"sportsmatik_local" } )
@Index("wep_cnt_id",["khtCnt",])
@Index("wep_spo_id",["khtSpo",])
@Index("wet_knc_id",["khtKnc",])
@Index("wet_wcm_id",["khtWcm",])
@Index("gur_matik_knowhow_teams_ibfk_1",["khtWet",])
export class gur_matik_knowhow_teams {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"kht_id"
        })
    kht_id:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"kht_is_country"
        })
    kht_is_country:boolean;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_cnt_id'})
    khtCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_world_events_teams, (gur_world_events_teams: gur_world_events_teams)=>gur_world_events_teams.gurMatikKnowhowTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_wet_id'})
    khtWet:gur_world_events_teams | null;


   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_spo_id'})
    khtSpo:gur_wiki_sports | null;


   
    @ManyToOne(()=>gur_wiki_competitions, (gur_wiki_competitions: gur_wiki_competitions)=>gur_wiki_competitions.gurMatikKnowhowTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_wcm_id'})
    khtWcm:gur_wiki_competitions | null;


   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_knc_id'})
    khtKnc:gur_matik_knowhow_tab_categories | null;


    @Column("enum",{ 
        nullable:true,
        enum:["m","f"],
        name:"kht_gender"
        })
    kht_gender:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"kht_match_count"
        })
    kht_match_count:number;
        

    @Column("int",{ 
        nullable:false,
        name:"kht_rank"
        })
    kht_rank:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        default: () => "'Rank'",
        name:"kht_rank_label"
        })
    kht_rank_label:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kht_first_match"
        })
    kht_first_match:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"kht_is_other"
        })
    kht_is_other:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kht_createdby"
        })
    kht_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kht_modifiedby"
        })
    kht_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kht_modifiedtime"
        })
    kht_modifiedtime:Date;
        
}
