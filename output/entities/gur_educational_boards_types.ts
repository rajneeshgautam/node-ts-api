import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_qualifications} from "./gur_user_qualifications";


@Entity("gur_educational_boards_types" ,{schema:"sportsmatik_local" } )
@Index("ebt_name",["ebt_name",],{unique:true})
export class gur_educational_boards_types {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ebt_id"
        })
    ebt_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"ebt_name"
        })
    ebt_name:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["all-india","state"],
        name:"ebt_type"
        })
    ebt_type:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ebt_status"
        })
    ebt_status:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ebt_createdby"
        })
    ebt_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"ebt_modifiedby"
        })
    ebt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ebt_modifiedtime"
        })
    ebt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfBoardName,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    
}
