import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_popular_sports} from "./gur_wiki_popular_sports";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_popular_sports_ls" ,{schema:"sportsmatik_local" } )
@Index("nsl_wns_id",["nslWns",])
@Index("nsl_lng_code",["nslLngCode",])
export class gur_wiki_popular_sports_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"nsl_id"
        })
    nsl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_popular_sports, (gur_wiki_popular_sports: gur_wiki_popular_sports)=>gur_wiki_popular_sports.gurWikiPopularSportsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'nsl_wns_id'})
    nslWns:gur_wiki_popular_sports | null;


    @Column("text",{ 
        nullable:true,
        name:"nsl_desc"
        })
    nsl_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"nsl_meta_title"
        })
    nsl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"nsl_meta_desc"
        })
    nsl_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiPopularSportsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'nsl_lng_code'})
    nslLngCode:gur_languages | null;

}
