import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_user_plans_check_not_used} from "./gur_user_plans_check_not_used";


@Entity("gur_subuser_usage_history" ,{schema:"sportsmatik_local" } )
@Index("rat_rated_by",["suh_user_id",])
@Index("rat_rated_by_type",["suhUsrType",])
@Index("mtg_for_type",["suhPch",])
@Index("suh_sport_id",["suhSubuser",])
export class gur_subuser_usage_history {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"suh_id"
        })
    suh_id:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"suh_user_id"
        })
    suh_user_id:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSubuserUsageHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'suh_usr_type'})
    suhUsrType:gur_user_types | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSubuserUsageHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'suh_subuser_id'})
    suhSubuser:gur_users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"suh_subuser_type"
        })
    suh_subuser_type:boolean;
        

   
    @ManyToOne(()=>gur_user_plans_check_not_used, (gur_user_plans_check_not_used: gur_user_plans_check_not_used)=>gur_user_plans_check_not_used.gurSubuserUsageHistorys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'suh_pch_id'})
    suhPch:gur_user_plans_check_not_used | null;


    @Column("date",{ 
        nullable:true,
        name:"suh_created_on"
        })
    suh_created_on:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"suh_createdby"
        })
    suh_createdby:string | null;
        
}
