import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_para_shooting_profile" ,{schema:"sportsmatik_local" } )
@Index("psh_usr_id",["pshUsr",])
@Index("psh_disablity_class",["psh_disablity_class",])
@Index("psh_createdby_user",["pshCreatedbyUser",])
export class gur_user_para_shooting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"psh_id"
        })
    psh_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaShootingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psh_usr_id'})
    pshUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"psh_master_eye"
        })
    psh_master_eye:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"psh_disablity_class"
        })
    psh_disablity_class:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"psh_events"
        })
    psh_events:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"psh_modifiedtime"
        })
    psh_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaShootingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psh_createdby_user'})
    pshCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"psh_modifiedby"
        })
    psh_modifiedby:string | null;
        
}
