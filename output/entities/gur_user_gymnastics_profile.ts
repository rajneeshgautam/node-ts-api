import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_gymnastics_profile" ,{schema:"sportsmatik_local" } )
@Index("gym_usr_id",["gymUsr",])
@Index("gym_createdby_user",["gymCreatedbyUser",])
export class gur_user_gymnastics_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"gym_id"
        })
    gym_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGymnasticsProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gym_usr_id'})
    gymUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"gym_artistics"
        })
    gym_artistics:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"gym_rhythmic"
        })
    gym_rhythmic:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"gym_trampoline"
        })
    gym_trampoline:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"gym_acrobatics"
        })
    gym_acrobatics:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"gym_aerobics"
        })
    gym_aerobics:boolean | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"gym_modifiedtime"
        })
    gym_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGymnasticsProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gym_createdby_user'})
    gymCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"gym_modifiedby"
        })
    gym_modifiedby:string | null;
        
}
