import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_motogp_profile" ,{schema:"sportsmatik_local" } )
@Index("mgp_usr_id",["mgpUsr",])
@Index("mgp_createdby_user",["mgpCreatedbyUser",])
export class gur_user_motogp_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"mgp_id"
        })
    mgp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserMotogpProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mgp_usr_id'})
    mgpUsr:gur_users | null;


    @Column("year",{ 
        nullable:true,
        name:"mgp_debut_date"
        })
    mgp_debut_date:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mgp_debut_competition"
        })
    mgp_debut_competition:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mgp_current_team"
        })
    mgp_current_team:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mgp_modifiedtime"
        })
    mgp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserMotogpProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mgp_createdby_user'})
    mgpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"mgp_modifiedby"
        })
    mgp_modifiedby:string | null;
        
}
