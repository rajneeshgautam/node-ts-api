import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_curriculums" ,{schema:"sportsmatik_local" } )
@Index("cur_uty_id",["curUty",])
export class gur_curriculums {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"cur_id"
        })
    cur_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCurriculumss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cur_uty_id'})
    curUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"cur_usr_id"
        })
    cur_usr_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cur_heading"
        })
    cur_heading:string;
        

    @Column("text",{ 
        nullable:false,
        name:"cur_desc"
        })
    cur_desc:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cur_photo"
        })
    cur_photo:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cur_modifiedtime"
        })
    cur_modifiedtime:Date;
        
}
