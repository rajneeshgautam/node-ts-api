import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_jujitsu_profile" ,{schema:"sportsmatik_local" } )
@Index("juj_usr_id",["jujUsr",])
@Index("juj_weight_class",["jujWeightClass",])
@Index("juj_degree",["jujDegree",])
@Index("juj_degree_belt",["jujBelt",])
@Index("juj_createdby_user",["jujCreatedbyUser",])
export class gur_user_jujitsu_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"juj_id"
        })
    juj_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJujitsuProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'juj_usr_id'})
    jujUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserJujitsuProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'juj_weight_class'})
    jujWeightClass:gur_value_list | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"juj_style"
        })
    juj_style:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserJujitsuProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'juj_degree'})
    jujDegree:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserJujitsuProfiles3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'juj_belt'})
    jujBelt:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"juj_modifiedtime"
        })
    juj_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJujitsuProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'juj_createdby_user'})
    jujCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"juj_modifiedby"
        })
    juj_modifiedby:string | null;
        
}
