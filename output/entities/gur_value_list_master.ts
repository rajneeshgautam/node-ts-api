import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_value_list_master" ,{schema:"sportsmatik_local" } )
@Index("vlm_value",["vlm_value",],{unique:true})
export class gur_value_list_master {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"vlm_id"
        })
    vlm_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"vlm_value"
        })
    vlm_value:string;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"vlm_sort_order"
        })
    vlm_sort_order:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vlm_createdby"
        })
    vlm_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"vlm_modifiedby"
        })
    vlm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vlm_modifiedtime"
        })
    vlm_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.vlcVlm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurValueLists:gur_value_list[];
    
}
