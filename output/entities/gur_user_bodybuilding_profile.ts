import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_bodybuilding_profile" ,{schema:"sportsmatik_local" } )
@Index("bod_usr_id",["bodUsr",])
@Index("bod_createdby_user",["bodCreatedbyUser",])
export class gur_user_bodybuilding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bod_id"
        })
    bod_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBodybuildingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bod_usr_id'})
    bodUsr:gur_users | null;


    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_off_season_weight"
        })
    bod_off_season_weight:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_competition_weight"
        })
    bod_competition_weight:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_biceps"
        })
    bod_biceps:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_chest"
        })
    bod_chest:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_thighs"
        })
    bod_thighs:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_calves"
        })
    bod_calves:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"bod_waist"
        })
    bod_waist:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bod_modifiedtime"
        })
    bod_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBodybuildingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bod_createdby_user'})
    bodCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bod_modifiedby"
        })
    bod_modifiedby:string | null;
        
}
