import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_badminton_profile" ,{schema:"sportsmatik_local" } )
@Index("pbd_usr_id",["pbdUsr",])
@Index("pbd_disability",["pbdDisability",])
@Index("pbd_createdby_user",["pbdCreatedbyUser",])
export class gur_user_para_badminton_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pbd_id"
        })
    pbd_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBadmintonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbd_usr_id'})
    pbdUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pbd_primary_hand"
        })
    pbd_primary_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaBadmintonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbd_disability'})
    pbdDisability:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pbd_modifiedtime"
        })
    pbd_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBadmintonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbd_createdby_user'})
    pbdCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pbd_modifiedby"
        })
    pbd_modifiedby:string | null;
        
}
