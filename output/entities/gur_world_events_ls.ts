import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events} from "./gur_world_events";


@Entity("gur_world_events_ls" ,{schema:"sportsmatik_local" } )
@Index("evl_lng_code",["evl_lng_code",])
@Index("gur_world_events_ls_ibfk_1",["evlEvt",])
export class gur_world_events_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"evl_id"
        })
    evl_id:number;
        

   
    @ManyToOne(()=>gur_world_events, (gur_world_events: gur_world_events)=>gur_world_events.gurWorldEventsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evl_evt_id'})
    evlEvt:gur_world_events | null;


    @Column("varchar",{ 
        nullable:true,
        name:"evl_name"
        })
    evl_name:string | null;
        

    @Column("longtext",{ 
        nullable:true,
        name:"evl_descriptions"
        })
    evl_descriptions:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"evl_lng_code"
        })
    evl_lng_code:string;
        

    @Column("text",{ 
        nullable:true,
        name:"evl_short_desc"
        })
    evl_short_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"evl_detail"
        })
    evl_detail:string | null;
        
}
