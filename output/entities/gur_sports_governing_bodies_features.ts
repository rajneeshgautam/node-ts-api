import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_governing_bodies} from "./gur_sports_governing_bodies";


@Entity("gur_sports_governing_bodies_features" ,{schema:"sportsmatik_local" } )
@Index("gur_sports_governing_bodies_features_ibfk_2",["gbfSgb",])
export class gur_sports_governing_bodies_features {

   
    @ManyToOne(()=>gur_sports_governing_bodies, (gur_sports_governing_bodies: gur_sports_governing_bodies)=>gur_sports_governing_bodies.gurSportsGoverningBodiesFeaturess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gbf_sgb_id'})
    gbfSgb:gur_sports_governing_bodies | null;

    @ManyToOne(()=>gur_sports_governing_bodies, (gur_sports_governing_bodies: gur_sports_governing_bodies)=>gur_sports_governing_bodies.gurSportsGoverningBodiesFeaturess2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gbf_sgb_id'})
    gbfSgb:gur_sports_governing_bodies | null;


    @Column("varchar",{ 
        nullable:false,
        name:"gbf_title"
        })
    gbf_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"gbf_description"
        })
    gbf_description:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"gbf_sort_order"
        })
    gbf_sort_order:number;
        
}
