import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";


@Entity("gur_wiki_venues_sports_map" ,{schema:"sportsmatik_local" } )
@Index("sports_venue_map",["wsmWvn","wsmSpo",],{unique:true})
@Index("wsm_spo_id",["wsmSpo",])
@Index("gur_wiki_venues_sports_map_ibfk_3",["wsmCategory",])
export class gur_wiki_venues_sports_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wsm_id"
        })
    wsm_id:string;
        

   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurWikiVenuesSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsm_wvn_id'})
    wsmWvn:gur_wiki_venues | null;


   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiVenuesSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsm_spo_id'})
    wsmSpo:gur_wiki_sports | null;


    @Column("text",{ 
        nullable:true,
        name:"wsm_records"
        })
    wsm_records:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wsm_tournaments"
        })
    wsm_tournaments:string | null;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWikiVenuesSportsMaps,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'wsm_category'})
    wsmCategory:gur_matik_knowhow_tab_categories | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wsm_teams"
        })
    wsm_teams:string | null;
        
}
