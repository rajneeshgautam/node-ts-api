import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_carrom_profile" ,{schema:"sportsmatik_local" } )
@Index("car_usr_id",["carUsr",])
@Index("car_createdby_user",["carCreatedbyUser",])
export class gur_user_carrom_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"car_id"
        })
    car_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCarromProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'car_usr_id'})
    carUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"car_primary_hand"
        })
    car_primary_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"car_modifiedtime"
        })
    car_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCarromProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'car_createdby_user'})
    carCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"car_modifiedby"
        })
    car_modifiedby:string | null;
        
}
