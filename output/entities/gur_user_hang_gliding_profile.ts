import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_hang_gliding_profile" ,{schema:"sportsmatik_local" } )
@Index("han_usr_id",["hanUsr",])
@Index("han_createdby_user",["hanCreatedbyUser",])
export class gur_user_hang_gliding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"han_id"
        })
    han_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserHangGlidingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'han_usr_id'})
    hanUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"han_classes"
        })
    han_classes:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"han_modifiedtime"
        })
    han_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserHangGlidingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'han_createdby_user'})
    hanCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"han_modifiedby"
        })
    han_modifiedby:string | null;
        
}
