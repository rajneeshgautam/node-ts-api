import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_plan_orders_temp} from "./gur_plan_orders_temp";


@Entity("gur_order_items_temp" ,{schema:"sportsmatik_local" } )
@Index("gur_order_items_ibfk_2",["oriOrd",])
@Index("gur_order_items_ibfk_3",["ori_coupon_id",])
@Index("gur_order_items_ibfk_4",["ori_pln_id",])
@Index("gur_order_items_ibfk_5",["ori_plc_id",])
@Index("gur_order_items_ibfk_6",["ori_plp_id",])
export class gur_order_items_temp {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ori_id"
        })
    ori_id:number;
        

   
    @ManyToOne(()=>gur_plan_orders_temp, (gur_plan_orders_temp: gur_plan_orders_temp)=>gur_plan_orders_temp.gurOrderItemsTemps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'ori_ord_id'})
    oriOrd:gur_plan_orders_temp | null;


    @Column("varchar",{ 
        nullable:false,
        name:"ori_name"
        })
    ori_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"ori_hsn"
        })
    ori_hsn:string | null;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ori_mrp"
        })
    ori_mrp:number;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ori_sale_price"
        })
    ori_sale_price:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"ori_qty"
        })
    ori_qty:number;
        

    @Column("date",{ 
        nullable:true,
        name:"ori_expiry_date"
        })
    ori_expiry_date:string | null;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ori_pln_id"
        })
    ori_pln_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ori_plc_id"
        })
    ori_plc_id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"ori_plp_id"
        })
    ori_plp_id:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"ori_coupon_code"
        })
    ori_coupon_code:string | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"ori_coupon_discount"
        })
    ori_coupon_discount:number | null;
        

    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"ori_coupon_id"
        })
    ori_coupon_id:string | null;
        
}
