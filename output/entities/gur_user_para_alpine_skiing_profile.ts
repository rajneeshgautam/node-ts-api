import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_alpine_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("pas_usr_id",["pasUsr",])
@Index("pas_disability",["pasDisability",])
@Index("pas_createdby_user",["pasCreatedbyUser",])
export class gur_user_para_alpine_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pas_id"
        })
    pas_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaAlpineSkiingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pas_usr_id'})
    pasUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"pas_discipline"
        })
    pas_discipline:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaAlpineSkiingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pas_disability'})
    pasDisability:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pas_modifiedtime"
        })
    pas_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaAlpineSkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pas_createdby_user'})
    pasCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pas_modifiedby"
        })
    pas_modifiedby:string | null;
        
}
