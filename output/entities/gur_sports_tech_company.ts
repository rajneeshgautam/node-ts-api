import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_sports_tech_portfolio} from "./gur_sports_tech_portfolio";
import {gur_sports_tech_product_services} from "./gur_sports_tech_product_services";


@Entity("gur_sports_tech_company" ,{schema:"sportsmatik_local" } )
@Index("gst_number",["stc_gst_number",],{unique:true})
@Index("phone2",["stc_phone2",],{unique:true})
@Index("phone1",["stc_phone1",],{unique:true})
@Index("name",["stc_name",])
@Index("sts_id",["stc_sts_id",])
@Index("dis_id",["stc_dis_id",])
@Index("usr_id",["stcUsr",])
@Index("country",["stc_country",])
@Index("createdby_user",["stc_createdby_user",])
export class gur_sports_tech_company {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"stc_id"
        })
    stc_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stc_slug"
        })
    stc_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"stc_name"
        })
    stc_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsTechCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stc_usr_id'})
    stcUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"stc_address"
        })
    stc_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stc_address2"
        })
    stc_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"stc_pincode"
        })
    stc_pincode:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"stc_country"
        })
    stc_country:number;
        

    @Column("int",{ 
        nullable:true,
        name:"stc_dis_id"
        })
    stc_dis_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"stc_sts_id"
        })
    stc_sts_id:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"stc_address_google_map"
        })
    stc_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stc_email"
        })
    stc_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stc_email_verified"
        })
    stc_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"stc_email_verify_time"
        })
    stc_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"stc_phone1"
        })
    stc_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stc_phone1_verified"
        })
    stc_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"stc_phone1_otp"
        })
    stc_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"stc_phone1_otp_time"
        })
    stc_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"stc_phone2"
        })
    stc_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stc_phone2_verified"
        })
    stc_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"stc_phone2_otp"
        })
    stc_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"stc_phone2_otp_time"
        })
    stc_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"stc_status"
        })
    stc_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stc_profile_pic"
        })
    stc_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stc_timeline_pic"
        })
    stc_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stc_intro_status"
        })
    stc_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"stc_profile_summary"
        })
    stc_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"stc_founded_year"
        })
    stc_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"stc_founded_month"
        })
    stc_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"stc_last_login_date"
        })
    stc_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stc_last_login_details"
        })
    stc_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"stc_intro_modifiedtime"
        })
    stc_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"stc_profile_pic_modifiedtime"
        })
    stc_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"stc_free_sports"
        })
    stc_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"stc_free_sub_users"
        })
    stc_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"stc_gst_number"
        })
    stc_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stc_viewable"
        })
    stc_viewable:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"stc_createdby_user"
        })
    stc_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"stc_createdby"
        })
    stc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"stc_modifiedby"
        })
    stc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"stc_modifiedtime"
        })
    stc_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_sports_tech_portfolio, (gur_sports_tech_portfolio: gur_sports_tech_portfolio)=>gur_sports_tech_portfolio.stpUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsTechPortfolios:gur_sports_tech_portfolio[];
    

   
    @OneToMany(()=>gur_sports_tech_product_services, (gur_sports_tech_product_services: gur_sports_tech_product_services)=>gur_sports_tech_product_services.stpUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsTechProductServicess:gur_sports_tech_product_services[];
    
}
