import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_billiards_profile" ,{schema:"sportsmatik_local" } )
@Index("bil_usr_id",["bilUsr",])
@Index("bil_createdby_user",["bilCreatedbyUser",])
export class gur_user_billiards_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"bil_id"
        })
    bil_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBilliardsProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bil_usr_id'})
    bilUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"bil_primary_hand"
        })
    bil_primary_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bil_modifiedtime"
        })
    bil_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBilliardsProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bil_createdby_user'})
    bilCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bil_modifiedby"
        })
    bil_modifiedby:string | null;
        
}
