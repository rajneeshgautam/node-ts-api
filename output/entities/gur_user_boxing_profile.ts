import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_boxing_profile" ,{schema:"sportsmatik_local" } )
@Index("box_usr_id",["boxUsr",])
@Index("box_createdby_user",["boxCreatedbyUser",])
export class gur_user_boxing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"box_id"
        })
    box_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBoxingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'box_usr_id'})
    boxUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["orthodox","southpaw"],
        name:"box_stance"
        })
    box_stance:string;
        

    @Column("int",{ 
        nullable:false,
        name:"box_event"
        })
    box_event:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"box_modifiedtime"
        })
    box_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBoxingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'box_createdby_user'})
    boxCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"box_modifiedby"
        })
    box_modifiedby:string | null;
        
}
