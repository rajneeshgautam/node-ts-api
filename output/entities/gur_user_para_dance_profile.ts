import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_dance_profile" ,{schema:"sportsmatik_local" } )
@Index("pds_usr_id",["pdsUsr",])
@Index("pds_disability_class",["pdsDisabilityClass",])
@Index("pds_createdby_user",["pdsCreatedbyUser",])
export class gur_user_para_dance_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pds_id"
        })
    pds_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaDanceProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pds_usr_id'})
    pdsUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"pds_disciplines"
        })
    pds_disciplines:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pds_dance_forms"
        })
    pds_dance_forms:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaDanceProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pds_disability_class'})
    pdsDisabilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pds_modifiedtime"
        })
    pds_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaDanceProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pds_createdby_user'})
    pdsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pds_modifiedby"
        })
    pds_modifiedby:string | null;
        
}
