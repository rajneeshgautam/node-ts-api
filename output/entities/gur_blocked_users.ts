import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_blocked_users" ,{schema:"sportsmatik_local" } )
@Index("blk_by",["blkCreatedby",])
@Index("rat_rated_by_type",["blkByType",])
@Index("rat_rated_by",["blk_by_user",])
@Index("mtg_for_type",["blkToType",])
@Index("blk_to",["blk_to",])
export class gur_blocked_users {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"blk_id"
        })
    blk_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurBlockedUserss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'blk_createdby'})
    blkCreatedby:gur_users | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurBlockedUserss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'blk_by_type'})
    blkByType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"blk_by_user"
        })
    blk_by_user:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurBlockedUserss2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'blk_to_type'})
    blkToType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"blk_to"
        })
    blk_to:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        default: () => "'-'",
        name:"blk_modifiedby"
        })
    blk_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"blk_modifiedtime"
        })
    blk_modifiedtime:Date;
        
}
