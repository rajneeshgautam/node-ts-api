import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_wheelchair_basketball_profile" ,{schema:"sportsmatik_local" } )
@Index("pwb_usr_id",["pwbUsr",])
@Index("pwb_primary_position",["pwbPrimaryPosition",])
@Index("pwb_alternate_position",["pwbAlternatePosition",])
@Index("pwb_disability_class",["pwbDisabilityClass",])
@Index("pwb_createdby_user",["pwbCreatedbyUser",])
export class gur_user_para_wheelchair_basketball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pwb_id"
        })
    pwb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaWheelchairBasketballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwb_usr_id'})
    pwbUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pwb_primary_hand"
        })
    pwb_primary_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaWheelchairBasketballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwb_primary_position'})
    pwbPrimaryPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaWheelchairBasketballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwb_alternate_position'})
    pwbAlternatePosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaWheelchairBasketballProfiles3,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwb_disability_class'})
    pwbDisabilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pwb_modifiedtime"
        })
    pwb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaWheelchairBasketballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwb_createdby_user'})
    pwbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pwb_modifiedby"
        })
    pwb_modifiedby:string | null;
        
}
