import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_admin_users} from "./gur_admin_users";
import {gur_admin_user_departments} from "./gur_admin_user_departments";
import {gur_support_ticket_comments} from "./gur_support_ticket_comments";
import {gur_support_ticket_logs} from "./gur_support_ticket_logs";


@Entity("gur_support_ticket" ,{schema:"sportsmatik_local" } )
@Index("stk_usr_id",["stk_usr_id",])
@Index("stk_admin_id",["stkAdm",])
@Index("stk_uty_code",["stkUtyCode",])
@Index("stk_usr_type",["stkUsrType",])
@Index("gur_support_ticket_ibfk_5_idx",["stkDepartment",])
export class gur_support_ticket {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"stk_id"
        })
    stk_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stk_type"
        })
    stk_type:boolean;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSupportTickets,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stk_uty_code'})
    stkUtyCode:gur_user_types | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSupportTickets2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stk_usr_type'})
    stkUsrType:gur_user_types | null;


    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"stk_usr_id"
        })
    stk_usr_id:number | null;
        

   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurSupportTickets,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stk_adm_id'})
    stkAdm:gur_admin_users | null;


   
    @ManyToOne(()=>gur_admin_user_departments, (gur_admin_user_departments: gur_admin_user_departments)=>gur_admin_user_departments.gurSupportTickets,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stk_department'})
    stkDepartment:gur_admin_user_departments | null;


    @Column("varchar",{ 
        nullable:false,
        name:"stk_subject"
        })
    stk_subject:string;
        

    @Column("text",{ 
        nullable:true,
        name:"stk_message"
        })
    stk_message:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stk_attachment1"
        })
    stk_attachment1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stk_attachment2"
        })
    stk_attachment2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stk_attachment3"
        })
    stk_attachment3:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stk_internal_note"
        })
    stk_internal_note:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"stk_sent_on"
        })
    stk_sent_on:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stk_createdby"
        })
    stk_createdby:string | null;
        

   
    @OneToMany(()=>gur_support_ticket_comments, (gur_support_ticket_comments: gur_support_ticket_comments)=>gur_support_ticket_comments.strStk,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTicketCommentss:gur_support_ticket_comments[];
    

   
    @OneToMany(()=>gur_support_ticket_logs, (gur_support_ticket_logs: gur_support_ticket_logs)=>gur_support_ticket_logs.stlStk,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTicketLogss:gur_support_ticket_logs[];
    
}
