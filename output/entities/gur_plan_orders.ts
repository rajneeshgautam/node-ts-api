import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_order_status} from "./gur_order_status";
import {gur_user_types} from "./gur_user_types";
import {gur_order_items} from "./gur_order_items";
import {gur_user_plans} from "./gur_user_plans";
import {gur_user_plans_addons} from "./gur_user_plans_addons";


@Entity("gur_plan_orders" ,{schema:"sportsmatik_local" } )
@Index("gur_order_history_ibfk_2",["ord_user_id",])
@Index("gur_order_history_ibfk_4",["ord_done_by",])
@Index("ord_ors_id",["ordStatus",])
@Index("ord_payment_method",["ord_payment_mode",])
@Index("ord_usr_type",["ordUty",])
export class gur_plan_orders {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ord_id"
        })
    ord_id:string;
        

   
    @ManyToOne(()=>gur_order_status, (gur_order_status: gur_order_status)=>gur_order_status.gurPlanOrderss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'ord_status'})
    ordStatus:gur_order_status | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"ord_payment_received"
        })
    ord_payment_received:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ord_deleted"
        })
    ord_deleted:boolean;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurPlanOrderss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'ord_uty_id'})
    ordUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ord_user_id"
        })
    ord_user_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ord_paid_via"
        })
    ord_paid_via:string | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ord_payment_mode"
        })
    ord_payment_mode:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ord_payment_method"
        })
    ord_payment_method:string | null;
        

    @Column("char",{ 
        nullable:false,
        length:3,
        name:"ord_currency_code"
        })
    ord_currency_code:string;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ord_currency_value_inr"
        })
    ord_currency_value_inr:number;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"ord_total"
        })
    ord_total:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"ord_final_amt"
        })
    ord_final_amt:number | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ord_tax_percent"
        })
    ord_tax_percent:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_txn_id"
        })
    ord_txn_id:string | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ord_done_by"
        })
    ord_done_by:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_user_name"
        })
    ord_user_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_user_email"
        })
    ord_user_email:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"ord_user_mobile"
        })
    ord_user_mobile:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_address1"
        })
    ord_address1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_address2"
        })
    ord_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ord_country"
        })
    ord_country:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ord_state"
        })
    ord_state:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ord_district"
        })
    ord_district:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"ord_pincode"
        })
    ord_pincode:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ord_state_gst_code"
        })
    ord_state_gst_code:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"ord_gstin"
        })
    ord_gstin:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ord_amount_breakdown"
        })
    ord_amount_breakdown:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ord_session_id"
        })
    ord_session_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"ord_client_ip"
        })
    ord_client_ip:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ord_createdon"
        })
    ord_createdon:Date;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"ord_modifiedon"
        })
    ord_modifiedon:Date | null;
        

   
    @OneToMany(()=>gur_order_items, (gur_order_items: gur_order_items)=>gur_order_items.oriOrd,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrderItemss:gur_order_items[];
    

   
    @OneToMany(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.pchOrder,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlanss:gur_user_plans[];
    

   
    @OneToMany(()=>gur_user_plans_addons, (gur_user_plans_addons: gur_user_plans_addons)=>gur_user_plans_addons.upaOrd,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansAddonss:gur_user_plans_addons[];
    
}
