import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events} from "./gur_world_events";
import {gur_countries} from "./gur_countries";
import {gur_wiki_sports} from "./gur_wiki_sports";


@Entity("gur_world_events_records" ,{schema:"sportsmatik_local" } )
@Index("gur_world_events_records_ibfk_1",["werEvt",])
@Index("gur_world_events_records_ibfk_2",["werCnt",])
@Index("gur_world_events_records_ibfk_3",["werSpo",])
export class gur_world_events_records {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"wer_id"
        })
    wer_id:number;
        

   
    @ManyToOne(()=>gur_world_events, (gur_world_events: gur_world_events)=>gur_world_events.gurWorldEventsRecordss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'wer_evt_id'})
    werEvt:gur_world_events | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wer_name"
        })
    wer_name:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWorldEventsRecordss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'wer_cnt_id'})
    werCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWorldEventsRecordss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'wer_spo_id'})
    werSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wer_event_name"
        })
    wer_event_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"wer_record"
        })
    wer_record:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"wer_createdby"
        })
    wer_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wer_modifiedby"
        })
    wer_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wer_modifiedtime"
        })
    wer_modifiedtime:Date;
        
}
