import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_cricket_profile" ,{schema:"sportsmatik_local" } )
@Index("ucp_player_type",["ucpPlayerType",])
@Index("ucp_usr_id",["ucpUsr",])
@Index("ucp_batting_positions",["ucpBattingPositions",])
@Index("ucp_batting_nature",["ucpPlayingNature",])
@Index("ucp_bowling_style",["ucpBowlingStyle",])
@Index("ucp_createdby_user",["ucpCreatedbyUser",])
export class gur_user_cricket_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ucp_id"
        })
    ucp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCricketProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_usr_id'})
    ucpUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCricketProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_player_type'})
    ucpPlayerType:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"ucp_batting_hand"
        })
    ucp_batting_hand:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCricketProfiles2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_batting_positions'})
    ucpBattingPositions:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCricketProfiles3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_playing_nature'})
    ucpPlayingNature:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"ucp_bowling_hand"
        })
    ucp_bowling_hand:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCricketProfiles4,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_bowling_style'})
    ucpBowlingStyle:gur_value_list | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCricketProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_createdby_user'})
    ucpCreatedbyUser:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ucp_modifiedtime"
        })
    ucp_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ucp_modifiedby"
        })
    ucp_modifiedby:string | null;
        
}
