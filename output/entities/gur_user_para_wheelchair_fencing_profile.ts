import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_wheelchair_fencing_profile" ,{schema:"sportsmatik_local" } )
@Index("pwf_usr_id",["pwfUsr",])
@Index("pwf_disability_class",["pwfDisabilityClass",])
@Index("pwf_createdby_user",["pwfCreatedbyUser",])
export class gur_user_para_wheelchair_fencing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pwf_id"
        })
    pwf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaWheelchairFencingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwf_usr_id'})
    pwfUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"pwf_hand"
        })
    pwf_hand:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pwf_weapon_epee"
        })
    pwf_weapon_epee:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pwf_weapon_sabre"
        })
    pwf_weapon_sabre:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pwf_weapon_foil"
        })
    pwf_weapon_foil:boolean;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaWheelchairFencingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwf_disability_class'})
    pwfDisabilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pwf_modifiedtime"
        })
    pwf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaWheelchairFencingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwf_createdby_user'})
    pwfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pwf_modifiedby"
        })
    pwf_modifiedby:string | null;
        
}
