import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_plans_check_backup" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["pch_uty_id",])
@Index("pch_cat_id",["pch_cat_id",])
@Index("pch_cat_id_2",["pch_cat_id",])
@Index("pch_order_id",["pch_order_id",])
@Index("pch_prp_id",["pch_prp_id",])
export class gur_user_plans_check_backup {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"pch_id"
        })
    pch_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"pch_uty_id"
        })
    pch_uty_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"pch_user_id"
        })
    pch_user_id:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"pch_order_id"
        })
    pch_order_id:number | null;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"pch_cat_id"
        })
    pch_cat_id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"pch_prp_id"
        })
    pch_prp_id:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"pch_credits"
        })
    pch_credits:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"pch_total_credits"
        })
    pch_total_credits:number;
        

    @Column("date",{ 
        nullable:false,
        name:"pch_start_date"
        })
    pch_start_date:string;
        

    @Column("date",{ 
        nullable:true,
        name:"pch_end_date"
        })
    pch_end_date:string | null;
        
}
