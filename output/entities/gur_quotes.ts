import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quotes_by} from "./gur_quotes_by";
import {gur_quotes_ls} from "./gur_quotes_ls";


@Entity("gur_quotes" ,{schema:"sportsmatik_local" } )
@Index("quo_qub_id",["quoQub",])
export class gur_quotes {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"quo_id"
        })
    quo_id:number;
        

   
    @ManyToOne(()=>gur_quotes_by, (gur_quotes_by: gur_quotes_by)=>gur_quotes_by.gurQuotess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'quo_qub_id'})
    quoQub:gur_quotes_by | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"quo_status"
        })
    quo_status:boolean;
        

    @Column("date",{ 
        nullable:true,
        name:"quo_date_used"
        })
    quo_date_used:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"quo_image"
        })
    quo_image:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"quo_vid_url"
        })
    quo_vid_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"quo_vid_thumb"
        })
    quo_vid_thumb:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"quo_createdby"
        })
    quo_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"quo_modifiedby"
        })
    quo_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"quo_modifiedtime"
        })
    quo_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_quotes_ls, (gur_quotes_ls: gur_quotes_ls)=>gur_quotes_ls.qolQuo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuotesLss:gur_quotes_ls[];
    
}
