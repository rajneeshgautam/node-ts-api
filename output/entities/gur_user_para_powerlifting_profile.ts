import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_powerlifting_profile" ,{schema:"sportsmatik_local" } )
@Index("ppl_usr_id",["pplUsr",])
@Index("ppl_sport_card_status",["pplSportCardStatus",])
@Index("ppl_weight_event",["pplWeightEvent",])
@Index("ppl_disability_classes",["pplDisabilityClasses",])
@Index("ppl_createdby_user",["pplCreatedbyUser",])
export class gur_user_para_powerlifting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ppl_id"
        })
    ppl_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaPowerliftingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_usr_id'})
    pplUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaPowerliftingProfiles2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_weight_event'})
    pplWeightEvent:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaPowerliftingProfiles3,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_disability_classes'})
    pplDisabilityClasses:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaPowerliftingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_sport_card_status'})
    pplSportCardStatus:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ppl_modifiedtime"
        })
    ppl_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaPowerliftingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ppl_createdby_user'})
    pplCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ppl_modifiedby"
        })
    ppl_modifiedby:string | null;
        
}
