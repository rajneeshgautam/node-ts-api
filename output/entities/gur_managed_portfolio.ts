import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_users} from "./gur_users";
import {gur_managed_portfolio_map} from "./gur_managed_portfolio_map";


@Entity("gur_managed_portfolio" ,{schema:"sportsmatik_local" } )
@Index("mht_team_player_coached",["mht_team_player_managed",])
@Index("mht_spo_id",["mhtSpo",])
@Index("mht_country",["mhtCountry","mhtCity",])
@Index("mht_city",["mhtCity",])
@Index("mht_createdby_user",["mhtCreatedbyUser",])
@Index("mht_uty_id",["mhtUty",])
export class gur_managed_portfolio {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mht_id"
        })
    mht_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurManagedPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mht_uty_id'})
    mhtUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mht_user_id"
        })
    mht_user_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"mht_type"
        })
    mht_type:boolean;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurManagedPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mht_spo_id'})
    mhtSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"mht_team_player_managed"
        })
    mht_team_player_managed:string;
        

    @Column("enum",{ 
        nullable:true,
        enum:["f","m","o"],
        name:"mht_gender"
        })
    mht_gender:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"mht_dob"
        })
    mht_dob:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurManagedPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mht_country'})
    mhtCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurManagedPortfolios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mht_city'})
    mhtCity:gur_all_cities | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"mht_matches_played"
        })
    mht_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"mht_matches_won"
        })
    mht_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"mht_matches_lost"
        })
    mht_matches_lost:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"mht_matches_draw"
        })
    mht_matches_draw:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"mht_matches_no_results"
        })
    mht_matches_no_results:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"mht_winning_percentage"
        })
    mht_winning_percentage:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mht_stats_not_applicable"
        })
    mht_stats_not_applicable:boolean;
        

    @Column("date",{ 
        nullable:false,
        name:"mht_duration_start"
        })
    mht_duration_start:string;
        

    @Column("date",{ 
        nullable:true,
        name:"mht_duration_end"
        })
    mht_duration_end:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"mht_currently_managing"
        })
    mht_currently_managing:boolean | null;
        

    @Column("text",{ 
        nullable:true,
        name:"mht_desc"
        })
    mht_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mht_photo"
        })
    mht_photo:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"mht_status"
        })
    mht_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mht_admin_review"
        })
    mht_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurManagedPortfolios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mht_createdby_user'})
    mhtCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"mht_createdby"
        })
    mht_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"mht_modifiedby"
        })
    mht_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mht_modifiedtime"
        })
    mht_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_managed_portfolio_map, (gur_managed_portfolio_map: gur_managed_portfolio_map)=>gur_managed_portfolio_map.mhmMht,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurManagedPortfolioMaps:gur_managed_portfolio_map[];
    
}
