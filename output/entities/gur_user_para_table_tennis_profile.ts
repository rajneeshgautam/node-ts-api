import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_table_tennis_profile" ,{schema:"sportsmatik_local" } )
@Index("ptt_usr_id",["pttUsr",])
@Index("ptt_disablity_class",["pttDisablityClass",])
@Index("ptt_createdby_user",["pttCreatedbyUser",])
export class gur_user_para_table_tennis_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ptt_id"
        })
    ptt_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaTableTennisProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptt_usr_id'})
    pttUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right","na"],
        name:"ptt_hand"
        })
    ptt_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaTableTennisProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptt_disablity_class'})
    pttDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ptt_modifiedtime"
        })
    ptt_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaTableTennisProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptt_createdby_user'})
    pttCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ptt_modifiedby"
        })
    ptt_modifiedby:string | null;
        
}
