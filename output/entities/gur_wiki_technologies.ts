import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_wiki_features} from "./gur_wiki_features";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";
import {gur_wiki_technologies_ls} from "./gur_wiki_technologies_ls";
import {gur_wiki_sports} from "./gur_wiki_sports";


@Entity("gur_wiki_technologies" ,{schema:"sportsmatik_local" } )
@Index("tech_knc_id",["tecKnc",])
export class gur_wiki_technologies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"tec_id"
        })
    tec_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"tec_slug"
        })
    tec_slug:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tec_primary_photo"
        })
    tec_primary_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"tec_photo_source_details"
        })
    tec_photo_source_details:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"tec_multi"
        })
    tec_multi:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"tec_status"
        })
    tec_status:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"tec_sort_order"
        })
    tec_sort_order:number | null;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWikiTechnologiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tec_knc_id'})
    tecKnc:gur_matik_knowhow_tab_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"tec_is_featured"
        })
    tec_is_featured:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"tec_intro_year"
        })
    tec_intro_year:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tec_createdby"
        })
    tec_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"tec_modifiedby"
        })
    tec_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tec_modifiedtime"
        })
    tec_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_features, (gur_wiki_features: gur_wiki_features)=>gur_wiki_features.wvfTec,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiFeaturess:gur_wiki_features[];
    

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgTec,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    

   
    @OneToMany(()=>gur_wiki_technologies_ls, (gur_wiki_technologies_ls: gur_wiki_technologies_ls)=>gur_wiki_technologies_ls.telTec,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiTechnologiesLss:gur_wiki_technologies_ls[];
    

   
    @ManyToMany(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiTechnologiess,{  nullable:false, })
    @JoinTable({ name:'gur_wiki_technologies_sports_map'})
    gurWikiSportss:gur_wiki_sports[];
    
}
