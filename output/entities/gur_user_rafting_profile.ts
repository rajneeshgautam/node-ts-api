import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_rafting_profile" ,{schema:"sportsmatik_local" } )
@Index("raf_usr_id",["rafUsr",])
@Index("raf_skill_level",["rafSkillLevel",])
@Index("raf_createdby_user",["rafCreatedbyUser",])
export class gur_user_rafting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"raf_id"
        })
    raf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRaftingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'raf_usr_id'})
    rafUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"raf_discipline"
        })
    raf_discipline:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRaftingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'raf_skill_level'})
    rafSkillLevel:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"raf_modifiedtime"
        })
    raf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRaftingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'raf_createdby_user'})
    rafCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"raf_modifiedby"
        })
    raf_modifiedby:string | null;
        
}
