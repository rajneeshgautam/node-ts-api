import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_ski_flying_profile" ,{schema:"sportsmatik_local" } )
@Index("skf_usr_id",["skfUsr",])
@Index("skf_createdby_user",["skfCreatedbyUser",])
export class gur_user_ski_flying_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"skf_id"
        })
    skf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkiFlyingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'skf_usr_id'})
    skfUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"skf_modifiedtime"
        })
    skf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSkiFlyingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'skf_createdby_user'})
    skfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"skf_modifiedby"
        })
    skf_modifiedby:string | null;
        
}
