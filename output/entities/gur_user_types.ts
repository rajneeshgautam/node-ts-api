import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_address_book} from "./gur_address_book";
import {gur_advertiser_ads_hits} from "./gur_advertiser_ads_hits";
import {gur_blocked_users} from "./gur_blocked_users";
import {gur_ca_individual_professional_details} from "./gur_ca_individual_professional_details";
import {gur_community_answer} from "./gur_community_answer";
import {gur_community_answer_like} from "./gur_community_answer_like";
import {gur_community_follow} from "./gur_community_follow";
import {gur_community_question} from "./gur_community_question";
import {gur_community_question_like} from "./gur_community_question_like";
import {gur_community_question_view} from "./gur_community_question_view";
import {gur_contact_view_history} from "./gur_contact_view_history";
import {gur_corporates_sport_map} from "./gur_corporates_sport_map";
import {gur_coupons} from "./gur_coupons";
import {gur_curriculums} from "./gur_curriculums";
import {gur_email_to_be_sent} from "./gur_email_to_be_sent";
import {gur_events} from "./gur_events";
import {gur_facilities} from "./gur_facilities";
import {gur_featured_matik_users} from "./gur_featured_matik_users";
import {gur_feedback} from "./gur_feedback";
import {gur_fitness_center_services} from "./gur_fitness_center_services";
import {gur_health_consulting_entities} from "./gur_health_consulting_entities";
import {gur_hours_of_operation} from "./gur_hours_of_operation";
import {gur_infra_portfolio} from "./gur_infra_portfolio";
import {gur_infra_user_products} from "./gur_infra_user_products";
import {gur_institute_branches} from "./gur_institute_branches";
import {gur_institute_course_and_fees} from "./gur_institute_course_and_fees";
import {gur_institutional_staff} from "./gur_institutional_staff";
import {gur_institutional_teams} from "./gur_institutional_teams";
import {gur_institutional_teams_contact_usage} from "./gur_institutional_teams_contact_usage";
import {gur_institutional_trophies} from "./gur_institutional_trophies";
import {gur_journalist_writer_portfolio} from "./gur_journalist_writer_portfolio";
import {gur_knowledge_base_articles_like} from "./gur_knowledge_base_articles_like";
import {gur_knowledge_base_articles_view} from "./gur_knowledge_base_articles_view";
import {gur_lawyer_ca_portfolio} from "./gur_lawyer_ca_portfolio";
import {gur_lawyer_ca_services} from "./gur_lawyer_ca_services";
import {gur_logistics_services} from "./gur_logistics_services";
import {gur_managed_portfolio} from "./gur_managed_portfolio";
import {gur_matik_tags} from "./gur_matik_tags";
import {gur_matikmail_masters} from "./gur_matikmail_masters";
import {gur_matikmail_messages} from "./gur_matikmail_messages";
import {gur_membership_fees} from "./gur_membership_fees";
import {gur_notifications} from "./gur_notifications";
import {gur_orders_old} from "./gur_orders_old";
import {gur_plan_features_map} from "./gur_plan_features_map";
import {gur_plan_orders} from "./gur_plan_orders";
import {gur_plan_usage_log} from "./gur_plan_usage_log";
import {gur_pricing_plans_map} from "./gur_pricing_plans_map";
import {gur_privacy_settings} from "./gur_privacy_settings";
import {gur_profile_completeness_modules_user_map} from "./gur_profile_completeness_modules_user_map";
import {gur_quiz_result} from "./gur_quiz_result";
import {gur_ratings} from "./gur_ratings";
import {gur_ratings_like} from "./gur_ratings_like";
import {gur_recruitment_history} from "./gur_recruitment_history";
import {gur_sponsorship} from "./gur_sponsorship";
import {gur_sports_logistics_portfolio} from "./gur_sports_logistics_portfolio";
import {gur_sports_usage_history} from "./gur_sports_usage_history";
import {gur_sports_vacancies} from "./gur_sports_vacancies";
import {gur_sports_vacancies_applicants} from "./gur_sports_vacancies_applicants";
import {gur_sports_vacancies_featured_users} from "./gur_sports_vacancies_featured_users";
import {gur_squads_group} from "./gur_squads_group";
import {gur_squads_list} from "./gur_squads_list";
import {gur_sub_users} from "./gur_sub_users";
import {gur_subuser_usage_history} from "./gur_subuser_usage_history";
import {gur_support_ticket} from "./gur_support_ticket";
import {gur_support_ticket_comments} from "./gur_support_ticket_comments";
import {gur_support_ticket_logs} from "./gur_support_ticket_logs";
import {gur_training_camp_users_map} from "./gur_training_camp_users_map";
import {gur_user_albums} from "./gur_user_albums";
import {gur_user_awards_achievements} from "./gur_user_awards_achievements";
import {gur_user_basic_moderation} from "./gur_user_basic_moderation";
import {gur_user_leads_old} from "./gur_user_leads_old";
import {gur_user_modules_map} from "./gur_user_modules_map";
import {gur_user_plans} from "./gur_user_plans";
import {gur_user_plans_check} from "./gur_user_plans_check";
import {gur_user_plans_check_not_used} from "./gur_user_plans_check_not_used";
import {gur_user_plans_details} from "./gur_user_plans_details";
import {gur_user_profile_views} from "./gur_user_profile_views";
import {gur_user_sports_map} from "./gur_user_sports_map";
import {gur_user_sub_type_map} from "./gur_user_sub_type_map";
import {gur_users} from "./gur_users";
import {gur_users_contact_view_log} from "./gur_users_contact_view_log";


@Entity("gur_user_types" ,{schema:"sportsmatik_local" } )
@Index("uty_name",["uty_name",],{unique:true})
@Index("uty_constant",["uty_constant",],{unique:true})
@Index("uty_slug",["uty_slug",],{unique:true})
@Index("uty_code",["uty_code",])
export class gur_user_types {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"uty_id"
        })
    uty_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:6,
        name:"uty_code_label"
        })
    uty_code_label:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uty_type"
        })
    uty_type:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"uty_code"
        })
    uty_code:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"uty_name"
        })
    uty_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"uty_constant"
        })
    uty_constant:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"uty_slug"
        })
    uty_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uty_show_label"
        })
    uty_show_label:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"uty_sort_order"
        })
    uty_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"uty_free_sports"
        })
    uty_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"uty_free_sub_users"
        })
    uty_free_sub_users:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_applicable_for_sub_users"
        })
    uty_applicable_for_sub_users:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_applicable_for_modules"
        })
    uty_applicable_for_modules:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_show_on_registration"
        })
    uty_show_on_registration:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uty_show_on_report"
        })
    uty_show_on_report:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_show_on_user_search"
        })
    uty_show_on_user_search:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"uty_show_on_pricing"
        })
    uty_show_on_pricing:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uty_show_on_matik_tags"
        })
    uty_show_on_matik_tags:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:300,
        name:"uty_short_desc"
        })
    uty_short_desc:string | null;
        

   
    @OneToMany(()=>gur_address_book, (gur_address_book: gur_address_book)=>gur_address_book.adbUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAddressBooks:gur_address_book[];
    

   
    @OneToMany(()=>gur_advertiser_ads_hits, (gur_advertiser_ads_hits: gur_advertiser_ads_hits)=>gur_advertiser_ads_hits.adhUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAdvertiserAdsHitss:gur_advertiser_ads_hits[];
    

   
    @OneToMany(()=>gur_blocked_users, (gur_blocked_users: gur_blocked_users)=>gur_blocked_users.blkByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlockedUserss:gur_blocked_users[];
    

   
    @OneToMany(()=>gur_blocked_users, (gur_blocked_users: gur_blocked_users)=>gur_blocked_users.blkToType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlockedUserss2:gur_blocked_users[];
    

   
    @OneToMany(()=>gur_ca_individual_professional_details, (gur_ca_individual_professional_details: gur_ca_individual_professional_details)=>gur_ca_individual_professional_details.cipUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCaIndividualProfessionalDetailss:gur_ca_individual_professional_details[];
    

   
    @OneToMany(()=>gur_community_answer, (gur_community_answer: gur_community_answer)=>gur_community_answer.cmaUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityAnswers:gur_community_answer[];
    

   
    @OneToMany(()=>gur_community_answer_like, (gur_community_answer_like: gur_community_answer_like)=>gur_community_answer_like.calUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityAnswerLikes:gur_community_answer_like[];
    

   
    @OneToMany(()=>gur_community_follow, (gur_community_follow: gur_community_follow)=>gur_community_follow.cfoUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityFollows:gur_community_follow[];
    

   
    @OneToMany(()=>gur_community_question, (gur_community_question: gur_community_question)=>gur_community_question.comUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityQuestions:gur_community_question[];
    

   
    @OneToMany(()=>gur_community_question_like, (gur_community_question_like: gur_community_question_like)=>gur_community_question_like.cqlUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityQuestionLikes:gur_community_question_like[];
    

   
    @OneToMany(()=>gur_community_question_view, (gur_community_question_view: gur_community_question_view)=>gur_community_question_view.cvuUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityQuestionViews:gur_community_question_view[];
    

   
    @OneToMany(()=>gur_contact_view_history, (gur_contact_view_history: gur_contact_view_history)=>gur_contact_view_history.chrByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurContactViewHistorys:gur_contact_view_history[];
    

   
    @OneToMany(()=>gur_contact_view_history, (gur_contact_view_history: gur_contact_view_history)=>gur_contact_view_history.chrForType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurContactViewHistorys2:gur_contact_view_history[];
    

   
    @OneToMany(()=>gur_corporates_sport_map, (gur_corporates_sport_map: gur_corporates_sport_map)=>gur_corporates_sport_map.csmUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatesSportMaps:gur_corporates_sport_map[];
    

   
    @OneToMany(()=>gur_coupons, (gur_coupons: gur_coupons)=>gur_coupons.couUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCouponss:gur_coupons[];
    

   
    @OneToMany(()=>gur_curriculums, (gur_curriculums: gur_curriculums)=>gur_curriculums.curUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCurriculumss:gur_curriculums[];
    

   
    @OneToMany(()=>gur_email_to_be_sent, (gur_email_to_be_sent: gur_email_to_be_sent)=>gur_email_to_be_sent.etsUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEmailToBeSents:gur_email_to_be_sent[];
    

   
    @OneToMany(()=>gur_events, (gur_events: gur_events)=>gur_events.evtUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEventss:gur_events[];
    

   
    @OneToMany(()=>gur_facilities, (gur_facilities: gur_facilities)=>gur_facilities.facUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFacilitiess:gur_facilities[];
    

   
    @OneToMany(()=>gur_featured_matik_users, (gur_featured_matik_users: gur_featured_matik_users)=>gur_featured_matik_users.fmuUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFeaturedMatikUserss:gur_featured_matik_users[];
    

   
    @OneToMany(()=>gur_feedback, (gur_feedback: gur_feedback)=>gur_feedback.fedUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFeedbacks:gur_feedback[];
    

   
    @OneToMany(()=>gur_fitness_center_services, (gur_fitness_center_services: gur_fitness_center_services)=>gur_fitness_center_services.fcsUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenterServicess:gur_fitness_center_services[];
    

   
    @OneToMany(()=>gur_health_consulting_entities, (gur_health_consulting_entities: gur_health_consulting_entities)=>gur_health_consulting_entities.hceUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthConsultingEntitiess:gur_health_consulting_entities[];
    

   
    @OneToMany(()=>gur_hours_of_operation, (gur_hours_of_operation: gur_hours_of_operation)=>gur_hours_of_operation.hopUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHoursOfOperations:gur_hours_of_operation[];
    

   
    @OneToMany(()=>gur_infra_portfolio, (gur_infra_portfolio: gur_infra_portfolio)=>gur_infra_portfolio.ipoUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInfraPortfolios:gur_infra_portfolio[];
    

   
    @OneToMany(()=>gur_infra_user_products, (gur_infra_user_products: gur_infra_user_products)=>gur_infra_user_products.iupUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInfraUserProductss:gur_infra_user_products[];
    

   
    @OneToMany(()=>gur_institute_branches, (gur_institute_branches: gur_institute_branches)=>gur_institute_branches.ibrUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteBranchess:gur_institute_branches[];
    

   
    @OneToMany(()=>gur_institute_course_and_fees, (gur_institute_course_and_fees: gur_institute_course_and_fees)=>gur_institute_course_and_fees.icfUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteCourseAndFeess:gur_institute_course_and_fees[];
    

   
    @OneToMany(()=>gur_institutional_staff, (gur_institutional_staff: gur_institutional_staff)=>gur_institutional_staff.itsUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalStaffs:gur_institutional_staff[];
    

   
    @OneToMany(()=>gur_institutional_teams, (gur_institutional_teams: gur_institutional_teams)=>gur_institutional_teams.itnUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTeamss:gur_institutional_teams[];
    

   
    @OneToMany(()=>gur_institutional_teams_contact_usage, (gur_institutional_teams_contact_usage: gur_institutional_teams_contact_usage)=>gur_institutional_teams_contact_usage.tcuUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTeamsContactUsages:gur_institutional_teams_contact_usage[];
    

   
    @OneToMany(()=>gur_institutional_trophies, (gur_institutional_trophies: gur_institutional_trophies)=>gur_institutional_trophies.intUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTrophiess:gur_institutional_trophies[];
    

   
    @OneToMany(()=>gur_journalist_writer_portfolio, (gur_journalist_writer_portfolio: gur_journalist_writer_portfolio)=>gur_journalist_writer_portfolio.jwpUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJournalistWriterPortfolios:gur_journalist_writer_portfolio[];
    

   
    @OneToMany(()=>gur_knowledge_base_articles_like, (gur_knowledge_base_articles_like: gur_knowledge_base_articles_like)=>gur_knowledge_base_articles_like.kblUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurKnowledgeBaseArticlesLikes:gur_knowledge_base_articles_like[];
    

   
    @OneToMany(()=>gur_knowledge_base_articles_view, (gur_knowledge_base_articles_view: gur_knowledge_base_articles_view)=>gur_knowledge_base_articles_view.kvuUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurKnowledgeBaseArticlesViews:gur_knowledge_base_articles_view[];
    

   
    @OneToMany(()=>gur_lawyer_ca_portfolio, (gur_lawyer_ca_portfolio: gur_lawyer_ca_portfolio)=>gur_lawyer_ca_portfolio.lwpUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawyerCaPortfolios:gur_lawyer_ca_portfolio[];
    

   
    @OneToMany(()=>gur_lawyer_ca_services, (gur_lawyer_ca_services: gur_lawyer_ca_services)=>gur_lawyer_ca_services.lwcUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawyerCaServicess:gur_lawyer_ca_services[];
    

   
    @OneToMany(()=>gur_logistics_services, (gur_logistics_services: gur_logistics_services)=>gur_logistics_services.lgsUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLogisticsServicess:gur_logistics_services[];
    

   
    @OneToMany(()=>gur_managed_portfolio, (gur_managed_portfolio: gur_managed_portfolio)=>gur_managed_portfolio.mhtUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurManagedPortfolios:gur_managed_portfolio[];
    

   
    @OneToMany(()=>gur_matik_tags, (gur_matik_tags: gur_matik_tags)=>gur_matik_tags.mtgByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikTagss:gur_matik_tags[];
    

   
    @OneToMany(()=>gur_matik_tags, (gur_matik_tags: gur_matik_tags)=>gur_matik_tags.mtgForType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikTagss2:gur_matik_tags[];
    

   
    @OneToMany(()=>gur_matikmail_masters, (gur_matikmail_masters: gur_matikmail_masters)=>gur_matikmail_masters.emlUsrSenderType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikmailMasterss:gur_matikmail_masters[];
    

   
    @OneToMany(()=>gur_matikmail_masters, (gur_matikmail_masters: gur_matikmail_masters)=>gur_matikmail_masters.emlUsrRecipientType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikmailMasterss2:gur_matikmail_masters[];
    

   
    @OneToMany(()=>gur_matikmail_messages, (gur_matikmail_messages: gur_matikmail_messages)=>gur_matikmail_messages.emsUsrSenderType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikmailMessagess:gur_matikmail_messages[];
    

   
    @OneToMany(()=>gur_matikmail_messages, (gur_matikmail_messages: gur_matikmail_messages)=>gur_matikmail_messages.emsUsrRecipientType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikmailMessagess2:gur_matikmail_messages[];
    

   
    @OneToMany(()=>gur_membership_fees, (gur_membership_fees: gur_membership_fees)=>gur_membership_fees.fmfUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMembershipFeess:gur_membership_fees[];
    

   
    @OneToMany(()=>gur_notifications, (gur_notifications: gur_notifications)=>gur_notifications.notByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurNotificationss:gur_notifications[];
    

   
    @OneToMany(()=>gur_orders_old, (gur_orders_old: gur_orders_old)=>gur_orders_old.ordUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrdersOlds:gur_orders_old[];
    

   
    @OneToMany(()=>gur_plan_features_map, (gur_plan_features_map: gur_plan_features_map)=>gur_plan_features_map.pfmUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanFeaturesMaps:gur_plan_features_map[];
    

   
    @OneToMany(()=>gur_plan_orders, (gur_plan_orders: gur_plan_orders)=>gur_plan_orders.ordUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurPlanOrderss:gur_plan_orders[];
    

   
    @OneToMany(()=>gur_plan_usage_log, (gur_plan_usage_log: gur_plan_usage_log)=>gur_plan_usage_log.pulUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanUsageLogs:gur_plan_usage_log[];
    

   
    @OneToMany(()=>gur_pricing_plans_map, (gur_pricing_plans_map: gur_pricing_plans_map)=>gur_pricing_plans_map.prmUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPricingPlansMaps:gur_pricing_plans_map[];
    

   
    @OneToMany(()=>gur_privacy_settings, (gur_privacy_settings: gur_privacy_settings)=>gur_privacy_settings.prvUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPrivacySettingss:gur_privacy_settings[];
    

   
    @OneToMany(()=>gur_profile_completeness_modules_user_map, (gur_profile_completeness_modules_user_map: gur_profile_completeness_modules_user_map)=>gur_profile_completeness_modules_user_map.pumUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurProfileCompletenessModulesUserMaps:gur_profile_completeness_modules_user_map[];
    

   
    @OneToMany(()=>gur_quiz_result, (gur_quiz_result: gur_quiz_result)=>gur_quiz_result.qrsUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResults:gur_quiz_result[];
    

   
    @OneToMany(()=>gur_ratings, (gur_ratings: gur_ratings)=>gur_ratings.ratRatedByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRatingss:gur_ratings[];
    

   
    @OneToMany(()=>gur_ratings, (gur_ratings: gur_ratings)=>gur_ratings.ratUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRatingss2:gur_ratings[];
    

   
    @OneToMany(()=>gur_ratings_like, (gur_ratings_like: gur_ratings_like)=>gur_ratings_like.rtlUty,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurRatingsLikes:gur_ratings_like[];
    

   
    @OneToMany(()=>gur_recruitment_history, (gur_recruitment_history: gur_recruitment_history)=>gur_recruitment_history.rehUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRecruitmentHistorys:gur_recruitment_history[];
    

   
    @OneToMany(()=>gur_sponsorship, (gur_sponsorship: gur_sponsorship)=>gur_sponsorship.spwUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSponsorships:gur_sponsorship[];
    

   
    @OneToMany(()=>gur_sports_logistics_portfolio, (gur_sports_logistics_portfolio: gur_sports_logistics_portfolio)=>gur_sports_logistics_portfolio.slpUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsPortfolios:gur_sports_logistics_portfolio[];
    

   
    @OneToMany(()=>gur_sports_usage_history, (gur_sports_usage_history: gur_sports_usage_history)=>gur_sports_usage_history.suhUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsUsageHistorys:gur_sports_usage_history[];
    

   
    @OneToMany(()=>gur_sports_vacancies, (gur_sports_vacancies: gur_sports_vacancies)=>gur_sports_vacancies.upvUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciess:gur_sports_vacancies[];
    

   
    @OneToMany(()=>gur_sports_vacancies, (gur_sports_vacancies: gur_sports_vacancies)=>gur_sports_vacancies.upvUserType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciess2:gur_sports_vacancies[];
    

   
    @OneToMany(()=>gur_sports_vacancies_applicants, (gur_sports_vacancies_applicants: gur_sports_vacancies_applicants)=>gur_sports_vacancies_applicants.upaUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciesApplicantss:gur_sports_vacancies_applicants[];
    

   
    @OneToMany(()=>gur_sports_vacancies_featured_users, (gur_sports_vacancies_featured_users: gur_sports_vacancies_featured_users)=>gur_sports_vacancies_featured_users.svuUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciesFeaturedUserss:gur_sports_vacancies_featured_users[];
    

   
    @OneToMany(()=>gur_squads_group, (gur_squads_group: gur_squads_group)=>gur_squads_group.sqdUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSquadsGroups:gur_squads_group[];
    

   
    @OneToMany(()=>gur_squads_list, (gur_squads_list: gur_squads_list)=>gur_squads_list.sqlUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSquadsLists:gur_squads_list[];
    

   
    @OneToMany(()=>gur_sub_users, (gur_sub_users: gur_sub_users)=>gur_sub_users.msuUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSubUserss:gur_sub_users[];
    

   
    @OneToMany(()=>gur_subuser_usage_history, (gur_subuser_usage_history: gur_subuser_usage_history)=>gur_subuser_usage_history.suhUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSubuserUsageHistorys:gur_subuser_usage_history[];
    

   
    @OneToMany(()=>gur_support_ticket, (gur_support_ticket: gur_support_ticket)=>gur_support_ticket.stkUtyCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTickets:gur_support_ticket[];
    

   
    @OneToMany(()=>gur_support_ticket, (gur_support_ticket: gur_support_ticket)=>gur_support_ticket.stkUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTickets2:gur_support_ticket[];
    

   
    @OneToMany(()=>gur_support_ticket_comments, (gur_support_ticket_comments: gur_support_ticket_comments)=>gur_support_ticket_comments.strUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTicketCommentss:gur_support_ticket_comments[];
    

   
    @OneToMany(()=>gur_support_ticket_logs, (gur_support_ticket_logs: gur_support_ticket_logs)=>gur_support_ticket_logs.stlUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTicketLogss:gur_support_ticket_logs[];
    

   
    @OneToMany(()=>gur_training_camp_users_map, (gur_training_camp_users_map: gur_training_camp_users_map)=>gur_training_camp_users_map.tcuUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTrainingCampUsersMaps:gur_training_camp_users_map[];
    

   
    @OneToMany(()=>gur_user_albums, (gur_user_albums: gur_user_albums)=>gur_user_albums.albUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAlbumss:gur_user_albums[];
    

   
    @OneToMany(()=>gur_user_awards_achievements, (gur_user_awards_achievements: gur_user_awards_achievements)=>gur_user_awards_achievements.uaaUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAwardsAchievementss:gur_user_awards_achievements[];
    

   
    @OneToMany(()=>gur_user_basic_moderation, (gur_user_basic_moderation: gur_user_basic_moderation)=>gur_user_basic_moderation.ubmUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasicModerations:gur_user_basic_moderation[];
    

   
    @OneToMany(()=>gur_user_leads_old, (gur_user_leads_old: gur_user_leads_old)=>gur_user_leads_old.ulsUsrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLeadsOlds:gur_user_leads_old[];
    

   
    @OneToMany(()=>gur_user_modules_map, (gur_user_modules_map: gur_user_modules_map)=>gur_user_modules_map.ummUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModulesMaps:gur_user_modules_map[];
    

   
    @OneToMany(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.pchUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlanss:gur_user_plans[];
    

   
    @OneToMany(()=>gur_user_plans_check, (gur_user_plans_check: gur_user_plans_check)=>gur_user_plans_check.pchUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansChecks:gur_user_plans_check[];
    

   
    @OneToMany(()=>gur_user_plans_check_not_used, (gur_user_plans_check_not_used: gur_user_plans_check_not_used)=>gur_user_plans_check_not_used.pchUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansCheckNotUseds:gur_user_plans_check_not_used[];
    

   
    @OneToMany(()=>gur_user_plans_details, (gur_user_plans_details: gur_user_plans_details)=>gur_user_plans_details.updUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansDetailss:gur_user_plans_details[];
    

   
    @OneToMany(()=>gur_user_profile_views, (gur_user_profile_views: gur_user_profile_views)=>gur_user_profile_views.upvForUtype,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserProfileViewss:gur_user_profile_views[];
    

   
    @OneToMany(()=>gur_user_profile_views, (gur_user_profile_views: gur_user_profile_views)=>gur_user_profile_views.upvByUtype,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserProfileViewss2:gur_user_profile_views[];
    

   
    @OneToMany(()=>gur_user_sports_map, (gur_user_sports_map: gur_user_sports_map)=>gur_user_sports_map.upmUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSportsMaps:gur_user_sports_map[];
    

   
    @OneToMany(()=>gur_user_sub_type_map, (gur_user_sub_type_map: gur_user_sub_type_map)=>gur_user_sub_type_map.usmUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSubTypeMaps:gur_user_sub_type_map[];
    

   
    @OneToMany(()=>gur_users, (gur_users: gur_users)=>gur_users.usrType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserss:gur_users[];
    

   
    @OneToMany(()=>gur_users, (gur_users: gur_users)=>gur_users.userReferredByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserss2:gur_users[];
    

   
    @OneToMany(()=>gur_users_contact_view_log, (gur_users_contact_view_log: gur_users_contact_view_log)=>gur_users_contact_view_log.cvlByType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUsersContactViewLogs:gur_users_contact_view_log[];
    

   
    @OneToMany(()=>gur_users_contact_view_log, (gur_users_contact_view_log: gur_users_contact_view_log)=>gur_users_contact_view_log.cvlForType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUsersContactViewLogs2:gur_users_contact_view_log[];
    
}
