import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_infra_products} from "./gur_infra_products";
import {gur_users} from "./gur_users";


@Entity("gur_infra_user_products" ,{schema:"sportsmatik_local" } )
@Index("iup_uty_id",["iupUty",])
@Index("iup_ipr_id",["iupIpr",])
@Index("iup_createdby_user",["iupCreatedbyUser",])
@Index("iup_user_id",["iup_user_id",])
export class gur_infra_user_products {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"iup_id"
        })
    iup_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInfraUserProductss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'iup_uty_id'})
    iupUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"iup_user_id"
        })
    iup_user_id:number;
        

    @Column("text",{ 
        nullable:false,
        name:"iup_sports"
        })
    iup_sports:string;
        

   
    @ManyToOne(()=>gur_infra_products, (gur_infra_products: gur_infra_products)=>gur_infra_products.gurInfraUserProductss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'iup_ipr_id'})
    iupIpr:gur_infra_products | null;


    @Column("varchar",{ 
        nullable:true,
        name:"iup_image"
        })
    iup_image:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:1000,
        name:"iup_description"
        })
    iup_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"iup_others"
        })
    iup_others:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"iup_status"
        })
    iup_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"iup_admin_review"
        })
    iup_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInfraUserProductss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'iup_createdby_user'})
    iupCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"iup_createdby"
        })
    iup_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"iup_modifiedby"
        })
    iup_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"iup_modified_time"
        })
    iup_modified_time:Date;
        
}
