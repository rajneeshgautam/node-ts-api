import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_survey_answers} from "./gur_survey_answers";
import {gur_survey_questions} from "./gur_survey_questions";


@Entity("gur_surveys" ,{schema:"sportsmatik_local" } )
@Index("sur_name",["sur_name",],{unique:true})
export class gur_surveys {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sur_id"
        })
    sur_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"sur_name"
        })
    sur_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sur_status"
        })
    sur_status:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sur_createdby"
        })
    sur_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sur_modifiedby"
        })
    sur_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sur_modifiedtime"
        })
    sur_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_survey_answers, (gur_survey_answers: gur_survey_answers)=>gur_survey_answers.sanSur,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSurveyAnswerss:gur_survey_answers[];
    

   
    @OneToMany(()=>gur_survey_questions, (gur_survey_questions: gur_survey_questions)=>gur_survey_questions.sqtSur,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSurveyQuestionss:gur_survey_questions[];
    
}
