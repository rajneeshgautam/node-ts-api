import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_snooker_profile" ,{schema:"sportsmatik_local" } )
@Index("sno_usr_id",["snoUsr",])
@Index("sno_createdby_user",["snoCreatedbyUser",])
export class gur_user_snooker_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sno_id"
        })
    sno_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSnookerProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sno_usr_id'})
    snoUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"sno_primary_hand"
        })
    sno_primary_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sno_modifiedtime"
        })
    sno_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSnookerProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sno_createdby_user'})
    snoCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sno_modifiedby"
        })
    sno_modifiedby:string | null;
        
}
