import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_address_book} from "./gur_matik_play_address_book";


@Entity("gur_matik_play_users" ,{schema:"sportsmatik_local" } )
export class gur_matik_play_users {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mpu_id"
        })
    mpu_id:string;
        

    @Column("int",{ 
        nullable:true,
        name:"mpu_salutation"
        })
    mpu_salutation:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpu_fname"
        })
    mpu_fname:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_mname"
        })
    mpu_mname:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_lname"
        })
    mpu_lname:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"mpu_birthday"
        })
    mpu_birthday:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","o"],
        name:"mpu_sex"
        })
    mpu_sex:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_picture"
        })
    mpu_picture:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_email"
        })
    mpu_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpu_email_verified"
        })
    mpu_email_verified:boolean;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"mpu_email_verification_time"
        })
    mpu_email_verification_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_password"
        })
    mpu_password:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"mpu_phone1"
        })
    mpu_phone1:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"mpu_phone1_otp"
        })
    mpu_phone1_otp:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"mpu_phone1_otp_time"
        })
    mpu_phone1_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpu_phone1_verified"
        })
    mpu_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'99'",
        name:"mpu_country"
        })
    mpu_country:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"mpu_state"
        })
    mpu_state:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"mpu_district"
        })
    mpu_district:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_address"
        })
    mpu_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"mpu_postcode"
        })
    mpu_postcode:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Unverified'",
        enum:["Active","Inactive","Unverified","Deleted","Deactivated"],
        name:"mpu_status"
        })
    mpu_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"mpu_refer_code"
        })
    mpu_refer_code:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpu_register_time"
        })
    mpu_register_time:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"mpu_last_login_date"
        })
    mpu_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"mpu_last_login_ip"
        })
    mpu_last_login_ip:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpu_createdby"
        })
    mpu_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"mpu_modifiedby"
        })
    mpu_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpu_modifiedtime"
        })
    mpu_modifiedtime:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"user_migrated"
        })
    user_migrated:boolean;
        

   
    @OneToMany(()=>gur_matik_play_address_book, (gur_matik_play_address_book: gur_matik_play_address_book)=>gur_matik_play_address_book.adbMpu,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayAddressBooks:gur_matik_play_address_book[];
    
}
