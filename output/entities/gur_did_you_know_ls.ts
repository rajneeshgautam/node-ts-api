import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_did_you_know} from "./gur_did_you_know";


@Entity("gur_did_you_know_ls" ,{schema:"sportsmatik_local" } )
@Index("dyl_dyk_id",["dylDyk",])
export class gur_did_you_know_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"dyl_id"
        })
    dyl_id:number;
        

   
    @ManyToOne(()=>gur_did_you_know, (gur_did_you_know: gur_did_you_know)=>gur_did_you_know.gurDidYouKnowLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dyl_dyk_id'})
    dylDyk:gur_did_you_know | null;


    @Column("varchar",{ 
        nullable:false,
        name:"dyl_text"
        })
    dyl_text:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"dyl_lng_code"
        })
    dyl_lng_code:string;
        
}
