import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_squash_stats" ,{schema:"sportsmatik_local" } )
@Index("sqs_usr_id",["sqsUsr",])
@Index("sqs_ucp_id",["sqsUcp",])
@Index("sqs_event",["sqsEvent",])
@Index("sqs_rank",["sqsRank",])
export class gur_user_squash_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"sqs_id"
        })
    sqs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSquashStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqs_usr_id'})
    sqsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserSquashStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqs_ucp_id'})
    sqsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSquashStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqs_event'})
    sqsEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"sqs_matches_played"
        })
    sqs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"sqs_matches_won"
        })
    sqs_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"sqs_matches_loss"
        })
    sqs_matches_loss:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSquashStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqs_rank'})
    sqsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sqs_modifiedtime"
        })
    sqs_modifiedtime:Date;
        
}
