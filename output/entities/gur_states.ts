import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_academies} from "./gur_academies";
import {gur_clubs} from "./gur_clubs";
import {gur_colleges} from "./gur_colleges";
import {gur_corporates} from "./gur_corporates";
import {gur_districts} from "./gur_districts";
import {gur_fitness_center} from "./gur_fitness_center";
import {gur_job} from "./gur_job";
import {gur_job_application} from "./gur_job_application";
import {gur_job_education} from "./gur_job_education";
import {gur_job_experience} from "./gur_job_experience";
import {gur_matik_play_address_book} from "./gur_matik_play_address_book";
import {gur_multiple_centres} from "./gur_multiple_centres";
import {gur_schools} from "./gur_schools";
import {gur_sports_governing_body} from "./gur_sports_governing_body";
import {gur_sports_infrastructure_company} from "./gur_sports_infrastructure_company";
import {gur_sports_marketing_company} from "./gur_sports_marketing_company";
import {gur_users} from "./gur_users";
import {gur_vendors} from "./gur_vendors";


@Entity("gur_states" ,{schema:"sportsmatik_local" } )
@Index("sts_cnt_id",["stsCnt",])
@Index("sts_name",["sts_name",])
export class gur_states {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sts_id"
        })
    sts_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sts_name"
        })
    sts_name:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurStatess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sts_cnt_id'})
    stsCnt:gur_countries | null;


    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"sts_extra_code"
        })
    sts_extra_code:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sts_gst_code"
        })
    sts_gst_code:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sts_gst_tin_code"
        })
    sts_gst_tin_code:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sts_createdby"
        })
    sts_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sts_modifiedby"
        })
    sts_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sts_modifiedtime"
        })
    sts_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_academies, (gur_academies: gur_academies)=>gur_academies.acaSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAcademiess:gur_academies[];
    

   
    @OneToMany(()=>gur_clubs, (gur_clubs: gur_clubs)=>gur_clubs.clbSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurClubss:gur_clubs[];
    

   
    @OneToMany(()=>gur_colleges, (gur_colleges: gur_colleges)=>gur_colleges.clgSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCollegess:gur_colleges[];
    

   
    @OneToMany(()=>gur_corporates, (gur_corporates: gur_corporates)=>gur_corporates.corSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatess:gur_corporates[];
    

   
    @OneToMany(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.disSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurDistrictss:gur_districts[];
    

   
    @OneToMany(()=>gur_fitness_center, (gur_fitness_center: gur_fitness_center)=>gur_fitness_center.fitSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenters:gur_fitness_center[];
    

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobs:gur_job[];
    

   
    @OneToMany(()=>gur_job_application, (gur_job_application: gur_job_application)=>gur_job_application.joaSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobApplications:gur_job_application[];
    

   
    @OneToMany(()=>gur_job_education, (gur_job_education: gur_job_education)=>gur_job_education.jedSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobEducations:gur_job_education[];
    

   
    @OneToMany(()=>gur_job_experience, (gur_job_experience: gur_job_experience)=>gur_job_experience.joeSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobExperiences:gur_job_experience[];
    

   
    @OneToMany(()=>gur_matik_play_address_book, (gur_matik_play_address_book: gur_matik_play_address_book)=>gur_matik_play_address_book.adbSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayAddressBooks:gur_matik_play_address_book[];
    

   
    @OneToMany(()=>gur_multiple_centres, (gur_multiple_centres: gur_multiple_centres)=>gur_multiple_centres.mlcSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMultipleCentress:gur_multiple_centres[];
    

   
    @OneToMany(()=>gur_schools, (gur_schools: gur_schools)=>gur_schools.schSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSchoolss:gur_schools[];
    

   
    @OneToMany(()=>gur_sports_governing_body, (gur_sports_governing_body: gur_sports_governing_body)=>gur_sports_governing_body.sgbSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodys:gur_sports_governing_body[];
    

   
    @OneToMany(()=>gur_sports_infrastructure_company, (gur_sports_infrastructure_company: gur_sports_infrastructure_company)=>gur_sports_infrastructure_company.sicSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsInfrastructureCompanys:gur_sports_infrastructure_company[];
    

   
    @OneToMany(()=>gur_sports_marketing_company, (gur_sports_marketing_company: gur_sports_marketing_company)=>gur_sports_marketing_company.smcSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsMarketingCompanys:gur_sports_marketing_company[];
    

   
    @OneToMany(()=>gur_users, (gur_users: gur_users)=>gur_users.usrSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserss:gur_users[];
    

   
    @OneToMany(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.venSts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorss:gur_vendors[];
    
}
