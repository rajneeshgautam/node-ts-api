import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_wiki_knowhow_events} from "./gur_wiki_knowhow_events";
import {gur_wiki_knowhow_ls} from "./gur_wiki_knowhow_ls";
import {gur_wiki_knowhow_players_officials} from "./gur_wiki_knowhow_players_officials";
import {gur_wiki_knowhow_rules} from "./gur_wiki_knowhow_rules";
import {gur_wiki_knowhow_techniques} from "./gur_wiki_knowhow_techniques";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";


@Entity("gur_wiki_knowhow" ,{schema:"sportsmatik_local" } )
@Index("wkh_spo_id",["wkhSpo",],{unique:true})
export class gur_wiki_knowhow {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wkh_id"
        })
    wkh_id:string;
        

   
    @OneToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiKnowhow,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wkh_spo_id'})
    wkhSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"wkh_sort_order"
        })
    wkh_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wkh_photo"
        })
    wkh_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wkh_photo_source_details"
        })
    wkh_photo_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wkh_photo_title"
        })
    wkh_photo_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wkh_createdby"
        })
    wkh_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wkh_modifiedby"
        })
    wkh_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wkh_modifiedtime"
        })
    wkh_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_knowhow_events, (gur_wiki_knowhow_events: gur_wiki_knowhow_events)=>gur_wiki_knowhow_events.wevWkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowEventss:gur_wiki_knowhow_events[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_ls, (gur_wiki_knowhow_ls: gur_wiki_knowhow_ls)=>gur_wiki_knowhow_ls.khlWkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowLss:gur_wiki_knowhow_ls[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_players_officials, (gur_wiki_knowhow_players_officials: gur_wiki_knowhow_players_officials)=>gur_wiki_knowhow_players_officials.pmpWkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowPlayersOfficialss:gur_wiki_knowhow_players_officials[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_rules, (gur_wiki_knowhow_rules: gur_wiki_knowhow_rules)=>gur_wiki_knowhow_rules.wrlWkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowRuless:gur_wiki_knowhow_rules[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_techniques, (gur_wiki_knowhow_techniques: gur_wiki_knowhow_techniques)=>gur_wiki_knowhow_techniques.tecWkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowTechniquess:gur_wiki_knowhow_techniques[];
    

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgWkh,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    
}
