import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_blog_categories} from "./gur_blog_categories";
import {gur_languages} from "./gur_languages";


@Entity("gur_blog_categories_ls" ,{schema:"sportsmatik_local" } )
@Index("blc_name_unique",["bcl_name","bclLngCode",],{unique:true})
@Index("bcl_bct_id",["bclBct",])
@Index("bcl_lng_code",["bclLngCode",])
export class gur_blog_categories_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bcl_id"
        })
    bcl_id:number;
        

   
    @ManyToOne(()=>gur_blog_categories, (gur_blog_categories: gur_blog_categories)=>gur_blog_categories.gurBlogCategoriesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bcl_bct_id'})
    bclBct:gur_blog_categories | null;


    @Column("varchar",{ 
        nullable:true,
        name:"bcl_name"
        })
    bcl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"bcl_descriptions"
        })
    bcl_descriptions:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bcl_meta_title"
        })
    bcl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bcl_meta_description"
        })
    bcl_meta_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"bcl_meta_keywords"
        })
    bcl_meta_keywords:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurBlogCategoriesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bcl_lng_code'})
    bclLngCode:gur_languages | null;

}
