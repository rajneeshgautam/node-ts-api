import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_users} from "./gur_matik_play_users";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";
import {gur_countries} from "./gur_countries";


@Entity("gur_matik_play_address_book" ,{schema:"sportsmatik_local" } )
@Index("evt_dist_city",["adbDistCity",])
@Index("evt_country",["adbCountry",])
@Index("adb_user_id",["adbMpu",])
@Index("adb_sts_id",["adbSts",])
export class gur_matik_play_address_book {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"adb_id"
        })
    adb_id:string;
        

   
    @ManyToOne(()=>gur_matik_play_users, (gur_matik_play_users: gur_matik_play_users)=>gur_matik_play_users.gurMatikPlayAddressBooks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adb_mpu_id'})
    adbMpu:gur_matik_play_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"adb_name"
        })
    adb_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"adb_street_address1"
        })
    adb_street_address1:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"adb_street_address2"
        })
    adb_street_address2:string | null;
        

   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurMatikPlayAddressBooks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adb_dist_city'})
    adbDistCity:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurMatikPlayAddressBooks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adb_sts_id'})
    adbSts:gur_states | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikPlayAddressBooks,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adb_country'})
    adbCountry:gur_countries | null;


    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"adb_pincode"
        })
    adb_pincode:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"adb_email"
        })
    adb_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"adb_phone"
        })
    adb_phone:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"adb_createdby"
        })
    adb_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"adb_modifiedby"
        })
    adb_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"adb_modifiedtime"
        })
    adb_modifiedtime:Date;
        
}
