import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events_players_bkup} from "./gur_world_events_players_bkup";


@Entity("gur_world_events_players_ls_bakup" ,{schema:"sportsmatik_local" } )
@Index("gur_world_events_players_ls_bakup_ibfk_1",["wplWep",])
export class gur_world_events_players_ls_bakup {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"wpl_id"
        })
    wpl_id:number;
        

   
    @ManyToOne(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.gurWorldEventsPlayersLsBakups,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wpl_wep_id'})
    wplWep:gur_world_events_players_bkup | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wpl_name"
        })
    wpl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_description"
        })
    wpl_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_records"
        })
    wpl_records:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"wpl_lng_code"
        })
    wpl_lng_code:string;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_key_points"
        })
    wpl_key_points:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wpl_designation"
        })
    wpl_designation:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wpl_profession"
        })
    wpl_profession:string | null;
        
}
