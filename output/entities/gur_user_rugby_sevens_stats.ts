import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_rugby_sevens_stats" ,{schema:"sportsmatik_local" } )
@Index("rgs_usr_id",["rgsUsr",])
@Index("rgs_ucp_id",["rgsUcp",])
export class gur_user_rugby_sevens_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"rgs_id"
        })
    rgs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbySevensStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgs_usr_id'})
    rgsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserRugbySevensStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgs_ucp_id'})
    rgsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"rgs_matches_played"
        })
    rgs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"rgs_matches_starts"
        })
    rgs_matches_starts:number;
        

    @Column("int",{ 
        nullable:false,
        name:"rgs_subs_ins"
        })
    rgs_subs_ins:number;
        

    @Column("int",{ 
        nullable:false,
        name:"rgs_matches_won"
        })
    rgs_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"rgs_matches_lost"
        })
    rgs_matches_lost:number;
        

    @Column("int",{ 
        nullable:false,
        name:"rgs_matches_draw"
        })
    rgs_matches_draw:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"rgs_points"
        })
    rgs_points:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"rgs_tries"
        })
    rgs_tries:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"rgs_conversion"
        })
    rgs_conversion:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"rgs_penalties"
        })
    rgs_penalties:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"rgs_drop_goals"
        })
    rgs_drop_goals:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"rgs_red_cards"
        })
    rgs_red_cards:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"rgs_yellow_cards"
        })
    rgs_yellow_cards:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rgs_modifiedtime"
        })
    rgs_modifiedtime:Date;
        
}
