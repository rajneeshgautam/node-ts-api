import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list} from "./gur_value_list";
import {gur_countries} from "./gur_countries";
import {gur_states} from "./gur_states";
import {gur_districts} from "./gur_districts";
import {gur_vendor_categories_map} from "./gur_vendor_categories_map";
import {gur_vendors_branches} from "./gur_vendors_branches";
import {gur_vendors_brands_map} from "./gur_vendors_brands_map";
import {gur_vendors_gallery_media} from "./gur_vendors_gallery_media";
import {gur_vendors_products} from "./gur_vendors_products";
import {gur_vendors_sports_map} from "./gur_vendors_sports_map";
import {gur_vendor_type} from "./gur_vendor_type";


@Entity("gur_vendors" ,{schema:"sportsmatik_local" } )
@Index("ven_slug",["ven_slug",],{unique:true})
@Index("ven_gst_number",["ven_gst_number",],{unique:true})
@Index("ven_phone1",["ven_phone1_old",],{unique:true})
@Index("ven_phone2",["ven_phone2_old",],{unique:true})
@Index("ven_bussiness_name",["ven_bussiness_name",])
@Index("ven_country",["venCountry",])
@Index("ven_state",["venSts",])
@Index("ven_city",["venDis",])
@Index("ven_salutation",["venSalutation",])
@Index("ven_fname",["ven_fname_old","ven_lname_old",])
export class gur_vendors {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ven_id"
        })
    ven_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"ven_slug"
        })
    ven_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ven_pin"
        })
    ven_pin:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurVendorss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ven_salutation'})
    venSalutation:gur_value_list | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ven_fname_old"
        })
    ven_fname_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_fname"
        })
    ven_fname:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_mname_old"
        })
    ven_mname_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_mname"
        })
    ven_mname:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_lname_old"
        })
    ven_lname_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_lname"
        })
    ven_lname:Buffer | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["f","m","o"],
        name:"ven_sex"
        })
    ven_sex:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"ven_birthday_old"
        })
    ven_birthday_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_birthday"
        })
    ven_birthday:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_bussiness_name"
        })
    ven_bussiness_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ven_password"
        })
    ven_password:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_email_old"
        })
    ven_email_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_email"
        })
    ven_email:Buffer | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_email_verified"
        })
    ven_email_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"ven_phone1_old"
        })
    ven_phone1_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_phone1"
        })
    ven_phone1:Buffer | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_phone1_otp"
        })
    ven_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_phone1_otp_time"
        })
    ven_phone1_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_phone1_verified"
        })
    ven_phone1_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"ven_phone2_old"
        })
    ven_phone2_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"ven_phone2"
        })
    ven_phone2:Buffer | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ven_phone2_otp"
        })
    ven_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_phone2_otp_time"
        })
    ven_phone2_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_phone2_verified"
        })
    ven_phone2_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_address"
        })
    ven_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_address2"
        })
    ven_address2:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurVendorss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ven_country'})
    venCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurVendorss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ven_sts_id'})
    venSts:gur_states | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurVendorss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ven_dis_id'})
    venDis:gur_districts | null;


    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ven_pincode"
        })
    ven_pincode:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ven_address_google_map"
        })
    ven_address_google_map:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Unverified'",
        enum:["Active","Inactive","Deleted","Unverified","Deactivated"],
        name:"ven_status"
        })
    ven_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_profile_pic"
        })
    ven_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_timeline_pic"
        })
    ven_timeline_pic:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ven_profile_summary"
        })
    ven_profile_summary:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"ven_intro_modifiedtime"
        })
    ven_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_email_verify_time"
        })
    ven_email_verify_time:Date | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ven_profile_completeness"
        })
    ven_profile_completeness:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_basic_profile_fill"
        })
    ven_basic_profile_fill:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"ven_gst_number"
        })
    ven_gst_number:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_last_login_date"
        })
    ven_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ven_last_login_ip"
        })
    ven_last_login_ip:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"ven_register_date"
        })
    ven_register_date:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_profile_pic_modifiedtime"
        })
    ven_profile_pic_modifiedtime:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"ven_password_reset_pin"
        })
    ven_password_reset_pin:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"ven_password_reset_time"
        })
    ven_password_reset_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ven_viewable"
        })
    ven_viewable:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_google_id"
        })
    ven_google_id:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'2'",
        name:"ven_dob_change"
        })
    ven_dob_change:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_is_user_verified"
        })
    ven_is_user_verified:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ven_createdby"
        })
    ven_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"ven_modifiedby"
        })
    ven_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ven_modifiedtime"
        })
    ven_modifiedtime:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ven_show_in_knowhow"
        })
    ven_show_in_knowhow:boolean;
        

   
    @OneToMany(()=>gur_vendor_categories_map, (gur_vendor_categories_map: gur_vendor_categories_map)=>gur_vendor_categories_map.vcmVen,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorCategoriesMaps:gur_vendor_categories_map[];
    

   
    @OneToMany(()=>gur_vendors_branches, (gur_vendors_branches: gur_vendors_branches)=>gur_vendors_branches.vbrVen,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsBranchess:gur_vendors_branches[];
    

   
    @OneToMany(()=>gur_vendors_brands_map, (gur_vendors_brands_map: gur_vendors_brands_map)=>gur_vendors_brands_map.vbmVen,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsBrandsMaps:gur_vendors_brands_map[];
    

   
    @OneToMany(()=>gur_vendors_gallery_media, (gur_vendors_gallery_media: gur_vendors_gallery_media)=>gur_vendors_gallery_media.vgmVen,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsGalleryMedias:gur_vendors_gallery_media[];
    

   
    @OneToMany(()=>gur_vendors_products, (gur_vendors_products: gur_vendors_products)=>gur_vendors_products.vprVen,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsProductss:gur_vendors_products[];
    

   
    @OneToMany(()=>gur_vendors_sports_map, (gur_vendors_sports_map: gur_vendors_sports_map)=>gur_vendors_sports_map.vsmUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsSportsMaps:gur_vendors_sports_map[];
    

   
    @ManyToMany(()=>gur_vendor_type, (gur_vendor_type: gur_vendor_type)=>gur_vendor_type.gurVendorss,{  nullable:false, })
    @JoinTable({ name:'gur_vendors_type_map'})
    gurVendorTypes:gur_vendor_type[];
    
}
