import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_qualifications} from "./gur_user_qualifications";


@Entity("gur_educational_language_medium" ,{schema:"sportsmatik_local" } )
@Index("elm_name",["elm_name",],{unique:true})
export class gur_educational_language_medium {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"elm_id"
        })
    elm_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"elm_name"
        })
    elm_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"elm_status"
        })
    elm_status:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"elm_createdby"
        })
    elm_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"elm_modifiedby"
        })
    elm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"elm_modifiedtime"
        })
    elm_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfMediumName,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    
}
