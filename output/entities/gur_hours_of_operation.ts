import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_hours_of_operation_map} from "./gur_hours_of_operation_map";


@Entity("gur_hours_of_operation" ,{schema:"sportsmatik_local" } )
@Index("hop_uty_id",["hopUty",])
@Index("hop_createdby_user",["hopCreatedbyUser",])
export class gur_hours_of_operation {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"hop_id"
        })
    hop_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurHoursOfOperations,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hop_uty_id'})
    hopUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"hop_user_id"
        })
    hop_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"hop_day"
        })
    hop_day:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurHoursOfOperations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hop_createdby_user'})
    hopCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"hop_modifiedby"
        })
    hop_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hop_modifiedtime"
        })
    hop_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_hours_of_operation_map, (gur_hours_of_operation_map: gur_hours_of_operation_map)=>gur_hours_of_operation_map.homHop,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    gurHoursOfOperationMaps:gur_hours_of_operation_map[];
    
}
