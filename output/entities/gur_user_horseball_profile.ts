import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_horseball_profile" ,{schema:"sportsmatik_local" } )
@Index("hrb_usr_id",["hrbUsr",])
@Index("hrb_createdby_user",["hrbCreatedbyUser",])
export class gur_user_horseball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"hrb_id"
        })
    hrb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserHorseballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hrb_usr_id'})
    hrbUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hrb_modifiedtime"
        })
    hrb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserHorseballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hrb_createdby_user'})
    hrbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"hrb_modifiedby"
        })
    hrb_modifiedby:string | null;
        
}
