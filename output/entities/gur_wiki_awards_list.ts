import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_awards_sports_wise} from "./gur_wiki_awards_sports_wise";
import {gur_wiki_awards_country_wise} from "./gur_wiki_awards_country_wise";
import {gur_wiki_awards_list_ls} from "./gur_wiki_awards_list_ls";


@Entity("gur_wiki_awards_list" ,{schema:"sportsmatik_local" } )
@Index("wal_was_id",["walWas",])
@Index("wal_wac_id",["walWac",])
export class gur_wiki_awards_list {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wal_id"
        })
    wal_id:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["country","sports"],
        name:"wal_tye"
        })
    wal_tye:string;
        

   
    @ManyToOne(()=>gur_wiki_awards_sports_wise, (gur_wiki_awards_sports_wise: gur_wiki_awards_sports_wise)=>gur_wiki_awards_sports_wise.gurWikiAwardsLists,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wal_was_id'})
    walWas:gur_wiki_awards_sports_wise | null;


   
    @ManyToOne(()=>gur_wiki_awards_country_wise, (gur_wiki_awards_country_wise: gur_wiki_awards_country_wise)=>gur_wiki_awards_country_wise.gurWikiAwardsLists,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wal_wac_id'})
    walWac:gur_wiki_awards_country_wise | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wal_photo"
        })
    wal_photo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wal_photo_title"
        })
    wal_photo_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wal_photo_alt"
        })
    wal_photo_alt:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["url","file"],
        name:"wal_video_type"
        })
    wal_video_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wal_video"
        })
    wal_video:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wal_video_title"
        })
    wal_video_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wal_video_alt"
        })
    wal_video_alt:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wal_sources_detail"
        })
    wal_sources_detail:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"wal_sort_order"
        })
    wal_sort_order:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wal_createdby"
        })
    wal_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wal_modifiedby"
        })
    wal_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wal_modifiedtime"
        })
    wal_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_awards_list_ls, (gur_wiki_awards_list_ls: gur_wiki_awards_list_ls)=>gur_wiki_awards_list_ls.wllWal,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsListLss:gur_wiki_awards_list_ls[];
    
}
