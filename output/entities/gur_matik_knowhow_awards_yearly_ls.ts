import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_awards_yearly} from "./gur_matik_knowhow_awards_yearly";


@Entity("gur_matik_knowhow_awards_yearly_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_awards_yearly_ls_ibfk_1",["wylAwy",])
export class gur_matik_knowhow_awards_yearly_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wyl_id"
        })
    wyl_id:number;
        

   
    @ManyToOne(()=>gur_matik_knowhow_awards_yearly, (gur_matik_knowhow_awards_yearly: gur_matik_knowhow_awards_yearly)=>gur_matik_knowhow_awards_yearly.gurMatikKnowhowAwardsYearlyLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wyl_awy_id'})
    wylAwy:gur_matik_knowhow_awards_yearly | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wyl_title"
        })
    wyl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wyl_description"
        })
    wyl_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:4,
        default: () => "'en'",
        name:"wyl_lng_code"
        })
    wyl_lng_code:string | null;
        
}
