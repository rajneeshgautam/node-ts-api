import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";
import {gur_wiki_terminologies_ls} from "./gur_wiki_terminologies_ls";


@Entity("gur_wiki_terminologies" ,{schema:"sportsmatik_local" } )
@Index("ter_spo_id",["terSpo",])
export class gur_wiki_terminologies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ter_id"
        })
    ter_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiTerminologiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ter_spo_id'})
    terSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"ter_sort_order"
        })
    ter_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ter_createdby"
        })
    ter_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"ter_modifiedby"
        })
    ter_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ter_modifiedtime"
        })
    ter_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgTer,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    

   
    @OneToMany(()=>gur_wiki_terminologies_ls, (gur_wiki_terminologies_ls: gur_wiki_terminologies_ls)=>gur_wiki_terminologies_ls.trlTer,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiTerminologiesLss:gur_wiki_terminologies_ls[];
    
}
