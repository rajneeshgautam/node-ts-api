import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_job_category} from "./gur_job_category";
import {gur_job_profile} from "./gur_job_profile";
import {gur_countries} from "./gur_countries";
import {gur_states} from "./gur_states";
import {gur_districts} from "./gur_districts";
import {gur_admin_users} from "./gur_admin_users";
import {gur_job_application} from "./gur_job_application";


@Entity("gur_job" ,{schema:"sportsmatik_local" } )
@Index("job_slug",["job_slug",],{unique:true})
@Index("job_joc_id",["jobJoc",])
@Index("job_jop_id",["jobJop",])
@Index("job_cnt_id",["jobCnt",])
@Index("job_sts_id",["jobSts",])
@Index("job_dis_id",["jobDis",])
@Index("job_created_by",["jobCreatedBy",])
@Index("job_updated_by",["jobUpdatedBy",])
export class gur_job {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"job_id"
        })
    job_id:number;
        

   
    @ManyToOne(()=>gur_job_category, (gur_job_category: gur_job_category)=>gur_job_category.gurJobs,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_joc_id'})
    jobJoc:gur_job_category | null;


   
    @ManyToOne(()=>gur_job_profile, (gur_job_profile: gur_job_profile)=>gur_job_profile.gurJobs,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_jop_id'})
    jobJop:gur_job_profile | null;


    @Column("enum",{ 
        nullable:false,
        enum:["full-time","part-time"],
        name:"job_type"
        })
    job_type:string;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"job_open_vacancy"
        })
    job_open_vacancy:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"job_title"
        })
    job_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"job_slug"
        })
    job_slug:string;
        

    @Column("text",{ 
        nullable:false,
        name:"job_description"
        })
    job_description:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurJobs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_cnt_id'})
    jobCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurJobs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_sts_id'})
    jobSts:gur_states | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurJobs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_dis_id'})
    jobDis:gur_districts | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"job_is_active"
        })
    job_is_active:boolean;
        

    @Column("date",{ 
        nullable:true,
        name:"job_start_date"
        })
    job_start_date:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"job_end_date"
        })
    job_end_date:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"job_created_at"
        })
    job_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"job_updated_at"
        })
    job_updated_at:Date | null;
        

   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurJobs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_created_by'})
    jobCreatedBy:gur_admin_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurJobs2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'job_updated_by'})
    jobUpdatedBy:gur_admin_users | null;


   
    @OneToMany(()=>gur_job_application, (gur_job_application: gur_job_application)=>gur_job_application.joaJob,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobApplications:gur_job_application[];
    
}
