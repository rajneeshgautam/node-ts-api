import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_rodeo_profile" ,{schema:"sportsmatik_local" } )
@Index("rod_usr_id",["rodUsr",])
@Index("rod_createdby_user",["rodCreatedbyUser",])
export class gur_user_rodeo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"rod_id"
        })
    rod_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRodeoProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rod_usr_id'})
    rodUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"rod_events"
        })
    rod_events:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rod_modifiedtime"
        })
    rod_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRodeoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rod_createdby_user'})
    rodCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rod_modifiedby"
        })
    rod_modifiedby:string | null;
        
}
