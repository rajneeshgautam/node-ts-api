import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_badminton_profile" ,{schema:"sportsmatik_local" } )
@Index("bad_usr_id",["badUsr",])
@Index("bad_createdby_user",["badCreatedbyUser",])
export class gur_user_badminton_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"bad_id"
        })
    bad_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBadmintonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bad_usr_id'})
    badUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"bad_primary_hand"
        })
    bad_primary_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bad_modifiedtime"
        })
    bad_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBadmintonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bad_createdby_user'})
    badCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bad_modifiedby"
        })
    bad_modifiedby:string | null;
        
}
