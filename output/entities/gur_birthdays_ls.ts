import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthdays} from "./gur_birthdays";
import {gur_birthdays_photos} from "./gur_birthdays_photos";
import {gur_languages} from "./gur_languages";


@Entity("gur_birthdays_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_birthdays_ls_old_ibfk_1",["bilBir",])
@Index("gur_birthdays_ls_old_ibfk_2",["bilBrp",])
@Index("gur_birthdays_ls_old_ibfk_3",["bilLngCode",])
export class gur_birthdays_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bil_id"
        })
    bil_id:number;
        

   
    @ManyToOne(()=>gur_birthdays, (gur_birthdays: gur_birthdays)=>gur_birthdays.gurBirthdaysLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bil_bir_id'})
    bilBir:gur_birthdays | null;


   
    @ManyToOne(()=>gur_birthdays_photos, (gur_birthdays_photos: gur_birthdays_photos)=>gur_birthdays_photos.gurBirthdaysLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bil_brp_id'})
    bilBrp:gur_birthdays_photos | null;


    @Column("varchar",{ 
        nullable:true,
        name:"bil_name"
        })
    bil_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bil_profession"
        })
    bil_profession:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"bil_designation"
        })
    bil_designation:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"bil_achievements"
        })
    bil_achievements:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurBirthdaysLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bil_lng_code'})
    bilLngCode:gur_languages | null;

}
