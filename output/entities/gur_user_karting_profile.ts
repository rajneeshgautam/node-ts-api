import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_karting_profile" ,{schema:"sportsmatik_local" } )
@Index("kar_usr_id",["karUsr",])
@Index("kar_createdby_user",["kar_createdby_user",])
export class gur_user_karting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kar_id"
        })
    kar_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKartingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kar_usr_id'})
    karUsr:gur_users | null;


    @Column("year",{ 
        nullable:true,
        name:"kar_debut_date"
        })
    kar_debut_date:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kar_debut_competition"
        })
    kar_debut_competition:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kar_current_team"
        })
    kar_current_team:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kar_modifiedtime"
        })
    kar_modifiedtime:Date;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"kar_createdby_user"
        })
    kar_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kar_modifiedby"
        })
    kar_modifiedby:string | null;
        
}
