import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";
import {gur_sports} from "./gur_sports";


@Entity("gur_vendors_sports_map" ,{schema:"sportsmatik_local" } )
@Index("sport_once",["vsmSpo","vsmUser",],{unique:true})
@Index("upm_usr_id",["vsmUser",])
export class gur_vendors_sports_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"vsm_id"
        })
    vsm_id:string;
        

   
    @ManyToOne(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorsSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vsm_user_id'})
    vsmUser:gur_vendors | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurVendorsSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vsm_spo_id'})
    vsmSpo:gur_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"vsm_status"
        })
    vsm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"vsm_admin_review"
        })
    vsm_admin_review:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:1000,
        name:"vsm_description"
        })
    vsm_description:string | null;
        

    @Column("date",{ 
        nullable:false,
        name:"vsm_date_created"
        })
    vsm_date_created:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"vsm_createdby"
        })
    vsm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vsm_modifiedby"
        })
    vsm_modifiedby:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"vsm_date_modified"
        })
    vsm_date_modified:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vsm_type"
        })
    vsm_type:boolean;
        

    @Column("bigint",{ 
        nullable:true,
        name:"vsm_pch_id"
        })
    vsm_pch_id:string | null;
        
}
