import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_cycling_profile" ,{schema:"sportsmatik_local" } )
@Index("cyc_usr_id",["cycUsr",])
@Index("cyc_createdby_user",["cycCreatedbyUser",])
export class gur_user_cycling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cyc_id"
        })
    cyc_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCyclingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cyc_usr_id'})
    cycUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"cyc_cyclist_type"
        })
    cyc_cyclist_type:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"cyc_track"
        })
    cyc_track:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"cyc_road"
        })
    cyc_road:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"cyc_mountain"
        })
    cyc_mountain:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"cyc_bmx"
        })
    cyc_bmx:boolean | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cyc_modifiedtime"
        })
    cyc_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCyclingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cyc_createdby_user'})
    cycCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"cyc_modifiedby"
        })
    cyc_modifiedby:string | null;
        
}
