import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_racquetball_profile" ,{schema:"sportsmatik_local" } )
@Index("raq_usr_id",["raqUsr",])
@Index("raq_createdby_user",["raqCreatedbyUser",])
export class gur_user_racquetball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"raq_id"
        })
    raq_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRacquetballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'raq_usr_id'})
    raqUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"raq_hand"
        })
    raq_hand:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"raq_modifiedtime"
        })
    raq_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRacquetballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'raq_createdby_user'})
    raqCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"raq_modifiedby"
        })
    raq_modifiedby:string | null;
        
}
