import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_support_ticket} from "./gur_support_ticket";
import {gur_user_types} from "./gur_user_types";
import {gur_admin_users} from "./gur_admin_users";
import {gur_support_ticket_logs} from "./gur_support_ticket_logs";


@Entity("gur_support_ticket_comments" ,{schema:"sportsmatik_local" } )
@Index("stk_usr_id",["str_usr_id",])
@Index("stk_adm_id",["strAdm",])
@Index("str_stk_id",["strStk",])
@Index("str_usr_type",["strUsrType",])
export class gur_support_ticket_comments {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"str_id"
        })
    str_id:string;
        

   
    @ManyToOne(()=>gur_support_ticket, (gur_support_ticket: gur_support_ticket)=>gur_support_ticket.gurSupportTicketCommentss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'str_stk_id'})
    strStk:gur_support_ticket | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSupportTicketCommentss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'str_usr_type'})
    strUsrType:gur_user_types | null;


    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"str_usr_id"
        })
    str_usr_id:number | null;
        

   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurSupportTicketCommentss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'str_adm_id'})
    strAdm:gur_admin_users | null;


    @Column("text",{ 
        nullable:true,
        name:"str_message"
        })
    str_message:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"str_attachment1"
        })
    str_attachment1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"str_attachment2"
        })
    str_attachment2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"str_attachment3"
        })
    str_attachment3:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"str_internal_note"
        })
    str_internal_note:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"str_last_replied"
        })
    str_last_replied:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"str_createdby"
        })
    str_createdby:string | null;
        

   
    @OneToMany(()=>gur_support_ticket_logs, (gur_support_ticket_logs: gur_support_ticket_logs)=>gur_support_ticket_logs.stlStr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTicketLogss:gur_support_ticket_logs[];
    
}
