import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_plans} from "./gur_user_plans";
import {gur_plan_orders} from "./gur_plan_orders";
import {gur_plans} from "./gur_plans";


@Entity("gur_user_plans_addons" ,{schema:"sportsmatik_local" } )
@Index("gur_user_plans_addons_ibfk_1",["upaPch",])
@Index("gur_user_plans_addons_ibfk_2",["upaOrd",])
@Index("gur_user_plans_addons_ibfk_3",["upaPln",])
export class gur_user_plans_addons {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"upa_id"
        })
    upa_id:string;
        

   
    @ManyToOne(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.gurUserPlansAddonss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upa_pch_id'})
    upaPch:gur_user_plans | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"upa_credits_left"
        })
    upa_credits_left:number;
        

    @Column("int",{ 
        nullable:false,
        name:"upa_credits"
        })
    upa_credits:number;
        

   
    @ManyToOne(()=>gur_plan_orders, (gur_plan_orders: gur_plan_orders)=>gur_plan_orders.gurUserPlansAddonss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upa_ord_id'})
    upaOrd:gur_plan_orders | null;


   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurUserPlansAddonss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upa_pln_id'})
    upaPln:gur_plans | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"upa_purchased_on"
        })
    upa_purchased_on:Date;
        
}
