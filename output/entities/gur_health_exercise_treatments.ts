import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_health_exercise_treatments_ls} from "./gur_health_exercise_treatments_ls";


@Entity("gur_health_exercise_treatments" ,{schema:"sportsmatik_local" } )
export class gur_health_exercise_treatments {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"het_id"
        })
    het_id:number;
        

    @Column("text",{ 
        nullable:false,
        name:"het_sports"
        })
    het_sports:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"het_photo"
        })
    het_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"het_photo_data"
        })
    het_photo_data:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"het_createdby"
        })
    het_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"het_modifiedby"
        })
    het_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"het_modifiedtime"
        })
    het_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_health_exercise_treatments_ls, (gur_health_exercise_treatments_ls: gur_health_exercise_treatments_ls)=>gur_health_exercise_treatments_ls.htlHet,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthExerciseTreatmentsLss:gur_health_exercise_treatments_ls[];
    
}
