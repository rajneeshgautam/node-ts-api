import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow_techniques} from "./gur_wiki_knowhow_techniques";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_knowhow_techniques_ls" ,{schema:"sportsmatik_local" } )
@Index("tel_tec_id",["telTec",])
@Index("tel_lng_code",["telLngCode",])
export class gur_wiki_knowhow_techniques_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"tel_id"
        })
    tel_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow_techniques, (gur_wiki_knowhow_techniques: gur_wiki_knowhow_techniques)=>gur_wiki_knowhow_techniques.gurWikiKnowhowTechniquesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tel_tec_id'})
    telTec:gur_wiki_knowhow_techniques | null;


    @Column("varchar",{ 
        nullable:false,
        name:"tel_name"
        })
    tel_name:string;
        

    @Column("text",{ 
        nullable:true,
        name:"tel_desc"
        })
    tel_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiKnowhowTechniquesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tel_lng_code'})
    telLngCode:gur_languages | null;

}
