import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_health_consulting_entities" ,{schema:"sportsmatik_local" } )
@Index("hce_user_id",["hce_user_id",])
@Index("gur_health_consulting_entities_ibfk_1",["hceUty",])
export class gur_health_consulting_entities {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"hce_id"
        })
    hce_id:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"hce_user_id"
        })
    hce_user_id:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurHealthConsultingEntitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hce_uty_id'})
    hceUty:gur_user_types | null;


    @Column("text",{ 
        nullable:true,
        name:"hce_description"
        })
    hce_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"hce_createdby"
        })
    hce_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hce_modifiedby"
        })
    hce_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hce_modifiedtime"
        })
    hce_modifiedtime:Date;
        
}
