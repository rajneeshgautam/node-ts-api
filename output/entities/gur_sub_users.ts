import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_sub_users" ,{schema:"sportsmatik_local" } )
@Index("uaa_uty_id",["msuUty",])
@Index("msu_sub_user",["msuSubUser",])
export class gur_sub_users {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"msu_id"
        })
    msu_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSubUserss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'msu_uty_id'})
    msuUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"msu_user_id"
        })
    msu_user_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSubUserss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'msu_sub_user'})
    msuSubUser:gur_users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"msu_type"
        })
    msu_type:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'2'",
        name:"msu_status"
        })
    msu_status:boolean;
        

    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"msu_pch_id"
        })
    msu_pch_id:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"msu_createdon"
        })
    msu_createdon:Date | null;
        

    @Column("date",{ 
        nullable:true,
        name:"msu_date_expired"
        })
    msu_date_expired:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"msu_modifiedtime"
        })
    msu_modifiedtime:Date;
        
}
