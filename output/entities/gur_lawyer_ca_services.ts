import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_law_ca_services_categories} from "./gur_law_ca_services_categories";
import {gur_users} from "./gur_users";


@Entity("gur_lawyer_ca_services" ,{schema:"sportsmatik_local" } )
@Index("unique_service",["lwc_user_id","lwcUserType","lwcService",],{unique:true})
@Index("lwc_service",["lwcService",])
@Index("lwc_created_by_user",["lwcCreatedByUser",])
@Index("lwc_user_type",["lwcUserType",])
export class gur_lawyer_ca_services {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"lwc_id"
        })
    lwc_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"lwc_user_id"
        })
    lwc_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurLawyerCaServicess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwc_user_type'})
    lwcUserType:gur_user_types | null;


   
    @ManyToOne(()=>gur_law_ca_services_categories, (gur_law_ca_services_categories: gur_law_ca_services_categories)=>gur_law_ca_services_categories.gurLawyerCaServicess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwc_service'})
    lwcService:gur_law_ca_services_categories | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"lwc_description"
        })
    lwc_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"lwc_status"
        })
    lwc_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lwc_admin_review"
        })
    lwc_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurLawyerCaServicess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwc_created_by_user'})
    lwcCreatedByUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"lwc_createdby"
        })
    lwc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lwc_modifiedby"
        })
    lwc_modifiedby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lwc_modified_time"
        })
    lwc_modified_time:string | null;
        
}
