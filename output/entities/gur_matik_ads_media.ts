import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_ads} from "./gur_matik_ads";


@Entity("gur_matik_ads_media" ,{schema:"sportsmatik_local" } )
@Index("adm_ads_id",["admAds",])
export class gur_matik_ads_media {

   
    @ManyToOne(()=>gur_matik_ads, (gur_matik_ads: gur_matik_ads)=>gur_matik_ads.gurMatikAdsMedias,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adm_ads_id'})
    admAds:gur_matik_ads | null;


    @Column("varchar",{ 
        nullable:false,
        name:"adm_media"
        })
    adm_media:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adm_primary"
        })
    adm_primary:boolean | null;
        
}
