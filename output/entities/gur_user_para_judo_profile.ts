import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_judo_profile" ,{schema:"sportsmatik_local" } )
@Index("pjd_usr_id",["pjdUsr",])
@Index("pjd_weight_event",["pjdWeightEvent",])
@Index("pjd_disability_classes",["pjdDisabilityClasses",])
@Index("pjd_createdby_user",["pjdCreatedbyUser",])
export class gur_user_para_judo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pjd_id"
        })
    pjd_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaJudoProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pjd_usr_id'})
    pjdUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaJudoProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pjd_weight_event'})
    pjdWeightEvent:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaJudoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pjd_disability_classes'})
    pjdDisabilityClasses:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pjd_modifiedtime"
        })
    pjd_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaJudoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pjd_createdby_user'})
    pjdCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pjd_modifiedby"
        })
    pjd_modifiedby:string | null;
        
}
