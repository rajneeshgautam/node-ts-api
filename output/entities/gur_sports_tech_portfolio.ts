import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_tech_company} from "./gur_sports_tech_company";


@Entity("gur_sports_tech_portfolio" ,{schema:"sportsmatik_local" } )
@Index("stp_stc_id",["stpUser",])
export class gur_sports_tech_portfolio {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"stp_id"
        })
    stp_id:number;
        

   
    @ManyToOne(()=>gur_sports_tech_company, (gur_sports_tech_company: gur_sports_tech_company)=>gur_sports_tech_company.gurSportsTechPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'stp_user_id'})
    stpUser:gur_sports_tech_company | null;


    @Column("enum",{ 
        nullable:false,
        enum:["c","p","r"],
        name:"stp_type"
        })
    stp_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"stp_name"
        })
    stp_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stp_link"
        })
    stp_link:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stp_image"
        })
    stp_image:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"stp_description"
        })
    stp_description:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"stp_status"
        })
    stp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"stp_admin_review"
        })
    stp_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"stp_createdby"
        })
    stp_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"stp_modifiedby"
        })
    stp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"stp_modified_time"
        })
    stp_modified_time:Date;
        
}
