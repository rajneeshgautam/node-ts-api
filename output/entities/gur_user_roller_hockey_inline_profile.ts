import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_roller_hockey_inline_profile" ,{schema:"sportsmatik_local" } )
@Index("rhi_usr_id",["rhiUsr",])
@Index("rhi_primary_position",["rhiPosition",])
@Index("rhi_alternate_position",["rhiPosition2",])
@Index("rhi_createdby_user",["rhiCreatedbyUser",])
export class gur_user_roller_hockey_inline_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"rhi_id"
        })
    rhi_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRollerHockeyInlineProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhi_usr_id'})
    rhiUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"rhi_shoots"
        })
    rhi_shoots:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRollerHockeyInlineProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhi_position1'})
    rhiPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRollerHockeyInlineProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhi_position2'})
    rhiPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rhi_modifiedtime"
        })
    rhi_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRollerHockeyInlineProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rhi_createdby_user'})
    rhiCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rhi_modifiedby"
        })
    rhi_modifiedby:string | null;
        
}
