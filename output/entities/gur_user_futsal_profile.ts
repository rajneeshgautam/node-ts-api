import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_futsal_profile" ,{schema:"sportsmatik_local" } )
@Index("fut_usr_id",["futUsr",])
@Index("fut_position1",["futPosition",])
@Index("fut_position2",["futPosition2",])
@Index("fut_createdby_user",["futCreatedbyUser",])
@Index("fut_foot",["fut_foot",])
export class gur_user_futsal_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fut_id"
        })
    fut_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFutsalProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fut_usr_id'})
    futUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFutsalProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fut_position1'})
    futPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFutsalProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fut_position2'})
    futPosition2:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"fut_foot"
        })
    fut_foot:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fut_modifiedtime"
        })
    fut_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFutsalProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fut_createdby_user'})
    futCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"fut_modifiedby"
        })
    fut_modifiedby:string | null;
        
}
