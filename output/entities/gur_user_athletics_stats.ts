import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_athletics_stats" ,{schema:"sportsmatik_local" } )
@Index("usp_usr_id",["atsUsr",])
@Index("usp_ucp_id",["atsUcp",])
@Index("usp_rank",["atsRank",])
@Index("ats_event",["atsEvent",])
export class gur_user_athletics_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"ats_id"
        })
    ats_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAthleticsStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ats_usr_id'})
    atsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserAthleticsStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ats_ucp_id'})
    atsUcp:gur_user_competition_played | null;


    @Column("enum",{ 
        nullable:false,
        enum:["sprint","middle-long","race-walk","road-running","cross-country","hurdles","relay","high-jump","pole-vault","long-jump","triple-jump","shot-put","discus-throw","hammer-throw","javelin-throw","pentathlon","heptathlon","octathlon","decathlon"],
        name:"ats_mode"
        })
    ats_mode:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAthleticsStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ats_event'})
    atsEvent:gur_value_list | null;


    @Column("time",{ 
        nullable:true,
        name:"ats_value_time"
        })
    ats_value_time:string | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"ats_value"
        })
    ats_value:number | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAthleticsStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ats_rank'})
    atsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ats_modifiedtime"
        })
    ats_modifiedtime:Date;
        
}
