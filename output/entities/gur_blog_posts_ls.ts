import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_blog_posts} from "./gur_blog_posts";
import {gur_languages} from "./gur_languages";


@Entity("gur_blog_posts_ls" ,{schema:"sportsmatik_local" } )
@Index("bpl_bpt_id",["bplBpt",])
@Index("bpl_lng_code",["bplLngCode",])
export class gur_blog_posts_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bpl_id"
        })
    bpl_id:number;
        

   
    @ManyToOne(()=>gur_blog_posts, (gur_blog_posts: gur_blog_posts)=>gur_blog_posts.gurBlogPostsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bpl_bpt_id'})
    bplBpt:gur_blog_posts | null;


    @Column("varchar",{ 
        nullable:true,
        name:"bpl_title"
        })
    bpl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"bpl_contents"
        })
    bpl_contents:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bpl_excerpt"
        })
    bpl_excerpt:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bpl_meta_title"
        })
    bpl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bpl_meta_description"
        })
    bpl_meta_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bpl_meta_keywords"
        })
    bpl_meta_keywords:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurBlogPostsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bpl_lng_code'})
    bplLngCode:gur_languages | null;

}
