import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_paddleball_profile" ,{schema:"sportsmatik_local" } )
@Index("pad_usr_id",["padUsr",])
@Index("pad_createdby_user",["padCreatedbyUser",])
export class gur_user_paddleball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pad_id"
        })
    pad_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPaddleballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pad_usr_id'})
    padUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pad_hand"
        })
    pad_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pad_modifiedtime"
        })
    pad_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPaddleballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pad_createdby_user'})
    padCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pad_modifiedby"
        })
    pad_modifiedby:string | null;
        
}
