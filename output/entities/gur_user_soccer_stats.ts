import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_soccer_stats" ,{schema:"sportsmatik_local" } )
@Index("uss_usr_id",["ussUsr",])
@Index("uss_ucp_id",["ussUcp",])
export class gur_user_soccer_stats {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"uss_id"
        })
    uss_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSoccerStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uss_usr_id'})
    ussUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserSoccerStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uss_ucp_id'})
    ussUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"uss_matches_played"
        })
    uss_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uss_games_starts"
        })
    uss_games_starts:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uss_games_sub_ins"
        })
    uss_games_sub_ins:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uss_total_minutes_played"
        })
    uss_total_minutes_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uss_goals_scored"
        })
    uss_goals_scored:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uss_goals_assist"
        })
    uss_goals_assist:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_shots_attempted"
        })
    uss_shots_attempted:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_shots_on_target"
        })
    uss_shots_on_target:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_matches_won"
        })
    uss_matches_won:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_matches_draw"
        })
    uss_matches_draw:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_matches_lose"
        })
    uss_matches_lose:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_yellow_card"
        })
    uss_yellow_card:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_red_card"
        })
    uss_red_card:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_goals_saves"
        })
    uss_goals_saves:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_goals_conceded"
        })
    uss_goals_conceded:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"uss_goals_against_average"
        })
    uss_goals_against_average:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"uss_clean_sheets"
        })
    uss_clean_sheets:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uss_modifiedtime"
        })
    uss_modifiedtime:Date;
        
}
