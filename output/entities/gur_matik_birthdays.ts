import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_personalities} from "./gur_sports_personalities";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";


@Entity("gur_matik_birthdays" ,{schema:"sportsmatik_local" } )
@Index("bir_wep_id_2",["birWep",],{unique:true})
@Index("gur_matik_birthdays_ibfk_2",["birKnc",])
export class gur_matik_birthdays {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"bir_id"
        })
    bir_id:string;
        

   
    @OneToOne(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.gurMatikBirthdays,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bir_wep_id'})
    birWep:gur_sports_personalities | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bir_featured"
        })
    bir_featured:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bir_only_knowhow"
        })
    bir_only_knowhow:boolean;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikBirthdayss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bir_knc_id'})
    birKnc:gur_matik_knowhow_tab_categories | null;


    @Column("varchar",{ 
        nullable:false,
        name:"bir_createdby"
        })
    bir_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bir_modifiedby"
        })
    bir_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bir_modifiedtime"
        })
    bir_modifiedtime:Date;
        
}
