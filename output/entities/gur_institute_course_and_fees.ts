import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";


@Entity("gur_institute_course_and_fees" ,{schema:"sportsmatik_local" } )
@Index("icf_user_id",["icf_user_id",])
@Index("gur_institute_course_and_fees_ibfk_1",["icfUty",])
@Index("gur_institute_course_and_fees_ibfk_2",["icfCreatedbyUser",])
@Index("gur_institute_course_and_fees_ibfk_3",["icfCreatedbyAdmin",])
export class gur_institute_course_and_fees {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"icf_id"
        })
    icf_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"icf_user_id"
        })
    icf_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInstituteCourseAndFeess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'icf_uty_id'})
    icfUty:gur_user_types | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"icf_sports"
        })
    icf_sports:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"icf_name"
        })
    icf_name:string;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"icf_fees"
        })
    icf_fees:number;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"icf_duration"
        })
    icf_duration:number;
        

    @Column("enum",{ 
        nullable:true,
        enum:["1","2","3","4"],
        name:"icf_duration_box"
        })
    icf_duration_box:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:1000,
        name:"icf_description"
        })
    icf_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"icf_status"
        })
    icf_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"icf_admin_review"
        })
    icf_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInstituteCourseAndFeess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'icf_createdby_user'})
    icfCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurInstituteCourseAndFeess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'icf_createdby_admin'})
    icfCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"icf_createdby"
        })
    icf_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"icf_modifiedby"
        })
    icf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"icf_modifiedtime"
        })
    icf_modifiedtime:Date;
        
}
