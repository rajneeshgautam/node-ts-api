import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_schools" ,{schema:"sportsmatik_local" } )
@Index("sch_gst_number",["sch_gst_number",],{unique:true})
@Index("sch_usr_id",["schUsr",])
@Index("sch_createdby_user",["schCreatedbyUser",])
@Index("sch_sts_id",["schSts",])
@Index("sch_dis_id",["schDis",])
@Index("sch_name",["sch_name",])
@Index("gur_schools_ibfk_4",["schCountry",])
export class gur_schools {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sch_id"
        })
    sch_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sch_slug"
        })
    sch_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sch_name"
        })
    sch_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSchoolss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sch_usr_id'})
    schUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"sch_address"
        })
    sch_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sch_address2"
        })
    sch_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sch_pincode"
        })
    sch_pincode:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSchoolss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sch_country'})
    schCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurSchoolss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sch_dis_id'})
    schDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurSchoolss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sch_sts_id'})
    schSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"sch_address_google_map"
        })
    sch_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sch_email"
        })
    sch_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sch_email_verified"
        })
    sch_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sch_email_verify_time"
        })
    sch_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sch_phone1"
        })
    sch_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"sch_phone1_verified"
        })
    sch_phone1_verified:number;
        

    @Column("int",{ 
        nullable:true,
        name:"sch_phone1_otp"
        })
    sch_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sch_phone1_otp_time"
        })
    sch_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sch_phone2"
        })
    sch_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"sch_phone2_verified"
        })
    sch_phone2_verified:number;
        

    @Column("int",{ 
        nullable:true,
        name:"sch_phone2_otp"
        })
    sch_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sch_phone2_otp_time"
        })
    sch_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"sch_status"
        })
    sch_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sch_profile_pic"
        })
    sch_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sch_timeline_pic"
        })
    sch_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sch_intro_status"
        })
    sch_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"sch_profile_summary"
        })
    sch_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sch_founded_year"
        })
    sch_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sch_founded_month"
        })
    sch_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sch_last_login_date"
        })
    sch_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sch_last_login_details"
        })
    sch_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"sch_intro_modifiedtime"
        })
    sch_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sch_profile_pic_modifiedtime"
        })
    sch_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sch_free_sports"
        })
    sch_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sch_free_sub_users"
        })
    sch_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"sch_gst_number"
        })
    sch_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sch_viewable"
        })
    sch_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSchoolss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sch_createdby_user'})
    schCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sch_createdby"
        })
    sch_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sch_modifiedby"
        })
    sch_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sch_modifiedtime"
        })
    sch_modifiedtime:Date;
        
}
