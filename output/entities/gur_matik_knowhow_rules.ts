import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_rules_ls} from "./gur_matik_knowhow_rules_ls";


@Entity("gur_matik_knowhow_rules" ,{schema:"sportsmatik_local" } )
@Index("khl_wkh_id",["khrMkh",])
export class gur_matik_knowhow_rules {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khr_id"
        })
    khr_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowRuless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khr_mkh_id'})
    khrMkh:gur_matik_knowhow | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"khr_gender"
        })
    khr_gender:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"khr_photo_data"
        })
    khr_photo_data:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"khr_sort_order"
        })
    khr_sort_order:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khr_createdby"
        })
    khr_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khr_modifiedby"
        })
    khr_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khr_modifiedtime"
        })
    khr_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_rules_ls, (gur_matik_knowhow_rules_ls: gur_matik_knowhow_rules_ls)=>gur_matik_knowhow_rules_ls.krlKhr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowRulesLss:gur_matik_knowhow_rules_ls[];
    
}
