import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_faq_categories} from "./gur_faq_categories";
import {gur_languages} from "./gur_languages";


@Entity("gur_faq_categories_ls" ,{schema:"sportsmatik_local" } )
@Index("blc_name_unique",["fal_name","falLngCode",],{unique:true})
@Index("fal_fac_id",["falFac",])
@Index("fal_lng_code",["falLngCode",])
export class gur_faq_categories_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fal_id"
        })
    fal_id:number;
        

   
    @ManyToOne(()=>gur_faq_categories, (gur_faq_categories: gur_faq_categories)=>gur_faq_categories.gurFaqCategoriesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fal_fac_id'})
    falFac:gur_faq_categories | null;


    @Column("varchar",{ 
        nullable:true,
        name:"fal_name"
        })
    fal_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"fal_descriptions"
        })
    fal_descriptions:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fal_meta_title"
        })
    fal_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fal_meta_description"
        })
    fal_meta_description:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurFaqCategoriesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fal_lng_code'})
    falLngCode:gur_languages | null;

}
