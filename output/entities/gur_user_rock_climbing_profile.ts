import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_rock_climbing_profile" ,{schema:"sportsmatik_local" } )
@Index("rcb_usr_id",["rcbUsr",])
@Index("rcb_createdby_user",["rcbCreatedbyUser",])
export class gur_user_rock_climbing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"rcb_id"
        })
    rcb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRockClimbingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rcb_usr_id'})
    rcbUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"rcb_climber_types"
        })
    rcb_climber_types:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rcb_modifiedtime"
        })
    rcb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRockClimbingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rcb_createdby_user'})
    rcbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rcb_modifiedby"
        })
    rcb_modifiedby:string | null;
        
}
