import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_advertiser_ads_placements} from "./gur_advertiser_ads_placements";
import {gur_advertiser_ads} from "./gur_advertiser_ads";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_advertiser_ads_hits" ,{schema:"sportsmatik_local" } )
@Index("adh_apl_id",["adhApl",])
@Index("adh_ads_id",["adhAds",])
@Index("adh_uty_id",["adhUty",])
export class gur_advertiser_ads_hits {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"adh_id"
        })
    adh_id:string;
        

   
    @ManyToOne(()=>gur_advertiser_ads_placements, (gur_advertiser_ads_placements: gur_advertiser_ads_placements)=>gur_advertiser_ads_placements.gurAdvertiserAdsHitss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adh_apl_id'})
    adhApl:gur_advertiser_ads_placements | null;


   
    @ManyToOne(()=>gur_advertiser_ads, (gur_advertiser_ads: gur_advertiser_ads)=>gur_advertiser_ads.gurAdvertiserAdsHitss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adh_ads_id'})
    adhAds:gur_advertiser_ads | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurAdvertiserAdsHitss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adh_uty_id'})
    adhUty:gur_user_types | null;


    @Column("enum",{ 
        nullable:true,
        enum:["imp","click"],
        name:"adh_type"
        })
    adh_type:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"adh_page_url"
        })
    adh_page_url:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"adh_modifiedtime"
        })
    adh_modifiedtime:Date;
        
}
