import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_knowledge_base_articles} from "./gur_knowledge_base_articles";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_knowledge_base_articles_view" ,{schema:"sportsmatik_local" } )
@Index("kvu_kba_id",["kvuKba",])
@Index("kvu_uty_id",["kvuUty",])
export class gur_knowledge_base_articles_view {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kvu_id"
        })
    kvu_id:number;
        

   
    @ManyToOne(()=>gur_knowledge_base_articles, (gur_knowledge_base_articles: gur_knowledge_base_articles)=>gur_knowledge_base_articles.gurKnowledgeBaseArticlesViews,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'kvu_kba_id'})
    kvuKba:gur_knowledge_base_articles | null;


    @Column("varchar",{ 
        nullable:false,
        length:16,
        name:"kvu_ip"
        })
    kvu_ip:string;
        

    @Column("bigint",{ 
        nullable:true,
        name:"kvu_user_id"
        })
    kvu_user_id:string | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurKnowledgeBaseArticlesViews,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'kvu_uty_id'})
    kvuUty:gur_user_types | null;

}
