import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_archive_category} from "./gur_archive_category";


@Entity("gur_archive" ,{schema:"sportsmatik_local" } )
@Index("arc_module",["arcAct",])
export class gur_archive {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"arc_id"
        })
    arc_id:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"arc_usr_id"
        })
    arc_usr_id:number | null;
        

   
    @ManyToOne(()=>gur_archive_category, (gur_archive_category: gur_archive_category)=>gur_archive_category.gurArchives,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arc_act_id'})
    arcAct:gur_archive_category | null;


    @Column("text",{ 
        nullable:false,
        name:"arc_data"
        })
    arc_data:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"arc_reason"
        })
    arc_reason:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"arc_modifiedby"
        })
    arc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"arc_modifiedtime"
        })
    arc_modifiedtime:Date;
        
}
