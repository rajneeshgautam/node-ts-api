import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_sepak_takraw_profile" ,{schema:"sportsmatik_local" } )
@Index("sep_usr_id",["sepUsr",])
@Index("sep_position1",["sepPosition",])
@Index("sep_position2",["sepPosition2",])
@Index("sep_createdby_user",["sepCreatedbyUser",])
export class gur_user_sepak_takraw_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sep_id"
        })
    sep_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSepakTakrawProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sep_usr_id'})
    sepUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSepakTakrawProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sep_position1'})
    sepPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSepakTakrawProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sep_position2'})
    sepPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sep_modifiedtime"
        })
    sep_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSepakTakrawProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sep_createdby_user'})
    sepCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sep_modifiedby"
        })
    sep_modifiedby:string | null;
        
}
