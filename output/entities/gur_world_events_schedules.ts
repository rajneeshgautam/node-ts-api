import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events} from "./gur_world_events";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_world_events_sports_categories} from "./gur_world_events_sports_categories";
import {gur_world_events_venues} from "./gur_world_events_venues";
import {gur_world_events_results} from "./gur_world_events_results";
import {gur_world_events_schedules_map} from "./gur_world_events_schedules_map";
import {gur_world_events_standings} from "./gur_world_events_standings";


@Entity("gur_world_events_schedules" ,{schema:"sportsmatik_local" } )
@Index("gur_world_events_schedules_ibfk_1",["evsEvt",])
@Index("gur_world_events_schedules_ibfk_2",["evsVenue",])
@Index("gur_world_events_schedules_ibfk_3",["evsSpo",])
@Index("gur_world_events_schedules_ibfk_4",["evsCat",])
export class gur_world_events_schedules {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"evs_id"
        })
    evs_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evs_title"
        })
    evs_title:string | null;
        

   
    @ManyToOne(()=>gur_world_events, (gur_world_events: gur_world_events)=>gur_world_events.gurWorldEventsScheduless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_evt_id'})
    evsEvt:gur_world_events | null;


    @Column("varchar",{ 
        nullable:true,
        name:"evs_image"
        })
    evs_image:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"evs_start"
        })
    evs_start:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"evs_end"
        })
    evs_end:Date | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"evs_show_time"
        })
    evs_show_time:boolean | null;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWorldEventsScheduless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_spo_id'})
    evsSpo:gur_wiki_sports | null;


   
    @ManyToOne(()=>gur_world_events_sports_categories, (gur_world_events_sports_categories: gur_world_events_sports_categories)=>gur_world_events_sports_categories.gurWorldEventsScheduless,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_cat_id'})
    evsCat:gur_world_events_sports_categories | null;


   
    @ManyToOne(()=>gur_world_events_venues, (gur_world_events_venues: gur_world_events_venues)=>gur_world_events_venues.gurWorldEventsScheduless,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evs_venue'})
    evsVenue:gur_world_events_venues | null;


    @Column("enum",{ 
        nullable:false,
        enum:["country","team","individual","doubles"],
        name:"evs_team_type"
        })
    evs_team_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        default: () => "'Score'",
        name:"evs_score_label"
        })
    evs_score_label:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"evs_result_layout"
        })
    evs_result_layout:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        default: () => "'Result'",
        name:"evs_result_label"
        })
    evs_result_label:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"evs_createdby"
        })
    evs_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evs_modifiedby"
        })
    evs_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"evs_modifiedtime"
        })
    evs_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_world_events_results, (gur_world_events_results: gur_world_events_results)=>gur_world_events_results.werEvs,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsResultss:gur_world_events_results[];
    

   
    @OneToMany(()=>gur_world_events_schedules_map, (gur_world_events_schedules_map: gur_world_events_schedules_map)=>gur_world_events_schedules_map.esmEvs,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurWorldEventsSchedulesMaps:gur_world_events_schedules_map[];
    

   
    @OneToMany(()=>gur_world_events_standings, (gur_world_events_standings: gur_world_events_standings)=>gur_world_events_standings.wesEvs,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsStandingss:gur_world_events_standings[];
    
}
