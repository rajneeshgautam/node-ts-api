import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_blind_cricket_profile" ,{schema:"sportsmatik_local" } )
@Index("bcp_usr_id",["bcpUsr",])
@Index("bcp_player_type",["bcpPlayerType",])
@Index("bcp_createdby_user",["bcpCreatedbyUser",])
export class gur_user_blind_cricket_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bcp_id"
        })
    bcp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBlindCricketProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bcp_usr_id'})
    bcpUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserBlindCricketProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bcp_player_type'})
    bcpPlayerType:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"bcp_batting_hand"
        })
    bcp_batting_hand:string | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"bcp_bowling_hand"
        })
    bcp_bowling_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bcp_modifiedtime"
        })
    bcp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBlindCricketProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bcp_createdby_user'})
    bcpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bcp_modifiedby"
        })
    bcp_modifiedby:string | null;
        
}
