import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_ca_individual_professional_details" ,{schema:"sportsmatik_local" } )
@Index("cip_usr_id",["cip_user_id",])
@Index("cip_user_type",["cipUserType",])
export class gur_ca_individual_professional_details {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cip_id"
        })
    cip_id:number;
        

    @Column("bigint",{ 
        nullable:false,
        unsigned: true,
        name:"cip_user_id"
        })
    cip_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCaIndividualProfessionalDetailss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cip_user_type'})
    cipUserType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        name:"cip_years"
        })
    cip_years:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"cip_status"
        })
    cip_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cip_admin_review"
        })
    cip_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cip_createdby"
        })
    cip_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cip_modifiedby"
        })
    cip_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cip_modified_time"
        })
    cip_modified_time:Date;
        
}
