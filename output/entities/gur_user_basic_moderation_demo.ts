import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_basic_moderation_demo" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["ubm_uty_id",])
@Index("evt_clb_id",["ubm_clb_id",])
@Index("evt_aca_id",["ubm_aca_id",])
@Index("evt_clg_id",["ubm_clg_id",])
@Index("evt_cor_id",["ubm_cor_id",])
@Index("evt_smc_id",["ubm_smc_id",])
@Index("upc_usr_id",["ubm_usr_id",])
@Index("upc_spn_id",["ubm_spn_id",])
@Index("pch_sch_id",["ubm_sch_id",])
@Index("ubm_ven_id",["ubm_ven_id",])
export class gur_user_basic_moderation_demo {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ubmd_id"
        })
    ubmd_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"ubm_uty_id"
        })
    ubm_uty_id:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_usr_id"
        })
    ubm_usr_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_clb_id"
        })
    ubm_clb_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_aca_id"
        })
    ubm_aca_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_sch_id"
        })
    ubm_sch_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_clg_id"
        })
    ubm_clg_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_cor_id"
        })
    ubm_cor_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_smc_id"
        })
    ubm_smc_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_spn_id"
        })
    ubm_spn_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_slg_id"
        })
    ubm_slg_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_cha_id"
        })
    ubm_cha_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_sec_id"
        })
    ubm_sec_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_stc_id"
        })
    ubm_stc_id:number | null;
        

    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_ven_id"
        })
    ubm_ven_id:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ubm_profile_summary"
        })
    ubm_profile_summary:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ubm_profile_summary_admin_review"
        })
    ubm_profile_summary_admin_review:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ubm_profile_pic_admin_review"
        })
    ubm_profile_pic_admin_review:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ubm_modifiedby"
        })
    ubm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ubm_modifiedtime"
        })
    ubm_modifiedtime:Date;
        
}
