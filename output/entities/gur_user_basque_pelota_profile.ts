import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_basque_pelota_profile" ,{schema:"sportsmatik_local" } )
@Index("bpl_usr_id",["bplUsr",])
@Index("bpl_positions",["bplPositions",])
@Index("bpl_createdby_user",["bplCreatedbyUser",])
export class gur_user_basque_pelota_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bpl_id"
        })
    bpl_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBasquePelotaProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bpl_usr_id'})
    bplUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"bpl_hand"
        })
    bpl_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserBasquePelotaProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bpl_positions'})
    bplPositions:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        name:"bpl_categories"
        })
    bpl_categories:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bpl_modifiedtime"
        })
    bpl_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBasquePelotaProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bpl_createdby_user'})
    bplCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bpl_modifiedby"
        })
    bpl_modifiedby:string | null;
        
}
