import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_digital_consent" ,{schema:"sportsmatik_local" } )
@Index("uaa_usr_id",["udc_usr_id",])
@Index("uaa_clb_id",["udc_gua_id",])
export class gur_digital_consent {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"udc_id"
        })
    udc_id:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"udc_usr_id"
        })
    udc_usr_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"udc_gua_id"
        })
    udc_gua_id:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"udc_gua_is_parent"
        })
    udc_gua_is_parent:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"udc_gua_relation"
        })
    udc_gua_relation:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"udc_usr_contact"
        })
    udc_usr_contact:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"udc_legal_gua_name"
        })
    udc_legal_gua_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"udc_legal_gua_relation"
        })
    udc_legal_gua_relation:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"udc_legal_gua_dob"
        })
    udc_legal_gua_dob:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"udc_legal_gua_address"
        })
    udc_legal_gua_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"udc_legal_gua_phone"
        })
    udc_legal_gua_phone:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"udc_legal_gua_cnt_id"
        })
    udc_legal_gua_cnt_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"udc_legal_gua_city_id"
        })
    udc_legal_gua_city_id:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"udc_legal_gua_email"
        })
    udc_legal_gua_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"udc_activated"
        })
    udc_activated:number;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"udc_activation_sent"
        })
    udc_activation_sent:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        default: () => "CURRENT_TIMESTAMP",
        name:"udc_modifiedby"
        })
    udc_modifiedby:Date | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"udc_createdby"
        })
    udc_createdby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"udc_modifiedtime"
        })
    udc_modifiedtime:Date;
        
}
