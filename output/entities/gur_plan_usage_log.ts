import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_user_plans} from "./gur_user_plans";


@Entity("gur_plan_usage_log" ,{schema:"sportsmatik_local" } )
@Index("pul_plan_used",["pulPch",])
@Index("gur_plan_usage_log_ibfk_1",["pulUty",])
@Index("gur_plan_usage_log_ibfk_2",["pulByUser",])
export class gur_plan_usage_log {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"pul_id"
        })
    pul_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"pul_user_id"
        })
    pul_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurPlanUsageLogs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pul_uty_id'})
    pulUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurPlanUsageLogs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pul_by_user'})
    pulByUser:gur_users | null;


    @Column("text",{ 
        nullable:false,
        name:"pul_description"
        })
    pul_description:string;
        

   
    @ManyToOne(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.gurPlanUsageLogs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pul_pch_id'})
    pulPch:gur_user_plans | null;


    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"pul_addons"
        })
    pul_addons:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pul_createdon"
        })
    pul_createdon:Date;
        
}
