import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_pricing_plans} from "./gur_pricing_plans";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_pricing_plans_map" ,{schema:"sportsmatik_local" } )
@Index("uniq",["prmPrp","prmUty",],{unique:true})
@Index("prm_uty_id",["prmUty",])
@Index("prm_prp_id",["prmPrp",])
export class gur_pricing_plans_map {

   
    @ManyToOne(()=>gur_pricing_plans, (gur_pricing_plans: gur_pricing_plans)=>gur_pricing_plans.gurPricingPlansMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prm_prp_id'})
    prmPrp:gur_pricing_plans | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurPricingPlansMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prm_uty_id'})
    prmUty:gur_user_types | null;


    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"prm_price"
        })
    prm_price:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"prm_modules"
        })
    prm_modules:string | null;
        
}
