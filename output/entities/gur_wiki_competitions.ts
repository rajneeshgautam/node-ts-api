import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_history_timeline} from "./gur_matik_knowhow_history_timeline";
import {gur_matik_knowhow_teams} from "./gur_matik_knowhow_teams";
import {gur_wiki_competitions_ls} from "./gur_wiki_competitions_ls";
import {gur_wiki_competitions_sports_map} from "./gur_wiki_competitions_sports_map";
import {gur_wiki_features} from "./gur_wiki_features";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";


@Entity("gur_wiki_competitions" ,{schema:"sportsmatik_local" } )
@Index("wcm_event_duration",["wcm_event_duration",])
export class gur_wiki_competitions {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wcm_id"
        })
    wcm_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"wcm_slug"
        })
    wcm_slug:string;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"wcm_sort_order"
        })
    wcm_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wcm_status"
        })
    wcm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wcm_multi"
        })
    wcm_multi:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wcm_olympic"
        })
    wcm_olympic:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"wcm_cnt_id"
        })
    wcm_cnt_id:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcm_countries"
        })
    wcm_countries:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"wcm_comp_location_multi"
        })
    wcm_comp_location_multi:boolean | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["male","female","other","both"],
        name:"wcm_gender"
        })
    wcm_gender:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["international","domestic","regional","intercontinental"],
        name:"wcm_comp_type"
        })
    wcm_comp_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcm_primary_photo"
        })
    wcm_primary_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wcm_photo_source_details"
        })
    wcm_photo_source_details:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wcm_gov_body_primary"
        })
    wcm_gov_body_primary:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcm_gov_body"
        })
    wcm_gov_body:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wcm_event_duration"
        })
    wcm_event_duration:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wcm_disabled"
        })
    wcm_disabled:boolean;
        

    @Column("enum",{ 
        nullable:true,
        enum:["above","below","between"],
        name:"wcm_age_criteria_type"
        })
    wcm_age_criteria_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"wcm_age_criteria"
        })
    wcm_age_criteria:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"wcm_is_local"
        })
    wcm_is_local:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcm_createdby"
        })
    wcm_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wcm_modifiedby"
        })
    wcm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wcm_modifiedtime"
        })
    wcm_modifiedtime:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"wcm_for_knowhow"
        })
    wcm_for_knowhow:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:2500,
        name:"wcm_type2"
        })
    wcm_type2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wcm_is_featured"
        })
    wcm_is_featured:boolean;
        

   
    @OneToMany(()=>gur_matik_knowhow_history_timeline, (gur_matik_knowhow_history_timeline: gur_matik_knowhow_history_timeline)=>gur_matik_knowhow_history_timeline.khtWcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowHistoryTimelines:gur_matik_knowhow_history_timeline[];
    

   
    @OneToMany(()=>gur_matik_knowhow_teams, (gur_matik_knowhow_teams: gur_matik_knowhow_teams)=>gur_matik_knowhow_teams.khtWcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowTeamss:gur_matik_knowhow_teams[];
    

   
    @OneToMany(()=>gur_wiki_competitions_ls, (gur_wiki_competitions_ls: gur_wiki_competitions_ls)=>gur_wiki_competitions_ls.wclWcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiCompetitionsLss:gur_wiki_competitions_ls[];
    

   
    @OneToMany(()=>gur_wiki_competitions_sports_map, (gur_wiki_competitions_sports_map: gur_wiki_competitions_sports_map)=>gur_wiki_competitions_sports_map.wsmWcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiCompetitionsSportsMaps:gur_wiki_competitions_sports_map[];
    

   
    @OneToMany(()=>gur_wiki_features, (gur_wiki_features: gur_wiki_features)=>gur_wiki_features.wvfWcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiFeaturess:gur_wiki_features[];
    

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgWcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    
}
