import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_ratings_media} from "./gur_ratings_media";


@Entity("gur_ratings" ,{schema:"sportsmatik_local" } )
@Index("rat_rated_by",["rat_rated_by",])
@Index("rat_rated_by_type",["ratRatedByType",])
@Index("rat_uty_id",["ratUty",])
export class gur_ratings {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"rat_id"
        })
    rat_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"rat_rated_by"
        })
    rat_rated_by:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurRatingss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rat_rated_by_type'})
    ratRatedByType:gur_user_types | null;


    @Column("bigint",{ 
        nullable:false,
        name:"rat_rated_by_user"
        })
    rat_rated_by_user:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurRatingss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rat_uty_id'})
    ratUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"rat_user_id"
        })
    rat_user_id:number;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"rat_value"
        })
    rat_value:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"rat_title"
        })
    rat_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"rat_desc"
        })
    rat_desc:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rat_posted"
        })
    rat_posted:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"rat_createdby"
        })
    rat_createdby:string | null;
        

   
    @OneToMany(()=>gur_ratings_media, (gur_ratings_media: gur_ratings_media)=>gur_ratings_media.rtmRat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRatingsMedias:gur_ratings_media[];
    
}
