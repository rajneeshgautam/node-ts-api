import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_goalball_profile" ,{schema:"sportsmatik_local" } )
@Index("pgb_usr_id",["pgbUsr",])
@Index("pgb_position1",["pgbPosition",])
@Index("pgb_position2",["pgbPosition2",])
@Index("pgb_disablility_class",["pgbDisablilityClass",])
@Index("pgb_createdby_user",["pgbCreatedbyUser",])
export class gur_user_para_goalball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pgb_id"
        })
    pgb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaGoalballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgb_usr_id'})
    pgbUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaGoalballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgb_position1'})
    pgbPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaGoalballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgb_position2'})
    pgbPosition2:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaGoalballProfiles3,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgb_disablility_class'})
    pgbDisablilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pgb_modifiedtime"
        })
    pgb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaGoalballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pgb_createdby_user'})
    pgbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pgb_modifiedby"
        })
    pgb_modifiedby:string | null;
        
}
