import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_fencing_profile" ,{schema:"sportsmatik_local" } )
@Index("fen_usr_id",["fenUsr",])
@Index("fen_createdby_user",["fenCreatedbyUser",])
export class gur_user_fencing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fen_id"
        })
    fen_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFencingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fen_usr_id'})
    fenUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"fen_hand"
        })
    fen_hand:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fen_weapon_epee"
        })
    fen_weapon_epee:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fen_weapon_sabre"
        })
    fen_weapon_sabre:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fen_weapon_foil"
        })
    fen_weapon_foil:boolean;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fen_modifiedtime"
        })
    fen_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFencingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fen_createdby_user'})
    fenCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"fen_modifiedby"
        })
    fen_modifiedby:string | null;
        
}
