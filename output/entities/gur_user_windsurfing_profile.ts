import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_windsurfing_profile" ,{schema:"sportsmatik_local" } )
@Index("win_usr_id",["winUsr",])
@Index("win_createdby_user",["winCreatedbyUser",])
export class gur_user_windsurfing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"win_id"
        })
    win_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWindsurfingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'win_usr_id'})
    winUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"win_discipline"
        })
    win_discipline:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"win_board"
        })
    win_board:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"win_sails"
        })
    win_sails:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"win_modifiedtime"
        })
    win_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWindsurfingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'win_createdby_user'})
    winCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"win_modifiedby"
        })
    win_modifiedby:string | null;
        
}
