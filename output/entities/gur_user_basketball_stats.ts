import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_basketball_stats" ,{schema:"sportsmatik_local" } )
@Index("bas_usr_id",["basUsr",])
@Index("bas_ucp_id",["basUcp",])
export class gur_user_basketball_stats {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bas_id"
        })
    bas_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBasketballStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bas_usr_id'})
    basUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserBasketballStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bas_ucp_id'})
    basUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"bas_matches_played"
        })
    bas_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bas_matches_started"
        })
    bas_matches_started:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bas_total_minutes_played"
        })
    bas_total_minutes_played:number;
        

    @Column("int",{ 
        nullable:true,
        name:"bas_matches_won"
        })
    bas_matches_won:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"bas_matches_draw"
        })
    bas_matches_draw:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"bas_matches_lose"
        })
    bas_matches_lose:number | null;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"bas_points"
        })
    bas_points:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_field_goals_made"
        })
    bas_field_goals_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_field_goals_attempted"
        })
    bas_field_goals_attempted:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"bas_field_goals_percentage"
        })
    bas_field_goals_percentage:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_two_points_field_goals_made"
        })
    bas_two_points_field_goals_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_two_points_field_goals_attempted"
        })
    bas_two_points_field_goals_attempted:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"bas_two_points_field_goals_percentage"
        })
    bas_two_points_field_goals_percentage:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_three_points_field_goals_made"
        })
    bas_three_points_field_goals_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_three_points_field_goals_attempted"
        })
    bas_three_points_field_goals_attempted:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"bas_three_points_field_goals_percentage"
        })
    bas_three_points_field_goals_percentage:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_free_throws_made"
        })
    bas_free_throws_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_free_throws_attempted"
        })
    bas_free_throws_attempted:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"bas_free_throws_percentage"
        })
    bas_free_throws_percentage:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_offensive_rebounds_made"
        })
    bas_offensive_rebounds_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_defensive_rebounds_made"
        })
    bas_defensive_rebounds_made:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_assists"
        })
    bas_assists:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_turnovers"
        })
    bas_turnovers:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_steals"
        })
    bas_steals:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_block"
        })
    bas_block:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"bas_fouls"
        })
    bas_fouls:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bas_modifiedtime"
        })
    bas_modifiedtime:Date;
        
}
