import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_variants_ls} from "./gur_matik_knowhow_variants_ls";


@Entity("gur_matik_knowhow_variants" ,{schema:"sportsmatik_local" } )
@Index("khl_wkh_id",["khvMkh",])
export class gur_matik_knowhow_variants {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khv_id"
        })
    khv_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowVariantss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khv_mkh_id'})
    khvMkh:gur_matik_knowhow | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"khv_gender"
        })
    khv_gender:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_photo_data"
        })
    khv_photo_data:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"khv_sort_order"
        })
    khv_sort_order:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khv_createdby"
        })
    khv_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khv_modifiedby"
        })
    khv_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khv_modifiedtime"
        })
    khv_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_variants_ls, (gur_matik_knowhow_variants_ls: gur_matik_knowhow_variants_ls)=>gur_matik_knowhow_variants_ls.kvlKhv,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowVariantsLss:gur_matik_knowhow_variants_ls[];
    
}
