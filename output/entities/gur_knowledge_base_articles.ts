import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_knowledge_base_categories} from "./gur_knowledge_base_categories";
import {gur_knowledge_base_articles_like} from "./gur_knowledge_base_articles_like";
import {gur_knowledge_base_articles_ls} from "./gur_knowledge_base_articles_ls";
import {gur_knowledge_base_articles_view} from "./gur_knowledge_base_articles_view";


@Entity("gur_knowledge_base_articles" ,{schema:"sportsmatik_local" } )
@Index("kba_kbc_id",["kbaKbc",])
export class gur_knowledge_base_articles {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"kba_id"
        })
    kba_id:string;
        

   
    @ManyToOne(()=>gur_knowledge_base_categories, (gur_knowledge_base_categories: gur_knowledge_base_categories)=>gur_knowledge_base_categories.gurKnowledgeBaseArticless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kba_kbc_id'})
    kbaKbc:gur_knowledge_base_categories | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"kba_promoted"
        })
    kba_promoted:boolean | null;
        

    @Column("text",{ 
        nullable:true,
        name:"kba_attachment"
        })
    kba_attachment:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kba_createdby"
        })
    kba_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kba_modifiedby"
        })
    kba_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kba_modifiedtime"
        })
    kba_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_knowledge_base_articles_like, (gur_knowledge_base_articles_like: gur_knowledge_base_articles_like)=>gur_knowledge_base_articles_like.kblKba,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurKnowledgeBaseArticlesLikes:gur_knowledge_base_articles_like[];
    

   
    @OneToMany(()=>gur_knowledge_base_articles_ls, (gur_knowledge_base_articles_ls: gur_knowledge_base_articles_ls)=>gur_knowledge_base_articles_ls.kalKba,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurKnowledgeBaseArticlesLss:gur_knowledge_base_articles_ls[];
    

   
    @OneToMany(()=>gur_knowledge_base_articles_view, (gur_knowledge_base_articles_view: gur_knowledge_base_articles_view)=>gur_knowledge_base_articles_view.kvuKba,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurKnowledgeBaseArticlesViews:gur_knowledge_base_articles_view[];
    
}
