import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_soccer_profile" ,{schema:"sportsmatik_local" } )
@Index("usp_usr_id",["uspUsr",])
@Index("usp_position1",["uspPosition",])
@Index("usp_position2",["uspPosition2",])
@Index("usp_disablity_classes",["uspDisablityClasses",])
@Index("usp_createdby_user",["uspCreatedbyUser",])
export class gur_user_para_soccer_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"usp_id"
        })
    usp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSoccerProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usp_usr_id'})
    uspUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usp_5_side"
        })
    usp_5_side:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usp_7_side"
        })
    usp_7_side:boolean;
        

    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"usp_primary_foot"
        })
    usp_primary_foot:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaSoccerProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usp_position1'})
    uspPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaSoccerProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usp_position2'})
    uspPosition2:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaSoccerProfiles3,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usp_disablity_classes'})
    uspDisablityClasses:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["5","7"],
        name:"usp_sports_type"
        })
    usp_sports_type:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"usp_modifiedtime"
        })
    usp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSoccerProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usp_createdby_user'})
    uspCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"usp_modifiedby"
        })
    usp_modifiedby:string | null;
        
}
