import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_plans} from "./gur_plans";
import {gur_order_items} from "./gur_order_items";


@Entity("gur_plans_price" ,{schema:"sportsmatik_local" } )
@Index("plp_currency_code",["plpCurrencyCode","plpPln",],{unique:true})
@Index("plp_cnt_id",["plpCurrencyCode",])
@Index("gur_plans_price_ibfk_2",["plpPln",])
export class gur_plans_price {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"plp_id"
        })
    plp_id:number;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurPlansPrices,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'plp_currency_code'})
    plpCurrencyCode:gur_countries | null;


   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurPlansPrices,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'plp_pln_id'})
    plpPln:gur_plans | null;


    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"plp_mrp"
        })
    plp_mrp:number | null;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"plp_price"
        })
    plp_price:number;
        

   
    @OneToMany(()=>gur_order_items, (gur_order_items: gur_order_items)=>gur_order_items.oriPlp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrderItemss:gur_order_items[];
    
}
