import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_fitness_center_services" ,{schema:"sportsmatik_local" } )
@Index("fcs_uty_id",["fcsUty",])
@Index("fcs_createdby_user",["fcsCreatedbyUser",])
export class gur_fitness_center_services {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"fcs_id"
        })
    fcs_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurFitnessCenterServicess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fcs_uty_id'})
    fcsUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"fcs_user_id"
        })
    fcs_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fcs_name"
        })
    fcs_name:string;
        

    @Column("text",{ 
        nullable:true,
        name:"fcs_desc"
        })
    fcs_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fcs_photo"
        })
    fcs_photo:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"fcs_status"
        })
    fcs_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fcs_admin_review"
        })
    fcs_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurFitnessCenterServicess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fcs_createdby_user'})
    fcsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"fcs_createdby"
        })
    fcs_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"fcs_modifiedby"
        })
    fcs_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fcs_modifiedtime"
        })
    fcs_modifiedtime:Date;
        
}
