import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_plans} from "./gur_plans";


@Entity("gur_sports_usage_history" ,{schema:"sportsmatik_local" } )
@Index("rat_rated_by",["suh_user_id",])
@Index("rat_rated_by_type",["suhUsrType",])
@Index("suh_sport_id",["suhSport",])
@Index("mtg_for_type",["suhPln",])
export class gur_sports_usage_history {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"suh_id"
        })
    suh_id:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"suh_user_id"
        })
    suh_user_id:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSportsUsageHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'suh_usr_type'})
    suhUsrType:gur_user_types | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurSportsUsageHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'suh_sport_id'})
    suhSport:gur_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"suh_sport_type"
        })
    suh_sport_type:boolean;
        

   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurSportsUsageHistorys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'suh_pln_id'})
    suhPln:gur_plans | null;


    @Column("date",{ 
        nullable:true,
        name:"suh_created_on"
        })
    suh_created_on:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"suh_createdby"
        })
    suh_createdby:string | null;
        
}
