import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_water_polo_profile" ,{schema:"sportsmatik_local" } )
@Index("wap_usr_id",["wapUsr",])
@Index("wap_primary_position",["wapPosition",])
@Index("wap_playing_nature",["wapPlayingNature",])
@Index("wap_alternate_position",["wapPosition2",])
@Index("wap_createdby_user",["wapCreatedbyUser",])
export class gur_user_water_polo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"wap_id"
        })
    wap_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWaterPoloProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wap_usr_id'})
    wapUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"wap_primary_hand"
        })
    wap_primary_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWaterPoloProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wap_position1'})
    wapPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWaterPoloProfiles3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wap_position2'})
    wapPosition2:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWaterPoloProfiles2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wap_playing_nature'})
    wapPlayingNature:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wap_modifiedtime"
        })
    wap_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWaterPoloProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wap_createdby_user'})
    wapCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"wap_modifiedby"
        })
    wap_modifiedby:string | null;
        
}
