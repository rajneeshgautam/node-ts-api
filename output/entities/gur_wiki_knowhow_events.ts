import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_wiki_knowhow_events_ls} from "./gur_wiki_knowhow_events_ls";


@Entity("gur_wiki_knowhow_events" ,{schema:"sportsmatik_local" } )
@Index("wev_wkh_id",["wevWkh",])
export class gur_wiki_knowhow_events {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wev_id"
        })
    wev_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.gurWikiKnowhowEventss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wev_wkh_id'})
    wevWkh:gur_wiki_knowhow | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wev_photo"
        })
    wev_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wev_photo_source_details"
        })
    wev_photo_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wev_photo_title"
        })
    wev_photo_title:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["url","file"],
        name:"wev_video_type"
        })
    wev_video_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wev_video"
        })
    wev_video:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wev_video_source_details"
        })
    wev_video_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wev_video_title"
        })
    wev_video_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wev_createdby"
        })
    wev_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wev_modifiedby"
        })
    wev_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wev_modifiedtime"
        })
    wev_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_knowhow_events_ls, (gur_wiki_knowhow_events_ls: gur_wiki_knowhow_events_ls)=>gur_wiki_knowhow_events_ls.evlWev,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowEventsLss:gur_wiki_knowhow_events_ls[];
    
}
