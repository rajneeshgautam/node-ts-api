import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_faq_lists} from "./gur_faq_lists";
import {gur_languages} from "./gur_languages";


@Entity("gur_faq_lists_ls" ,{schema:"sportsmatik_local" } )
@Index("bpl_name_unique",["fls_name","flsLngCode",],{unique:true})
@Index("fls_fll_id",["flsFll",])
@Index("fls_lng_code",["flsLngCode",])
export class gur_faq_lists_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"fls_id"
        })
    fls_id:string;
        

   
    @ManyToOne(()=>gur_faq_lists, (gur_faq_lists: gur_faq_lists)=>gur_faq_lists.gurFaqListsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fls_fll_id'})
    flsFll:gur_faq_lists | null;


    @Column("varchar",{ 
        nullable:true,
        name:"fls_name"
        })
    fls_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"fls_descriptions"
        })
    fls_descriptions:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fls_meta_title"
        })
    fls_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fls_meta_description"
        })
    fls_meta_description:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurFaqListsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fls_lng_code'})
    flsLngCode:gur_languages | null;

}
