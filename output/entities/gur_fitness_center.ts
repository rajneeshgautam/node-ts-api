import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_fitness_center" ,{schema:"sportsmatik_local" } )
@Index("fit_gst_number",["fit_gst_number",],{unique:true})
@Index("fit_usr_id",["fitUsr",])
@Index("fit_createdby_user",["fitCreatedbyUser",])
@Index("fit_country",["fitCountry",])
@Index("fit_sts_id",["fitSts",])
@Index("fit_dis_id",["fitDis",])
@Index("fit_name",["fit_name",])
export class gur_fitness_center {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fit_id"
        })
    fit_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"fit_type"
        })
    fit_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fit_slug"
        })
    fit_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fit_name"
        })
    fit_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurFitnessCenters,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fit_usr_id'})
    fitUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"fit_address"
        })
    fit_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fit_address2"
        })
    fit_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"fit_pincode"
        })
    fit_pincode:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurFitnessCenters,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fit_country'})
    fitCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurFitnessCenters,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fit_dis_id'})
    fitDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurFitnessCenters,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fit_sts_id'})
    fitSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"fit_address_google_map"
        })
    fit_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fit_email"
        })
    fit_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fit_email_verified"
        })
    fit_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"fit_email_verify_time"
        })
    fit_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"fit_phone1"
        })
    fit_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fit_phone1_verified"
        })
    fit_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"fit_phone1_otp"
        })
    fit_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"fit_phone1_otp_time"
        })
    fit_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"fit_phone2"
        })
    fit_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fit_phone2_verified"
        })
    fit_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"fit_phone2_otp"
        })
    fit_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"fit_phone2_otp_time"
        })
    fit_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"fit_status"
        })
    fit_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fit_profile_pic"
        })
    fit_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fit_intro_status"
        })
    fit_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"fit_profile_summary"
        })
    fit_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fit_founded_year"
        })
    fit_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fit_founded_month"
        })
    fit_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"fit_last_login_date"
        })
    fit_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fit_last_login_details"
        })
    fit_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"fit_intro_modifiedtime"
        })
    fit_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"fit_profile_pic_modifiedtime"
        })
    fit_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"fit_free_sports"
        })
    fit_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"fit_free_sub_users"
        })
    fit_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"fit_gst_number"
        })
    fit_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fit_viewable"
        })
    fit_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurFitnessCenters2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fit_createdby_user'})
    fitCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"fit_createdby"
        })
    fit_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"fit_modifiedby"
        })
    fit_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fit_modifiedtime"
        })
    fit_modifiedtime:Date;
        
}
