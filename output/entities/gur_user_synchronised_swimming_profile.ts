import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_synchronised_swimming_profile" ,{schema:"sportsmatik_local" } )
@Index("sys_usr_id",["sysUsr",])
@Index("sys_createdby_user",["sysCreatedbyUser",])
export class gur_user_synchronised_swimming_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sys_id"
        })
    sys_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSynchronisedSwimmingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sys_usr_id'})
    sysUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sys_modifiedtime"
        })
    sys_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSynchronisedSwimmingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sys_createdby_user'})
    sysCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sys_modifiedby"
        })
    sys_modifiedby:string | null;
        
}
