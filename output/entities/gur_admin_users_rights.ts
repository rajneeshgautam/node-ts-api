import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_admin_users} from "./gur_admin_users";
import {gur_modules} from "./gur_modules";


@Entity("gur_admin_users_rights" ,{schema:"sportsmatik_local" } )
@Index("adr_moduleid",["adrModule",])
export class gur_admin_users_rights {

   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurAdminUsersRightss,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adr_userid'})
    adrUser:gur_admin_users | null;


   
    @ManyToOne(()=>gur_modules, (gur_modules: gur_modules)=>gur_modules.gurAdminUsersRightss,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'adr_moduleid'})
    adrModule:gur_modules | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_create"
        })
    adr_create:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_edit"
        })
    adr_edit:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_delete"
        })
    adr_delete:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"adr_view"
        })
    adr_view:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"adr_createdby"
        })
    adr_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"adr_modifiedby"
        })
    adr_modifiedby:string | null;
        
}
