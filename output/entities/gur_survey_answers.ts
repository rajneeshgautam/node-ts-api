import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_surveys} from "./gur_surveys";
import {gur_users} from "./gur_users";


@Entity("gur_survey_answers" ,{schema:"sportsmatik_local" } )
@Index("san_sur_id",["sanSur",])
@Index("san_usr_id",["sanUsr",])
export class gur_survey_answers {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"san_id"
        })
    san_id:string;
        

   
    @ManyToOne(()=>gur_surveys, (gur_surveys: gur_surveys)=>gur_surveys.gurSurveyAnswerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'san_sur_id'})
    sanSur:gur_surveys | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSurveyAnswerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'san_usr_id'})
    sanUsr:gur_users | null;


    @Column("text",{ 
        nullable:false,
        name:"san_answers"
        })
    san_answers:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"san_createdby"
        })
    san_createdby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"san_submit_on"
        })
    san_submit_on:Date;
        
}
