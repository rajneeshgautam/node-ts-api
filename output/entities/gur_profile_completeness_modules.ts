import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_profile_completeness_modules_user_map} from "./gur_profile_completeness_modules_user_map";
import {gur_user_modules_map} from "./gur_user_modules_map";


@Entity("gur_profile_completeness_modules" ,{schema:"sportsmatik_local" } )
@Index("pcm_slug",["pcm_slug",],{unique:true})
export class gur_profile_completeness_modules {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pcm_id"
        })
    pcm_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"pcm_slug"
        })
    pcm_slug:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"pcm_name"
        })
    pcm_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pcm_desc"
        })
    pcm_desc:string | null;
        

    @Column("int",{ 
        nullable:false,
        name:"pcm_amd_id"
        })
    pcm_amd_id:number;
        

   
    @OneToMany(()=>gur_profile_completeness_modules_user_map, (gur_profile_completeness_modules_user_map: gur_profile_completeness_modules_user_map)=>gur_profile_completeness_modules_user_map.pumPcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurProfileCompletenessModulesUserMaps:gur_profile_completeness_modules_user_map[];
    

   
    @OneToMany(()=>gur_user_modules_map, (gur_user_modules_map: gur_user_modules_map)=>gur_user_modules_map.ummPcm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModulesMaps:gur_user_modules_map[];
    
}
