import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_contest} from "./gur_matik_play_contest";


@Entity("gur_matik_play_contest_winners" ,{schema:"sportsmatik_local" } )
@Index("mpw_mpc_id",["mpwMpc",])
@Index("mpw_mpu_id",["mpw_usr_id",])
export class gur_matik_play_contest_winners {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mpw_id"
        })
    mpw_id:number;
        

   
    @ManyToOne(()=>gur_matik_play_contest, (gur_matik_play_contest: gur_matik_play_contest)=>gur_matik_play_contest.gurMatikPlayContestWinnerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mpw_mpc_id'})
    mpwMpc:gur_matik_play_contest | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mpw_usr_id"
        })
    mpw_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'100'",
        name:"mpw_uty_id"
        })
    mpw_uty_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpw_win_image"
        })
    mpw_win_image:string | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"mpw_datetime"
        })
    mpw_datetime:Date;
        
}
