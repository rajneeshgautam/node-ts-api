import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_rugby_sevens_profile" ,{schema:"sportsmatik_local" } )
@Index("rgs_usr_id",["rgsUsr",])
@Index("rgs_primary_position",["rgsPosition",])
@Index("rgs_secondary_event",["rgsPosition2",])
@Index("rgs_createdby_user",["rgsCreatedbyUser",])
export class gur_user_rugby_sevens_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"rgs_id"
        })
    rgs_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbySevensProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgs_usr_id'})
    rgsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRugbySevensProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgs_position1'})
    rgsPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRugbySevensProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgs_position2'})
    rgsPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rgs_modifiedtime"
        })
    rgs_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbySevensProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgs_createdby_user'})
    rgsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rgs_modifiedby"
        })
    rgs_modifiedby:string | null;
        
}
