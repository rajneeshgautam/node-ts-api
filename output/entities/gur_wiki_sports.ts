import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthdays_photos} from "./gur_birthdays_photos";
import {gur_hall_of_fame} from "./gur_hall_of_fame";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_academies} from "./gur_matik_knowhow_academies";
import {gur_matik_knowhow_awards_winners} from "./gur_matik_knowhow_awards_winners";
import {gur_matik_knowhow_calendar} from "./gur_matik_knowhow_calendar";
import {gur_matik_knowhow_career_academies} from "./gur_matik_knowhow_career_academies";
import {gur_matik_knowhow_career_blogs} from "./gur_matik_knowhow_career_blogs";
import {gur_matik_knowhow_career_guides} from "./gur_matik_knowhow_career_guides";
import {gur_matik_knowhow_career_links} from "./gur_matik_knowhow_career_links";
import {gur_matik_knowhow_career_vacancies} from "./gur_matik_knowhow_career_vacancies";
import {gur_matik_knowhow_reference_links} from "./gur_matik_knowhow_reference_links";
import {gur_matik_knowhow_teams} from "./gur_matik_knowhow_teams";
import {gur_sports_event_schedule} from "./gur_sports_event_schedule";
import {gur_wiki_awards_sports_wise} from "./gur_wiki_awards_sports_wise";
import {gur_wiki_competitions_sports_map} from "./gur_wiki_competitions_sports_map";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_wiki_sports_equipage} from "./gur_wiki_sports_equipage";
import {gur_wiki_sports_ls} from "./gur_wiki_sports_ls";
import {gur_wiki_terminologies} from "./gur_wiki_terminologies";
import {gur_wiki_venues_sports_map} from "./gur_wiki_venues_sports_map";
import {gur_world_events_records} from "./gur_world_events_records";
import {gur_world_events_schedules} from "./gur_world_events_schedules";
import {gur_world_events_teams} from "./gur_world_events_teams";
import {gur_wiki_popular_sports} from "./gur_wiki_popular_sports";
import {gur_wiki_technologies} from "./gur_wiki_technologies";


@Entity("gur_wiki_sports" ,{schema:"sportsmatik_local" } )
@Index("spo_slug",["spo_slug",],{unique:true})
export class gur_wiki_sports {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"spo_id"
        })
    spo_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"spo_slug"
        })
    spo_slug:string;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"spo_sort_order"
        })
    spo_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"spo_active"
        })
    spo_active:boolean | null;
        

    @Column("int",{ 
        nullable:true,
        name:"spo_parent"
        })
    spo_parent:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_popular"
        })
    spo_is_popular:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"spo_terminologies_active"
        })
    spo_terminologies_active:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"spo_equipment_active"
        })
    spo_equipment_active:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"spo_knowhow_complete"
        })
    spo_knowhow_complete:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_primary_photo"
        })
    spo_primary_photo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_mascot_photo"
        })
    spo_mascot_photo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_gov_body"
        })
    spo_gov_body:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_gov_body_link"
        })
    spo_gov_body_link:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_para"
        })
    spo_is_para:boolean;
        

    @Column("enum",{ 
        nullable:true,
        enum:["ind","team","both"],
        name:"spo_ind_team_type"
        })
    spo_ind_team_type:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_timed_event"
        })
    spo_is_timed_event:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_distance_event"
        })
    spo_is_distance_event:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_scoring_event"
        })
    spo_is_scoring_event:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_judged_event"
        })
    spo_is_judged_event:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_time_period_set"
        })
    spo_is_time_period_set:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_olympics_active"
        })
    spo_olympics_active:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_strength"
        })
    spo_gtype_strength:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_water"
        })
    spo_gtype_water:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_underwater"
        })
    spo_gtype_underwater:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_ice"
        })
    spo_gtype_ice:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_snow"
        })
    spo_gtype_snow:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_air"
        })
    spo_gtype_air:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_small_ball"
        })
    spo_gtype_small_ball:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_large_ball"
        })
    spo_gtype_large_ball:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_racquet"
        })
    spo_gtype_racquet:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_wheel"
        })
    spo_gtype_wheel:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_motor"
        })
    spo_gtype_motor:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_combat"
        })
    spo_gtype_combat:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_multi_discpline"
        })
    spo_gtype_multi_discpline:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_mind"
        })
    spo_gtype_mind:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_paddle"
        })
    spo_gtype_paddle:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_animal"
        })
    spo_gtype_animal:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_gtype_accuracy"
        })
    spo_gtype_accuracy:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_played_on"
        })
    spo_played_on:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_createdby"
        })
    spo_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"spo_modifiedby"
        })
    spo_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"spo_modifiedtime"
        })
    spo_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_birthdays_photos, (gur_birthdays_photos: gur_birthdays_photos)=>gur_birthdays_photos.brpSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysPhotoss:gur_birthdays_photos[];
    

   
    @OneToMany(()=>gur_hall_of_fame, (gur_hall_of_fame: gur_hall_of_fame)=>gur_hall_of_fame.hofSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHallOfFames:gur_hall_of_fame[];
    

   
    @OneToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.mkhSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhow:gur_matik_knowhow | null;


   
    @OneToMany(()=>gur_matik_knowhow_academies, (gur_matik_knowhow_academies: gur_matik_knowhow_academies)=>gur_matik_knowhow_academies.kacSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAcademiess:gur_matik_knowhow_academies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_awards_winners, (gur_matik_knowhow_awards_winners: gur_matik_knowhow_awards_winners)=>gur_matik_knowhow_awards_winners.awwSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsWinnerss:gur_matik_knowhow_awards_winners[];
    

   
    @OneToMany(()=>gur_matik_knowhow_calendar, (gur_matik_knowhow_calendar: gur_matik_knowhow_calendar)=>gur_matik_knowhow_calendar.khcSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCalendars:gur_matik_knowhow_calendar[];
    

   
    @OneToMany(()=>gur_matik_knowhow_calendar, (gur_matik_knowhow_calendar: gur_matik_knowhow_calendar)=>gur_matik_knowhow_calendar.khcSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCalendars2:gur_matik_knowhow_calendar[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_academies, (gur_matik_knowhow_career_academies: gur_matik_knowhow_career_academies)=>gur_matik_knowhow_career_academies.khaSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerAcademiess:gur_matik_knowhow_career_academies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_blogs, (gur_matik_knowhow_career_blogs: gur_matik_knowhow_career_blogs)=>gur_matik_knowhow_career_blogs.khbSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerBlogss:gur_matik_knowhow_career_blogs[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_guides, (gur_matik_knowhow_career_guides: gur_matik_knowhow_career_guides)=>gur_matik_knowhow_career_guides.khgSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerGuidess:gur_matik_knowhow_career_guides[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_guides, (gur_matik_knowhow_career_guides: gur_matik_knowhow_career_guides)=>gur_matik_knowhow_career_guides.khgSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerGuidess2:gur_matik_knowhow_career_guides[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_links, (gur_matik_knowhow_career_links: gur_matik_knowhow_career_links)=>gur_matik_knowhow_career_links.khlSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerLinkss:gur_matik_knowhow_career_links[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_vacancies, (gur_matik_knowhow_career_vacancies: gur_matik_knowhow_career_vacancies)=>gur_matik_knowhow_career_vacancies.khvSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerVacanciess:gur_matik_knowhow_career_vacancies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_reference_links, (gur_matik_knowhow_reference_links: gur_matik_knowhow_reference_links)=>gur_matik_knowhow_reference_links.krlSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowReferenceLinkss:gur_matik_knowhow_reference_links[];
    

   
    @OneToMany(()=>gur_matik_knowhow_teams, (gur_matik_knowhow_teams: gur_matik_knowhow_teams)=>gur_matik_knowhow_teams.khtSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowTeamss:gur_matik_knowhow_teams[];
    

   
    @OneToMany(()=>gur_sports_event_schedule, (gur_sports_event_schedule: gur_sports_event_schedule)=>gur_sports_event_schedule.evsSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsEventSchedules:gur_sports_event_schedule[];
    

   
    @OneToMany(()=>gur_wiki_awards_sports_wise, (gur_wiki_awards_sports_wise: gur_wiki_awards_sports_wise)=>gur_wiki_awards_sports_wise.wasSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsSportsWises:gur_wiki_awards_sports_wise[];
    

   
    @OneToMany(()=>gur_wiki_competitions_sports_map, (gur_wiki_competitions_sports_map: gur_wiki_competitions_sports_map)=>gur_wiki_competitions_sports_map.wsmSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiCompetitionsSportsMaps:gur_wiki_competitions_sports_map[];
    

   
    @OneToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.wkhSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhow:gur_wiki_knowhow | null;


   
    @OneToMany(()=>gur_wiki_sports_equipage, (gur_wiki_sports_equipage: gur_wiki_sports_equipage)=>gur_wiki_sports_equipage.uaeSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsEquipages:gur_wiki_sports_equipage[];
    

   
    @OneToMany(()=>gur_wiki_sports_ls, (gur_wiki_sports_ls: gur_wiki_sports_ls)=>gur_wiki_sports_ls.splSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsLss:gur_wiki_sports_ls[];
    

   
    @OneToMany(()=>gur_wiki_terminologies, (gur_wiki_terminologies: gur_wiki_terminologies)=>gur_wiki_terminologies.terSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiTerminologiess:gur_wiki_terminologies[];
    

   
    @OneToMany(()=>gur_wiki_venues_sports_map, (gur_wiki_venues_sports_map: gur_wiki_venues_sports_map)=>gur_wiki_venues_sports_map.wsmSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiVenuesSportsMaps:gur_wiki_venues_sports_map[];
    

   
    @OneToMany(()=>gur_world_events_records, (gur_world_events_records: gur_world_events_records)=>gur_world_events_records.werSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurWorldEventsRecordss:gur_world_events_records[];
    

   
    @OneToMany(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.evsSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsScheduless:gur_world_events_schedules[];
    

   
    @OneToMany(()=>gur_world_events_teams, (gur_world_events_teams: gur_world_events_teams)=>gur_world_events_teams.wetSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurWorldEventsTeamss:gur_world_events_teams[];
    

   
    @ManyToMany(()=>gur_wiki_popular_sports, (gur_wiki_popular_sports: gur_wiki_popular_sports)=>gur_wiki_popular_sports.gurWikiSportss)
    gurWikiPopularSportss:gur_wiki_popular_sports[];
    

   
    @ManyToMany(()=>gur_wiki_technologies, (gur_wiki_technologies: gur_wiki_technologies)=>gur_wiki_technologies.gurWikiSportss)
    gurWikiTechnologiess:gur_wiki_technologies[];
    
}
