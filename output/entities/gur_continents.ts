import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_sports_governing_bodies} from "./gur_sports_governing_bodies";


@Entity("gur_continents" ,{schema:"sportsmatik_local" } )
export class gur_continents {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"con_id"
        })
    con_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"con_name"
        })
    con_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'Superadmin'",
        name:"con_createdby"
        })
    con_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"con_modifiedby"
        })
    con_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"con_modifiedtime"
        })
    con_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.cntCon,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCountriess:gur_countries[];
    

   
    @OneToMany(()=>gur_sports_governing_bodies, (gur_sports_governing_bodies: gur_sports_governing_bodies)=>gur_sports_governing_bodies.sgbCon,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodiess:gur_sports_governing_bodies[];
    
}
