import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_matik_knowhow_teams} from "./gur_matik_knowhow_teams";


@Entity("gur_world_events_teams" ,{schema:"sportsmatik_local" } )
@Index("wet_name",["wet_name","wetSpo",],{unique:true})
@Index("wep_cnt_id",["wetCnt",])
@Index("wep_spo_id",["wetSpo",])
export class gur_world_events_teams {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wet_id"
        })
    wet_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wet_name"
        })
    wet_name:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWorldEventsTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wet_cnt_id'})
    wetCnt:gur_countries | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wet_logo"
        })
    wet_logo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wet_logo_data"
        })
    wet_logo_data:string | null;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWorldEventsTeamss,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'wet_spo_id'})
    wetSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wet_createdby"
        })
    wet_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wet_modifiedby"
        })
    wet_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wet_modifiedtime"
        })
    wet_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_teams, (gur_matik_knowhow_teams: gur_matik_knowhow_teams)=>gur_matik_knowhow_teams.khtWet,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowTeamss:gur_matik_knowhow_teams[];
    
}
