import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_website_settings" ,{schema:"sportsmatik_local" } )
export class gur_website_settings {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sst_storeid"
        })
    sst_storeid:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sst_site_name"
        })
    sst_site_name:string;
        

    @Column("text",{ 
        nullable:true,
        name:"sst_address"
        })
    sst_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sst_logo"
        })
    sst_logo:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"sst_contact1"
        })
    sst_contact1:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sst_contact2"
        })
    sst_contact2:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sst_email"
        })
    sst_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sst_website"
        })
    sst_website:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sst_facebook_url"
        })
    sst_facebook_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sst_twitter_url"
        })
    sst_twitter_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sst_youtube_url"
        })
    sst_youtube_url:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sst_createdby"
        })
    sst_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sst_modifiedby"
        })
    sst_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sst_modifiedtime"
        })
    sst_modifiedtime:Date;
        
}
