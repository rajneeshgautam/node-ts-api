import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_hours_of_operation} from "./gur_hours_of_operation";


@Entity("gur_hours_of_operation_map" ,{schema:"sportsmatik_local" } )
@Index("hom_hop_id",["homHop",])
export class gur_hours_of_operation_map {

   
    @ManyToOne(()=>gur_hours_of_operation, (gur_hours_of_operation: gur_hours_of_operation)=>gur_hours_of_operation.gurHoursOfOperationMaps,{ primary:true, nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hom_hop_id'})
    homHop:gur_hours_of_operation | null;


    @Column("time",{ 
        nullable:false,
        primary:true,
        name:"hom_start_time"
        })
    hom_start_time:string;
        

    @Column("time",{ 
        nullable:false,
        primary:true,
        name:"hom_end_time"
        })
    hom_end_time:string;
        
}
