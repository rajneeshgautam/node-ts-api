import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_gliding_profile" ,{schema:"sportsmatik_local" } )
@Index("gli_usr_id",["gliUsr",])
@Index("gli_createdby_user",["gliCreatedbyUser",])
export class gur_user_gliding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"gli_id"
        })
    gli_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGlidingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gli_usr_id'})
    gliUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"gli_classes"
        })
    gli_classes:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"gli_modifiedtime"
        })
    gli_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGlidingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gli_createdby_user'})
    gliCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"gli_modifiedby"
        })
    gli_modifiedby:string | null;
        
}
