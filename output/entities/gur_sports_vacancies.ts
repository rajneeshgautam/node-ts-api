import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_sports_vacancies_applicants} from "./gur_sports_vacancies_applicants";
import {gur_sports_vacancies_featured} from "./gur_sports_vacancies_featured";


@Entity("gur_sports_vacancies" ,{schema:"sportsmatik_local" } )
@Index("gur_user_posted_vacancies_ibfk_1",["upvUty",])
@Index("gur_user_posted_vacancies_ibfk_3",["upv_createdby_user",])
@Index("gur_user_posted_vacancies_ibfk_4",["upv_createdby_admin",])
@Index("gur_user_posted_vacancies_ibfk_5",["upv_pln_id",])
@Index("upv_sport",["upvSport",])
@Index("upv_user_type",["upvUserType",])
export class gur_sports_vacancies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"upv_id"
        })
    upv_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"upv_title"
        })
    upv_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"upv_slug"
        })
    upv_slug:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSportsVacanciess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upv_uty_id'})
    upvUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"upv_user_id"
        })
    upv_user_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upv_status"
        })
    upv_status:boolean;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSportsVacanciess2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upv_user_type'})
    upvUserType:gur_user_types | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurSportsVacanciess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upv_sport'})
    upvSport:gur_sports | null;


    @Column("decimal",{ 
        nullable:false,
        precision:3,
        scale:1,
        name:"upv_min_experience"
        })
    upv_min_experience:string;
        

    @Column("decimal",{ 
        nullable:false,
        precision:3,
        scale:1,
        name:"upv_max_experience"
        })
    upv_max_experience:string;
        

    @Column("int",{ 
        nullable:true,
        name:"upv_city"
        })
    upv_city:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"upv_country"
        })
    upv_country:number;
        

    @Column("int",{ 
        nullable:false,
        name:"upv_salary"
        })
    upv_salary:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"upv_salary_currency"
        })
    upv_salary_currency:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:2000,
        name:"upv_description"
        })
    upv_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upv_is_posted"
        })
    upv_is_posted:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"upv_posted_date"
        })
    upv_posted_date:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"upv_posted_final"
        })
    upv_posted_final:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"upv_expiry_date"
        })
    upv_expiry_date:Date | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"upv_last_date"
        })
    upv_last_date:Date;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"upv_createdby_user"
        })
    upv_createdby_user:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"upv_createdby_admin"
        })
    upv_createdby_admin:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"upv_admin_review"
        })
    upv_admin_review:boolean | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"upv_pln_id"
        })
    upv_pln_id:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"upv_createdby"
        })
    upv_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"upv_modifiedby"
        })
    upv_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"upv_modifiedtime"
        })
    upv_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_sports_vacancies_applicants, (gur_sports_vacancies_applicants: gur_sports_vacancies_applicants)=>gur_sports_vacancies_applicants.upaUpv,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciesApplicantss:gur_sports_vacancies_applicants[];
    

   
    @OneToMany(()=>gur_sports_vacancies_featured, (gur_sports_vacancies_featured: gur_sports_vacancies_featured)=>gur_sports_vacancies_featured.svfUpv,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciesFeatureds:gur_sports_vacancies_featured[];
    
}
