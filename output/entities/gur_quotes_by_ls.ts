import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quotes_by} from "./gur_quotes_by";
import {gur_languages} from "./gur_languages";


@Entity("gur_quotes_by_ls" ,{schema:"sportsmatik_local" } )
@Index("qbl_qub_id",["qblQub",])
@Index("qbl_lng_code",["qblLngCode",])
export class gur_quotes_by_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"qbl_id"
        })
    qbl_id:number;
        

   
    @ManyToOne(()=>gur_quotes_by, (gur_quotes_by: gur_quotes_by)=>gur_quotes_by.gurQuotesByLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qbl_qub_id'})
    qblQub:gur_quotes_by | null;


    @Column("varchar",{ 
        nullable:true,
        name:"qbl_quoted_by"
        })
    qbl_quoted_by:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurQuotesByLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qbl_lng_code'})
    qblLngCode:gur_languages | null;

}
