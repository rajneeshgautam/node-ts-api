import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_sports} from "./gur_sports";
import {gur_user_plans} from "./gur_user_plans";
import {gur_sports_official_user_profile_map} from "./gur_sports_official_user_profile_map";


@Entity("gur_sports_official_user_profile" ,{schema:"sportsmatik_local" } )
@Index("tcu_usr_id",["somUser",])
@Index("tcu_createdby",["somCreatedbyUser",])
@Index("som_spo_id",["somSpo",])
@Index("som_pch_id",["somPch",])
export class gur_sports_official_user_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"som_id"
        })
    som_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsOfficialUserProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'som_user_id'})
    somUser:gur_users | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurSportsOfficialUserProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'som_spo_id'})
    somSpo:gur_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"som_type"
        })
    som_type:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"som_status"
        })
    som_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"som_admin_review"
        })
    som_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"som_primary"
        })
    som_primary:boolean;
        

   
    @ManyToOne(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.gurSportsOfficialUserProfiles,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'som_pch_id'})
    somPch:gur_user_plans | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsOfficialUserProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'som_createdby_user'})
    somCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"som_createdby"
        })
    som_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"som_modifiedby"
        })
    som_modifiedby:string;
        

    @Column("date",{ 
        nullable:false,
        name:"som_date_created"
        })
    som_date_created:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"som_date_modified"
        })
    som_date_modified:Date;
        

   
    @OneToMany(()=>gur_sports_official_user_profile_map, (gur_sports_official_user_profile_map: gur_sports_official_user_profile_map)=>gur_sports_official_user_profile_map.spmSom,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialUserProfileMaps:gur_sports_official_user_profile_map[];
    
}
