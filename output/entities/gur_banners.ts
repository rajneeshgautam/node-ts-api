import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_banners_ls} from "./gur_banners_ls";


@Entity("gur_banners" ,{schema:"sportsmatik_local" } )
export class gur_banners {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ban_id"
        })
    ban_id:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"ban_sort_order"
        })
    ban_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ban_status"
        })
    ban_status:boolean | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'website'",
        enum:["affiliates","website"],
        name:"ban_type"
        })
    ban_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ban_photo"
        })
    ban_photo:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"ban_createdby"
        })
    ban_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"ban_modifiedby"
        })
    ban_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ban_modifiedtime"
        })
    ban_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_banners_ls, (gur_banners_ls: gur_banners_ls)=>gur_banners_ls.bnlBan,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBannersLss:gur_banners_ls[];
    
}
