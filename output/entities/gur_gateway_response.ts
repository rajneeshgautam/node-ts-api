import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_gateway_response" ,{schema:"sportsmatik_local" } )
export class gur_gateway_response {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"gtw_id"
        })
    gtw_id:string;
        

    @Column("bigint",{ 
        nullable:false,
        name:"gtw_order_id"
        })
    gtw_order_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"gtw_tid"
        })
    gtw_tid:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"gtw_status"
        })
    gtw_status:string;
        

    @Column("text",{ 
        nullable:false,
        name:"gtw_response"
        })
    gtw_response:string;
        
}
