import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_user_sub_type_map" ,{schema:"sportsmatik_local" } )
@Index("usm_uty_id",["usmUty",])
export class gur_user_sub_type_map {

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserSubTypeMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usm_uty_id'})
    usmUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"usm_user_id"
        })
    usm_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"usm_type"
        })
    usm_type:string;
        
}
