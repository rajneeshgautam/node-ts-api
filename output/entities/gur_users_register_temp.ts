import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_users_register_temp" ,{schema:"sportsmatik_local" } )
@Index("urt_country",["urt_country",])
@Index("urt_uty_id",["urt_uty_id",])
export class gur_users_register_temp {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"urt_id"
        })
    urt_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"urt_uty_id"
        })
    urt_uty_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"urt_fname"
        })
    urt_fname:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"urt_lname"
        })
    urt_lname:string | null;
        

    @Column("date",{ 
        nullable:false,
        name:"urt_dob"
        })
    urt_dob:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"urt_phone"
        })
    urt_phone:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"urt_email"
        })
    urt_email:string;
        

    @Column("int",{ 
        nullable:true,
        name:"urt_country"
        })
    urt_country:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"urt_createdon"
        })
    urt_createdon:Date;
        
}
