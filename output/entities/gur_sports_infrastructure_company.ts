import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_sports_infrastructure_company" ,{schema:"sportsmatik_local" } )
@Index("sic_gst_number",["sic_gst_number",],{unique:true})
@Index("sic_usr_id",["sicUsr",])
@Index("sic_createdby_user",["sicCreatedbyUser",])
@Index("sic_country",["sicCountry",])
@Index("sic_dis_id",["sicDis",])
@Index("sic_sts_id",["sicSts",])
@Index("sic_name",["sic_name",])
export class gur_sports_infrastructure_company {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sic_id"
        })
    sic_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sic_slug"
        })
    sic_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sic_name"
        })
    sic_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsInfrastructureCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sic_usr_id'})
    sicUsr:gur_users | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsInfrastructureCompanys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sic_country'})
    sicCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurSportsInfrastructureCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sic_dis_id'})
    sicDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurSportsInfrastructureCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sic_sts_id'})
    sicSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"sic_address_google_map"
        })
    sic_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sic_address"
        })
    sic_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sic_address2"
        })
    sic_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sic_pincode"
        })
    sic_pincode:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sic_email"
        })
    sic_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sic_email_verified"
        })
    sic_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sic_email_verify_time"
        })
    sic_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sic_phone1"
        })
    sic_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sic_phone1_verified"
        })
    sic_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sic_phone1_otp"
        })
    sic_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sic_phone1_otp_time"
        })
    sic_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sic_phone2"
        })
    sic_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sic_phone2_verified"
        })
    sic_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sic_phone2_otp"
        })
    sic_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sic_phone2_otp_time"
        })
    sic_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"sic_status"
        })
    sic_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sic_profile_pic"
        })
    sic_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sic_intro_status"
        })
    sic_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"sic_profile_summary"
        })
    sic_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sic_founded_year"
        })
    sic_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sic_founded_month"
        })
    sic_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sic_last_login_date"
        })
    sic_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sic_last_login_details"
        })
    sic_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"sic_intro_modifiedtime"
        })
    sic_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sic_profile_pic_modifiedtime"
        })
    sic_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sic_free_sports"
        })
    sic_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sic_free_sub_users"
        })
    sic_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"sic_gst_number"
        })
    sic_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sic_viewable"
        })
    sic_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsInfrastructureCompanys2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sic_createdby_user'})
    sicCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sic_createdby"
        })
    sic_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sic_modifiedby"
        })
    sic_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sic_modifiedtime"
        })
    sic_modifiedtime:Date;
        
}
