import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_yoyo_profile" ,{schema:"sportsmatik_local" } )
@Index("yoy_usr_id",["yoyUsr",])
@Index("yoy_createdby_user",["yoyCreatedbyUser",])
export class gur_user_yoyo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"yoy_id"
        })
    yoy_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserYoyoProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'yoy_usr_id'})
    yoyUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"yoy_modifiedtime"
        })
    yoy_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserYoyoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'yoy_createdby_user'})
    yoyCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"yoy_modifiedby"
        })
    yoy_modifiedby:string | null;
        
}
