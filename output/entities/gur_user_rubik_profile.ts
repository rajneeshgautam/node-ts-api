import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_rubik_profile" ,{schema:"sportsmatik_local" } )
@Index("rub_usr_id",["rubUsr",])
@Index("rub_createdby_user",["rubCreatedbyUser",])
export class gur_user_rubik_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"rub_id"
        })
    rub_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRubikProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rub_usr_id'})
    rubUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rub_modifiedtime"
        })
    rub_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRubikProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rub_createdby_user'})
    rubCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rub_modifiedby"
        })
    rub_modifiedby:string | null;
        
}
