import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_wiki_competitions} from "./gur_wiki_competitions";
import {gur_wiki_technologies} from "./gur_wiki_technologies";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_features" ,{schema:"sportsmatik_local" } )
@Index("wvf_wvn_id",["wvfWvn",])
@Index("wvl_lng_code",["wvfLngCode",])
@Index("wvf_unique_code",["wvf_unique_code",])
@Index("wvf_wcm_id",["wvfWcm",])
@Index("wvf_tec_id",["wvfTec",])
export class gur_wiki_features {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wvf_id"
        })
    wvf_id:string;
        

   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurWikiFeaturess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvf_wvn_id'})
    wvfWvn:gur_wiki_venues | null;


   
    @ManyToOne(()=>gur_wiki_competitions, (gur_wiki_competitions: gur_wiki_competitions)=>gur_wiki_competitions.gurWikiFeaturess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvf_wcm_id'})
    wvfWcm:gur_wiki_competitions | null;


   
    @ManyToOne(()=>gur_wiki_technologies, (gur_wiki_technologies: gur_wiki_technologies)=>gur_wiki_technologies.gurWikiFeaturess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvf_tec_id'})
    wvfTec:gur_wiki_technologies | null;


    @Column("varchar",{ 
        nullable:false,
        length:40,
        name:"wvf_unique_code"
        })
    wvf_unique_code:string;
        

    @Column("text",{ 
        nullable:true,
        name:"wvf_feature_desc"
        })
    wvf_feature_desc:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"wvf_sort_order"
        })
    wvf_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wvf_trivia"
        })
    wvf_trivia:boolean;
        

    @Column("enum",{ 
        nullable:false,
        enum:["venues","technology","competition"],
        name:"wvf_type"
        })
    wvf_type:string;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiFeaturess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvf_lng_code'})
    wvfLngCode:gur_languages | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wvf_createdby"
        })
    wvf_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wvf_modifiedby"
        })
    wvf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wvf_modifiedtime"
        })
    wvf_modifiedtime:Date;
        
}
