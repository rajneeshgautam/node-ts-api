import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_biathlon_profile" ,{schema:"sportsmatik_local" } )
@Index("bia_usr_id",["biaUsr",])
@Index("bia_createdby_user",["biaCreatedbyUser",])
export class gur_user_biathlon_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bia_id"
        })
    bia_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBiathlonProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bia_usr_id'})
    biaUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bia_modifiedtime"
        })
    bia_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBiathlonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bia_createdby_user'})
    biaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bia_modifiedby"
        })
    bia_modifiedby:string | null;
        
}
