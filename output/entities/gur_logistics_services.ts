import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_logistics_services_map} from "./gur_logistics_services_map";


@Entity("gur_logistics_services" ,{schema:"sportsmatik_local" } )
@Index("lgs_user_type",["lgsUserType",])
@Index("lgs_createdby_user",["lgsCreatedbyUser",])
export class gur_logistics_services {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"lgs_id"
        })
    lgs_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"lgs_user_id"
        })
    lgs_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurLogisticsServicess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lgs_user_type'})
    lgsUserType:gur_user_types | null;


    @Column("varchar",{ 
        nullable:false,
        name:"lgs_service_type"
        })
    lgs_service_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"lgs_description"
        })
    lgs_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"lgs_status"
        })
    lgs_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lgs_admin_review"
        })
    lgs_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurLogisticsServicess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lgs_createdby_user'})
    lgsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"lgs_createdby"
        })
    lgs_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lgs_modifiedby"
        })
    lgs_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        default: () => "CURRENT_TIMESTAMP",
        name:"lgs_modified_time"
        })
    lgs_modified_time:Date | null;
        

   
    @OneToMany(()=>gur_logistics_services_map, (gur_logistics_services_map: gur_logistics_services_map)=>gur_logistics_services_map.lsmLgs,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLogisticsServicesMaps:gur_logistics_services_map[];
    
}
