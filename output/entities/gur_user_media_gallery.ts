import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_albums} from "./gur_user_albums";


@Entity("gur_user_media_gallery" ,{schema:"sportsmatik_local" } )
@Index("umg_usr_id",["umgCreatedbyUser",])
@Index("umg_alb_id",["umgAlb",])
export class gur_user_media_gallery {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"umg_id"
        })
    umg_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'umg_createdby_user'})
    umgCreatedbyUser:gur_users | null;


    @Column("int",{ 
        nullable:true,
        name:"umg_createdby_admin"
        })
    umg_createdby_admin:number | null;
        

   
    @ManyToOne(()=>gur_user_albums, (gur_user_albums: gur_user_albums)=>gur_user_albums.gurUserMediaGallerys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'umg_alb_id'})
    umgAlb:gur_user_albums | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"umg_sort_order"
        })
    umg_sort_order:number | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["audio","video","photo"],
        name:"umg_type"
        })
    umg_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"umg_file"
        })
    umg_file:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"umg_video_url"
        })
    umg_video_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"umg_vid_thumbnail"
        })
    umg_vid_thumbnail:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"umg_file_title"
        })
    umg_file_title:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"umg_status"
        })
    umg_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"umg_admin_review"
        })
    umg_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"umg_createdby"
        })
    umg_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"umg_modifiedby"
        })
    umg_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"umg_modifiedtime"
        })
    umg_modifiedtime:Date;
        
}
