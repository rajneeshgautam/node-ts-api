import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_gaelic_football_profile" ,{schema:"sportsmatik_local" } )
@Index("gaf_usr_id",["gafUsr",])
@Index("gaf_position1",["gafPosition",])
@Index("gaf_position2",["gafPosition2",])
@Index("gaf_createdby_user",["gafCreatedbyUser",])
export class gur_user_gaelic_football_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"gaf_id"
        })
    gaf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGaelicFootballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gaf_usr_id'})
    gafUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserGaelicFootballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gaf_position1'})
    gafPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserGaelicFootballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gaf_position2'})
    gafPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"gaf_modifiedtime"
        })
    gaf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGaelicFootballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gaf_createdby_user'})
    gafCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"gaf_modifiedby"
        })
    gaf_modifiedby:string | null;
        
}
