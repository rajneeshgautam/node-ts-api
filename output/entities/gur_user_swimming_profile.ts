import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_swimming_profile" ,{schema:"sportsmatik_local" } )
@Index("swm_usr_id",["swmUsr",])
@Index("swm_createdby_user",["swmCreatedbyUser",])
export class gur_user_swimming_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"swm_id"
        })
    swm_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSwimmingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'swm_usr_id'})
    swmUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"swm_freestyle"
        })
    swm_freestyle:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"swm_backstroke"
        })
    swm_backstroke:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"swm_breaststroke"
        })
    swm_breaststroke:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"swm_butterfly"
        })
    swm_butterfly:boolean | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"swm_modifiedtime"
        })
    swm_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSwimmingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'swm_createdby_user'})
    swmCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"swm_modifiedby"
        })
    swm_modifiedby:string | null;
        
}
