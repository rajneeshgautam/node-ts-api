import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_beach_volleyball_profile" ,{schema:"sportsmatik_local" } )
@Index("pbv_usr_id",["pbvUsr",])
@Index("pbv_disablity_class",["pbvDisablityClass",])
@Index("pbv_createdby_user",["pbvCreatedbyUser",])
export class gur_user_para_beach_volleyball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pbv_id"
        })
    pbv_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBeachVolleyballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbv_usr_id'})
    pbvUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["standing","sitting"],
        name:"pbv_game_type"
        })
    pbv_game_type:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaBeachVolleyballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbv_disablity_class'})
    pbvDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pbv_modifiedtime"
        })
    pbv_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBeachVolleyballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbv_createdby_user'})
    pbvCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pbv_modifiedby"
        })
    pbv_modifiedby:string | null;
        
}
