import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_matik_knowhow_fun_facts_ls} from "./gur_matik_knowhow_fun_facts_ls";


@Entity("gur_matik_knowhow_fun_facts" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_fun_facts_ibfk_1",["khfKnc",])
@Index("gur_matik_knowhow_fun_facts_ibfk_2",["khfMkh",])
export class gur_matik_knowhow_fun_facts {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khf_id"
        })
    khf_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowFunFactss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khf_mkh_id'})
    khfMkh:gur_matik_knowhow | null;


   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowFunFactss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khf_knc_id'})
    khfKnc:gur_matik_knowhow_tab_categories | null;


    @Column("varchar",{ 
        nullable:false,
        name:"khf_createdby"
        })
    khf_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khf_modifiedby"
        })
    khf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khf_modifiedtime"
        })
    khf_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_fun_facts_ls, (gur_matik_knowhow_fun_facts_ls: gur_matik_knowhow_fun_facts_ls)=>gur_matik_knowhow_fun_facts_ls.kflKhf,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowFunFactsLss:gur_matik_knowhow_fun_facts_ls[];
    
}
