import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_email_to_be_sent} from "./gur_email_to_be_sent";


@Entity("gur_email_template" ,{schema:"sportsmatik_local" } )
export class gur_email_template {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"emt_id"
        })
    emt_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"emt_name"
        })
    emt_name:string;
        

    @Column("mediumtext",{ 
        nullable:false,
        name:"emt_body"
        })
    emt_body:string;
        

    @Column("tinytext",{ 
        nullable:true,
        name:"emt_attachments"
        })
    emt_attachments:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"emt_created_at"
        })
    emt_created_at:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"emt_updated_at"
        })
    emt_updated_at:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"emt_is_active"
        })
    emt_is_active:boolean;
        

   
    @OneToMany(()=>gur_email_to_be_sent, (gur_email_to_be_sent: gur_email_to_be_sent)=>gur_email_to_be_sent.etsEmt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEmailToBeSents:gur_email_to_be_sent[];
    
}
