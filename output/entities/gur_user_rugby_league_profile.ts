import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_rugby_league_profile" ,{schema:"sportsmatik_local" } )
@Index("rgl_usr_id",["rglUsr",])
@Index("rgl_positions1",["rglPosition",])
@Index("rgl_positions2",["rglPosition2",])
@Index("rgl_createdby_user",["rglCreatedbyUser",])
export class gur_user_rugby_league_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"rgl_id"
        })
    rgl_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbyLeagueProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgl_usr_id'})
    rglUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRugbyLeagueProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgl_position1'})
    rglPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRugbyLeagueProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgl_position2'})
    rglPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rgl_modifiedtime"
        })
    rgl_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRugbyLeagueProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rgl_createdby_user'})
    rglCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rgl_modifiedby"
        })
    rgl_modifiedby:string | null;
        
}
