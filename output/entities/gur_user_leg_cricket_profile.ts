import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_leg_cricket_profile" ,{schema:"sportsmatik_local" } )
@Index("hrb_usr_id",["lgcUsr",])
@Index("hrb_createdby_user",["lgcCreatedbyUser",])
export class gur_user_leg_cricket_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"lgc_id"
        })
    lgc_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserLegCricketProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lgc_usr_id'})
    lgcUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lgc_modifiedtime"
        })
    lgc_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserLegCricketProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lgc_createdby_user'})
    lgcCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"lgc_modifiedby"
        })
    lgc_modifiedby:string | null;
        
}
