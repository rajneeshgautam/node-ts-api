import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_job_category} from "./gur_job_category";
import {gur_job} from "./gur_job";


@Entity("gur_job_profile" ,{schema:"sportsmatik_local" } )
@Index("job_slug",["jop_slug",],{unique:true})
@Index("jop_code",["jop_code",],{unique:true})
@Index("jop_joc_id",["jopJoc",])
export class gur_job_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"jop_id"
        })
    jop_id:number;
        

   
    @ManyToOne(()=>gur_job_category, (gur_job_category: gur_job_category)=>gur_job_category.gurJobProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jop_joc_id'})
    jopJoc:gur_job_category | null;


    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"jop_title"
        })
    jop_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"jop_slug"
        })
    jop_slug:string;
        

    @Column("char",{ 
        nullable:false,
        unique: true,
        length:3,
        name:"jop_code"
        })
    jop_code:string;
        

    @Column("tinytext",{ 
        nullable:true,
        name:"jop_description"
        })
    jop_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"jop_is_active"
        })
    jop_is_active:boolean;
        

    @Column("datetime",{ 
        nullable:false,
        name:"jop_created_at"
        })
    jop_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"jop_updated_at"
        })
    jop_updated_at:Date | null;
        

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobJop,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    gurJobs:gur_job[];
    
}
