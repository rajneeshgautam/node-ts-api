import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_user_plans} from "./gur_user_plans";


@Entity("gur_corporates_sport_map" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["csmUty",])
@Index("csm_spo_id",["csmSpo",])
@Index("csm_createdby_user",["csm_createdby_user",])
@Index("gur_corporates_sport_map_ibfk_3",["csmPch",])
export class gur_corporates_sport_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"csm_id"
        })
    csm_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCorporatesSportMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_uty_id'})
    csmUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"csm_user_id"
        })
    csm_user_id:number;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurCorporatesSportMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_spo_id'})
    csmSpo:gur_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"csm_primary"
        })
    csm_primary:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"csm_type"
        })
    csm_type:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"csm_status"
        })
    csm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"csm_admin_review"
        })
    csm_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"csm_apporoved"
        })
    csm_apporoved:boolean;
        

   
    @ManyToOne(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.gurCorporatesSportMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_pch_id'})
    csmPch:gur_user_plans | null;


    @Column("date",{ 
        nullable:false,
        name:"csm_date_created"
        })
    csm_date_created:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"csm_createdby"
        })
    csm_createdby:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"csm_createdby_user"
        })
    csm_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"csm_modifiedby"
        })
    csm_modifiedby:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"csm_date_modified"
        })
    csm_date_modified:Date | null;
        
}
