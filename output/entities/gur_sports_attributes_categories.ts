import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports} from "./gur_sports";
import {gur_sports_attributes} from "./gur_sports_attributes";


@Entity("gur_sports_attributes_categories" ,{schema:"sportsmatik_local" } )
@Index("gur_sports_attributes_categories_ibfk_1",["sacSpo",])
export class gur_sports_attributes_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sac_id"
        })
    sac_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sac_title"
        })
    sac_title:string;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurSportsAttributesCategoriess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sac_spo_id'})
    sacSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"sac_createdby"
        })
    sac_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sac_modifiedby"
        })
    sac_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sac_modifiedtime"
        })
    sac_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_sports_attributes, (gur_sports_attributes: gur_sports_attributes)=>gur_sports_attributes.sabSac,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsAttributess:gur_sports_attributes[];
    
}
