import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_institutional_staff_map} from "./gur_institutional_staff_map";
import {gur_institutional_teams_map} from "./gur_institutional_teams_map";


@Entity("gur_institutional_staff" ,{schema:"sportsmatik_local" } )
@Index("inl_uty_id",["itsUty",])
@Index("inl_name",["its_fname",])
@Index("its_createdby_user",["itsCreatedbyUser",])
@Index("its_lname",["its_lname",])
export class gur_institutional_staff {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"its_id"
        })
    its_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInstitutionalStaffs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'its_uty_id'})
    itsUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"its_user_id"
        })
    its_user_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"its_type"
        })
    its_type:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"its_fname"
        })
    its_fname:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"its_mname"
        })
    its_mname:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"its_lname"
        })
    its_lname:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"its_dob"
        })
    its_dob:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"its_phone"
        })
    its_phone:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"its_email"
        })
    its_email:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"its_desc"
        })
    its_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"its_photo"
        })
    its_photo:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"its_star_performer"
        })
    its_star_performer:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"its_is_coach"
        })
    its_is_coach:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"its_status"
        })
    its_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"its_admin_review"
        })
    its_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInstitutionalStaffs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'its_createdby_user'})
    itsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"its_createdby"
        })
    its_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"its_modifiedby"
        })
    its_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"its_modifiedtime"
        })
    its_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_institutional_staff_map, (gur_institutional_staff_map: gur_institutional_staff_map)=>gur_institutional_staff_map.ismIts,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalStaffMaps:gur_institutional_staff_map[];
    

   
    @OneToMany(()=>gur_institutional_teams_map, (gur_institutional_teams_map: gur_institutional_teams_map)=>gur_institutional_teams_map.itmMember,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTeamsMaps:gur_institutional_teams_map[];
    
}
