import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_community_question} from "./gur_community_question";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_community_question_like" ,{schema:"sportsmatik_local" } )
@Index("cql_com_id",["cqlCom","cql_user_id","cqlUty",],{unique:true})
@Index("clk_uty_id",["cqlUty",])
@Index("cql_user_id",["cql_user_id",])
export class gur_community_question_like {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cql_id"
        })
    cql_id:number;
        

   
    @ManyToOne(()=>gur_community_question, (gur_community_question: gur_community_question)=>gur_community_question.gurCommunityQuestionLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cql_com_id'})
    cqlCom:gur_community_question | null;


    @Column("bigint",{ 
        nullable:false,
        name:"cql_user_id"
        })
    cql_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCommunityQuestionLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cql_uty_id'})
    cqlUty:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"cql_is_liked"
        })
    cql_is_liked:boolean;
        
}
