import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_users_backup" ,{schema:"sportsmatik_local" } )
@Index("usr_slug",["usr_slug","usr_type",],{unique:true})
@Index("user_referred_by",["user_referred_by",])
@Index("user_referred_by_type",["user_referred_by_type",])
@Index("usr_aff_code",["usr_aff_code",])
@Index("usr_country",["usr_country",])
@Index("usr_dis_id",["usr_dis_id",])
@Index("usr_email",["usr_email",])
@Index("usr_fname",["usr_fname",])
@Index("usr_salutation",["usr_salutation",])
@Index("usr_sts_id",["usr_sts_id",])
@Index("usr_type",["usr_type",])
export class gur_users_backup {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"usr_id"
        })
    usr_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_slug"
        })
    usr_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_pin"
        })
    usr_pin:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"usr_type"
        })
    usr_type:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_additional"
        })
    usr_additional:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"user_referred_by"
        })
    user_referred_by:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"user_referred_by_type"
        })
    user_referred_by_type:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"usr_aff_code"
        })
    usr_aff_code:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"usr_salutation"
        })
    usr_salutation:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"usr_fname"
        })
    usr_fname:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_mname"
        })
    usr_mname:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_lname"
        })
    usr_lname:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_screen_name"
        })
    usr_screen_name:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","o"],
        name:"usr_sex"
        })
    usr_sex:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"usr_birthday"
        })
    usr_birthday:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_differently_abled"
        })
    usr_differently_abled:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_email"
        })
    usr_email:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"usr_password"
        })
    usr_password:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_cookie_selector"
        })
    usr_cookie_selector:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_cookie_verifier"
        })
    usr_cookie_verifier:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"usr_email_verified"
        })
    usr_email_verified:boolean | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_email_verify_time"
        })
    usr_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"usr_phone1"
        })
    usr_phone1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_phone1_otp"
        })
    usr_phone1_otp:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_phone1_otp_time"
        })
    usr_phone1_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"usr_phone1_verified"
        })
    usr_phone1_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"usr_phone2"
        })
    usr_phone2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_phone2_otp"
        })
    usr_phone2_otp:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_phone2_otp_time"
        })
    usr_phone2_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"usr_phone2_verified"
        })
    usr_phone2_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_address"
        })
    usr_address:string | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"usr_height"
        })
    usr_height:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"usr_weight"
        })
    usr_weight:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"usr_sts_id"
        })
    usr_sts_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"usr_dis_id"
        })
    usr_dis_id:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"usr_country"
        })
    usr_country:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_pincode"
        })
    usr_pincode:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"usr_profile_summary"
        })
    usr_profile_summary:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Unverified'",
        enum:["Active","Inactive","Deleted","Unverified","Deactivated"],
        name:"usr_status"
        })
    usr_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_profile_pic"
        })
    usr_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_timeline_pic"
        })
    usr_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_intro_status"
        })
    usr_intro_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_view_contact_person"
        })
    usr_view_contact_person:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_last_login_date"
        })
    usr_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"usr_last_login_ip"
        })
    usr_last_login_ip:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_register_date"
        })
    usr_register_date:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"usr_intro_modifiedtime"
        })
    usr_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_profile_pic_modifiedtime"
        })
    usr_profile_pic_modifiedtime:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_password_reset_pin"
        })
    usr_password_reset_pin:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_password_reset_time"
        })
    usr_password_reset_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"usr_free_sports"
        })
    usr_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"usr_free_sub_users"
        })
    usr_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_consent_form"
        })
    usr_consent_form:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_viewable"
        })
    usr_viewable:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_google_id"
        })
    usr_google_id:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"usr_dob_change"
        })
    usr_dob_change:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_createdby"
        })
    usr_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"usr_modifiedby"
        })
    usr_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"usr_modifiedtime"
        })
    usr_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"usr_nationality"
        })
    usr_nationality:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_team"
        })
    usr_team:string | null;
        
}
