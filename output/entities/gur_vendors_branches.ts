import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_vendors_branches" ,{schema:"sportsmatik_local" } )
@Index("vbr_country",["vbrCountry",])
@Index("vbr_ven_id",["vbrVen",])
@Index("vbr_createdby_admin",["vbr_createdby_admin",])
@Index("vbr_createdby_user",["vbr_createdby_user",])
@Index("vbr_dis_id",["vbrCit",])
export class gur_vendors_branches {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"vbr_id"
        })
    vbr_id:number;
        

   
    @ManyToOne(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorsBranchess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vbr_ven_id'})
    vbrVen:gur_vendors | null;


    @Column("varchar",{ 
        nullable:false,
        name:"vbr_address"
        })
    vbr_address:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vbr_address2"
        })
    vbr_address2:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurVendorsBranchess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vbr_country'})
    vbrCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurVendorsBranchess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vbr_cit_id'})
    vbrCit:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"vbr_pincode"
        })
    vbr_pincode:string;
        

    @Column("text",{ 
        nullable:true,
        name:"vbr_address_google_map"
        })
    vbr_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"vbr_email"
        })
    vbr_email:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vbr_email_verified"
        })
    vbr_email_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"vbr_phone1"
        })
    vbr_phone1:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vbr_phone1_verified"
        })
    vbr_phone1_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"vbr_phone2"
        })
    vbr_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"vbr_phone2_verified"
        })
    vbr_phone2_verified:boolean | null;
        

    @Column("text",{ 
        nullable:true,
        name:"vbr_desc"
        })
    vbr_desc:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"vbr_createdby_user"
        })
    vbr_createdby_user:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"vbr_createdby_admin"
        })
    vbr_createdby_admin:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"vbr_status"
        })
    vbr_status:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vbr_admin_review"
        })
    vbr_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vbr_modifiedby"
        })
    vbr_modifiedby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vbr_createdby"
        })
    vbr_createdby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vbr_modifiedtime"
        })
    vbr_modifiedtime:Date;
        
}
