import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_wiki_knowhow_rules_ls} from "./gur_wiki_knowhow_rules_ls";


@Entity("gur_wiki_knowhow_rules" ,{schema:"sportsmatik_local" } )
@Index("wrl_wkh_id",["wrlWkh",])
export class gur_wiki_knowhow_rules {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wrl_id"
        })
    wrl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.gurWikiKnowhowRuless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wrl_wkh_id'})
    wrlWkh:gur_wiki_knowhow | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wrl_photo"
        })
    wrl_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wrl_photo_source_details"
        })
    wrl_photo_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wrl_photo_title"
        })
    wrl_photo_title:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["url","file"],
        name:"wrl_video_type"
        })
    wrl_video_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wrl_video"
        })
    wrl_video:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wrl_video_source_details"
        })
    wrl_video_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wrl_video_title"
        })
    wrl_video_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wrl_createdby"
        })
    wrl_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wrl_modifiedby"
        })
    wrl_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wrl_modifiedtime"
        })
    wrl_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_knowhow_rules_ls, (gur_wiki_knowhow_rules_ls: gur_wiki_knowhow_rules_ls)=>gur_wiki_knowhow_rules_ls.rulWrl,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowRulesLss:gur_wiki_knowhow_rules_ls[];
    
}
