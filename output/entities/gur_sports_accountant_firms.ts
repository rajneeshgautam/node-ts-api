import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_sports_accountant_firms" ,{schema:"sportsmatik_local" } )
@Index("clg_gst_number",["cha_gst_number",],{unique:true})
@Index("clg_usr_id",["cha_usr_id",])
@Index("clg_createdby_user",["cha_createdby_user",])
@Index("clg_country",["cha_country",])
@Index("clg_sts_id",["cha_sts_id",])
@Index("clg_dis_id",["cha_dis_id",])
@Index("clg_name",["cha_name",])
@Index("clg_name_2",["cha_name",])
export class gur_sports_accountant_firms {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cha_id"
        })
    cha_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cha_slug"
        })
    cha_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cha_name"
        })
    cha_name:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"cha_usr_id"
        })
    cha_usr_id:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cha_address"
        })
    cha_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cha_address2"
        })
    cha_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"cha_pincode"
        })
    cha_pincode:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"cha_country"
        })
    cha_country:number;
        

    @Column("int",{ 
        nullable:true,
        name:"cha_dis_id"
        })
    cha_dis_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cha_sts_id"
        })
    cha_sts_id:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"cha_address_google_map"
        })
    cha_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cha_email"
        })
    cha_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cha_email_verified"
        })
    cha_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cha_email_verify_time"
        })
    cha_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"cha_phone1"
        })
    cha_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cha_phone1_verified"
        })
    cha_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"cha_phone1_otp"
        })
    cha_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cha_phone1_otp_time"
        })
    cha_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"cha_phone2"
        })
    cha_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cha_phone2_verified"
        })
    cha_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"cha_phone2_otp"
        })
    cha_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cha_phone2_otp_time"
        })
    cha_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"cha_status"
        })
    cha_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cha_profile_pic"
        })
    cha_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cha_intro_status"
        })
    cha_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"cha_profile_summary"
        })
    cha_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cha_founded_year"
        })
    cha_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cha_founded_month"
        })
    cha_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cha_last_login_date"
        })
    cha_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cha_last_login_details"
        })
    cha_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"cha_intro_modifiedtime"
        })
    cha_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cha_profile_pic_modifiedtime"
        })
    cha_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"cha_free_sports"
        })
    cha_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"cha_free_sub_users"
        })
    cha_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"cha_gst_number"
        })
    cha_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cha_viewable"
        })
    cha_viewable:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"cha_createdby_user"
        })
    cha_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cha_createdby"
        })
    cha_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cha_modifiedby"
        })
    cha_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cha_modifiedtime"
        })
    cha_modifiedtime:Date;
        
}
