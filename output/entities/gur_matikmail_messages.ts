import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matikmail_masters} from "./gur_matikmail_masters";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_matikmail_messages" ,{schema:"sportsmatik_local" } )
@Index("ems_usr_sender",["ems_usr_sender",])
@Index("ems_usr_recipient",["ems_usr_recipient",])
@Index("ems_createdby",["ems_createdby",])
@Index("ems_usr_sender_type",["emsUsrSenderType",])
@Index("ems_usr_recipient_type",["emsUsrRecipientType",])
@Index("ems_eml_id",["emsEml",])
export class gur_matikmail_messages {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ems_id"
        })
    ems_id:string;
        

   
    @ManyToOne(()=>gur_matikmail_masters, (gur_matikmail_masters: gur_matikmail_masters)=>gur_matikmail_masters.gurMatikmailMessagess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ems_eml_id'})
    emsEml:gur_matikmail_masters | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMatikmailMessagess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ems_usr_sender_type'})
    emsUsrSenderType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ems_usr_sender"
        })
    ems_usr_sender:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMatikmailMessagess2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ems_usr_recipient_type'})
    emsUsrRecipientType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ems_usr_recipient"
        })
    ems_usr_recipient:number;
        

    @Column("text",{ 
        nullable:true,
        name:"ems_message"
        })
    ems_message:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ems_sent_datetime"
        })
    ems_sent_datetime:Date;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ems_recipient_read"
        })
    ems_recipient_read:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ems_usr_sender_show"
        })
    ems_usr_sender_show:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ems_usr_recipient_show"
        })
    ems_usr_recipient_show:boolean;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ems_createdby"
        })
    ems_createdby:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ems_modifiedby"
        })
    ems_modifiedby:string | null;
        
}
