import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_wheelchair_cricket_profile" ,{schema:"sportsmatik_local" } )
@Index("wcp_player_type",["wcpPlayerType",])
@Index("wcp_usr_id",["wcpUsr",])
@Index("wcp_batting_positions",["wcpBattingPositions",])
@Index("wcp_batting_nature",["wcpPlayingNature",])
@Index("wcp_bowling_style",["wcpBowlingStyle",])
@Index("wcp_createdby_user",["wcpCreatedbyUser",])
export class gur_user_wheelchair_cricket_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wcp_id"
        })
    wcp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWheelchairCricketProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcp_usr_id'})
    wcpUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWheelchairCricketProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcp_player_type'})
    wcpPlayerType:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"wcp_batting_hand"
        })
    wcp_batting_hand:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWheelchairCricketProfiles2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcp_batting_positions'})
    wcpBattingPositions:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWheelchairCricketProfiles3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcp_playing_nature'})
    wcpPlayingNature:gur_value_list | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"wcp_bowling_hand"
        })
    wcp_bowling_hand:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWheelchairCricketProfiles4,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcp_bowling_style'})
    wcpBowlingStyle:gur_value_list | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWheelchairCricketProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcp_createdby_user'})
    wcpCreatedbyUser:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wcp_modifiedtime"
        })
    wcp_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcp_modifiedby"
        })
    wcp_modifiedby:string | null;
        
}
