import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_quiz_results} from "./gur_matik_play_quiz_results";


@Entity("gur_matik_play_quiz_results_answers_set" ,{schema:"sportsmatik_local" } )
@Index("mpw_mpc_id",["rasPqr",])
@Index("ras_pqr_id",["rasPqr",])
export class gur_matik_play_quiz_results_answers_set {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ras_id"
        })
    ras_id:number;
        

   
    @ManyToOne(()=>gur_matik_play_quiz_results, (gur_matik_play_quiz_results: gur_matik_play_quiz_results)=>gur_matik_play_quiz_results.gurMatikPlayQuizResultsAnswersSets,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'ras_pqr_id'})
    rasPqr:gur_matik_play_quiz_results | null;


    @Column("text",{ 
        nullable:false,
        name:"ras_question"
        })
    ras_question:string;
        

    @Column("text",{ 
        nullable:false,
        name:"ras_options"
        })
    ras_options:string;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        name:"ras_correct_answer"
        })
    ras_correct_answer:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        name:"ras_answer_attempted"
        })
    ras_answer_attempted:number;
        
}
