import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_boxing_stats" ,{schema:"sportsmatik_local" } )
@Index("bos_usr_id",["bosUsr",])
@Index("bos_ucp_id",["bosUcp",])
@Index("bos_weight_category",["bosWeightCategory",])
@Index("bos_rank",["bosRank",])
export class gur_user_boxing_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"bos_id"
        })
    bos_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBoxingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bos_usr_id'})
    bosUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserBoxingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bos_ucp_id'})
    bosUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserBoxingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bos_weight_category'})
    bosWeightCategory:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"bos_matches_played"
        })
    bos_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bos_matches_won"
        })
    bos_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bos_matches_loss"
        })
    bos_matches_loss:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"bos_win_by_knockout"
        })
    bos_win_by_knockout:number | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserBoxingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bos_rank'})
    bosRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bos_modifiedtime"
        })
    bos_modifiedtime:Date;
        
}
