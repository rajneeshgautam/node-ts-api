import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_variants} from "./gur_matik_knowhow_variants";


@Entity("gur_matik_knowhow_variants_ls" ,{schema:"sportsmatik_local" } )
@Index("khl_lng_code",["kvl_lng_code",])
@Index("khl_wkh_id",["kvlKhv",])
export class gur_matik_knowhow_variants_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"kvl_id"
        })
    kvl_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_variants, (gur_matik_knowhow_variants: gur_matik_knowhow_variants)=>gur_matik_knowhow_variants.gurMatikKnowhowVariantsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kvl_khv_id'})
    kvlKhv:gur_matik_knowhow_variants | null;


    @Column("varchar",{ 
        nullable:true,
        name:"kvl_title"
        })
    kvl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"kvl_description"
        })
    kvl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"kvl_lng_code"
        })
    kvl_lng_code:string;
        
}
