import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_speed_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("sps_usr_id",["spsUsr",])
@Index("sps_createdby_user",["spsCreatedbyUser",])
export class gur_user_speed_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sps_id"
        })
    sps_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSpeedSkiingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sps_usr_id'})
    spsUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sps_modifiedtime"
        })
    sps_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSpeedSkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sps_createdby_user'})
    spsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sps_modifiedby"
        })
    sps_modifiedby:string | null;
        
}
