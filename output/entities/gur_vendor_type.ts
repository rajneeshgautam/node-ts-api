import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";


@Entity("gur_vendor_type" ,{schema:"sportsmatik_local" } )
@Index("ven_title",["vnt_title",],{unique:true})
export class gur_vendor_type {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"vnt_id"
        })
    vnt_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"vnt_title"
        })
    vnt_title:string;
        

   
    @ManyToMany(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorTypes)
    gurVendorss:gur_vendors[];
    
}
