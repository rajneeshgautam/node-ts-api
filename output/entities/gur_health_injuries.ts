import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_health_injuries_titles} from "./gur_health_injuries_titles";
import {gur_health_injuries_ls} from "./gur_health_injuries_ls";


@Entity("gur_health_injuries" ,{schema:"sportsmatik_local" } )
@Index("gur_health_injuries_ibfk_1",["hwiHwt",])
export class gur_health_injuries {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"hwi_id"
        })
    hwi_id:number;
        

   
    @ManyToOne(()=>gur_health_injuries_titles, (gur_health_injuries_titles: gur_health_injuries_titles)=>gur_health_injuries_titles.gurHealthInjuriess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hwi_hwt_id'})
    hwiHwt:gur_health_injuries_titles | null;


    @Column("varchar",{ 
        nullable:false,
        name:"hwi_createdby"
        })
    hwi_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hwi_modifiedby"
        })
    hwi_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hwi_modifiedtime"
        })
    hwi_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_health_injuries_ls, (gur_health_injuries_ls: gur_health_injuries_ls)=>gur_health_injuries_ls.wilHwi,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthInjuriesLss:gur_health_injuries_ls[];
    
}
