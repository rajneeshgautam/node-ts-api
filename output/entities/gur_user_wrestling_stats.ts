import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_wrestling_stats" ,{schema:"sportsmatik_local" } )
@Index("wrs_usr_id",["wrsUsr",])
@Index("wrs_ucp_id",["wrsUcp",])
@Index("wrs_freestyle_weight_category",["wrsWeightCategory",])
@Index("wrs_rank",["wrsRank",])
export class gur_user_wrestling_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wrs_id"
        })
    wrs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWrestlingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wrs_usr_id'})
    wrsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserWrestlingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wrs_ucp_id'})
    wrsUcp:gur_user_competition_played | null;


    @Column("enum",{ 
        nullable:false,
        enum:["f","gr"],
        name:"wrs_type"
        })
    wrs_type:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWrestlingStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wrs_weight_category'})
    wrsWeightCategory:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"wrs_matches_played"
        })
    wrs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wrs_matches_won"
        })
    wrs_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wrs_matches_loss"
        })
    wrs_matches_loss:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWrestlingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wrs_rank'})
    wrsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wrs_modifiedtime"
        })
    wrs_modifiedtime:Date;
        
}
