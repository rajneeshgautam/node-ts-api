import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_pricing_plan_categories} from "./gur_pricing_plan_categories";
import {gur_pricing_plans_map} from "./gur_pricing_plans_map";


@Entity("gur_pricing_plans" ,{schema:"sportsmatik_local" } )
@Index("uniq",["prp_name","prpCategory",],{unique:true})
@Index("prp_hsn",["prp_hsn",],{unique:true})
@Index("prp_category",["prpCategory",])
export class gur_pricing_plans {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"prp_id"
        })
    prp_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"prp_name"
        })
    prp_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"prp_hsn"
        })
    prp_hsn:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prp_status"
        })
    prp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prp_is_free"
        })
    prp_is_free:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prp_is_best"
        })
    prp_is_best:boolean;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"prp_credits"
        })
    prp_credits:number;
        

   
    @ManyToOne(()=>gur_pricing_plan_categories, (gur_pricing_plan_categories: gur_pricing_plan_categories)=>gur_pricing_plan_categories.gurPricingPlanss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prp_category'})
    prpCategory:gur_pricing_plan_categories | null;


    @Column("int",{ 
        nullable:false,
        name:"prp_duration"
        })
    prp_duration:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["day","month","year"],
        name:"prp_duration_unit"
        })
    prp_duration_unit:string;
        

    @Column("text",{ 
        nullable:true,
        name:"prp_description"
        })
    prp_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"prp_createdby"
        })
    prp_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"prp_modifiedby"
        })
    prp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"prp_modifiedtime"
        })
    prp_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_pricing_plans_map, (gur_pricing_plans_map: gur_pricing_plans_map)=>gur_pricing_plans_map.prmPrp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPricingPlansMaps:gur_pricing_plans_map[];
    
}
