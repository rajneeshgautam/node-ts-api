import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_matikmail_messages} from "./gur_matikmail_messages";


@Entity("gur_matikmail_masters" ,{schema:"sportsmatik_local" } )
@Index("eml_usr_sender",["eml_usr_sender",])
@Index("eml_usr_recipient",["eml_usr_recipient",])
@Index("eml_usr_sender_type",["emlUsrSenderType",])
@Index("eml_usr_recipient_type",["emlUsrRecipientType",])
@Index("eml_createdby",["eml_createdby",])
export class gur_matikmail_masters {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"eml_id"
        })
    eml_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMatikmailMasterss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'eml_usr_sender_type'})
    emlUsrSenderType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"eml_usr_sender"
        })
    eml_usr_sender:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMatikmailMasterss2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'eml_usr_recipient_type'})
    emlUsrRecipientType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"eml_usr_recipient"
        })
    eml_usr_recipient:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"eml_subject"
        })
    eml_subject:string;
        

    @Column("text",{ 
        nullable:true,
        name:"eml_message"
        })
    eml_message:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"eml_sent_datetime"
        })
    eml_sent_datetime:Date;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"eml_createdby"
        })
    eml_createdby:number;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"eml_recipient_read"
        })
    eml_recipient_read:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"eml_trash_sender"
        })
    eml_trash_sender:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"eml_trash_recipeint"
        })
    eml_trash_recipeint:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"eml_modifiedby"
        })
    eml_modifiedby:string | null;
        

   
    @OneToMany(()=>gur_matikmail_messages, (gur_matikmail_messages: gur_matikmail_messages)=>gur_matikmail_messages.emsEml,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikmailMessagess:gur_matikmail_messages[];
    
}
