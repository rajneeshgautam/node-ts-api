import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_support_ticket} from "./gur_support_ticket";


@Entity("gur_admin_user_departments" ,{schema:"sportsmatik_local" } )
export class gur_admin_user_departments {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"aud_id"
        })
    aud_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"aud_name"
        })
    aud_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"aud_slug"
        })
    aud_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aud_for_user"
        })
    aud_for_user:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"aud_name_user"
        })
    aud_name_user:string | null;
        

   
    @OneToMany(()=>gur_support_ticket, (gur_support_ticket: gur_support_ticket)=>gur_support_ticket.stkDepartment,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSupportTickets:gur_support_ticket[];
    
}
