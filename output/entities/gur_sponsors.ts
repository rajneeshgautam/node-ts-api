import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_sponsors" ,{schema:"sportsmatik_local" } )
@Index("clb_phone1",["spn_phone1",],{unique:true})
@Index("clb_phone2",["spn_phone2",],{unique:true})
@Index("clb_slug",["spn_slug",],{unique:true})
@Index("spn_gst_number",["spn_gst_number",],{unique:true})
@Index("clb_createdby_user",["spn_createdby_user",])
@Index("clb_country",["spn_country",])
@Index("clb_usr_id",["spnUsr",])
@Index("clb_dis_id",["spn_dis_id",])
@Index("clb_sts_id",["spn_sts_id",])
@Index("spn_name",["spn_name",])
export class gur_sponsors {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"spn_id"
        })
    spn_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        name:"spn_slug"
        })
    spn_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"spn_name"
        })
    spn_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSponsorss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'spn_usr_id'})
    spnUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"spn_address"
        })
    spn_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spn_address2"
        })
    spn_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"spn_pincode"
        })
    spn_pincode:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"spn_country"
        })
    spn_country:number;
        

    @Column("int",{ 
        nullable:true,
        name:"spn_dis_id"
        })
    spn_dis_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"spn_sts_id"
        })
    spn_sts_id:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"spn_address_google_map"
        })
    spn_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spn_email"
        })
    spn_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spn_email_verified"
        })
    spn_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"spn_email_verify_time"
        })
    spn_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"spn_phone1"
        })
    spn_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spn_phone1_verified"
        })
    spn_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"spn_phone1_otp"
        })
    spn_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"spn_phone1_otp_time"
        })
    spn_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"spn_phone2"
        })
    spn_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spn_phone2_verified"
        })
    spn_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"spn_phone2_otp"
        })
    spn_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"spn_phone2_otp_time"
        })
    spn_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"spn_status"
        })
    spn_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spn_profile_pic"
        })
    spn_profile_pic:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"spn_profile_summary"
        })
    spn_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"spn_founded_year"
        })
    spn_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"spn_founded_month"
        })
    spn_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"spn_last_login_date"
        })
    spn_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spn_last_login_details"
        })
    spn_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"spn_intro_modifiedtime"
        })
    spn_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"spn_profile_pic_modifiedtime"
        })
    spn_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"spn_free_sports"
        })
    spn_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"spn_free_sub_users"
        })
    spn_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"spn_gst_number"
        })
    spn_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spn_viewable"
        })
    spn_viewable:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"spn_createdby_user"
        })
    spn_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"spn_createdby"
        })
    spn_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"spn_modifiedby"
        })
    spn_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"spn_modifiedtime"
        })
    spn_modifiedtime:Date;
        
}
