import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_community_answer} from "./gur_community_answer";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_community_answer_like" ,{schema:"sportsmatik_local" } )
@Index("cal_cma_id",["calCma","cal_user_id","calUty",],{unique:true})
@Index("cal_user_id",["cal_user_id",])
@Index("cal_uty_id",["calUty",])
export class gur_community_answer_like {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cal_id"
        })
    cal_id:number;
        

   
    @ManyToOne(()=>gur_community_answer, (gur_community_answer: gur_community_answer)=>gur_community_answer.gurCommunityAnswerLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cal_cma_id'})
    calCma:gur_community_answer | null;


    @Column("bigint",{ 
        nullable:false,
        name:"cal_user_id"
        })
    cal_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCommunityAnswerLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cal_uty_id'})
    calUty:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"cal_is_liked"
        })
    cal_is_liked:boolean;
        
}
