import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports_equipage} from "./gur_wiki_sports_equipage";


@Entity("gur_wiki_sports_equipage_desc" ,{schema:"sportsmatik_local" } )
@Index("uel_lng_code",["ued_lng_code",])
@Index("uel_title",["ued_title",])
@Index("uel_uae_id",["uedUae",])
export class gur_wiki_sports_equipage_desc {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ued_id"
        })
    ued_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports_equipage, (gur_wiki_sports_equipage: gur_wiki_sports_equipage)=>gur_wiki_sports_equipage.gurWikiSportsEquipageDescs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ued_uae_id'})
    uedUae:gur_wiki_sports_equipage | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ued_title"
        })
    ued_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"ued_desc"
        })
    ued_desc:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"ued_lng_code"
        })
    ued_lng_code:string;
        
}
