import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_judo_stats" ,{schema:"sportsmatik_local" } )
@Index("jus_usr_id",["jusUsr",])
@Index("jus_ucp_id",["jusUcp",])
@Index("jus_weight_event",["jusWeightEvent",])
@Index("jus_rank",["jusRank",])
export class gur_user_judo_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"jus_id"
        })
    jus_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserJudoStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jus_usr_id'})
    jusUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserJudoStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jus_ucp_id'})
    jusUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserJudoStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jus_weight_event'})
    jusWeightEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"jus_matches_played"
        })
    jus_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"jus_matches_won"
        })
    jus_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"jus_matches_lost"
        })
    jus_matches_lost:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"jus_ippon_scored"
        })
    jus_ippon_scored:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"jus_wazaari_scored"
        })
    jus_wazaari_scored:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"jus_yuko_scored"
        })
    jus_yuko_scored:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"jus_koka_scored"
        })
    jus_koka_scored:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"jus_penalty_given"
        })
    jus_penalty_given:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserJudoStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jus_rank'})
    jusRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"jus_modifiedtime"
        })
    jus_modifiedtime:Date;
        
}
