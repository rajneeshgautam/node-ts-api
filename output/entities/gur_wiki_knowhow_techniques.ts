import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_wiki_knowhow_techniques_ls} from "./gur_wiki_knowhow_techniques_ls";


@Entity("gur_wiki_knowhow_techniques" ,{schema:"sportsmatik_local" } )
@Index("tec_wkh_id",["tecWkh",])
export class gur_wiki_knowhow_techniques {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"tec_id"
        })
    tec_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.gurWikiKnowhowTechniquess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tec_wkh_id'})
    tecWkh:gur_wiki_knowhow | null;


    @Column("varchar",{ 
        nullable:true,
        name:"tec_photo"
        })
    tec_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"tec_photo_source_details"
        })
    tec_photo_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tec_photo_title"
        })
    tec_photo_title:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["url","file"],
        name:"tec_video_type"
        })
    tec_video_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tec_video"
        })
    tec_video:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"tec_video_source_details"
        })
    tec_video_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tec_video_title"
        })
    tec_video_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tec_createdby"
        })
    tec_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"tec_modifiedby"
        })
    tec_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tec_modifiedtime"
        })
    tec_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_knowhow_techniques_ls, (gur_wiki_knowhow_techniques_ls: gur_wiki_knowhow_techniques_ls)=>gur_wiki_knowhow_techniques_ls.telTec,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowTechniquesLss:gur_wiki_knowhow_techniques_ls[];
    
}
