import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_community_question} from "./gur_community_question";
import {gur_user_types} from "./gur_user_types";
import {gur_community_answer_like} from "./gur_community_answer_like";


@Entity("gur_community_answer" ,{schema:"sportsmatik_local" } )
@Index("cma_com_id",["cmaCom",])
@Index("cma_user_id",["cma_user_id",])
@Index("cma_uty_id",["cmaUty",])
@Index("cma_parent_id",["cmaParent",])
export class gur_community_answer {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"cma_id"
        })
    cma_id:string;
        

   
    @ManyToOne(()=>gur_community_answer, (gur_community_answer: gur_community_answer)=>gur_community_answer.gurCommunityAnswers,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cma_parent_id'})
    cmaParent:gur_community_answer | null;


   
    @ManyToOne(()=>gur_community_question, (gur_community_question: gur_community_question)=>gur_community_question.gurCommunityAnswers,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cma_com_id'})
    cmaCom:gur_community_question | null;


    @Column("mediumtext",{ 
        nullable:true,
        name:"cma_answer"
        })
    cma_answer:string | null;
        

    @Column("bigint",{ 
        nullable:false,
        name:"cma_user_id"
        })
    cma_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCommunityAnswers,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cma_uty_id'})
    cmaUty:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'1'",
        name:"cma_is_active"
        })
    cma_is_active:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cma_admin_review"
        })
    cma_admin_review:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cma_created_at"
        })
    cma_created_at:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cma_updated_at"
        })
    cma_updated_at:Date | null;
        

   
    @OneToMany(()=>gur_community_answer, (gur_community_answer: gur_community_answer)=>gur_community_answer.cmaParent,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityAnswers:gur_community_answer[];
    

   
    @OneToMany(()=>gur_community_answer_like, (gur_community_answer_like: gur_community_answer_like)=>gur_community_answer_like.calCma,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurCommunityAnswerLikes:gur_community_answer_like[];
    
}
