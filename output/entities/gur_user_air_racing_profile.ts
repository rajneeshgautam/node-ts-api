import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_air_racing_profile" ,{schema:"sportsmatik_local" } )
@Index("air_usr_id",["airUsr",])
@Index("air_class",["airClass",])
@Index("air_createdby_user",["airCreatedbyUser",])
export class gur_user_air_racing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"air_id"
        })
    air_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAirRacingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'air_usr_id'})
    airUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAirRacingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'air_class'})
    airClass:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        name:"air_craft"
        })
    air_craft:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"air_modifiedtime"
        })
    air_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAirRacingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'air_createdby_user'})
    airCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"air_modifiedby"
        })
    air_modifiedby:string | null;
        
}
