import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_sponsorship" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["spwUty",])
export class gur_sponsorship {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"spw_id"
        })
    spw_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSponsorships,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'spw_uty_id'})
    spwUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"spw_user_id"
        })
    spw_user_id:number;
        

    @Column("text",{ 
        nullable:false,
        name:"spw_sports"
        })
    spw_sports:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spw_individual"
        })
    spw_individual:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"spw_for_individual"
        })
    spw_for_individual:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spw_team"
        })
    spw_team:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"spw_for_team"
        })
    spw_for_team:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"spw_modifiedby"
        })
    spw_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"spw_modifiedtime"
        })
    spw_modifiedtime:Date;
        
}
