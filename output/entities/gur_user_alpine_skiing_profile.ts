import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_alpine_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("alp_usr_id",["alpUsr",])
@Index("alp_createdby_user",["alpCreatedbyUser",])
export class gur_user_alpine_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"alp_id"
        })
    alp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAlpineSkiingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'alp_usr_id'})
    alpUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"alp_disciplines"
        })
    alp_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"alp_modifiedtime"
        })
    alp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAlpineSkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'alp_createdby_user'})
    alpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"alp_modifiedby"
        })
    alp_modifiedby:string | null;
        
}
