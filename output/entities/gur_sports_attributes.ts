import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_attributes_categories} from "./gur_sports_attributes_categories";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";


@Entity("gur_sports_attributes" ,{schema:"sportsmatik_local" } )
@Index("one_attrib_for_a_cat",["sab_title","sabSac",],{unique:true})
@Index("admin_id",["sabCreatedbyAdmin",])
@Index("sab_cat_id",["sabSac",])
@Index("sab_title",["sab_title",])
@Index("user_id",["sabCreatedbyUser",])
export class gur_sports_attributes {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"sab_id"
        })
    sab_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sab_title"
        })
    sab_title:string;
        

   
    @ManyToOne(()=>gur_sports_attributes_categories, (gur_sports_attributes_categories: gur_sports_attributes_categories)=>gur_sports_attributes_categories.gurSportsAttributess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sab_sac_id'})
    sabSac:gur_sports_attributes_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sab_status"
        })
    sab_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sab_authentic"
        })
    sab_authentic:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsAttributess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sab_createdby_user'})
    sabCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurSportsAttributess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sab_createdby_admin'})
    sabCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"sab_createdby"
        })
    sab_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sab_modifiedby"
        })
    sab_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sab_modifiedtime"
        })
    sab_modifiedtime:Date;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"sab_reviewed_on"
        })
    sab_reviewed_on:Date | null;
        
}
