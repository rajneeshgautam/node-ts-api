import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_triathlon_profile" ,{schema:"sportsmatik_local" } )
@Index("ptr_usr_id",["ptrUsr",])
@Index("ptr_disability_classes",["ptrDisabilityClasses",])
@Index("ptr_createdby_user",["ptrCreatedbyUser",])
export class gur_user_para_triathlon_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ptr_id"
        })
    ptr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaTriathlonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptr_usr_id'})
    ptrUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaTriathlonProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptr_disability_classes'})
    ptrDisabilityClasses:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ptr_modifiedtime"
        })
    ptr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaTriathlonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ptr_createdby_user'})
    ptrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ptr_modifiedby"
        })
    ptr_modifiedby:string | null;
        
}
