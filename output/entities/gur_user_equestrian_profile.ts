import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_equestrian_profile" ,{schema:"sportsmatik_local" } )
@Index("equ_usr_id",["equUsr",])
@Index("equ_createdby_user",["equCreatedbyUser",])
export class gur_user_equestrian_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"equ_id"
        })
    equ_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserEquestrianProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'equ_usr_id'})
    equUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"equ_disciplines"
        })
    equ_disciplines:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"equ_horse_name"
        })
    equ_horse_name:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"equ_modifiedtime"
        })
    equ_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserEquestrianProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'equ_createdby_user'})
    equCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"equ_modifiedby"
        })
    equ_modifiedby:string | null;
        
}
