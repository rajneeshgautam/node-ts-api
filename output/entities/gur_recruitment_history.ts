import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports} from "./gur_sports";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_users} from "./gur_users";


@Entity("gur_recruitment_history" ,{schema:"sportsmatik_local" } )
@Index("reh_name",["reh_name",])
@Index("reh_spo_id",["rehSpo",])
@Index("reh_country",["rehCountry","rehCity",])
@Index("reh_city",["rehCity",])
@Index("reh_createdby_user",["rehCreatedbyUser",])
@Index("reh_uty_id",["rehUty",])
export class gur_recruitment_history {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"reh_id"
        })
    reh_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurRecruitmentHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'reh_uty_id'})
    rehUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"reh_user_id"
        })
    reh_user_id:number;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurRecruitmentHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'reh_spo_id'})
    rehSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        name:"reh_name"
        })
    reh_name:string;
        

    @Column("enum",{ 
        nullable:true,
        enum:["f","m","o"],
        name:"reh_gender"
        })
    reh_gender:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"reh_dob"
        })
    reh_dob:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurRecruitmentHistorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'reh_country'})
    rehCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurRecruitmentHistorys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'reh_city'})
    rehCity:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:false,
        name:"reh_recruiter_name"
        })
    reh_recruiter_name:string;
        

    @Column("text",{ 
        nullable:true,
        name:"reh_desc"
        })
    reh_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"reh_photo"
        })
    reh_photo:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"reh_status"
        })
    reh_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"reh_admin_review"
        })
    reh_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurRecruitmentHistorys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'reh_createdby_user'})
    rehCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"reh_createdby"
        })
    reh_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"reh_modifiedby"
        })
    reh_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"reh_modifiedtime"
        })
    reh_modifiedtime:Date;
        
}
