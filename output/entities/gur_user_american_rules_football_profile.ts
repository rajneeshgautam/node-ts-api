import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_american_rules_football_profile" ,{schema:"sportsmatik_local" } )
@Index("arf_usr_id",["arfUsr",])
@Index("arf_position1",["arfPosition",])
@Index("arf_position2",["arfPosition2",])
@Index("arf_createdby_user",["arfCreatedbyUser",])
export class gur_user_american_rules_football_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"arf_id"
        })
    arf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAmericanRulesFootballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arf_usr_id'})
    arfUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAmericanRulesFootballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arf_position1'})
    arfPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAmericanRulesFootballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arf_position2'})
    arfPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"arf_modifiedtime"
        })
    arf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAmericanRulesFootballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arf_createdby_user'})
    arfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"arf_modifiedby"
        })
    arf_modifiedby:string | null;
        
}
