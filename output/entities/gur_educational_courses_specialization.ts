import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_user_qualifications} from "./gur_user_qualifications";
import {gur_educational_courses} from "./gur_educational_courses";


@Entity("gur_educational_courses_specialization" ,{schema:"sportsmatik_local" } )
@Index("csp_name",["csp_name",])
@Index("csp_createdby_user",["cspCreatedbyUser",])
@Index("csp_createdby_admin",["cspCreatedbyAdmin",])
export class gur_educational_courses_specialization {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"csp_id"
        })
    csp_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"csp_name"
        })
    csp_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"csp_status"
        })
    csp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"csp_authentic"
        })
    csp_authentic:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurEducationalCoursesSpecializations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csp_createdby_user'})
    cspCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurEducationalCoursesSpecializations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csp_createdby_admin'})
    cspCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"csp_createdby"
        })
    csp_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"csp_modifiedby"
        })
    csp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"csp_modifiedtime"
        })
    csp_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfSpecializationName,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    

   
    @ManyToMany(()=>gur_educational_courses, (gur_educational_courses: gur_educational_courses)=>gur_educational_courses.gurEducationalCoursesSpecializations)
    gurEducationalCoursess:gur_educational_courses[];
    
}
