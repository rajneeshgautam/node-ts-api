import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_inline_skating_profile" ,{schema:"sportsmatik_local" } )
@Index("ins_usr_id",["insUsr",])
@Index("ins_createdby_user",["insCreatedbyUser",])
export class gur_user_inline_skating_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ins_id"
        })
    ins_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineSkatingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ins_usr_id'})
    insUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ins_track"
        })
    ins_track:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ins_road"
        })
    ins_road:boolean;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ins_modifiedtime"
        })
    ins_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineSkatingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ins_createdby_user'})
    insCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ins_modifiedby"
        })
    ins_modifiedby:string | null;
        
}
