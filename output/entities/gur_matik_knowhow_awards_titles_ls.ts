import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_awards_titles} from "./gur_matik_knowhow_awards_titles";


@Entity("gur_matik_knowhow_awards_titles_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_awards_titles_ls_ibfk_1",["wtlAwt",])
export class gur_matik_knowhow_awards_titles_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wtl_id"
        })
    wtl_id:number;
        

   
    @ManyToOne(()=>gur_matik_knowhow_awards_titles, (gur_matik_knowhow_awards_titles: gur_matik_knowhow_awards_titles)=>gur_matik_knowhow_awards_titles.gurMatikKnowhowAwardsTitlesLss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wtl_awt_id'})
    wtlAwt:gur_matik_knowhow_awards_titles | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wtl_title"
        })
    wtl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wtl_description"
        })
    wtl_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:4,
        default: () => "'en'",
        name:"wtl_lng_code"
        })
    wtl_lng_code:string | null;
        
}
