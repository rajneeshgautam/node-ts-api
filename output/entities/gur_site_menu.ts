import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_site_menu_ls} from "./gur_site_menu_ls";


@Entity("gur_site_menu" ,{schema:"sportsmatik_local" } )
export class gur_site_menu {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"stm_id"
        })
    stm_id:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"stm_parent"
        })
    stm_parent:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"stm_url"
        })
    stm_url:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"stm_target"
        })
    stm_target:string;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'header-top'",
        enum:["header-top","header-sidebar","header-mega-menu","footer","footer-strip"],
        name:"stm_position"
        })
    stm_position:string;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"stm_sort_order"
        })
    stm_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"stm_createdby"
        })
    stm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"stm_modifiedby"
        })
    stm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"stm_modifiedtime"
        })
    stm_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_site_menu_ls, (gur_site_menu_ls: gur_site_menu_ls)=>gur_site_menu_ls.stlStm,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSiteMenuLss:gur_site_menu_ls[];
    
}
