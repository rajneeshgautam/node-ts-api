import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_weightlifting_stats" ,{schema:"sportsmatik_local" } )
@Index("uws_usr_id",["uwsUsr",])
@Index("uws_ucp_id",["uwsUcp",])
@Index("uws_event_played",["uwsEventPlayed",])
@Index("uws_rank",["uwsRank",])
export class gur_user_weightlifting_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"uws_id"
        })
    uws_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWeightliftingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uws_usr_id'})
    uwsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserWeightliftingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uws_ucp_id'})
    uwsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWeightliftingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uws_event_played'})
    uwsEventPlayed:gur_value_list | null;


    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"uws_snatch_value"
        })
    uws_snatch_value:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"uws_clean_and_jerk_value"
        })
    uws_clean_and_jerk_value:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWeightliftingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uws_rank'})
    uwsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uws_modifiedtime"
        })
    uws_modifiedtime:Date;
        
}
