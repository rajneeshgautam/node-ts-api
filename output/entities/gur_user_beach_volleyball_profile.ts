import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_beach_volleyball_profile" ,{schema:"sportsmatik_local" } )
@Index("bvb_usr_id",["bvbUsr",])
@Index("bvb_createdby_user",["bvbCreatedbyUser",])
export class gur_user_beach_volleyball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"bvb_id"
        })
    bvb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBeachVolleyballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bvb_usr_id'})
    bvbUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"bvb_playing_hand"
        })
    bvb_playing_hand:string;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"bvb_block_value"
        })
    bvb_block_value:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"bvb_spike_value"
        })
    bvb_spike_value:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bvb_modifiedtime"
        })
    bvb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBeachVolleyballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bvb_createdby_user'})
    bvbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"bvb_modifiedby"
        })
    bvb_modifiedby:string | null;
        
}
