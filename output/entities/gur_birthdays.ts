import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthdays_ls} from "./gur_birthdays_ls";
import {gur_birthdays_photos} from "./gur_birthdays_photos";


@Entity("gur_birthdays" ,{schema:"sportsmatik_local" } )
@Index("bir_date",["bir_date",],{unique:true})
export class gur_birthdays {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bir_id"
        })
    bir_id:number;
        

    @Column("date",{ 
        nullable:false,
        unique: true,
        name:"bir_date"
        })
    bir_date:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bir_photo"
        })
    bir_photo:string;
        

    @Column("text",{ 
        nullable:true,
        name:"bir_source_detail"
        })
    bir_source_detail:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bir_createdby"
        })
    bir_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"bir_modifiedby"
        })
    bir_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bir_modifiedtime"
        })
    bir_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_birthdays_ls, (gur_birthdays_ls: gur_birthdays_ls)=>gur_birthdays_ls.bilBir,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysLss:gur_birthdays_ls[];
    

   
    @OneToMany(()=>gur_birthdays_photos, (gur_birthdays_photos: gur_birthdays_photos)=>gur_birthdays_photos.brpBir,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysPhotoss:gur_birthdays_photos[];
    
}
