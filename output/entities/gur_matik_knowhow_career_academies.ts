import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_all_cities} from "./gur_all_cities";
import {gur_countries} from "./gur_countries";


@Entity("gur_matik_knowhow_career_academies" ,{schema:"sportsmatik_local" } )
@Index("khv_job_city",["khaCity",])
@Index("khv_job_cnt",["khaCnt",])
@Index("khv_spo_id",["khaSpo",])
export class gur_matik_knowhow_career_academies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"kha_id"
        })
    kha_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCareerAcademiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kha_spo_id'})
    khaSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"kha_status"
        })
    kha_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kha_name"
        })
    kha_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kha_pic"
        })
    kha_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kha_address"
        })
    kha_address:string | null;
        

   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurMatikKnowhowCareerAcademiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kha_city'})
    khaCity:gur_all_cities | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowCareerAcademiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kha_cnt'})
    khaCnt:gur_countries | null;


    @Column("text",{ 
        nullable:false,
        name:"kha_description"
        })
    kha_description:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kha_createdby"
        })
    kha_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kha_modifiedby"
        })
    kha_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kha_modifiedtime"
        })
    kha_modifiedtime:Date;
        
}
