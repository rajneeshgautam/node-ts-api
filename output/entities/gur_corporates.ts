import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_corporates" ,{schema:"sportsmatik_local" } )
@Index("cor_gst_number",["cor_gst_number",],{unique:true})
@Index("cor_usr_id",["corUsr",])
@Index("cor_createdby_user",["corCreatedbyUser",])
@Index("cor_country",["corCountry",])
@Index("cor_dis_id",["corDis",])
@Index("cor_sts_id",["corSts",])
@Index("cor_name",["cor_name",])
export class gur_corporates {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cor_id"
        })
    cor_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_slug"
        })
    cor_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cor_name"
        })
    cor_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurCorporatess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_usr_id'})
    corUsr:gur_users | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurCorporatess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_country'})
    corCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurCorporatess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_dis_id'})
    corDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurCorporatess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_sts_id'})
    corSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"cor_address_google_map"
        })
    cor_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_address"
        })
    cor_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_address2"
        })
    cor_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"cor_pincode"
        })
    cor_pincode:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_email"
        })
    cor_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_email_verified"
        })
    cor_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cor_email_verify_time"
        })
    cor_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"cor_phone1"
        })
    cor_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_phone1_verified"
        })
    cor_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"cor_phone1_otp"
        })
    cor_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cor_phone1_otp_time"
        })
    cor_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"cor_phone2"
        })
    cor_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_phone2_verified"
        })
    cor_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"cor_phone2_otp"
        })
    cor_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cor_phone2_otp_time"
        })
    cor_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"cor_status"
        })
    cor_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_profile_pic"
        })
    cor_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_timeline_pic"
        })
    cor_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_intro_status"
        })
    cor_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"cor_profile_summary"
        })
    cor_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cor_founded_year"
        })
    cor_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cor_founded_month"
        })
    cor_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cor_last_login_date"
        })
    cor_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cor_last_login_details"
        })
    cor_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"cor_intro_modifiedtime"
        })
    cor_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"cor_profile_pic_modifiedtime"
        })
    cor_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"cor_free_sports"
        })
    cor_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"cor_free_sub_users"
        })
    cor_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"cor_gst_number"
        })
    cor_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cor_viewable"
        })
    cor_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurCorporatess2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cor_createdby_user'})
    corCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cor_createdby"
        })
    cor_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cor_modifiedby"
        })
    cor_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cor_modifiedtime"
        })
    cor_modifiedtime:Date;
        
}
