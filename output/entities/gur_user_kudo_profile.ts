import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_kudo_profile" ,{schema:"sportsmatik_local" } )
@Index("aik_usr_id",["kud_usr_id",])
@Index("aik_degree",["kud_belt",])
@Index("aik_degree_belt",["kud_dan",])
@Index("aik_createdby_user",["kud_createdby_user",])
export class gur_user_kudo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"kud_id"
        })
    kud_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"kud_usr_id"
        })
    kud_usr_id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"kud_dan"
        })
    kud_dan:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"kud_belt"
        })
    kud_belt:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kud_modifiedtime"
        })
    kud_modifiedtime:Date;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"kud_createdby_user"
        })
    kud_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kud_modifiedby"
        })
    kud_modifiedby:string | null;
        
}
