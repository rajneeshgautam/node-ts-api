import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_institutional_staff} from "./gur_institutional_staff";
import {gur_sports} from "./gur_sports";


@Entity("gur_institutional_staff_map" ,{schema:"sportsmatik_local" } )
@Index("ism_its_id",["ismIts",])
@Index("ism_spo_id",["ismSpo",])
export class gur_institutional_staff_map {

   
    @ManyToOne(()=>gur_institutional_staff, (gur_institutional_staff: gur_institutional_staff)=>gur_institutional_staff.gurInstitutionalStaffMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ism_its_id'})
    ismIts:gur_institutional_staff | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurInstitutionalStaffMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ism_spo_id'})
    ismSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"ism_role"
        })
    ism_role:string;
        
}
