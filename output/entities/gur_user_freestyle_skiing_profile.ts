import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_freestyle_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("frs_usr_id",["frsUsr",])
@Index("frs_createdby_user",["frsCreatedbyUser",])
export class gur_user_freestyle_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"frs_id"
        })
    frs_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFreestyleSkiingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'frs_usr_id'})
    frsUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"frs_discipline"
        })
    frs_discipline:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"frs_modifiedtime"
        })
    frs_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFreestyleSkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'frs_createdby_user'})
    frsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"frs_modifiedby"
        })
    frs_modifiedby:string | null;
        
}
