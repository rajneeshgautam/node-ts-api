import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_wiki_knowhow_players_officials_ls} from "./gur_wiki_knowhow_players_officials_ls";


@Entity("gur_wiki_knowhow_players_officials" ,{schema:"sportsmatik_local" } )
@Index("pmp_wkh_id",["pmpWkh",])
export class gur_wiki_knowhow_players_officials {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"pmp_id"
        })
    pmp_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.gurWikiKnowhowPlayersOfficialss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pmp_wkh_id'})
    pmpWkh:gur_wiki_knowhow | null;


    @Column("enum",{ 
        nullable:false,
        enum:["player","official"],
        name:"pmp_type"
        })
    pmp_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_photo"
        })
    pmp_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"pmp_photo_source_details"
        })
    pmp_photo_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_photo_title"
        })
    pmp_photo_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_photo_alt"
        })
    pmp_photo_alt:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_video"
        })
    pmp_video:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"pmp_video_source_details"
        })
    pmp_video_source_details:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_video_title"
        })
    pmp_video_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_video_alt"
        })
    pmp_video_alt:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["url","file"],
        name:"pmp_video_type"
        })
    pmp_video_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_link"
        })
    pmp_link:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pmp_createdby"
        })
    pmp_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"pmp_modifiedby"
        })
    pmp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pmp_modifiedtime"
        })
    pmp_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_wiki_knowhow_players_officials_ls, (gur_wiki_knowhow_players_officials_ls: gur_wiki_knowhow_players_officials_ls)=>gur_wiki_knowhow_players_officials_ls.pplPmp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowPlayersOfficialsLss:gur_wiki_knowhow_players_officials_ls[];
    
}
