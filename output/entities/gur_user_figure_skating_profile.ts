import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_figure_skating_profile" ,{schema:"sportsmatik_local" } )
@Index("fig_usr_id",["figUsr",])
@Index("fig_createdby_user",["figCreatedbyUser",])
export class gur_user_figure_skating_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fig_id"
        })
    fig_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFigureSkatingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fig_usr_id'})
    figUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"fig_choreographer"
        })
    fig_choreographer:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fig_modifiedtime"
        })
    fig_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFigureSkatingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fig_createdby_user'})
    figCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"fig_modifiedby"
        })
    fig_modifiedby:string | null;
        
}
