import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_wrestling_profile" ,{schema:"sportsmatik_local" } )
@Index("uwr_usr_id",["uwrUsr",])
@Index("uwr_freestyle_weight_category",["uwrFreestyleWeightCategory",])
@Index("uwr_greco_roman_weight_category",["uwrGrecoRomanWeightCategory",])
@Index("uwr_createdby_user",["uwrCreatedbyUser",])
export class gur_user_wrestling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"uwr_id"
        })
    uwr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWrestlingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwr_usr_id'})
    uwrUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uwr_freestyle"
        })
    uwr_freestyle:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWrestlingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwr_freestyle_weight_category'})
    uwrFreestyleWeightCategory:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uwr_greco_roman"
        })
    uwr_greco_roman:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWrestlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwr_greco_roman_weight_category'})
    uwrGrecoRomanWeightCategory:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uwr_modifiedtime"
        })
    uwr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWrestlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwr_createdby_user'})
    uwrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"uwr_modifiedby"
        })
    uwr_modifiedby:string | null;
        
}
