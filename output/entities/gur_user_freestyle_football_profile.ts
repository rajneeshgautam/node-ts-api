import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_freestyle_football_profile" ,{schema:"sportsmatik_local" } )
@Index("fsf_usr_id",["fsfUsr",])
@Index("fsf_createdby_user",["fsfCreatedbyUser",])
export class gur_user_freestyle_football_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fsf_id"
        })
    fsf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFreestyleFootballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fsf_usr_id'})
    fsfUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fsf_modifiedtime"
        })
    fsf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFreestyleFootballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fsf_createdby_user'})
    fsfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"fsf_modifiedby"
        })
    fsf_modifiedby:string | null;
        
}
