import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_user_basic_moderation" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["ubmUty",])
@Index("ubm_entity_user_id",["ubmEntityUser",])
export class gur_user_basic_moderation {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ubm_id"
        })
    ubm_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserBasicModerations,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ubm_uty_id'})
    ubmUty:gur_user_types | null;


    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ubm_user_id"
        })
    ubm_user_id:number | null;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserBasicModerations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ubm_entity_user_id'})
    ubmEntityUser:gur_users | null;


    @Column("text",{ 
        nullable:true,
        name:"ubm_profile_summary"
        })
    ubm_profile_summary:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ubm_profile_summary_admin_review"
        })
    ubm_profile_summary_admin_review:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"ubm_profile_pic_admin_review"
        })
    ubm_profile_pic_admin_review:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ubm_modifiedby"
        })
    ubm_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ubm_modifiedtime"
        })
    ubm_modifiedtime:Date;
        
}
