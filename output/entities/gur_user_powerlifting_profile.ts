import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_powerlifting_profile" ,{schema:"sportsmatik_local" } )
@Index("upp_usr_id",["uppUsr",])
@Index("upp_event_weight",["uppEventWeight",])
@Index("upp_createdby_user",["uppCreatedbyUser",])
export class gur_user_powerlifting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"upp_id"
        })
    upp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPowerliftingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upp_usr_id'})
    uppUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserPowerliftingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upp_event_weight'})
    uppEventWeight:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"upp_modifiedtime"
        })
    upp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPowerliftingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upp_createdby_user'})
    uppCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"upp_modifiedby"
        })
    upp_modifiedby:string | null;
        
}
