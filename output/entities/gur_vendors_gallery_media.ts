import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";


@Entity("gur_vendors_gallery_media" ,{schema:"sportsmatik_local" } )
@Index("vgm_ven_id",["vgmVen",])
export class gur_vendors_gallery_media {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"vgm_id"
        })
    vgm_id:string;
        

   
    @ManyToOne(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorsGalleryMedias,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vgm_ven_id'})
    vgmVen:gur_vendors | null;


    @Column("varchar",{ 
        nullable:false,
        name:"vgm_file"
        })
    vgm_file:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vgm_title"
        })
    vgm_title:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"vgm_status"
        })
    vgm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vgm_admin_review"
        })
    vgm_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vgm_createdby"
        })
    vgm_createdby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vgm_modifiedtime"
        })
    vgm_modifiedtime:Date;
        
}
