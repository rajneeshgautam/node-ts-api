import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_para_sailing_profile" ,{schema:"sportsmatik_local" } )
@Index("psl_usr_id",["pslUsr",])
@Index("psl_createdby_user",["pslCreatedbyUser",])
export class gur_user_para_sailing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"psl_id"
        })
    psl_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSailingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psl_usr_id'})
    pslUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"psl_classes"
        })
    psl_classes:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"psl_modifiedtime"
        })
    psl_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSailingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psl_createdby_user'})
    pslCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"psl_modifiedby"
        })
    psl_modifiedby:string | null;
        
}
