import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_managed_portfolio} from "./gur_managed_portfolio";


@Entity("gur_managed_portfolio_map" ,{schema:"sportsmatik_local" } )
@Index("mhm_mht_id",["mhmMht",])
export class gur_managed_portfolio_map {

   
    @ManyToOne(()=>gur_managed_portfolio, (gur_managed_portfolio: gur_managed_portfolio)=>gur_managed_portfolio.gurManagedPortfolioMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mhm_mht_id'})
    mhmMht:gur_managed_portfolio | null;


    @Column("varchar",{ 
        nullable:true,
        name:"mhm_achivement"
        })
    mhm_achivement:string | null;
        
}
