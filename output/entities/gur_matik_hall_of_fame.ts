import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_personalities} from "./gur_sports_personalities";


@Entity("gur_matik_hall_of_fame" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_hall_of_fame_ibfk_1",["hofWep",])
export class gur_matik_hall_of_fame {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"hof_id"
        })
    hof_id:string;
        

   
    @ManyToOne(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.gurMatikHallOfFames,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hof_wep_id'})
    hofWep:gur_sports_personalities | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"hof_is_featured"
        })
    hof_is_featured:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"hof_status"
        })
    hof_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"hof_current"
        })
    hof_current:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hof_meta_title"
        })
    hof_meta_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"hof_meta_description"
        })
    hof_meta_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"hof_meta_keywords"
        })
    hof_meta_keywords:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"hof_createdby"
        })
    hof_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hof_modifiedby"
        })
    hof_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hof_modifiedtime"
        })
    hof_modifiedtime:Date;
        
}
