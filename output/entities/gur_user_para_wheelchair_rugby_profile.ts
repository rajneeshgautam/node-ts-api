import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_wheelchair_rugby_profile" ,{schema:"sportsmatik_local" } )
@Index("pwr_usr_id",["pwrUsr",])
@Index("pwr_disability_class",["pwrDisabilityClass",])
@Index("pwr_createdby_user",["pwrCreatedbyUser",])
export class gur_user_para_wheelchair_rugby_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pwr_id"
        })
    pwr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaWheelchairRugbyProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwr_usr_id'})
    pwrUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaWheelchairRugbyProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwr_disability_class'})
    pwrDisabilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pwr_modifiedtime"
        })
    pwr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaWheelchairRugbyProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pwr_createdby_user'})
    pwrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pwr_modifiedby"
        })
    pwr_modifiedby:string | null;
        
}
