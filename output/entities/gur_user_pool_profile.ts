import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_pool_profile" ,{schema:"sportsmatik_local" } )
@Index("pol_usr_id",["polUsr",])
@Index("pol_createdby_user",["polCreatedbyUser",])
export class gur_user_pool_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pol_id"
        })
    pol_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPoolProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pol_usr_id'})
    polUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pol_primary_hand"
        })
    pol_primary_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pol_modifiedtime"
        })
    pol_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPoolProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pol_createdby_user'})
    polCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pol_modifiedby"
        })
    pol_modifiedby:string | null;
        
}
