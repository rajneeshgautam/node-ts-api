import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_docs" ,{schema:"sportsmatik_local" } )
@Index("doc_createdby_admin",["doc_createdby_admin",])
@Index("doc_createdby_user",["doc_createdby_user",])
@Index("doc_usr_id",["doc_usr_id",])
@Index("doc_usr_type",["doc_usr_type",])
export class gur_user_docs {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"doc_id"
        })
    doc_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"doc_usr_id"
        })
    doc_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"doc_usr_type"
        })
    doc_usr_type:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"doc_title"
        })
    doc_title:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"doc_type"
        })
    doc_type:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"doc_file"
        })
    doc_file:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"doc_status"
        })
    doc_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"doc_admin_review"
        })
    doc_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"doc_createdby"
        })
    doc_createdby:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"doc_createdby_user"
        })
    doc_createdby_user:number | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"doc_createdby_admin"
        })
    doc_createdby_admin:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"doc_modifiedby"
        })
    doc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"doc_modifiedtime"
        })
    doc_modifiedtime:Date;
        
}
