import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events_sports_categories} from "./gur_world_events_sports_categories";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_value_list} from "./gur_value_list";
import {gur_matik_birthdays} from "./gur_matik_birthdays";
import {gur_matik_hall_of_fame} from "./gur_matik_hall_of_fame";
import {gur_matik_knowhow_awards_winners} from "./gur_matik_knowhow_awards_winners";
import {gur_sports_personalities_ls} from "./gur_sports_personalities_ls";


@Entity("gur_sports_personalities" ,{schema:"sportsmatik_local" } )
@Index("wep_cit_id",["wepBirthCity",])
@Index("wep_format",["wep_format",])
@Index("wep_spo_id",["wep_sports",])
@Index("wep_team",["wep_team",])
@Index("gur_sports_personalities_ibfk_1",["wepCat",])
@Index("gur_sports_personalities_ibfk_3",["wepHeroCat",])
@Index("gur_sports_personalities_ibfk_4",["wepCnt",])
@Index("gur_sports_personalities_ibfk_6",["wepBirthCountry",])
@Index("gur_sports_personalities_ibfk_7",["wepGrade",])
export class gur_sports_personalities {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wep_id"
        })
    wep_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_name"
        })
    wep_name:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"wep_dob"
        })
    wep_dob:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","o","x"],
        name:"wep_gender"
        })
    wep_gender:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_image"
        })
    wep_image:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wep_image_data"
        })
    wep_image_data:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wep_timeline_image_data"
        })
    wep_timeline_image_data:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_sports"
        })
    wep_sports:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_profile"
        })
    wep_profile:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wep_career_start"
        })
    wep_career_start:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wep_career_end"
        })
    wep_career_end:number | null;
        

   
    @ManyToOne(()=>gur_world_events_sports_categories, (gur_world_events_sports_categories: gur_world_events_sports_categories)=>gur_world_events_sports_categories.gurSportsPersonalitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_cat_id'})
    wepCat:gur_world_events_sports_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_is_player"
        })
    wep_is_player:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:200,
        name:"wep_player_cat"
        })
    wep_player_cat:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_is_hero"
        })
    wep_is_hero:boolean;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurSportsPersonalitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_hero_cat'})
    wepHeroCat:gur_matik_knowhow_tab_categories | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsPersonalitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_cnt_id'})
    wepCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurSportsPersonalitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_birth_city'})
    wepBirthCity:gur_all_cities | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsPersonalitiess2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_birth_country'})
    wepBirthCountry:gur_countries | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wep_createdby"
        })
    wep_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_modifiedby"
        })
    wep_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wep_modifiedtime"
        })
    wep_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_format"
        })
    wep_format:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_team"
        })
    wep_team:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurSportsPersonalitiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wep_grade'})
    wepGrade:gur_value_list | null;


    @Column("text",{ 
        nullable:true,
        name:"wep_social_links"
        })
    wep_social_links:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_birthplace"
        })
    wep_birthplace:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_parents"
        })
    wep_parents:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_spouse"
        })
    wep_spouse:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_children"
        })
    wep_children:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wep_family"
        })
    wep_family:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"wep_died_date"
        })
    wep_died_date:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wep_is_detailed"
        })
    wep_is_detailed:boolean;
        

   
    @OneToOne(()=>gur_matik_birthdays, (gur_matik_birthdays: gur_matik_birthdays)=>gur_matik_birthdays.birWep,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikBirthdays:gur_matik_birthdays | null;


   
    @OneToMany(()=>gur_matik_hall_of_fame, (gur_matik_hall_of_fame: gur_matik_hall_of_fame)=>gur_matik_hall_of_fame.hofWep,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikHallOfFames:gur_matik_hall_of_fame[];
    

   
    @OneToMany(()=>gur_matik_knowhow_awards_winners, (gur_matik_knowhow_awards_winners: gur_matik_knowhow_awards_winners)=>gur_matik_knowhow_awards_winners.awwWinner,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsWinnerss:gur_matik_knowhow_awards_winners[];
    

   
    @OneToMany(()=>gur_sports_personalities_ls, (gur_sports_personalities_ls: gur_sports_personalities_ls)=>gur_sports_personalities_ls.wplWep,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiesLss:gur_sports_personalities_ls[];
    
}
