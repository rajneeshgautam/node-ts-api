import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_translation_page_templates" ,{schema:"sportsmatik_local" } )
export class gur_translation_page_templates {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        name:"tpt_name"
        })
    tpt_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"tpt_createdby"
        })
    tpt_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"tpt_modifiedby"
        })
    tpt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tpt_modifiedtime"
        })
    tpt_modifiedtime:Date;
        
}
