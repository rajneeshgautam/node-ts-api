import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_email_subscriptions" ,{schema:"sportsmatik_local" } )
@Index("esb_email",["esb_email",],{unique:true})
export class gur_email_subscriptions {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"esb_id"
        })
    esb_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"esb_name"
        })
    esb_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"esb_email"
        })
    esb_email:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"esb_status"
        })
    esb_status:boolean | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"esb_joined"
        })
    esb_joined:Date;
        

    @Column("varchar",{ 
        nullable:true,
        name:"esb_createdby"
        })
    esb_createdby:string | null;
        
}
