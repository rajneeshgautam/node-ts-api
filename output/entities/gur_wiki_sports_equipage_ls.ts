import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports_equipage} from "./gur_wiki_sports_equipage";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_sports_equipage_ls" ,{schema:"sportsmatik_local" } )
@Index("uel_title",["uel_title",])
@Index("uel_uae_id",["uelUae",])
@Index("uel_lng_code",["uelLngCode",])
export class gur_wiki_sports_equipage_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uel_id"
        })
    uel_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports_equipage, (gur_wiki_sports_equipage: gur_wiki_sports_equipage)=>gur_wiki_sports_equipage.gurWikiSportsEquipageLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uel_uae_id'})
    uelUae:gur_wiki_sports_equipage | null;


    @Column("varchar",{ 
        nullable:true,
        name:"uel_title"
        })
    uel_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uel_desc"
        })
    uel_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uel_meta_title"
        })
    uel_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uel_meta_desc"
        })
    uel_meta_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"uel_meta_keywords"
        })
    uel_meta_keywords:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiSportsEquipageLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uel_lng_code'})
    uelLngCode:gur_languages | null;

}
