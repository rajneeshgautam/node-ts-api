import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_matik_knowhow_career_vacancies" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_career_vacancies_ibfk_1",["khvSpo",])
@Index("gur_matik_knowhow_career_vacancies_ibfk_2",["khvJobCity",])
@Index("gur_matik_knowhow_career_vacancies_ibfk_3",["khvJobCnt",])
export class gur_matik_knowhow_career_vacancies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"khv_id"
        })
    khv_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khv_title"
        })
    khv_title:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCareerVacanciess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khv_spo_id'})
    khvSpo:gur_wiki_sports | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"khv_status"
        })
    khv_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khv_name"
        })
    khv_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khv_pic"
        })
    khv_pic:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowCareerVacanciess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khv_job_cnt'})
    khvJobCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurMatikKnowhowCareerVacanciess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khv_job_city'})
    khvJobCity:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"khv_job_type"
        })
    khv_job_type:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"khv_min_salary"
        })
    khv_min_salary:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"khv_max_salary"
        })
    khv_max_salary:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"khv_salary_type"
        })
    khv_salary_type:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"khv_experience"
        })
    khv_experience:number;
        

    @Column("date",{ 
        nullable:false,
        name:"khv_posted_date"
        })
    khv_posted_date:string;
        

    @Column("date",{ 
        nullable:true,
        name:"khv_last_date"
        })
    khv_last_date:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_summary"
        })
    khv_summary:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_duties_desc"
        })
    khv_duties_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_skills_desc"
        })
    khv_skills_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_benefits_desc"
        })
    khv_benefits_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_application_desc"
        })
    khv_application_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khv_apply_link"
        })
    khv_apply_link:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khv_createdby"
        })
    khv_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khv_modifiedby"
        })
    khv_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khv_modifiedtime"
        })
    khv_modifiedtime:Date;
        
}
