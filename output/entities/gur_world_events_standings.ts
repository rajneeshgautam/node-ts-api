import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events_schedules} from "./gur_world_events_schedules";


@Entity("gur_world_events_standings" ,{schema:"sportsmatik_local" } )
@Index("wes_evs_id_2",["wesEvs",])
@Index("wes_participant",["wes_participant",])
export class gur_world_events_standings {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"wes_id"
        })
    wes_id:number;
        

   
    @ManyToOne(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.gurWorldEventsStandingss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wes_evs_id'})
    wesEvs:gur_world_events_schedules | null;


    @Column("bigint",{ 
        nullable:false,
        name:"wes_participant"
        })
    wes_participant:string;
        

    @Column("int",{ 
        nullable:true,
        name:"wes_rank"
        })
    wes_rank:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"wes_gold"
        })
    wes_gold:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"wes_silver"
        })
    wes_silver:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"wes_bronze"
        })
    wes_bronze:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"wes_createdby"
        })
    wes_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wes_modifiedby"
        })
    wes_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wes_modifiedtime"
        })
    wes_modifiedtime:Date;
        
}
