import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_inline_alpine_skating_profile" ,{schema:"sportsmatik_local" } )
@Index("ias_usr_id",["iasUsr",])
@Index("ias_createdby_user",["iasCreatedbyUser",])
export class gur_user_inline_alpine_skating_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ias_id"
        })
    ias_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineAlpineSkatingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ias_usr_id'})
    iasUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"ias_disciplines"
        })
    ias_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ias_modifiedtime"
        })
    ias_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineAlpineSkatingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ias_createdby_user'})
    iasCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ias_modifiedby"
        })
    ias_modifiedby:string | null;
        
}
