import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_matik_play_contest_pose_with_gillu" ,{schema:"sportsmatik_local" } )
@Index("mpg_mpu_id",["mpg_usr_id",])
export class gur_matik_play_contest_pose_with_gillu {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mpg_id"
        })
    mpg_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mpg_usr_id"
        })
    mpg_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'100'",
        name:"mpg_uty_id"
        })
    mpg_uty_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpg_picture"
        })
    mpg_picture:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpg_createdby"
        })
    mpg_createdby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpg_created_time"
        })
    mpg_created_time:Date;
        
}
