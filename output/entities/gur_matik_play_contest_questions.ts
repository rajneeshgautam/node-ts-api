import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_contest} from "./gur_matik_play_contest";
import {gur_matik_play_contest_user_answers} from "./gur_matik_play_contest_user_answers";


@Entity("gur_matik_play_contest_questions" ,{schema:"sportsmatik_local" } )
@Index("sqt_sur_id",["mpqMpc",])
export class gur_matik_play_contest_questions {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mpq_id"
        })
    mpq_id:number;
        

   
    @ManyToOne(()=>gur_matik_play_contest, (gur_matik_play_contest: gur_matik_play_contest)=>gur_matik_play_contest.gurMatikPlayContestQuestionss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mpq_mpc_id'})
    mpqMpc:gur_matik_play_contest | null;


    @Column("int",{ 
        nullable:false,
        name:"mpq_answer"
        })
    mpq_answer:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpq_question"
        })
    mpq_question:string;
        

    @Column("text",{ 
        nullable:true,
        name:"mpq_options"
        })
    mpq_options:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpq_option_count"
        })
    mpq_option_count:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"mpq_sort_order"
        })
    mpq_sort_order:boolean | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpq_createdby"
        })
    mpq_createdby:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"mpq_modifiedby"
        })
    mpq_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpq_modified_time"
        })
    mpq_modified_time:Date;
        

   
    @OneToMany(()=>gur_matik_play_contest_user_answers, (gur_matik_play_contest_user_answers: gur_matik_play_contest_user_answers)=>gur_matik_play_contest_user_answers.mpaMpq,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayContestUserAnswerss:gur_matik_play_contest_user_answers[];
    
}
