import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_privacy_settings" ,{schema:"sportsmatik_local" } )
@Index("rat_rated_by",["prv_user_id",])
@Index("rat_rated_by_type",["prvUserType",])
@Index("mtg_for_type",["prv_notification_email_to",])
export class gur_privacy_settings {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"prv_id"
        })
    prv_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurPrivacySettingss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'prv_user_type'})
    prvUserType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"prv_user_id"
        })
    prv_user_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_send_notification_mail"
        })
    prv_send_notification_mail:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"prv_notification_email_to"
        })
    prv_notification_email_to:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'both'",
        enum:["email","phone","both"],
        name:"prv_contact_mode"
        })
    prv_contact_mode:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_allow_ratings"
        })
    prv_allow_ratings:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prv_show_posted_details"
        })
    prv_show_posted_details:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"prv_deactivate_reason"
        })
    prv_deactivate_reason:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"prv_delete_reason"
        })
    prv_delete_reason:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"prv_public_gst"
        })
    prv_public_gst:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_is_listed"
        })
    prv_is_listed:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_listing_attempt"
        })
    prv_listing_attempt:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_allow_cookies"
        })
    prv_allow_cookies:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_allow_analytics"
        })
    prv_allow_analytics:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prv_allow_ads"
        })
    prv_allow_ads:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prv_allow_social_media"
        })
    prv_allow_social_media:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"prv_ask_me_later"
        })
    prv_ask_me_later:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"prv_available_for_job"
        })
    prv_available_for_job:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        default: () => "'-'",
        name:"prv_modifiedby"
        })
    prv_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"prv_modifiedtime"
        })
    prv_modifiedtime:Date;
        
}
