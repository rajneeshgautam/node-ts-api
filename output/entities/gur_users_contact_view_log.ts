import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_users_contact_view_log" ,{schema:"sportsmatik_local" } )
@Index("cvl_by_type",["cvlByType",])
@Index("cvl_for_type",["cvlForType",])
@Index("cvl_by_user",["cvlByUser",])
export class gur_users_contact_view_log {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"cvl_id"
        })
    cvl_id:string;
        

    @Column("enum",{ 
        nullable:true,
        default: () => "'contact'",
        enum:["email","contact"],
        name:"cvl_mode"
        })
    cvl_mode:string | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUsersContactViewLogs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cvl_by_type'})
    cvlByType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        name:"cvl_by_id"
        })
    cvl_by_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUsersContactViewLogs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cvl_by_user'})
    cvlByUser:gur_users | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUsersContactViewLogs2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cvl_for_type'})
    cvlForType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        name:"cvl_for_id"
        })
    cvl_for_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cvl_viewdby_ip"
        })
    cvl_viewdby_ip:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cvl_viewed_time"
        })
    cvl_viewed_time:Date;
        
}
