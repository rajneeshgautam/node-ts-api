import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_sports} from "./gur_sports";
import {gur_user_plans} from "./gur_user_plans";


@Entity("gur_user_sports_map" ,{schema:"sportsmatik_local" } )
@Index("upm_spo_id",["upmSpo","upmUser",],{unique:true})
@Index("upm_usr_id",["upmUser",])
@Index("gur_user_sports_map_ibfk_3",["upmPch",])
@Index("upm_uty_id",["upmUty",])
export class gur_user_sports_map {

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upm_uty_id'})
    upmUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upm_user_id'})
    upmUser:gur_users | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurUserSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upm_spo_id'})
    upmSpo:gur_sports | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"upm_primary"
        })
    upm_primary:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upm_type"
        })
    upm_type:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"upm_status"
        })
    upm_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upm_admin_review"
        })
    upm_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"upm_approved"
        })
    upm_approved:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:1000,
        name:"upm_description"
        })
    upm_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upm_attrib_approved"
        })
    upm_attrib_approved:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"upm_attributes"
        })
    upm_attributes:string | null;
        

   
    @ManyToOne(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.gurUserSportsMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upm_pch_id'})
    upmPch:gur_user_plans | null;


    @Column("date",{ 
        nullable:false,
        name:"upm_date_created"
        })
    upm_date_created:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"upm_createdby"
        })
    upm_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"upm_modifiedby"
        })
    upm_modifiedby:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"upm_date_modified"
        })
    upm_date_modified:Date | null;
        
}
