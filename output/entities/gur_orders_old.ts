import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_order_status} from "./gur_order_status";
import {gur_user_types} from "./gur_user_types";
import {gur_user_plans_check_not_used} from "./gur_user_plans_check_not_used";
import {gur_user_plans_details} from "./gur_user_plans_details";


@Entity("gur_orders_old" ,{schema:"sportsmatik_local" } )
@Index("gur_order_history_ibfk_2",["ord_usr_id",])
@Index("gur_order_history_ibfk_4",["ord_done_by",])
@Index("ord_ors_id",["ordStatus",])
@Index("ord_payment_method",["ord_payment_method",])
@Index("gur_orders_old_ibfk_1",["ordUsrType",])
export class gur_orders_old {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ord_id"
        })
    ord_id:number;
        

   
    @ManyToOne(()=>gur_order_status, (gur_order_status: gur_order_status)=>gur_order_status.gurOrdersOlds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ord_status'})
    ordStatus:gur_order_status | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ord_deleted"
        })
    ord_deleted:boolean;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurOrdersOlds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ord_usr_type'})
    ordUsrType:gur_user_types | null;


    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ord_usr_id"
        })
    ord_usr_id:number | null;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ord_payment_method"
        })
    ord_payment_method:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_txn_id"
        })
    ord_txn_id:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"ord_paid_via"
        })
    ord_paid_via:string | null;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ord_total"
        })
    ord_total:number;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"ord_tax_percent"
        })
    ord_tax_percent:number;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ord_done_by"
        })
    ord_done_by:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ord_user_name"
        })
    ord_user_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ord_user_email"
        })
    ord_user_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"ord_user_mobile"
        })
    ord_user_mobile:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ord_address1"
        })
    ord_address1:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ord_address2"
        })
    ord_address2:string | null;
        

    @Column("int",{ 
        nullable:false,
        name:"ord_cnt_id"
        })
    ord_cnt_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ord_sts_id"
        })
    ord_sts_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"ord_state_gst_code"
        })
    ord_state_gst_code:string;
        

    @Column("int",{ 
        nullable:false,
        name:"ord_dis_id"
        })
    ord_dis_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:20,
        name:"ord_pincode"
        })
    ord_pincode:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"ord_gstin"
        })
    ord_gstin:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ord_made_on"
        })
    ord_made_on:Date;
        

   
    @OneToMany(()=>gur_user_plans_check_not_used, (gur_user_plans_check_not_used: gur_user_plans_check_not_used)=>gur_user_plans_check_not_used.pchOrder,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansCheckNotUseds:gur_user_plans_check_not_used[];
    

   
    @OneToMany(()=>gur_user_plans_details, (gur_user_plans_details: gur_user_plans_details)=>gur_user_plans_details.updOrd,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansDetailss:gur_user_plans_details[];
    
}
