import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_american_handball_profile" ,{schema:"sportsmatik_local" } )
@Index("ahb_usr_id",["ahbUsr",])
@Index("ahb_createdby_user",["ahbCreatedbyUser",])
export class gur_user_american_handball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ahb_id"
        })
    ahb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAmericanHandballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ahb_usr_id'})
    ahbUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"ahb_hand"
        })
    ahb_hand:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ahb_modifiedtime"
        })
    ahb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAmericanHandballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ahb_createdby_user'})
    ahbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ahb_modifiedby"
        })
    ahb_modifiedby:string | null;
        
}
