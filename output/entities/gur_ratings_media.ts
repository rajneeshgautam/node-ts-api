import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_ratings} from "./gur_ratings";


@Entity("gur_ratings_media" ,{schema:"sportsmatik_local" } )
@Index("rtm_rat_id",["rtmRat",])
export class gur_ratings_media {

   
    @ManyToOne(()=>gur_ratings, (gur_ratings: gur_ratings)=>gur_ratings.gurRatingsMedias,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rtm_rat_id'})
    rtmRat:gur_ratings | null;


    @Column("varchar",{ 
        nullable:false,
        name:"rtm_media"
        })
    rtm_media:string;
        
}
