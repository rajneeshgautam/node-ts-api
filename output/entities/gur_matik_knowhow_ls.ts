import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";


@Entity("gur_matik_knowhow_ls" ,{schema:"sportsmatik_local" } )
@Index("khl_lng_code",["khl_lng_code",])
@Index("gur_matik_knowhow_ls_ibfk_1",["khlMkh",])
export class gur_matik_knowhow_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khl_id"
        })
    khl_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khl_mkh_id'})
    khlMkh:gur_matik_knowhow | null;


    @Column("text",{ 
        nullable:true,
        name:"khl_intro"
        })
    khl_intro:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khl_objective"
        })
    khl_objective:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"khl_lng_code"
        })
    khl_lng_code:string;
        
}
