import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_buzzwords} from "./gur_matik_knowhow_buzzwords";


@Entity("gur_matik_knowhow_buzzwords_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_buzzwords_ls_ibfk_1",["kblKhb",])
export class gur_matik_knowhow_buzzwords_ls {

   
    @ManyToOne(()=>gur_matik_knowhow_buzzwords, (gur_matik_knowhow_buzzwords: gur_matik_knowhow_buzzwords)=>gur_matik_knowhow_buzzwords.gurMatikKnowhowBuzzwordsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbl_khb_id'})
    kblKhb:gur_matik_knowhow_buzzwords | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"kbl_name"
        })
    kbl_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kbl_description"
        })
    kbl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"kbl_lng_code"
        })
    kbl_lng_code:string;
        
}
