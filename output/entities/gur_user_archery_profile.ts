import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_archery_profile" ,{schema:"sportsmatik_local" } )
@Index("arc_usr_id",["arcUsr",])
@Index("arc_createdby_user",["arcCreatedbyUser",])
export class gur_user_archery_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"arc_id"
        })
    arc_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserArcheryProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arc_usr_id'})
    arcUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"arc_primary_hand"
        })
    arc_primary_hand:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"arc_bow_type_recurve"
        })
    arc_bow_type_recurve:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"arc_bow_type_compound"
        })
    arc_bow_type_compound:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"arc_bow_type_bare"
        })
    arc_bow_type_bare:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"arc_bow_type_longbow"
        })
    arc_bow_type_longbow:boolean;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"arc_modifiedtime"
        })
    arc_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserArcheryProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arc_createdby_user'})
    arcCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"arc_modifiedby"
        })
    arc_modifiedby:string | null;
        
}
