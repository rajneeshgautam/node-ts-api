import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_plan_categories} from "./gur_plan_categories";
import {gur_matik_ads} from "./gur_matik_ads";
import {gur_matik_play_quiz_results} from "./gur_matik_play_quiz_results";
import {gur_order_items} from "./gur_order_items";
import {gur_plan_change_log} from "./gur_plan_change_log";
import {gur_plans_price} from "./gur_plans_price";
import {gur_sports_usage_history} from "./gur_sports_usage_history";
import {gur_user_plans} from "./gur_user_plans";
import {gur_user_plans_addons} from "./gur_user_plans_addons";
import {gur_vendor_categories_map} from "./gur_vendor_categories_map";


@Entity("gur_plans" ,{schema:"sportsmatik_local" } )
@Index("gur_plans_ibfk_1",["plnPlc",])
export class gur_plans {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pln_id"
        })
    pln_id:number;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'both'",
        enum:["individual","institution","both"],
        name:"pln_type"
        })
    pln_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"pln_name"
        })
    pln_name:string;
        

    @Column("int",{ 
        nullable:true,
        name:"pln_credits"
        })
    pln_credits:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"pln_validity"
        })
    pln_validity:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"pln_description"
        })
    pln_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pln_status"
        })
    pln_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"pln_free"
        })
    pln_free:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pln_best"
        })
    pln_best:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"pln_best_for"
        })
    pln_best_for:string | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"pln_mrp"
        })
    pln_mrp:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"pln_price"
        })
    pln_price:number | null;
        

    @Column("date",{ 
        nullable:true,
        name:"pln_start_date"
        })
    pln_start_date:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"pln_end_date"
        })
    pln_end_date:string | null;
        

   
    @ManyToOne(()=>gur_plan_categories, (gur_plan_categories: gur_plan_categories)=>gur_plan_categories.gurPlanss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pln_plc_id'})
    plnPlc:gur_plan_categories | null;


    @Column("enum",{ 
        nullable:true,
        enum:["basic","bulk"],
        name:"pln_addon_type"
        })
    pln_addon_type:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pln_sort_order"
        })
    pln_sort_order:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"pln_createdby"
        })
    pln_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pln_modifiedby"
        })
    pln_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pln_modifiedtime"
        })
    pln_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_ads, (gur_matik_ads: gur_matik_ads)=>gur_matik_ads.adsPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikAdss:gur_matik_ads[];
    

   
    @OneToMany(()=>gur_matik_play_quiz_results, (gur_matik_play_quiz_results: gur_matik_play_quiz_results)=>gur_matik_play_quiz_results.pqrPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayQuizResultss:gur_matik_play_quiz_results[];
    

   
    @OneToMany(()=>gur_order_items, (gur_order_items: gur_order_items)=>gur_order_items.oriPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrderItemss:gur_order_items[];
    

   
    @OneToMany(()=>gur_plan_change_log, (gur_plan_change_log: gur_plan_change_log)=>gur_plan_change_log.pclPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanChangeLogs:gur_plan_change_log[];
    

   
    @OneToMany(()=>gur_plans_price, (gur_plans_price: gur_plans_price)=>gur_plans_price.plpPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlansPrices:gur_plans_price[];
    

   
    @OneToMany(()=>gur_sports_usage_history, (gur_sports_usage_history: gur_sports_usage_history)=>gur_sports_usage_history.suhPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsUsageHistorys:gur_sports_usage_history[];
    

   
    @OneToMany(()=>gur_user_plans, (gur_user_plans: gur_user_plans)=>gur_user_plans.pchPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlanss:gur_user_plans[];
    

   
    @OneToMany(()=>gur_user_plans_addons, (gur_user_plans_addons: gur_user_plans_addons)=>gur_user_plans_addons.upaPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansAddonss:gur_user_plans_addons[];
    

   
    @OneToMany(()=>gur_vendor_categories_map, (gur_vendor_categories_map: gur_vendor_categories_map)=>gur_vendor_categories_map.vcmPln,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorCategoriesMaps:gur_vendor_categories_map[];
    
}
