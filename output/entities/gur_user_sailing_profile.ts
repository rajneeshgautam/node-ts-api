import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_sailing_profile" ,{schema:"sportsmatik_local" } )
@Index("sai_usr_id",["saiUsr",])
@Index("sai_createdby_user",["saiCreatedbyUser",])
export class gur_user_sailing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sai_id"
        })
    sai_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSailingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sai_usr_id'})
    saiUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"sai_classes"
        })
    sai_classes:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sai_modifiedtime"
        })
    sai_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSailingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'sai_createdby_user'})
    saiCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sai_modifiedby"
        })
    sai_modifiedby:string | null;
        
}
