import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_sports_gallery" ,{schema:"sportsmatik_local" } )
export class gur_sports_gallery {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"sgl_id"
        })
    sgl_id:string;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"sgl_category"
        })
    sgl_category:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"sgl_resource_category"
        })
    sgl_resource_category:number | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["image","video","audio","document"],
        name:"sgl_type"
        })
    sgl_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgl_file"
        })
    sgl_file:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgl_url"
        })
    sgl_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"sgl_title"
        })
    sgl_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgl_alt"
        })
    sgl_alt:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sgl_description"
        })
    sgl_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgl_thumb"
        })
    sgl_thumb:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"sgl_sort_order"
        })
    sgl_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sgl_is_resource"
        })
    sgl_is_resource:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sgl_createdby"
        })
    sgl_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgl_modifiedby"
        })
    sgl_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sgl_modifiedtime"
        })
    sgl_modifiedtime:Date;
        
}
