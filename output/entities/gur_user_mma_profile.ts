import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_mma_profile" ,{schema:"sportsmatik_local" } )
@Index("box_usr_id",["mma_usr_id",])
@Index("box_createdby_user",["mma_createdby_user",])
export class gur_user_mma_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mma_id"
        })
    mma_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mma_usr_id"
        })
    mma_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"mma_event"
        })
    mma_event:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mma_modifiedtime"
        })
    mma_modifiedtime:Date;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"mma_createdby_user"
        })
    mma_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"mma_modifiedby"
        })
    mma_modifiedby:string | null;
        
}
