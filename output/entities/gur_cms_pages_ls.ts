import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_cms_pages} from "./gur_cms_pages";
import {gur_languages} from "./gur_languages";


@Entity("gur_cms_pages_ls" ,{schema:"sportsmatik_local" } )
@Index("cml_cms_id",["cmlCms",])
@Index("cml_lng_code",["cmlLngCode",])
export class gur_cms_pages_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cml_id"
        })
    cml_id:number;
        

   
    @ManyToOne(()=>gur_cms_pages, (gur_cms_pages: gur_cms_pages)=>gur_cms_pages.gurCmsPagesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cml_cms_id'})
    cmlCms:gur_cms_pages | null;


    @Column("varchar",{ 
        nullable:true,
        name:"cml_title"
        })
    cml_title:string | null;
        

    @Column("longtext",{ 
        nullable:true,
        name:"cml_description"
        })
    cml_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cml_meta_title"
        })
    cml_meta_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"cml_meta_description"
        })
    cml_meta_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"cml_meta_keywords"
        })
    cml_meta_keywords:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurCmsPagesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cml_lng_code'})
    cmlLngCode:gur_languages | null;

}
