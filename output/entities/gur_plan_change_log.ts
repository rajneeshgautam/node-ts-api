import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_plans} from "./gur_plans";
import {gur_plan_features} from "./gur_plan_features";
import {gur_admin_users} from "./gur_admin_users";


@Entity("gur_plan_change_log" ,{schema:"sportsmatik_local" } )
@Index("gur_plan_change_log_ibfk_1",["pclAdmin",])
@Index("gur_plan_change_log_ibfk_2",["pclPln",])
@Index("gur_plan_change_log_ibfk_3",["pclPlf",])
export class gur_plan_change_log {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"pcl_id"
        })
    pcl_id:string;
        

   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurPlanChangeLogs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcl_pln_id'})
    pclPln:gur_plans | null;


   
    @ManyToOne(()=>gur_plan_features, (gur_plan_features: gur_plan_features)=>gur_plan_features.gurPlanChangeLogs,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcl_plf_id'})
    pclPlf:gur_plan_features | null;


    @Column("text",{ 
        nullable:false,
        name:"pcl_message"
        })
    pcl_message:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"pcl_createdby"
        })
    pcl_createdby:string;
        

   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurPlanChangeLogs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcl_admin_id'})
    pclAdmin:gur_admin_users | null;

}
