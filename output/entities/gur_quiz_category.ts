import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz} from "./gur_quiz";


@Entity("gur_quiz_category" ,{schema:"sportsmatik_local" } )
export class gur_quiz_category {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"qct_id"
        })
    qct_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"qct_slug"
        })
    qct_slug:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"qct_name"
        })
    qct_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"qct_image"
        })
    qct_image:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:1000,
        name:"qct_description"
        })
    qct_description:string;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"qct_sort_order"
        })
    qct_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"qct_is_active"
        })
    qct_is_active:boolean;
        

    @Column("int",{ 
        nullable:false,
        name:"qct_created_by"
        })
    qct_created_by:number;
        

    @Column("int",{ 
        nullable:true,
        name:"qct_updated_by"
        })
    qct_updated_by:number | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"qct_created_at"
        })
    qct_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"qct_updated_at"
        })
    qct_updated_at:Date | null;
        

   
    @OneToMany(()=>gur_quiz, (gur_quiz: gur_quiz)=>gur_quiz.qizQct,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizs:gur_quiz[];
    
}
