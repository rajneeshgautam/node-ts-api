import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_knowhow_ls" ,{schema:"sportsmatik_local" } )
@Index("khl_sportname",["khl_sportname",])
@Index("khl_wkh_id",["khlWkh",])
@Index("khl_lng_code",["khlLngCode",])
export class gur_wiki_knowhow_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khl_id"
        })
    khl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.gurWikiKnowhowLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khl_wkh_id'})
    khlWkh:gur_wiki_knowhow | null;


    @Column("varchar",{ 
        nullable:true,
        name:"khl_sportname"
        })
    khl_sportname:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khl_intro"
        })
    khl_intro:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khl_objectives"
        })
    khl_objectives:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khl_participants"
        })
    khl_participants:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khl_playing_area"
        })
    khl_playing_area:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"khl_injuries"
        })
    khl_injuries:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khl_meta_title"
        })
    khl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khl_meta_desc"
        })
    khl_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiKnowhowLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khl_lng_code'})
    khlLngCode:gur_languages | null;

}
