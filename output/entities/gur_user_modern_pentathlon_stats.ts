import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_modern_pentathlon_stats" ,{schema:"sportsmatik_local" } )
@Index("mps_usr_id",["mpsUsr",])
@Index("mps_ucp_id",["mpsUcp",])
@Index("mps_event",["mpsEvent",])
export class gur_user_modern_pentathlon_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"mps_id"
        })
    mps_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserModernPentathlonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mps_usr_id'})
    mpsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserModernPentathlonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mps_ucp_id'})
    mpsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserModernPentathlonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mps_event'})
    mpsEvent:gur_value_list | null;


    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"mps_points"
        })
    mps_points:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mps_modifiedtime"
        })
    mps_modifiedtime:Date;
        
}
