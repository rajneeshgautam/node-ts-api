import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_volleyball_stats" ,{schema:"sportsmatik_local" } )
@Index("vos_usr_id",["vosUsr",])
@Index("vos_ucp_id",["vosUcp",])
export class gur_user_volleyball_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"vos_id"
        })
    vos_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserVolleyballStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vos_usr_id'})
    vosUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserVolleyballStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vos_ucp_id'})
    vosUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_matches_played"
        })
    vos_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_matches_won"
        })
    vos_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_matches_lost"
        })
    vos_matches_lost:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_sets_played"
        })
    vos_sets_played:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_kills"
        })
    vos_kills:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_kills_per_sets"
        })
    vos_kills_per_sets:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_assists"
        })
    vos_assists:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_assists_per_sets"
        })
    vos_assists_per_sets:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"vos_digs"
        })
    vos_digs:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_digs_per_sets"
        })
    vos_digs_per_sets:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_service_aces"
        })
    vos_service_aces:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_service_aces_per_sets"
        })
    vos_service_aces_per_sets:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"vos_errors"
        })
    vos_errors:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_service_errors"
        })
    vos_service_errors:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_reception_errors"
        })
    vos_reception_errors:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_total_attempts"
        })
    vos_total_attempts:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_pct"
        })
    vos_pct:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_solo_blocks"
        })
    vos_solo_blocks:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_block_assists"
        })
    vos_block_assists:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"vos_total_blocks"
        })
    vos_total_blocks:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_total_blocks_per_set"
        })
    vos_total_blocks_per_set:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"vos_blocking_errors"
        })
    vos_blocking_errors:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"vos_ball_handling_errors"
        })
    vos_ball_handling_errors:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"vos_total_points"
        })
    vos_total_points:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vos_modifiedtime"
        })
    vos_modifiedtime:Date;
        
}
