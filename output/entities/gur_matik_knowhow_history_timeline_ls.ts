import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_history_timeline} from "./gur_matik_knowhow_history_timeline";


@Entity("gur_matik_knowhow_history_timeline_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_history_timeline_ls_ibfk_1",["htlKht",])
export class gur_matik_knowhow_history_timeline_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"htl_id"
        })
    htl_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow_history_timeline, (gur_matik_knowhow_history_timeline: gur_matik_knowhow_history_timeline)=>gur_matik_knowhow_history_timeline.gurMatikKnowhowHistoryTimelineLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'htl_kht_id'})
    htlKht:gur_matik_knowhow_history_timeline | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"htl_title"
        })
    htl_title:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"htl_short_desc"
        })
    htl_short_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"htl_description"
        })
    htl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"htl_lng_code"
        })
    htl_lng_code:string;
        
}
