import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_hall_of_fame_ls} from "./gur_hall_of_fame_ls";


@Entity("gur_hall_of_fame" ,{schema:"sportsmatik_local" } )
@Index("hof_spo_id",["hofSpo",])
@Index("hof_cnt_id",["hof_cnt_id",])
export class gur_hall_of_fame {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"hof_id"
        })
    hof_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"hof_photo"
        })
    hof_photo:string;
        

    @Column("text",{ 
        nullable:true,
        name:"hof_source_detail"
        })
    hof_source_detail:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"hof_date"
        })
    hof_date:string | null;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurHallOfFames,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hof_spo_id'})
    hofSpo:gur_wiki_sports | null;


    @Column("int",{ 
        nullable:true,
        name:"hof_cnt_id"
        })
    hof_cnt_id:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"hof_status"
        })
    hof_status:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"hof_current"
        })
    hof_current:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"hof_is_featured"
        })
    hof_is_featured:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hof_createdby"
        })
    hof_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"hof_modifiedby"
        })
    hof_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hof_modifiedtime"
        })
    hof_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_hall_of_fame_ls, (gur_hall_of_fame_ls: gur_hall_of_fame_ls)=>gur_hall_of_fame_ls.holHof,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHallOfFameLss:gur_hall_of_fame_ls[];
    
}
