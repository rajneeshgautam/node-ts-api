import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_continents} from "./gur_continents";
import {gur_sports_governing_bodies_features} from "./gur_sports_governing_bodies_features";


@Entity("gur_sports_governing_bodies" ,{schema:"sportsmatik_local" } )
@Index("kgb_cnt_id",["sgbCnt",])
@Index("gur_sports_governing_bodies_ibfk_2",["sgbCon",])
export class gur_sports_governing_bodies {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"sgb_id"
        })
    sgb_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"sgb_name"
        })
    sgb_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"sgb_abbreviation"
        })
    sgb_abbreviation:string | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["global","continental","national","regional"],
        name:"sgb_type"
        })
    sgb_type:string;
        

    @Column("text",{ 
        nullable:true,
        name:"sgb_sports"
        })
    sgb_sports:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_custom_cnt"
        })
    sgb_custom_cnt:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsGoverningBodiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_cnt_id'})
    sgbCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_continents, (gur_continents: gur_continents)=>gur_continents.gurSportsGoverningBodiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_con_id'})
    sgbCon:gur_continents | null;


    @Column("tinyint",{ 
        nullable:true,
        name:"sgb_founded_month"
        })
    sgb_founded_month:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sgb_founded_year"
        })
    sgb_founded_year:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"sgb_website"
        })
    sgb_website:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_hq"
        })
    sgb_hq:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_address"
        })
    sgb_address:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"sgb_description"
        })
    sgb_description:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_photo"
        })
    sgb_photo:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"sgb_photo_data"
        })
    sgb_photo_data:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sgb_createdby"
        })
    sgb_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_modifiedby"
        })
    sgb_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sgb_modifiedtime"
        })
    sgb_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_sports_governing_bodies_features, (gur_sports_governing_bodies_features: gur_sports_governing_bodies_features)=>gur_sports_governing_bodies_features.gbfSgb,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodiesFeaturess:gur_sports_governing_bodies_features[];
    

   
    @OneToMany(()=>gur_sports_governing_bodies_features, (gur_sports_governing_bodies_features: gur_sports_governing_bodies_features)=>gur_sports_governing_bodies_features.gbfSgb,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodiesFeaturess2:gur_sports_governing_bodies_features[];
    
}
