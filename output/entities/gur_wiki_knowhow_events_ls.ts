import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_knowhow_events} from "./gur_wiki_knowhow_events";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_knowhow_events_ls" ,{schema:"sportsmatik_local" } )
@Index("evl_wev_id",["evlWev",])
@Index("evl_lng_code",["evlLngCode",])
export class gur_wiki_knowhow_events_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"evl_id"
        })
    evl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_knowhow_events, (gur_wiki_knowhow_events: gur_wiki_knowhow_events)=>gur_wiki_knowhow_events.gurWikiKnowhowEventsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evl_wev_id'})
    evlWev:gur_wiki_knowhow_events | null;


    @Column("varchar",{ 
        nullable:false,
        name:"evl_name"
        })
    evl_name:string;
        

    @Column("text",{ 
        nullable:true,
        name:"evl_desc"
        })
    evl_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiKnowhowEventsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evl_lng_code'})
    evlLngCode:gur_languages | null;

}
