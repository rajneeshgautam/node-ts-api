import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_feedback} from "./gur_feedback";


@Entity("gur_feedback_types" ,{schema:"sportsmatik_local" } )
export class gur_feedback_types {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"fbt_id"
        })
    fbt_id:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["image","content","user","etc"],
        name:"fbt_mode"
        })
    fbt_mode:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"fbt_value"
        })
    fbt_value:string;
        

   
    @OneToMany(()=>gur_feedback, (gur_feedback: gur_feedback)=>gur_feedback.fedType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFeedbacks:gur_feedback[];
    
}
