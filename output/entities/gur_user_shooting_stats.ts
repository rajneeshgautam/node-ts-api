import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_shooting_stats" ,{schema:"sportsmatik_local" } )
@Index("shs_usr_id",["shsUsr",])
@Index("shs_ucp_id",["shsUcp",])
@Index("shs_event",["shsEvent",])
@Index("shs_rank",["shsRank",])
export class gur_user_shooting_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"shs_id"
        })
    shs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserShootingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'shs_usr_id'})
    shsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserShootingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'shs_ucp_id'})
    shsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserShootingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'shs_event'})
    shsEvent:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["individual","team","pair"],
        name:"shs_event_type"
        })
    shs_event_type:string;
        

    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"shs_total_score"
        })
    shs_total_score:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserShootingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'shs_rank'})
    shsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sch_modifiedtime"
        })
    sch_modifiedtime:Date;
        
}
