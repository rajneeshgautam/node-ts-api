import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_institute_branches} from "./gur_institute_branches";
import {gur_journalist_writer_portfolio} from "./gur_journalist_writer_portfolio";
import {gur_managed_portfolio} from "./gur_managed_portfolio";
import {gur_matik_ads} from "./gur_matik_ads";
import {gur_matik_knowhow_calendar} from "./gur_matik_knowhow_calendar";
import {gur_matik_knowhow_career_academies} from "./gur_matik_knowhow_career_academies";
import {gur_matik_knowhow_career_guides} from "./gur_matik_knowhow_career_guides";
import {gur_matik_knowhow_career_vacancies} from "./gur_matik_knowhow_career_vacancies";
import {gur_matik_knowhow_gallery_media} from "./gur_matik_knowhow_gallery_media";
import {gur_recruitment_history} from "./gur_recruitment_history";
import {gur_sports_event_schedule} from "./gur_sports_event_schedule";
import {gur_sports_logistics_portfolio} from "./gur_sports_logistics_portfolio";
import {gur_sports_logistics_venues} from "./gur_sports_logistics_venues";
import {gur_sports_personalities} from "./gur_sports_personalities";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_user_qualifications} from "./gur_user_qualifications";
import {gur_user_work_details} from "./gur_user_work_details";
import {gur_vendors_branches} from "./gur_vendors_branches";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_world_events_players_bkup} from "./gur_world_events_players_bkup";
import {gur_world_events_venues} from "./gur_world_events_venues";


@Entity("gur_all_cities" ,{schema:"sportsmatik_local" } )
@Index("cit_cnt_id",["citCnt",])
export class gur_all_cities {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"cit_id"
        })
    cit_id:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurAllCitiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cit_cnt_id'})
    citCnt:gur_countries | null;


    @Column("varchar",{ 
        nullable:false,
        name:"cit_name2"
        })
    cit_name2:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"cit_lat"
        })
    cit_lat:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"cit_long"
        })
    cit_long:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'Superadmin'",
        name:"cit_createdby"
        })
    cit_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cit_modifiedby"
        })
    cit_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cit_modifiedtime"
        })
    cit_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_institute_branches, (gur_institute_branches: gur_institute_branches)=>gur_institute_branches.ibrCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteBranchess:gur_institute_branches[];
    

   
    @OneToMany(()=>gur_journalist_writer_portfolio, (gur_journalist_writer_portfolio: gur_journalist_writer_portfolio)=>gur_journalist_writer_portfolio.jwpEvtCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJournalistWriterPortfolios:gur_journalist_writer_portfolio[];
    

   
    @OneToMany(()=>gur_managed_portfolio, (gur_managed_portfolio: gur_managed_portfolio)=>gur_managed_portfolio.mhtCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurManagedPortfolios:gur_managed_portfolio[];
    

   
    @OneToMany(()=>gur_matik_ads, (gur_matik_ads: gur_matik_ads)=>gur_matik_ads.adsCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikAdss:gur_matik_ads[];
    

   
    @OneToMany(()=>gur_matik_knowhow_calendar, (gur_matik_knowhow_calendar: gur_matik_knowhow_calendar)=>gur_matik_knowhow_calendar.khcCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCalendars:gur_matik_knowhow_calendar[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_academies, (gur_matik_knowhow_career_academies: gur_matik_knowhow_career_academies)=>gur_matik_knowhow_career_academies.khaCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerAcademiess:gur_matik_knowhow_career_academies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_guides, (gur_matik_knowhow_career_guides: gur_matik_knowhow_career_guides)=>gur_matik_knowhow_career_guides.khgCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerGuidess:gur_matik_knowhow_career_guides[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_vacancies, (gur_matik_knowhow_career_vacancies: gur_matik_knowhow_career_vacancies)=>gur_matik_knowhow_career_vacancies.khvJobCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerVacanciess:gur_matik_knowhow_career_vacancies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_gallery_media, (gur_matik_knowhow_gallery_media: gur_matik_knowhow_gallery_media)=>gur_matik_knowhow_gallery_media.kgmCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowGalleryMedias:gur_matik_knowhow_gallery_media[];
    

   
    @OneToMany(()=>gur_recruitment_history, (gur_recruitment_history: gur_recruitment_history)=>gur_recruitment_history.rehCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRecruitmentHistorys:gur_recruitment_history[];
    

   
    @OneToMany(()=>gur_sports_event_schedule, (gur_sports_event_schedule: gur_sports_event_schedule)=>gur_sports_event_schedule.evsCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsEventSchedules:gur_sports_event_schedule[];
    

   
    @OneToMany(()=>gur_sports_logistics_portfolio, (gur_sports_logistics_portfolio: gur_sports_logistics_portfolio)=>gur_sports_logistics_portfolio.slpCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsPortfolios:gur_sports_logistics_portfolio[];
    

   
    @OneToMany(()=>gur_sports_logistics_venues, (gur_sports_logistics_venues: gur_sports_logistics_venues)=>gur_sports_logistics_venues.lvnCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsVenuess:gur_sports_logistics_venues[];
    

   
    @OneToMany(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.wepBirthCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiess:gur_sports_personalities[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpVenueCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfZone,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    

   
    @OneToMany(()=>gur_user_work_details, (gur_user_work_details: gur_user_work_details)=>gur_user_work_details.uwdZone,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWorkDetailss:gur_user_work_details[];
    

   
    @OneToMany(()=>gur_vendors_branches, (gur_vendors_branches: gur_vendors_branches)=>gur_vendors_branches.vbrCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsBranchess:gur_vendors_branches[];
    

   
    @OneToMany(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.wvnCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiVenuess:gur_wiki_venues[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepBirthCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups:gur_world_events_players_bkup[];
    

   
    @OneToMany(()=>gur_world_events_venues, (gur_world_events_venues: gur_world_events_venues)=>gur_world_events_venues.wevCit,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsVenuess:gur_world_events_venues[];
    
}
