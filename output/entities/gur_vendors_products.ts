import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_vendors} from "./gur_vendors";
import {gur_ads_categories} from "./gur_ads_categories";


@Entity("gur_vendors_products" ,{schema:"sportsmatik_local" } )
@Index("vpr_ven_id",["vprVen",])
@Index("vpr_cat_id",["vprCat",])
export class gur_vendors_products {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"vpr_id"
        })
    vpr_id:string;
        

   
    @ManyToOne(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.gurVendorsProductss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vpr_ven_id'})
    vprVen:gur_vendors | null;


   
    @ManyToOne(()=>gur_ads_categories, (gur_ads_categories: gur_ads_categories)=>gur_ads_categories.gurVendorsProductss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vpr_cat_id'})
    vprCat:gur_ads_categories | null;


    @Column("varchar",{ 
        nullable:false,
        name:"vpr_prd_name"
        })
    vpr_prd_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"vpr_prd_image"
        })
    vpr_prd_image:string;
        

    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"vpr_prd_price"
        })
    vpr_prd_price:number;
        

    @Column("datetime",{ 
        nullable:false,
        name:"vpr_created_on"
        })
    vpr_created_on:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vpr_modifiedtime"
        })
    vpr_modifiedtime:Date;
        
}
