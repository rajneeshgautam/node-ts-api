import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_inline_downhill_skating_profile" ,{schema:"sportsmatik_local" } )
@Index("ids_usr_id",["idsUsr",])
@Index("ids_createdby_user",["idsCreatedbyUser",])
export class gur_user_inline_downhill_skating_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ids_id"
        })
    ids_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineDownhillSkatingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ids_usr_id'})
    idsUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ids_modifiedtime"
        })
    ids_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserInlineDownhillSkatingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ids_createdby_user'})
    idsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ids_modifiedby"
        })
    ids_modifiedby:string | null;
        
}
