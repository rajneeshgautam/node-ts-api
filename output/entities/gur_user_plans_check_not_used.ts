import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_orders_old} from "./gur_orders_old";
import {gur_pricing_plan_categories} from "./gur_pricing_plan_categories";
import {gur_subuser_usage_history} from "./gur_subuser_usage_history";


@Entity("gur_user_plans_check_not_used" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["pchUty",])
@Index("pch_cat_id_2",["pchCat",])
@Index("pch_prp_id",["pch_prp_id",])
@Index("gur_user_plans_check_not_used_ibfk_11",["pchOrder",])
export class gur_user_plans_check_not_used {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"pch_id"
        })
    pch_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserPlansCheckNotUseds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_uty_id'})
    pchUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"pch_user_id"
        })
    pch_user_id:number;
        

   
    @ManyToOne(()=>gur_orders_old, (gur_orders_old: gur_orders_old)=>gur_orders_old.gurUserPlansCheckNotUseds,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_order_id'})
    pchOrder:gur_orders_old | null;


   
    @ManyToOne(()=>gur_pricing_plan_categories, (gur_pricing_plan_categories: gur_pricing_plan_categories)=>gur_pricing_plan_categories.gurUserPlansCheckNotUseds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_cat_id'})
    pchCat:gur_pricing_plan_categories | null;


    @Column("int",{ 
        nullable:true,
        name:"pch_prp_id"
        })
    pch_prp_id:number | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"pch_credits"
        })
    pch_credits:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"pch_total_credits"
        })
    pch_total_credits:number;
        

    @Column("date",{ 
        nullable:false,
        name:"pch_start_date"
        })
    pch_start_date:string;
        

    @Column("date",{ 
        nullable:true,
        name:"pch_end_date"
        })
    pch_end_date:string | null;
        

   
    @OneToMany(()=>gur_subuser_usage_history, (gur_subuser_usage_history: gur_subuser_usage_history)=>gur_subuser_usage_history.suhPch,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSubuserUsageHistorys:gur_subuser_usage_history[];
    
}
