import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_quiz_questions_bank} from "./gur_matik_play_quiz_questions_bank";
import {gur_matik_play_quiz_results} from "./gur_matik_play_quiz_results";


@Entity("gur_matik_play_quiz_categories" ,{schema:"sportsmatik_local" } )
@Index("cat_name",["cat_name",],{unique:true})
@Index("cat_slug",["cat_slug",],{unique:true})
export class gur_matik_play_quiz_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cat_id"
        })
    cat_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:100,
        name:"cat_slug"
        })
    cat_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"cat_name"
        })
    cat_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cat_image"
        })
    cat_image:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"cat_image_data"
        })
    cat_image_data:string | null;
        

    @Column("time",{ 
        nullable:true,
        name:"cat_exp_complete_time"
        })
    cat_exp_complete_time:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cat_description"
        })
    cat_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"cat_review_desc"
        })
    cat_review_desc:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"cat_sort_order"
        })
    cat_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"cat_status"
        })
    cat_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cat_createdby"
        })
    cat_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cat_modifiedby"
        })
    cat_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cat_modifiedtime"
        })
    cat_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_play_quiz_questions_bank, (gur_matik_play_quiz_questions_bank: gur_matik_play_quiz_questions_bank)=>gur_matik_play_quiz_questions_bank.qqbCat,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurMatikPlayQuizQuestionsBanks:gur_matik_play_quiz_questions_bank[];
    

   
    @OneToMany(()=>gur_matik_play_quiz_results, (gur_matik_play_quiz_results: gur_matik_play_quiz_results)=>gur_matik_play_quiz_results.pqrCat,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurMatikPlayQuizResultss:gur_matik_play_quiz_results[];
    
}
