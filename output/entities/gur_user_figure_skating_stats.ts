import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_figure_skating_stats" ,{schema:"sportsmatik_local" } )
@Index("fss_usr_id",["fssUsr",])
@Index("fss_ucp_id",["fssUcp",])
@Index("fss_event",["fssEvent",])
@Index("fss_rank",["fssRank",])
export class gur_user_figure_skating_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"fss_id"
        })
    fss_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFigureSkatingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fss_usr_id'})
    fssUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserFigureSkatingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fss_ucp_id'})
    fssUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFigureSkatingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fss_event'})
    fssEvent:gur_value_list | null;


    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"fss_short_program_points"
        })
    fss_short_program_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fss_short_program_rank"
        })
    fss_short_program_rank:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"fss_free_skating_points"
        })
    fss_free_skating_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fss_free_skating_rank"
        })
    fss_free_skating_rank:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"fss_short_dance_points"
        })
    fss_short_dance_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fss_short_dance_rank"
        })
    fss_short_dance_rank:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"fss_free_dance_points"
        })
    fss_free_dance_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fss_free_dance_rank"
        })
    fss_free_dance_rank:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"fss_total_points"
        })
    fss_total_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fss_total_rank"
        })
    fss_total_rank:number | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserFigureSkatingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fss_rank'})
    fssRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fss_modifiedtime"
        })
    fss_modifiedtime:Date;
        
}
