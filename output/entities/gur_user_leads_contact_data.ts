import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_leads_contact_data" ,{schema:"sportsmatik_local" } )
@Index("uld_email",["uld_email",])
@Index("uld_phone1",["uld_phone1",])
@Index("uld_phone2",["uld_phone2",])
export class gur_user_leads_contact_data {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uld_id"
        })
    uld_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uld_phone1"
        })
    uld_phone1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uld_phone2"
        })
    uld_phone2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"uld_email"
        })
    uld_email:string | null;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"uld_user_id"
        })
    uld_user_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"uld_uty_id"
        })
    uld_uty_id:number;
        
}
