import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_institutional_teams_contact_usage" ,{schema:"sportsmatik_local" } )
@Index("gur_institutional_teams_contact_usage_ibfk_1",["tcuUty",])
@Index("gur_institutional_teams_contact_usage_ibfk_2",["tcuSendByUser",])
export class gur_institutional_teams_contact_usage {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"tcu_id"
        })
    tcu_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInstitutionalTeamsContactUsages,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcu_uty_id'})
    tcuUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        name:"tcu_user_id"
        })
    tcu_user_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInstitutionalTeamsContactUsages,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcu_send_by_user'})
    tcuSendByUser:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["mail","sms"],
        name:"tcu_mode"
        })
    tcu_mode:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"tcu_to_user"
        })
    tcu_to_user:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tcu_to_phone"
        })
    tcu_to_phone:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tcu_to_email"
        })
    tcu_to_email:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"tcu_message"
        })
    tcu_message:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"tcu_sendby"
        })
    tcu_sendby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tcu_sent_time"
        })
    tcu_sent_time:Date;
        
}
