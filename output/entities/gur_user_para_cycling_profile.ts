import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_cycling_profile" ,{schema:"sportsmatik_local" } )
@Index("pcy_usr_id",["pcyUsr",])
@Index("pcy_disability_class",["pcyDisabilityClass",])
@Index("pcy_createdby_user",["pcyCreatedbyUser",])
export class gur_user_para_cycling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pcy_id"
        })
    pcy_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCyclingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcy_usr_id'})
    pcyUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"pcy_cyclist_type"
        })
    pcy_cyclist_type:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaCyclingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcy_disability_class'})
    pcyDisabilityClass:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"pcy_track"
        })
    pcy_track:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"pcy_road"
        })
    pcy_road:boolean | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pcy_modifiedtime"
        })
    pcy_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCyclingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcy_createdby_user'})
    pcyCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pcy_modifiedby"
        })
    pcy_modifiedby:string | null;
        
}
