import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_continents} from "./gur_continents";
import {gur_academies} from "./gur_academies";
import {gur_all_cities} from "./gur_all_cities";
import {gur_birthdays_photos} from "./gur_birthdays_photos";
import {gur_clubs} from "./gur_clubs";
import {gur_colleges} from "./gur_colleges";
import {gur_corporates} from "./gur_corporates";
import {gur_fitness_center} from "./gur_fitness_center";
import {gur_institute_branches} from "./gur_institute_branches";
import {gur_job} from "./gur_job";
import {gur_job_application} from "./gur_job_application";
import {gur_job_education} from "./gur_job_education";
import {gur_job_experience} from "./gur_job_experience";
import {gur_journalist_writer_portfolio} from "./gur_journalist_writer_portfolio";
import {gur_logistics_services_map} from "./gur_logistics_services_map";
import {gur_managed_portfolio} from "./gur_managed_portfolio";
import {gur_matik_ads} from "./gur_matik_ads";
import {gur_matik_knowhow_awards_main} from "./gur_matik_knowhow_awards_main";
import {gur_matik_knowhow_career_academies} from "./gur_matik_knowhow_career_academies";
import {gur_matik_knowhow_career_guides} from "./gur_matik_knowhow_career_guides";
import {gur_matik_knowhow_career_vacancies} from "./gur_matik_knowhow_career_vacancies";
import {gur_matik_knowhow_gallery_media} from "./gur_matik_knowhow_gallery_media";
import {gur_matik_knowhow_history_timeline} from "./gur_matik_knowhow_history_timeline";
import {gur_matik_knowhow_teams} from "./gur_matik_knowhow_teams";
import {gur_matik_play_address_book} from "./gur_matik_play_address_book";
import {gur_multiple_centres} from "./gur_multiple_centres";
import {gur_plans_price} from "./gur_plans_price";
import {gur_recruitment_history} from "./gur_recruitment_history";
import {gur_schools} from "./gur_schools";
import {gur_sports_event_schedule} from "./gur_sports_event_schedule";
import {gur_sports_governing_bodies} from "./gur_sports_governing_bodies";
import {gur_sports_governing_body} from "./gur_sports_governing_body";
import {gur_sports_infrastructure_company} from "./gur_sports_infrastructure_company";
import {gur_sports_logistics_portfolio} from "./gur_sports_logistics_portfolio";
import {gur_sports_logistics_venues} from "./gur_sports_logistics_venues";
import {gur_sports_marketing_company} from "./gur_sports_marketing_company";
import {gur_sports_personalities} from "./gur_sports_personalities";
import {gur_states} from "./gur_states";
import {gur_teams} from "./gur_teams";
import {gur_training_camp_users_map} from "./gur_training_camp_users_map";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_user_qualifications} from "./gur_user_qualifications";
import {gur_users} from "./gur_users";
import {gur_vendors} from "./gur_vendors";
import {gur_vendors_branches} from "./gur_vendors_branches";
import {gur_wiki_awards_country_wise} from "./gur_wiki_awards_country_wise";
import {gur_wiki_popular_sports} from "./gur_wiki_popular_sports";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_world_events_players_bkup} from "./gur_world_events_players_bkup";
import {gur_world_events_records} from "./gur_world_events_records";
import {gur_world_events_teams} from "./gur_world_events_teams";
import {gur_world_events_venues} from "./gur_world_events_venues";


@Entity("gur_countries" ,{schema:"sportsmatik_local" } )
@Index("cnt_name",["cnt_name",])
@Index("cnt_currency_code",["cnt_currency_code",])
@Index("continent",["cntCon",])
export class gur_countries {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cnt_id"
        })
    cnt_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:64,
        name:"cnt_name"
        })
    cnt_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:3,
        name:"cnt_iso_code"
        })
    cnt_iso_code:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:3,
        name:"cnt_iso_three"
        })
    cnt_iso_three:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cnt_primary"
        })
    cnt_primary:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:8,
        name:"cnt_isd_code"
        })
    cnt_isd_code:string | null;
        

   
    @ManyToOne(()=>gur_continents, (gur_continents: gur_continents)=>gur_continents.gurCountriess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cnt_con_id'})
    cntCon:gur_continents | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cnt_use_currency_flag"
        })
    cnt_use_currency_flag:boolean;
        

    @Column("char",{ 
        nullable:true,
        length:3,
        name:"cnt_currency_code"
        })
    cnt_currency_code:string | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"cnt_currency_in_inr"
        })
    cnt_currency_in_inr:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"cnt_is_tax_required"
        })
    cnt_is_tax_required:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cnt_evt_only"
        })
    cnt_evt_only:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cnt_createdby"
        })
    cnt_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cnt_modifiedby"
        })
    cnt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cnt_modifiedtime"
        })
    cnt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_academies, (gur_academies: gur_academies)=>gur_academies.acaCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAcademiess:gur_academies[];
    

   
    @OneToMany(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.citCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAllCitiess:gur_all_cities[];
    

   
    @OneToMany(()=>gur_birthdays_photos, (gur_birthdays_photos: gur_birthdays_photos)=>gur_birthdays_photos.brpCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysPhotoss:gur_birthdays_photos[];
    

   
    @OneToMany(()=>gur_clubs, (gur_clubs: gur_clubs)=>gur_clubs.clbCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurClubss:gur_clubs[];
    

   
    @OneToMany(()=>gur_colleges, (gur_colleges: gur_colleges)=>gur_colleges.clgCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCollegess:gur_colleges[];
    

   
    @OneToMany(()=>gur_corporates, (gur_corporates: gur_corporates)=>gur_corporates.corCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatess:gur_corporates[];
    

   
    @OneToMany(()=>gur_fitness_center, (gur_fitness_center: gur_fitness_center)=>gur_fitness_center.fitCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenters:gur_fitness_center[];
    

   
    @OneToMany(()=>gur_institute_branches, (gur_institute_branches: gur_institute_branches)=>gur_institute_branches.ibrCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteBranchess:gur_institute_branches[];
    

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobs:gur_job[];
    

   
    @OneToMany(()=>gur_job_application, (gur_job_application: gur_job_application)=>gur_job_application.joaCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobApplications:gur_job_application[];
    

   
    @OneToMany(()=>gur_job_education, (gur_job_education: gur_job_education)=>gur_job_education.jedCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobEducations:gur_job_education[];
    

   
    @OneToMany(()=>gur_job_experience, (gur_job_experience: gur_job_experience)=>gur_job_experience.joeCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobExperiences:gur_job_experience[];
    

   
    @OneToMany(()=>gur_journalist_writer_portfolio, (gur_journalist_writer_portfolio: gur_journalist_writer_portfolio)=>gur_journalist_writer_portfolio.jwpEvtCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJournalistWriterPortfolios:gur_journalist_writer_portfolio[];
    

   
    @OneToMany(()=>gur_logistics_services_map, (gur_logistics_services_map: gur_logistics_services_map)=>gur_logistics_services_map.lsmCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLogisticsServicesMaps:gur_logistics_services_map[];
    

   
    @OneToMany(()=>gur_managed_portfolio, (gur_managed_portfolio: gur_managed_portfolio)=>gur_managed_portfolio.mhtCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurManagedPortfolios:gur_managed_portfolio[];
    

   
    @OneToMany(()=>gur_matik_ads, (gur_matik_ads: gur_matik_ads)=>gur_matik_ads.adsCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikAdss:gur_matik_ads[];
    

   
    @OneToMany(()=>gur_matik_knowhow_awards_main, (gur_matik_knowhow_awards_main: gur_matik_knowhow_awards_main)=>gur_matik_knowhow_awards_main.awmCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsMains:gur_matik_knowhow_awards_main[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_academies, (gur_matik_knowhow_career_academies: gur_matik_knowhow_career_academies)=>gur_matik_knowhow_career_academies.khaCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerAcademiess:gur_matik_knowhow_career_academies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_guides, (gur_matik_knowhow_career_guides: gur_matik_knowhow_career_guides)=>gur_matik_knowhow_career_guides.khgCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerGuidess:gur_matik_knowhow_career_guides[];
    

   
    @OneToMany(()=>gur_matik_knowhow_career_vacancies, (gur_matik_knowhow_career_vacancies: gur_matik_knowhow_career_vacancies)=>gur_matik_knowhow_career_vacancies.khvJobCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCareerVacanciess:gur_matik_knowhow_career_vacancies[];
    

   
    @OneToMany(()=>gur_matik_knowhow_gallery_media, (gur_matik_knowhow_gallery_media: gur_matik_knowhow_gallery_media)=>gur_matik_knowhow_gallery_media.kgmCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowGalleryMedias:gur_matik_knowhow_gallery_media[];
    

   
    @OneToMany(()=>gur_matik_knowhow_history_timeline, (gur_matik_knowhow_history_timeline: gur_matik_knowhow_history_timeline)=>gur_matik_knowhow_history_timeline.khtCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowHistoryTimelines:gur_matik_knowhow_history_timeline[];
    

   
    @OneToMany(()=>gur_matik_knowhow_teams, (gur_matik_knowhow_teams: gur_matik_knowhow_teams)=>gur_matik_knowhow_teams.khtCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowTeamss:gur_matik_knowhow_teams[];
    

   
    @OneToMany(()=>gur_matik_play_address_book, (gur_matik_play_address_book: gur_matik_play_address_book)=>gur_matik_play_address_book.adbCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayAddressBooks:gur_matik_play_address_book[];
    

   
    @OneToMany(()=>gur_multiple_centres, (gur_multiple_centres: gur_multiple_centres)=>gur_multiple_centres.mlcCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMultipleCentress:gur_multiple_centres[];
    

   
    @OneToMany(()=>gur_plans_price, (gur_plans_price: gur_plans_price)=>gur_plans_price.plpCurrencyCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlansPrices:gur_plans_price[];
    

   
    @OneToMany(()=>gur_recruitment_history, (gur_recruitment_history: gur_recruitment_history)=>gur_recruitment_history.rehCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRecruitmentHistorys:gur_recruitment_history[];
    

   
    @OneToMany(()=>gur_schools, (gur_schools: gur_schools)=>gur_schools.schCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSchoolss:gur_schools[];
    

   
    @OneToMany(()=>gur_sports_event_schedule, (gur_sports_event_schedule: gur_sports_event_schedule)=>gur_sports_event_schedule.evsCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsEventSchedules:gur_sports_event_schedule[];
    

   
    @OneToMany(()=>gur_sports_governing_bodies, (gur_sports_governing_bodies: gur_sports_governing_bodies)=>gur_sports_governing_bodies.sgbCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodiess:gur_sports_governing_bodies[];
    

   
    @OneToMany(()=>gur_sports_governing_body, (gur_sports_governing_body: gur_sports_governing_body)=>gur_sports_governing_body.sgbCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodys:gur_sports_governing_body[];
    

   
    @OneToMany(()=>gur_sports_infrastructure_company, (gur_sports_infrastructure_company: gur_sports_infrastructure_company)=>gur_sports_infrastructure_company.sicCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsInfrastructureCompanys:gur_sports_infrastructure_company[];
    

   
    @OneToMany(()=>gur_sports_logistics_portfolio, (gur_sports_logistics_portfolio: gur_sports_logistics_portfolio)=>gur_sports_logistics_portfolio.slpCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsPortfolios:gur_sports_logistics_portfolio[];
    

   
    @OneToMany(()=>gur_sports_logistics_venues, (gur_sports_logistics_venues: gur_sports_logistics_venues)=>gur_sports_logistics_venues.lvnCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsVenuess:gur_sports_logistics_venues[];
    

   
    @OneToMany(()=>gur_sports_marketing_company, (gur_sports_marketing_company: gur_sports_marketing_company)=>gur_sports_marketing_company.smcCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsMarketingCompanys:gur_sports_marketing_company[];
    

   
    @OneToMany(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.wepCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiess:gur_sports_personalities[];
    

   
    @OneToMany(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.wepBirthCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiess2:gur_sports_personalities[];
    

   
    @OneToMany(()=>gur_states, (gur_states: gur_states)=>gur_states.stsCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurStatess:gur_states[];
    

   
    @OneToMany(()=>gur_teams, (gur_teams: gur_teams)=>gur_teams.tmsCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTeamss:gur_teams[];
    

   
    @OneToMany(()=>gur_training_camp_users_map, (gur_training_camp_users_map: gur_training_camp_users_map)=>gur_training_camp_users_map.tcuCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTrainingCampUsersMaps:gur_training_camp_users_map[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpVenueCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    

   
    @OneToMany(()=>gur_users, (gur_users: gur_users)=>gur_users.usrCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserss:gur_users[];
    

   
    @OneToMany(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.venCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorss:gur_vendors[];
    

   
    @OneToMany(()=>gur_vendors_branches, (gur_vendors_branches: gur_vendors_branches)=>gur_vendors_branches.vbrCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsBranchess:gur_vendors_branches[];
    

   
    @OneToMany(()=>gur_wiki_awards_country_wise, (gur_wiki_awards_country_wise: gur_wiki_awards_country_wise)=>gur_wiki_awards_country_wise.wacCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsCountryWises:gur_wiki_awards_country_wise[];
    

   
    @OneToOne(()=>gur_wiki_popular_sports, (gur_wiki_popular_sports: gur_wiki_popular_sports)=>gur_wiki_popular_sports.wnsCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiPopularSports:gur_wiki_popular_sports | null;


   
    @OneToMany(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.wvnCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiVenuess:gur_wiki_venues[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepBirthCountry,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups:gur_world_events_players_bkup[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups2:gur_world_events_players_bkup[];
    

   
    @OneToMany(()=>gur_world_events_records, (gur_world_events_records: gur_world_events_records)=>gur_world_events_records.werCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurWorldEventsRecordss:gur_world_events_records[];
    

   
    @OneToMany(()=>gur_world_events_teams, (gur_world_events_teams: gur_world_events_teams)=>gur_world_events_teams.wetCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsTeamss:gur_world_events_teams[];
    

   
    @OneToMany(()=>gur_world_events_venues, (gur_world_events_venues: gur_world_events_venues)=>gur_world_events_venues.wevCnt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsVenuess:gur_world_events_venues[];
    
}
