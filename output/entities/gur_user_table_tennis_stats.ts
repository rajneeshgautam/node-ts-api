import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_table_tennis_stats" ,{schema:"sportsmatik_local" } )
@Index("tts_usr_id",["ttsUsr",])
@Index("tts_ucp_id",["ttsUcp",])
@Index("tts_event",["ttsEvent",])
@Index("tts_rank",["ttsRank",])
export class gur_user_table_tennis_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"tts_id"
        })
    tts_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTableTennisStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tts_usr_id'})
    ttsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserTableTennisStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tts_ucp_id'})
    ttsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTableTennisStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tts_event'})
    ttsEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"tts_matches_played"
        })
    tts_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"tts_matches_won"
        })
    tts_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"tts_matches_loss"
        })
    tts_matches_loss:number;
        

    @Column("int",{ 
        nullable:false,
        name:"tts_balance"
        })
    tts_balance:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTableTennisStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tts_rank'})
    ttsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tts_modifiedtime"
        })
    tts_modifiedtime:Date;
        
}
