import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_world_events_schedules} from "./gur_world_events_schedules";


@Entity("gur_world_events_venues" ,{schema:"sportsmatik_local" } )
@Index("wev_wvn_id",["wevWvn",],{unique:true})
@Index("gur_world_events_venues_ibfk_2",["wevCnt",])
@Index("gur_world_events_venues_ibfk_3",["wevCit",])
export class gur_world_events_venues {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wev_id"
        })
    wev_id:string;
        

   
    @OneToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurWorldEventsVenues,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wev_wvn_id'})
    wevWvn:gur_wiki_venues | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wev_name"
        })
    wev_name:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWorldEventsVenuess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wev_cnt_id'})
    wevCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurWorldEventsVenuess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wev_cit_id'})
    wevCit:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:false,
        name:"wev_createdby"
        })
    wev_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wev_modifiedby"
        })
    wev_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wev_modifiedtime"
        })
    wev_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.evsVenue,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsScheduless:gur_world_events_schedules[];
    
}
