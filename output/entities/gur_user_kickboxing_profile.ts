import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_kickboxing_profile" ,{schema:"sportsmatik_local" } )
@Index("kcb_usr_id",["kcbUsr",])
@Index("kcb_event",["kcbEvent",])
@Index("kcb_createdby_user",["kcbCreatedbyUser",])
export class gur_user_kickboxing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kcb_id"
        })
    kcb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKickboxingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kcb_usr_id'})
    kcbUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["orthodox","southpaw"],
        name:"kcb_stance"
        })
    kcb_stance:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKickboxingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kcb_event'})
    kcbEvent:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kcb_modifiedtime"
        })
    kcb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKickboxingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kcb_createdby_user'})
    kcbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"kcb_modifiedby"
        })
    kcb_modifiedby:string | null;
        
}
