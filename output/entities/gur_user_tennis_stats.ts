import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_tennis_stats" ,{schema:"sportsmatik_local" } )
@Index("uts_usr_id",["utsUsr",])
@Index("uts_ucp_id",["utsUcp",])
@Index("uts_type",["utsEvent",])
@Index("uts_rank",["utsRank",])
export class gur_user_tennis_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uts_id"
        })
    uts_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTennisStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uts_usr_id'})
    utsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserTennisStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uts_ucp_id'})
    utsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTennisStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uts_event'})
    utsEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"uts_matches_played"
        })
    uts_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uts_matches_won"
        })
    uts_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uts_matches_loss"
        })
    uts_matches_loss:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"uts_aces_won"
        })
    uts_aces_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"uts_double_faults"
        })
    uts_double_faults:number;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"uts_first_serve_percentage"
        })
    uts_first_serve_percentage:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"uts_first_serve_points_won"
        })
    uts_first_serve_points_won:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"uts_service_game_won_percentage"
        })
    uts_service_game_won_percentage:number | null;
        

    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"uts_second_serve_points_won"
        })
    uts_second_serve_points_won:number | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTennisStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uts_rank'})
    utsRank:gur_value_list | null;


    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"uts_hightest_serve_speed"
        })
    uts_hightest_serve_speed:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uts_modifiedtime"
        })
    uts_modifiedtime:Date;
        
}
