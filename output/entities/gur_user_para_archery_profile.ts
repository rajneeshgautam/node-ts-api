import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_archery_profile" ,{schema:"sportsmatik_local" } )
@Index("arc_usr_id",["arcUsr",])
@Index("arc_disability_class",["arcDisabilityClass",])
@Index("arc_createdby_user",["arcCreatedbyUser",])
export class gur_user_para_archery_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"arc_id"
        })
    arc_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaArcheryProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arc_usr_id'})
    arcUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"arc_division_recurve"
        })
    arc_division_recurve:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"arc_division_compound"
        })
    arc_division_compound:boolean | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"arc_division_visually_imparied"
        })
    arc_division_visually_imparied:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaArcheryProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arc_disability_class'})
    arcDisabilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"arc_modifiedtime"
        })
    arc_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaArcheryProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arc_createdby_user'})
    arcCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"arc_modifiedby"
        })
    arc_modifiedby:string | null;
        
}
