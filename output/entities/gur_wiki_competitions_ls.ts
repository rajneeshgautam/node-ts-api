import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_competitions} from "./gur_wiki_competitions";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_competitions_ls" ,{schema:"sportsmatik_local" } )
@Index("wcl_wcm_id",["wclWcm",])
@Index("wcl_lng_code",["wclLngCode",])
export class gur_wiki_competitions_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wcl_id"
        })
    wcl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_competitions, (gur_wiki_competitions: gur_wiki_competitions)=>gur_wiki_competitions.gurWikiCompetitionsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcl_wcm_id'})
    wclWcm:gur_wiki_competitions | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wcl_name"
        })
    wcl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wcl_intro_desc"
        })
    wcl_intro_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wcl_com_format"
        })
    wcl_com_format:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wcl_team_description"
        })
    wcl_team_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcl_meta_title"
        })
    wcl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wcl_meta_desc"
        })
    wcl_meta_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiCompetitionsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wcl_lng_code'})
    wclLngCode:gur_languages | null;

}
