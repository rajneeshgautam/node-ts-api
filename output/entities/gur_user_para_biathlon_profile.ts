import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_biathlon_profile" ,{schema:"sportsmatik_local" } )
@Index("pbi_usr_id",["pbiUsr",])
@Index("pbi_disablility_class",["pbiDisablilityClass",])
@Index("pbi_createdby_user",["pbiCreatedbyUser",])
export class gur_user_para_biathlon_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pbi_id"
        })
    pbi_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBiathlonProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbi_usr_id'})
    pbiUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaBiathlonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbi_disablility_class'})
    pbiDisablilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pbi_modifiedtime"
        })
    pbi_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaBiathlonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pbi_createdby_user'})
    pbiCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pbi_modifiedby"
        })
    pbi_modifiedby:string | null;
        
}
