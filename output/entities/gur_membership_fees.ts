import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_membership_fees" ,{schema:"sportsmatik_local" } )
@Index("fmf_user_id",["fmf_user_id",])
@Index("fmf_uty_id",["fmfUty",])
@Index("fmf_createdby_user",["fmfCreatedbyUser",])
@Index("fmf_name",["fmf_name",])
export class gur_membership_fees {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"fmf_id"
        })
    fmf_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"fmf_user_id"
        })
    fmf_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurMembershipFeess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fmf_uty_id'})
    fmfUty:gur_user_types | null;


    @Column("varchar",{ 
        nullable:false,
        name:"fmf_name"
        })
    fmf_name:string;
        

    @Column("double",{ 
        nullable:false,
        unsigned: true,
        precision:22,
        name:"fmf_fees"
        })
    fmf_fees:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"fmf_tax_inclusive"
        })
    fmf_tax_inclusive:boolean;
        

    @Column("int",{ 
        nullable:false,
        name:"fmf_duration"
        })
    fmf_duration:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:1000,
        name:"fmf_desc"
        })
    fmf_desc:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"fmf_status"
        })
    fmf_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"fmf_admin_review"
        })
    fmf_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurMembershipFeess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fmf_createdby_user'})
    fmfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"fmf_createdby"
        })
    fmf_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fmf_modifiedby"
        })
    fmf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fmf_modifiedtime"
        })
    fmf_modifiedtime:Date;
        
}
