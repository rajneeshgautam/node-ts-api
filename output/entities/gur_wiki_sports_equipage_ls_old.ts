import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_wiki_sports_equipage_ls_old" ,{schema:"sportsmatik_local" } )
@Index("uel_title",["uel_title",])
@Index("uel_uae_id",["uel_uae_id",])
@Index("uel_lng_code",["uel_lng_code",])
export class gur_wiki_sports_equipage_ls_old {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"uel_id"
        })
    uel_id:string;
        

    @Column("bigint",{ 
        nullable:false,
        unsigned: true,
        name:"uel_uae_id"
        })
    uel_uae_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uel_title"
        })
    uel_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uel_desc"
        })
    uel_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uel_meta_title"
        })
    uel_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uel_meta_desc"
        })
    uel_meta_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"uel_meta_keywords"
        })
    uel_meta_keywords:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"uel_lng_code"
        })
    uel_lng_code:string;
        
}
