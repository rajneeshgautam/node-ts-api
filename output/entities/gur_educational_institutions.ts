import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_user_qualifications} from "./gur_user_qualifications";


@Entity("gur_educational_institutions" ,{schema:"sportsmatik_local" } )
@Index("uni_name",["ins_name",],{unique:true})
@Index("ins_createdby_user",["insCreatedbyUser",])
@Index("ins_createdby_admin",["insCreatedbyAdmin",])
export class gur_educational_institutions {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ins_id"
        })
    ins_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"ins_name"
        })
    ins_name:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'2'",
        name:"ins_status"
        })
    ins_status:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ins_authentic"
        })
    ins_authentic:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurEducationalInstitutionss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ins_createdby_user'})
    insCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurEducationalInstitutionss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ins_createdby_admin'})
    insCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"ins_createdby"
        })
    ins_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"ins_modifiedby"
        })
    ins_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ins_modifiedtime"
        })
    ins_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfInstituteName,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    
}
