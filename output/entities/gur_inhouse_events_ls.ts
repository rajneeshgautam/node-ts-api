import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_inhouse_events} from "./gur_inhouse_events";
import {gur_languages} from "./gur_languages";


@Entity("gur_inhouse_events_ls" ,{schema:"sportsmatik_local" } )
@Index("evl_evt_id",["evlEvt",])
@Index("evl_lng_code",["evlLngCode",])
export class gur_inhouse_events_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"evl_id"
        })
    evl_id:number;
        

   
    @ManyToOne(()=>gur_inhouse_events, (gur_inhouse_events: gur_inhouse_events)=>gur_inhouse_events.gurInhouseEventsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evl_evt_id'})
    evlEvt:gur_inhouse_events | null;


    @Column("varchar",{ 
        nullable:true,
        name:"evl_name"
        })
    evl_name:string | null;
        

    @Column("longtext",{ 
        nullable:true,
        name:"evl_descriptions"
        })
    evl_descriptions:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurInhouseEventsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evl_lng_code'})
    evlLngCode:gur_languages | null;

}
