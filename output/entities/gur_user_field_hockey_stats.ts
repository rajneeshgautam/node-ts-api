import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_field_hockey_stats" ,{schema:"sportsmatik_local" } )
@Index("fhs_usr_id",["fhsUsr",])
@Index("fhs_ucp_id",["fhsUcp",])
export class gur_user_field_hockey_stats {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fhs_id"
        })
    fhs_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFieldHockeyStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fhs_usr_id'})
    fhsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserFieldHockeyStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fhs_ucp_id'})
    fhsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"fhs_matches_played"
        })
    fhs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"fhs_games_starts"
        })
    fhs_games_starts:number;
        

    @Column("int",{ 
        nullable:false,
        name:"fhs_games_sub_ins"
        })
    fhs_games_sub_ins:number;
        

    @Column("int",{ 
        nullable:false,
        name:"fhs_total_minutes_played"
        })
    fhs_total_minutes_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"fhs_goals_scored"
        })
    fhs_goals_scored:number;
        

    @Column("int",{ 
        nullable:false,
        name:"fhs_goals_assist"
        })
    fhs_goals_assist:number;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_shots_attempted"
        })
    fhs_shots_attempted:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_shots_on_target"
        })
    fhs_shots_on_target:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_matches_won"
        })
    fhs_matches_won:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_matches_draw"
        })
    fhs_matches_draw:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_matches_lose"
        })
    fhs_matches_lose:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_yellow_card"
        })
    fhs_yellow_card:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_red_card"
        })
    fhs_red_card:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_goals_saves"
        })
    fhs_goals_saves:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_goals_conceded"
        })
    fhs_goals_conceded:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"fhs_goals_against_average"
        })
    fhs_goals_against_average:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"fhs_clean_sheets"
        })
    fhs_clean_sheets:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fhs_modifiedtime"
        })
    fhs_modifiedtime:Date;
        
}
