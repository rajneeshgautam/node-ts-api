import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthday_wishes} from "./gur_birthday_wishes";
import {gur_languages} from "./gur_languages";


@Entity("gur_birthday_wishes_ls" ,{schema:"sportsmatik_local" } )
@Index("bwl_bws_id",["bwlBws",])
@Index("bwl_lng_code",["bwlLngCode",])
export class gur_birthday_wishes_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bwl_id"
        })
    bwl_id:number;
        

   
    @ManyToOne(()=>gur_birthday_wishes, (gur_birthday_wishes: gur_birthday_wishes)=>gur_birthday_wishes.gurBirthdayWishesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bwl_bws_id'})
    bwlBws:gur_birthday_wishes | null;


    @Column("text",{ 
        nullable:true,
        name:"bwl_desc"
        })
    bwl_desc:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurBirthdayWishesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bwl_lng_code'})
    bwlLngCode:gur_languages | null;

}
