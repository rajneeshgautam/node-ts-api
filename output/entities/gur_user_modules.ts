import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_modules_map} from "./gur_user_modules_map";
import {gur_users_permissions} from "./gur_users_permissions";


@Entity("gur_user_modules" ,{schema:"sportsmatik_local" } )
@Index("amd_code",["amd_code",],{unique:true})
export class gur_user_modules {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"amd_id"
        })
    amd_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"amd_code"
        })
    amd_code:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"amd_name"
        })
    amd_name:string;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'1'",
        name:"amd_sort_order"
        })
    amd_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"amd_pagename"
        })
    amd_pagename:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"amd_icon"
        })
    amd_icon:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_is_premium"
        })
    amd_is_premium:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_athlete"
        })
    amd_athlete:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sports_enthusiast"
        })
    amd_sports_enthusiast:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_coach"
        })
    amd_coach:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_physio"
        })
    amd_physio:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sport_doc"
        })
    amd_sport_doc:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_ath_manager"
        })
    amd_ath_manager:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_fitness_trainer"
        })
    amd_fitness_trainer:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sports_nutri"
        })
    amd_sports_nutri:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_journalist_writer"
        })
    amd_journalist_writer:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_lawyer"
        })
    amd_lawyer:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_club"
        })
    amd_club:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_academy"
        })
    amd_academy:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_school"
        })
    amd_school:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_college"
        })
    amd_college:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_corporate"
        })
    amd_corporate:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_logistics"
        })
    amd_logistics:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_smc"
        })
    amd_smc:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sponsor_individual"
        })
    amd_sponsor_individual:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sponsor_company"
        })
    amd_sponsor_company:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_acc_individual"
        })
    amd_acc_individual:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_acc_firm"
        })
    amd_acc_firm:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_fitness_center"
        })
    amd_fitness_center:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sports_governing_body"
        })
    amd_sports_governing_body:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_spo_official"
        })
    amd_spo_official:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_psychologist"
        })
    amd_psychologist:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_media_professionals"
        })
    amd_media_professionals:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_scout"
        })
    amd_scout:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_infrastructure_company"
        })
    amd_infrastructure_company:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_sec"
        })
    amd_sec:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"amd_stc"
        })
    amd_stc:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"amd_createdby"
        })
    amd_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"amd_modifiedby"
        })
    amd_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"amd_modifiedtime"
        })
    amd_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_modules_map, (gur_user_modules_map: gur_user_modules_map)=>gur_user_modules_map.ummAmd,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModulesMaps:gur_user_modules_map[];
    

   
    @OneToMany(()=>gur_users_permissions, (gur_users_permissions: gur_users_permissions)=>gur_users_permissions.adrModule,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUsersPermissionss:gur_users_permissions[];
    
}
