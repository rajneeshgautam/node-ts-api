import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_value_list} from "./gur_value_list";
import {gur_health_injuries} from "./gur_health_injuries";
import {gur_health_injuries_titles_ls} from "./gur_health_injuries_titles_ls";


@Entity("gur_health_injuries_titles" ,{schema:"sportsmatik_local" } )
@Index("hwi_body_part",["hwtBodyPart",])
@Index("hwi_sport",["hwt_sports",])
@Index("hwi_tab",["hwtTab",])
export class gur_health_injuries_titles {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"hwt_id"
        })
    hwt_id:number;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurHealthInjuriesTitless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hwt_tab'})
    hwtTab:gur_matik_knowhow_tab_categories | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurHealthInjuriesTitless,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hwt_body_part'})
    hwtBodyPart:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        name:"hwt_sports"
        })
    hwt_sports:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"hwt_createdby"
        })
    hwt_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hwt_modifiedby"
        })
    hwt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hwt_modifiedtime"
        })
    hwt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_health_injuries, (gur_health_injuries: gur_health_injuries)=>gur_health_injuries.hwiHwt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthInjuriess:gur_health_injuries[];
    

   
    @OneToMany(()=>gur_health_injuries_titles_ls, (gur_health_injuries_titles_ls: gur_health_injuries_titles_ls)=>gur_health_injuries_titles_ls.htlHwt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthInjuriesTitlesLss:gur_health_injuries_titles_ls[];
    
}
