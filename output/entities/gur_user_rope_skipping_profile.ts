import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_rope_skipping_profile" ,{schema:"sportsmatik_local" } )
@Index("tri_usr_id",["rpsUsr",])
@Index("tri_createdby_user",["rps_createdby_user",])
export class gur_user_rope_skipping_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"rps_id"
        })
    rps_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRopeSkippingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rps_usr_id'})
    rpsUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"rps_modifiedtime"
        })
    rps_modifiedtime:Date;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"rps_createdby_user"
        })
    rps_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"rps_modifiedby"
        })
    rps_modifiedby:string | null;
        
}
