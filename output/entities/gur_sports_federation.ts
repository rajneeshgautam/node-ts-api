import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_sports_federation" ,{schema:"sportsmatik_local" } )
@Index("sfd_gst_number",["sfd_gst_number",],{unique:true})
@Index("sfd_usr_id",["sfdUsr",])
@Index("sfd_createdby_user",["sfdCreatedbyUser",])
@Index("sfd_country",["sfdCountry",])
@Index("sfd_dis_id",["sfdDis",])
@Index("sfd_sts_id",["sfdSts",])
@Index("sfd_name",["sfd_name",])
export class gur_sports_federation {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sfd_id"
        })
    sfd_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sfd_slug"
        })
    sfd_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sfd_name"
        })
    sfd_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsFederations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sfd_usr_id'})
    sfdUsr:gur_users | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsFederations,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sfd_country'})
    sfdCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurSportsFederations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sfd_dis_id'})
    sfdDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurSportsFederations,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sfd_sts_id'})
    sfdSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"sfd_address_google_map"
        })
    sfd_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sfd_address"
        })
    sfd_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sfd_address2"
        })
    sfd_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sfd_pincode"
        })
    sfd_pincode:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sfd_email"
        })
    sfd_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sfd_email_verified"
        })
    sfd_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sfd_email_verify_time"
        })
    sfd_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sfd_phone1"
        })
    sfd_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sfd_phone1_verified"
        })
    sfd_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sfd_phone1_otp"
        })
    sfd_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sfd_phone1_otp_time"
        })
    sfd_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sfd_phone2"
        })
    sfd_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sfd_phone2_verified"
        })
    sfd_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sfd_phone2_otp"
        })
    sfd_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sfd_phone2_otp_time"
        })
    sfd_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"sfd_status"
        })
    sfd_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sfd_profile_pic"
        })
    sfd_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sfd_intro_status"
        })
    sfd_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"sfd_profile_summary"
        })
    sfd_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sfd_founded_year"
        })
    sfd_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sfd_founded_month"
        })
    sfd_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sfd_last_login_date"
        })
    sfd_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sfd_last_login_details"
        })
    sfd_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"sfd_intro_modifiedtime"
        })
    sfd_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sfd_profile_pic_modifiedtime"
        })
    sfd_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sfd_free_sports"
        })
    sfd_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sfd_free_sub_users"
        })
    sfd_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"sfd_gst_number"
        })
    sfd_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sfd_viewable"
        })
    sfd_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsFederations2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sfd_createdby_user'})
    sfdCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sfd_createdby"
        })
    sfd_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sfd_modifiedby"
        })
    sfd_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sfd_modifiedtime"
        })
    sfd_modifiedtime:Date;
        
}
