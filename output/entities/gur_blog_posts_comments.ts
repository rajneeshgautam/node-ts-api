import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_blog_posts} from "./gur_blog_posts";


@Entity("gur_blog_posts_comments" ,{schema:"sportsmatik_local" } )
@Index("bco_bpt_id",["bcoBpt",])
export class gur_blog_posts_comments {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"bco_id"
        })
    bco_id:string;
        

   
    @ManyToOne(()=>gur_blog_posts, (gur_blog_posts: gur_blog_posts)=>gur_blog_posts.gurBlogPostsCommentss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bco_bpt_id'})
    bcoBpt:gur_blog_posts | null;


    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"bco_replied_for"
        })
    bco_replied_for:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bco_name"
        })
    bco_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bco_email"
        })
    bco_email:string;
        

    @Column("text",{ 
        nullable:false,
        name:"bco_desc"
        })
    bco_desc:string;
        

    @Column("datetime",{ 
        nullable:false,
        name:"bco_postedon"
        })
    bco_postedon:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bco_status"
        })
    bco_status:boolean;
        
}
