import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_hall_of_fame} from "./gur_hall_of_fame";
import {gur_languages} from "./gur_languages";


@Entity("gur_hall_of_fame_ls" ,{schema:"sportsmatik_local" } )
@Index("hol_bir_id",["holHof",])
@Index("hol_lng_code",["holLngCode",])
export class gur_hall_of_fame_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"hol_id"
        })
    hol_id:number;
        

   
    @ManyToOne(()=>gur_hall_of_fame, (gur_hall_of_fame: gur_hall_of_fame)=>gur_hall_of_fame.gurHallOfFameLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hol_hof_id'})
    holHof:gur_hall_of_fame | null;


    @Column("varchar",{ 
        nullable:true,
        name:"hol_name"
        })
    hol_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hol_profession"
        })
    hol_profession:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"hol_descriptions"
        })
    hol_descriptions:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"hol_meta_title"
        })
    hol_meta_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"hol_meta_description"
        })
    hol_meta_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"hol_meta_keywords"
        })
    hol_meta_keywords:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurHallOfFameLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hol_lng_code'})
    holLngCode:gur_languages | null;

}
