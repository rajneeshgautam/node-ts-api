import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quotes} from "./gur_quotes";
import {gur_quotes_by_ls} from "./gur_quotes_by_ls";


@Entity("gur_quotes_by" ,{schema:"sportsmatik_local" } )
export class gur_quotes_by {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"qub_id"
        })
    qub_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"qub_link"
        })
    qub_link:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"qub_createdby"
        })
    qub_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"qub_modifiedby"
        })
    qub_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"qub_modifiedtime"
        })
    qub_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_quotes, (gur_quotes: gur_quotes)=>gur_quotes.quoQub,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuotess:gur_quotes[];
    

   
    @OneToMany(()=>gur_quotes_by_ls, (gur_quotes_by_ls: gur_quotes_by_ls)=>gur_quotes_by_ls.qblQub,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuotesByLss:gur_quotes_by_ls[];
    
}
