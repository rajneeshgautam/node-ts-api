import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_states} from "./gur_states";
import {gur_academies} from "./gur_academies";
import {gur_clubs} from "./gur_clubs";
import {gur_colleges} from "./gur_colleges";
import {gur_corporates} from "./gur_corporates";
import {gur_fitness_center} from "./gur_fitness_center";
import {gur_job} from "./gur_job";
import {gur_job_application} from "./gur_job_application";
import {gur_job_education} from "./gur_job_education";
import {gur_job_experience} from "./gur_job_experience";
import {gur_matik_play_address_book} from "./gur_matik_play_address_book";
import {gur_multiple_centres} from "./gur_multiple_centres";
import {gur_schools} from "./gur_schools";
import {gur_sports_governing_body} from "./gur_sports_governing_body";
import {gur_sports_infrastructure_company} from "./gur_sports_infrastructure_company";
import {gur_sports_marketing_company} from "./gur_sports_marketing_company";
import {gur_users} from "./gur_users";
import {gur_vendors} from "./gur_vendors";


@Entity("gur_districts" ,{schema:"sportsmatik_local" } )
@Index("dis_sts_id",["disSts",])
export class gur_districts {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"dis_id"
        })
    dis_id:number;
        

   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurDistrictss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dis_sts_id'})
    disSts:gur_states | null;


    @Column("varchar",{ 
        nullable:false,
        name:"dis_name"
        })
    dis_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"dst_hasc"
        })
    dst_hasc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"dst_stat"
        })
    dst_stat:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"dis_createdby"
        })
    dis_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"dis_modifiedby"
        })
    dis_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"dis_modifiedtime"
        })
    dis_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_academies, (gur_academies: gur_academies)=>gur_academies.acaDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAcademiess:gur_academies[];
    

   
    @OneToMany(()=>gur_clubs, (gur_clubs: gur_clubs)=>gur_clubs.clbDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurClubss:gur_clubs[];
    

   
    @OneToMany(()=>gur_colleges, (gur_colleges: gur_colleges)=>gur_colleges.clgDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCollegess:gur_colleges[];
    

   
    @OneToMany(()=>gur_corporates, (gur_corporates: gur_corporates)=>gur_corporates.corDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatess:gur_corporates[];
    

   
    @OneToMany(()=>gur_fitness_center, (gur_fitness_center: gur_fitness_center)=>gur_fitness_center.fitDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenters:gur_fitness_center[];
    

   
    @OneToMany(()=>gur_job, (gur_job: gur_job)=>gur_job.jobDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobs:gur_job[];
    

   
    @OneToMany(()=>gur_job_application, (gur_job_application: gur_job_application)=>gur_job_application.joaDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobApplications:gur_job_application[];
    

   
    @OneToMany(()=>gur_job_education, (gur_job_education: gur_job_education)=>gur_job_education.jedDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobEducations:gur_job_education[];
    

   
    @OneToMany(()=>gur_job_experience, (gur_job_experience: gur_job_experience)=>gur_job_experience.joeDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobExperiences:gur_job_experience[];
    

   
    @OneToMany(()=>gur_matik_play_address_book, (gur_matik_play_address_book: gur_matik_play_address_book)=>gur_matik_play_address_book.adbDistCity,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayAddressBooks:gur_matik_play_address_book[];
    

   
    @OneToMany(()=>gur_multiple_centres, (gur_multiple_centres: gur_multiple_centres)=>gur_multiple_centres.mlcDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMultipleCentress:gur_multiple_centres[];
    

   
    @OneToMany(()=>gur_schools, (gur_schools: gur_schools)=>gur_schools.schDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSchoolss:gur_schools[];
    

   
    @OneToMany(()=>gur_sports_governing_body, (gur_sports_governing_body: gur_sports_governing_body)=>gur_sports_governing_body.sgbDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodys:gur_sports_governing_body[];
    

   
    @OneToMany(()=>gur_sports_infrastructure_company, (gur_sports_infrastructure_company: gur_sports_infrastructure_company)=>gur_sports_infrastructure_company.sicDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsInfrastructureCompanys:gur_sports_infrastructure_company[];
    

   
    @OneToMany(()=>gur_sports_marketing_company, (gur_sports_marketing_company: gur_sports_marketing_company)=>gur_sports_marketing_company.smcDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsMarketingCompanys:gur_sports_marketing_company[];
    

   
    @OneToMany(()=>gur_users, (gur_users: gur_users)=>gur_users.usrDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserss:gur_users[];
    

   
    @OneToMany(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.venDis,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorss:gur_vendors[];
    
}
