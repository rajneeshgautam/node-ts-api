import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_law_ca_services_categories} from "./gur_law_ca_services_categories";


@Entity("gur_lawyer_ca_portfolio" ,{schema:"sportsmatik_local" } )
@Index("lwp_usr_id",["lwp_user_id",])
@Index("lwp_user_type",["lwpUserType",])
@Index("lwp_service",["lwpService",])
export class gur_lawyer_ca_portfolio {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"lwp_id"
        })
    lwp_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"lwp_user_id"
        })
    lwp_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurLawyerCaPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwp_user_type'})
    lwpUserType:gur_user_types | null;


    @Column("varchar",{ 
        nullable:false,
        name:"lwp_client"
        })
    lwp_client:string;
        

   
    @ManyToOne(()=>gur_law_ca_services_categories, (gur_law_ca_services_categories: gur_law_ca_services_categories)=>gur_law_ca_services_categories.gurLawyerCaPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwp_service'})
    lwpService:gur_law_ca_services_categories | null;


    @Column("date",{ 
        nullable:false,
        name:"lwp_start_date"
        })
    lwp_start_date:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lwp_handling"
        })
    lwp_handling:boolean;
        

    @Column("date",{ 
        nullable:true,
        name:"lwp_end_date"
        })
    lwp_end_date:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"lwp_description"
        })
    lwp_description:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"lwp_status"
        })
    lwp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lwp_admin_review"
        })
    lwp_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"lwp_createdby"
        })
    lwp_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"lwp_modifiedby"
        })
    lwp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lwp_modified_time"
        })
    lwp_modified_time:Date;
        
}
