import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_community_question} from "./gur_community_question";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_community_question_view" ,{schema:"sportsmatik_local" } )
@Index("cvu_com_id",["cvuCom","cvu_user_id","cvuUty",],{unique:true})
@Index("cvu_uty_id",["cvuUty",])
export class gur_community_question_view {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cvu_id"
        })
    cvu_id:number;
        

   
    @ManyToOne(()=>gur_community_question, (gur_community_question: gur_community_question)=>gur_community_question.gurCommunityQuestionViews,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cvu_com_id'})
    cvuCom:gur_community_question | null;


    @Column("varchar",{ 
        nullable:false,
        length:16,
        name:"cvu_ip"
        })
    cvu_ip:string;
        

    @Column("bigint",{ 
        nullable:true,
        name:"cvu_user_id"
        })
    cvu_user_id:string | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCommunityQuestionViews,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cvu_uty_id'})
    cvuUty:gur_user_types | null;

}
