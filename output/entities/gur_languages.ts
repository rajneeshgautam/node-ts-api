import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_banners_ls} from "./gur_banners_ls";
import {gur_birthday_wishes_ls} from "./gur_birthday_wishes_ls";
import {gur_birthdays_ls} from "./gur_birthdays_ls";
import {gur_blog_categories_ls} from "./gur_blog_categories_ls";
import {gur_blog_posts_ls} from "./gur_blog_posts_ls";
import {gur_cms_pages_ls} from "./gur_cms_pages_ls";
import {gur_faq_categories_ls} from "./gur_faq_categories_ls";
import {gur_faq_lists_ls} from "./gur_faq_lists_ls";
import {gur_hall_of_fame_ls} from "./gur_hall_of_fame_ls";
import {gur_inhouse_events_ls} from "./gur_inhouse_events_ls";
import {gur_quotes_by_ls} from "./gur_quotes_by_ls";
import {gur_quotes_ls} from "./gur_quotes_ls";
import {gur_site_menu_ls} from "./gur_site_menu_ls";
import {gur_sports_ls} from "./gur_sports_ls";
import {gur_wiki_awards_country_wise_ls} from "./gur_wiki_awards_country_wise_ls";
import {gur_wiki_awards_list_ls} from "./gur_wiki_awards_list_ls";
import {gur_wiki_awards_sports_wise_ls} from "./gur_wiki_awards_sports_wise_ls";
import {gur_wiki_competitions_ls} from "./gur_wiki_competitions_ls";
import {gur_wiki_features} from "./gur_wiki_features";
import {gur_wiki_knowhow_events_ls} from "./gur_wiki_knowhow_events_ls";
import {gur_wiki_knowhow_ls} from "./gur_wiki_knowhow_ls";
import {gur_wiki_knowhow_players_officials_ls} from "./gur_wiki_knowhow_players_officials_ls";
import {gur_wiki_knowhow_rules_ls} from "./gur_wiki_knowhow_rules_ls";
import {gur_wiki_knowhow_techniques_ls} from "./gur_wiki_knowhow_techniques_ls";
import {gur_wiki_popular_sports_ls} from "./gur_wiki_popular_sports_ls";
import {gur_wiki_sports_equipage_ls} from "./gur_wiki_sports_equipage_ls";
import {gur_wiki_sports_ls} from "./gur_wiki_sports_ls";
import {gur_wiki_technologies_ls} from "./gur_wiki_technologies_ls";
import {gur_wiki_terminologies_ls} from "./gur_wiki_terminologies_ls";
import {gur_wiki_venues_ls} from "./gur_wiki_venues_ls";


@Entity("gur_languages" ,{schema:"sportsmatik_local" } )
@Index("lng_name",["lng_name",],{unique:true})
export class gur_languages {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:4,
        name:"lng_code"
        })
    lng_code:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:64,
        name:"lng_name"
        })
    lng_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lng_primary"
        })
    lng_primary:boolean;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"lng_sort_order"
        })
    lng_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lng_status"
        })
    lng_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"lng_createdby"
        })
    lng_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"lng_modifiedby"
        })
    lng_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lng_modifiedtime"
        })
    lng_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_banners_ls, (gur_banners_ls: gur_banners_ls)=>gur_banners_ls.bnlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBannersLss:gur_banners_ls[];
    

   
    @OneToMany(()=>gur_birthday_wishes_ls, (gur_birthday_wishes_ls: gur_birthday_wishes_ls)=>gur_birthday_wishes_ls.bwlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdayWishesLss:gur_birthday_wishes_ls[];
    

   
    @OneToMany(()=>gur_birthdays_ls, (gur_birthdays_ls: gur_birthdays_ls)=>gur_birthdays_ls.bilLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysLss:gur_birthdays_ls[];
    

   
    @OneToMany(()=>gur_blog_categories_ls, (gur_blog_categories_ls: gur_blog_categories_ls)=>gur_blog_categories_ls.bclLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlogCategoriesLss:gur_blog_categories_ls[];
    

   
    @OneToMany(()=>gur_blog_posts_ls, (gur_blog_posts_ls: gur_blog_posts_ls)=>gur_blog_posts_ls.bplLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlogPostsLss:gur_blog_posts_ls[];
    

   
    @OneToMany(()=>gur_cms_pages_ls, (gur_cms_pages_ls: gur_cms_pages_ls)=>gur_cms_pages_ls.cmlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCmsPagesLss:gur_cms_pages_ls[];
    

   
    @OneToMany(()=>gur_faq_categories_ls, (gur_faq_categories_ls: gur_faq_categories_ls)=>gur_faq_categories_ls.falLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFaqCategoriesLss:gur_faq_categories_ls[];
    

   
    @OneToMany(()=>gur_faq_lists_ls, (gur_faq_lists_ls: gur_faq_lists_ls)=>gur_faq_lists_ls.flsLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFaqListsLss:gur_faq_lists_ls[];
    

   
    @OneToMany(()=>gur_hall_of_fame_ls, (gur_hall_of_fame_ls: gur_hall_of_fame_ls)=>gur_hall_of_fame_ls.holLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHallOfFameLss:gur_hall_of_fame_ls[];
    

   
    @OneToMany(()=>gur_inhouse_events_ls, (gur_inhouse_events_ls: gur_inhouse_events_ls)=>gur_inhouse_events_ls.evlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInhouseEventsLss:gur_inhouse_events_ls[];
    

   
    @OneToMany(()=>gur_quotes_by_ls, (gur_quotes_by_ls: gur_quotes_by_ls)=>gur_quotes_by_ls.qblLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuotesByLss:gur_quotes_by_ls[];
    

   
    @OneToMany(()=>gur_quotes_ls, (gur_quotes_ls: gur_quotes_ls)=>gur_quotes_ls.qolLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuotesLss:gur_quotes_ls[];
    

   
    @OneToMany(()=>gur_site_menu_ls, (gur_site_menu_ls: gur_site_menu_ls)=>gur_site_menu_ls.stlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSiteMenuLss:gur_site_menu_ls[];
    

   
    @OneToMany(()=>gur_sports_ls, (gur_sports_ls: gur_sports_ls)=>gur_sports_ls.splLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLss:gur_sports_ls[];
    

   
    @OneToMany(()=>gur_wiki_awards_country_wise_ls, (gur_wiki_awards_country_wise_ls: gur_wiki_awards_country_wise_ls)=>gur_wiki_awards_country_wise_ls.wclLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsCountryWiseLss:gur_wiki_awards_country_wise_ls[];
    

   
    @OneToMany(()=>gur_wiki_awards_list_ls, (gur_wiki_awards_list_ls: gur_wiki_awards_list_ls)=>gur_wiki_awards_list_ls.wllLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsListLss:gur_wiki_awards_list_ls[];
    

   
    @OneToMany(()=>gur_wiki_awards_sports_wise_ls, (gur_wiki_awards_sports_wise_ls: gur_wiki_awards_sports_wise_ls)=>gur_wiki_awards_sports_wise_ls.wslLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiAwardsSportsWiseLss:gur_wiki_awards_sports_wise_ls[];
    

   
    @OneToMany(()=>gur_wiki_competitions_ls, (gur_wiki_competitions_ls: gur_wiki_competitions_ls)=>gur_wiki_competitions_ls.wclLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiCompetitionsLss:gur_wiki_competitions_ls[];
    

   
    @OneToMany(()=>gur_wiki_features, (gur_wiki_features: gur_wiki_features)=>gur_wiki_features.wvfLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiFeaturess:gur_wiki_features[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_events_ls, (gur_wiki_knowhow_events_ls: gur_wiki_knowhow_events_ls)=>gur_wiki_knowhow_events_ls.evlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowEventsLss:gur_wiki_knowhow_events_ls[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_ls, (gur_wiki_knowhow_ls: gur_wiki_knowhow_ls)=>gur_wiki_knowhow_ls.khlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowLss:gur_wiki_knowhow_ls[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_players_officials_ls, (gur_wiki_knowhow_players_officials_ls: gur_wiki_knowhow_players_officials_ls)=>gur_wiki_knowhow_players_officials_ls.pplLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowPlayersOfficialsLss:gur_wiki_knowhow_players_officials_ls[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_rules_ls, (gur_wiki_knowhow_rules_ls: gur_wiki_knowhow_rules_ls)=>gur_wiki_knowhow_rules_ls.rulLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowRulesLss:gur_wiki_knowhow_rules_ls[];
    

   
    @OneToMany(()=>gur_wiki_knowhow_techniques_ls, (gur_wiki_knowhow_techniques_ls: gur_wiki_knowhow_techniques_ls)=>gur_wiki_knowhow_techniques_ls.telLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiKnowhowTechniquesLss:gur_wiki_knowhow_techniques_ls[];
    

   
    @OneToMany(()=>gur_wiki_popular_sports_ls, (gur_wiki_popular_sports_ls: gur_wiki_popular_sports_ls)=>gur_wiki_popular_sports_ls.nslLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiPopularSportsLss:gur_wiki_popular_sports_ls[];
    

   
    @OneToMany(()=>gur_wiki_sports_equipage_ls, (gur_wiki_sports_equipage_ls: gur_wiki_sports_equipage_ls)=>gur_wiki_sports_equipage_ls.uelLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsEquipageLss:gur_wiki_sports_equipage_ls[];
    

   
    @OneToMany(()=>gur_wiki_sports_ls, (gur_wiki_sports_ls: gur_wiki_sports_ls)=>gur_wiki_sports_ls.splLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiSportsLss:gur_wiki_sports_ls[];
    

   
    @OneToMany(()=>gur_wiki_technologies_ls, (gur_wiki_technologies_ls: gur_wiki_technologies_ls)=>gur_wiki_technologies_ls.telLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiTechnologiesLss:gur_wiki_technologies_ls[];
    

   
    @OneToMany(()=>gur_wiki_terminologies_ls, (gur_wiki_terminologies_ls: gur_wiki_terminologies_ls)=>gur_wiki_terminologies_ls.trlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiTerminologiesLss:gur_wiki_terminologies_ls[];
    

   
    @OneToMany(()=>gur_wiki_venues_ls, (gur_wiki_venues_ls: gur_wiki_venues_ls)=>gur_wiki_venues_ls.wvlLngCode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiVenuesLss:gur_wiki_venues_ls[];
    
}
