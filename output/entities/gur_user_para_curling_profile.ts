import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_curling_profile" ,{schema:"sportsmatik_local" } )
@Index("pcr_usr_id",["pcrUsr",])
@Index("pcr_position1",["pcrPosition",])
@Index("pcr_position2",["pcrPosition2",])
@Index("pcr_createdby_user",["pcrCreatedbyUser",])
export class gur_user_para_curling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pcr_id"
        })
    pcr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCurlingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcr_usr_id'})
    pcrUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaCurlingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcr_position1'})
    pcrPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaCurlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcr_position2'})
    pcrPosition2:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pcr_hand"
        })
    pcr_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pcr_modifiedtime"
        })
    pcr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCurlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcr_createdby_user'})
    pcrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pcr_modifiedby"
        })
    pcr_modifiedby:string | null;
        
}
