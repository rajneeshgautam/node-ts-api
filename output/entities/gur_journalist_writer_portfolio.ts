import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_types} from "./gur_user_types";
import {gur_value_list} from "./gur_value_list";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_journalist_writer_portfolio" ,{schema:"sportsmatik_local" } )
@Index("jwp_usr_id",["jwpUsr",])
@Index("jwp_type",["jwpType",])
@Index("jwp_uty_id",["jwpUty",])
@Index("jwp_evt_country",["jwpEvtCountry",])
@Index("jwp_city",["jwpEvtCity",])
export class gur_journalist_writer_portfolio {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"jwp_id"
        })
    jwp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurJournalistWriterPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jwp_usr_id'})
    jwpUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurJournalistWriterPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jwp_uty_id'})
    jwpUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurJournalistWriterPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jwp_type'})
    jwpType:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"jwp_title"
        })
    jwp_title:string;
        

    @Column("datetime",{ 
        nullable:false,
        name:"jwp_date"
        })
    jwp_date:Date;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"jwp_isbn_no"
        })
    jwp_isbn_no:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"jwp_publisher"
        })
    jwp_publisher:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"jwp_media_type"
        })
    jwp_media_type:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"jwp_evt_organizer"
        })
    jwp_evt_organizer:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurJournalistWriterPortfolios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jwp_evt_country'})
    jwpEvtCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurJournalistWriterPortfolios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'jwp_evt_city'})
    jwpEvtCity:gur_all_cities | null;


    @Column("varchar",{ 
        nullable:true,
        name:"jwp_evt_role"
        })
    jwp_evt_role:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"jwp_link"
        })
    jwp_link:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"jwp_description"
        })
    jwp_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"jwp_media"
        })
    jwp_media:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"jwp_status"
        })
    jwp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"jwp_admin_review"
        })
    jwp_admin_review:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"jwp_createdby"
        })
    jwp_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"jwp_modifiedby"
        })
    jwp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"jwp_modified_time"
        })
    jwp_modified_time:Date;
        
}
