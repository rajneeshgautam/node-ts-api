import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_play_quiz_categories} from "./gur_matik_play_quiz_categories";
import {gur_plans} from "./gur_plans";
import {gur_matik_play_quiz_results_answers_set} from "./gur_matik_play_quiz_results_answers_set";


@Entity("gur_matik_play_quiz_results" ,{schema:"sportsmatik_local" } )
@Index("pqr_uty_id",["pqr_uty_id",])
@Index("pqr_usr_id",["pqr_usr_id",])
@Index("pqr_cat_id",["pqrCat",])
@Index("gur_matik_play_quiz_results_ibfk_2",["pqrPln",])
export class gur_matik_play_quiz_results {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pqr_id"
        })
    pqr_id:number;
        

   
    @ManyToOne(()=>gur_matik_play_quiz_categories, (gur_matik_play_quiz_categories: gur_matik_play_quiz_categories)=>gur_matik_play_quiz_categories.gurMatikPlayQuizResultss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'pqr_cat_id'})
    pqrCat:gur_matik_play_quiz_categories | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'100'",
        name:"pqr_uty_id"
        })
    pqr_uty_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"pqr_usr_id"
        })
    pqr_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"pqr_total_question"
        })
    pqr_total_question:number;
        

    @Column("int",{ 
        nullable:false,
        name:"pqr_questions_attempted"
        })
    pqr_questions_attempted:number;
        

    @Column("int",{ 
        nullable:false,
        name:"pqr_correct_answer"
        })
    pqr_correct_answer:number;
        

    @Column("int",{ 
        nullable:false,
        name:"pqr_incorrect_answers"
        })
    pqr_incorrect_answers:number;
        

    @Column("datetime",{ 
        nullable:false,
        name:"pqr_participated"
        })
    pqr_participated:Date;
        

    @Column("time",{ 
        nullable:false,
        name:"pqr_time_taken"
        })
    pqr_time_taken:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pqr_winning_image"
        })
    pqr_winning_image:string | null;
        

   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurMatikPlayQuizResultss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pqr_pln_id'})
    pqrPln:gur_plans | null;


   
    @OneToMany(()=>gur_matik_play_quiz_results_answers_set, (gur_matik_play_quiz_results_answers_set: gur_matik_play_quiz_results_answers_set)=>gur_matik_play_quiz_results_answers_set.rasPqr,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurMatikPlayQuizResultsAnswersSets:gur_matik_play_quiz_results_answers_set[];
    
}
