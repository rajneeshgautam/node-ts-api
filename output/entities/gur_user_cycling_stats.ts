import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_cycling_stats" ,{schema:"sportsmatik_local" } )
@Index("cyr_usr_id",["cysUsr",])
@Index("cyr_ucp_id",["cysUcp",])
@Index("cyr_event",["cysEvent",])
@Index("cyr_rank",["cysRank",])
export class gur_user_cycling_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"cys_id"
        })
    cys_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCyclingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cys_usr_id'})
    cysUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserCyclingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cys_ucp_id'})
    cysUcp:gur_user_competition_played | null;


    @Column("enum",{ 
        nullable:false,
        enum:["road","track","bmx","mountain"],
        name:"cys_type"
        })
    cys_type:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCyclingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cys_event'})
    cysEvent:gur_value_list | null;


    @Column("time",{ 
        nullable:false,
        name:"cys_time"
        })
    cys_time:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCyclingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cys_rank'})
    cysRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cys_modifiedtime"
        })
    cys_modifiedtime:Date;
        
}
