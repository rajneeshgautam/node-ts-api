import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_darts_profile" ,{schema:"sportsmatik_local" } )
@Index("dar_usr_id",["darUsr",])
@Index("dar_createdby_user",["darCreatedbyUser",])
export class gur_user_darts_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"dar_id"
        })
    dar_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDartsProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dar_usr_id'})
    darUsr:gur_users | null;


    @Column("enum",{ 
        nullable:true,
        enum:["left","right"],
        name:"dar_hand"
        })
    dar_hand:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"dar_darts_used"
        })
    dar_darts_used:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"dar_modifiedtime"
        })
    dar_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDartsProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'dar_createdby_user'})
    darCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"dar_modifiedby"
        })
    dar_modifiedby:string | null;
        
}
