import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_modern_pentathlon_profile" ,{schema:"sportsmatik_local" } )
@Index("mop_usr_id",["mopUsr",])
@Index("mop_createdby_user",["mopCreatedbyUser",])
export class gur_user_modern_pentathlon_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mop_id"
        })
    mop_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserModernPentathlonProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mop_usr_id'})
    mopUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mop_modifiedtime"
        })
    mop_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserModernPentathlonProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mop_createdby_user'})
    mopCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"mop_modifiedby"
        })
    mop_modifiedby:string | null;
        
}
