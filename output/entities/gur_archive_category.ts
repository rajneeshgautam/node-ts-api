import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_archive} from "./gur_archive";


@Entity("gur_archive_category" ,{schema:"sportsmatik_local" } )
@Index("uty_name",["act_name",],{unique:true})
export class gur_archive_category {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"act_id"
        })
    act_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"act_name"
        })
    act_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"act_url"
        })
    act_url:string | null;
        

   
    @OneToMany(()=>gur_archive, (gur_archive: gur_archive)=>gur_archive.arcAct,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurArchives:gur_archive[];
    
}
