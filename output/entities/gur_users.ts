import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_value_list} from "./gur_value_list";
import {gur_states} from "./gur_states";
import {gur_districts} from "./gur_districts";
import {gur_countries} from "./gur_countries";
import {gur_academies} from "./gur_academies";
import {gur_blocked_users} from "./gur_blocked_users";
import {gur_clubs} from "./gur_clubs";
import {gur_colleges} from "./gur_colleges";
import {gur_competition_sports_map} from "./gur_competition_sports_map";
import {gur_competitions} from "./gur_competitions";
import {gur_contact_view_history} from "./gur_contact_view_history";
import {gur_corporates} from "./gur_corporates";
import {gur_educational_courses} from "./gur_educational_courses";
import {gur_educational_courses_specialization} from "./gur_educational_courses_specialization";
import {gur_educational_institutions} from "./gur_educational_institutions";
import {gur_events} from "./gur_events";
import {gur_facilities} from "./gur_facilities";
import {gur_fitness_center} from "./gur_fitness_center";
import {gur_fitness_center_services} from "./gur_fitness_center_services";
import {gur_hours_of_operation} from "./gur_hours_of_operation";
import {gur_infra_portfolio} from "./gur_infra_portfolio";
import {gur_infra_user_products} from "./gur_infra_user_products";
import {gur_institute_branches} from "./gur_institute_branches";
import {gur_institute_course_and_fees} from "./gur_institute_course_and_fees";
import {gur_institutional_staff} from "./gur_institutional_staff";
import {gur_institutional_teams} from "./gur_institutional_teams";
import {gur_institutional_teams_contact_usage} from "./gur_institutional_teams_contact_usage";
import {gur_institutional_trophies} from "./gur_institutional_trophies";
import {gur_journalist_writer_portfolio} from "./gur_journalist_writer_portfolio";
import {gur_law_ca_services_categories} from "./gur_law_ca_services_categories";
import {gur_lawyer_ca_services} from "./gur_lawyer_ca_services";
import {gur_lawyer_professional_details} from "./gur_lawyer_professional_details";
import {gur_logistics_services} from "./gur_logistics_services";
import {gur_managed_portfolio} from "./gur_managed_portfolio";
import {gur_matik_ads} from "./gur_matik_ads";
import {gur_matik_tags} from "./gur_matik_tags";
import {gur_membership_fees} from "./gur_membership_fees";
import {gur_plan_usage_log} from "./gur_plan_usage_log";
import {gur_recruitment_history} from "./gur_recruitment_history";
import {gur_schools} from "./gur_schools";
import {gur_sponsors} from "./gur_sponsors";
import {gur_sports_attributes} from "./gur_sports_attributes";
import {gur_sports_events_company} from "./gur_sports_events_company";
import {gur_sports_governing_body} from "./gur_sports_governing_body";
import {gur_sports_infrastructure_company} from "./gur_sports_infrastructure_company";
import {gur_sports_logistics} from "./gur_sports_logistics";
import {gur_sports_logistics_venues} from "./gur_sports_logistics_venues";
import {gur_sports_marketing_company} from "./gur_sports_marketing_company";
import {gur_sports_official_types} from "./gur_sports_official_types";
import {gur_sports_official_user_profile} from "./gur_sports_official_user_profile";
import {gur_sports_tech_company} from "./gur_sports_tech_company";
import {gur_sports_vacancies_applicants} from "./gur_sports_vacancies_applicants";
import {gur_squads_group} from "./gur_squads_group";
import {gur_squads_list} from "./gur_squads_list";
import {gur_sub_users} from "./gur_sub_users";
import {gur_subuser_usage_history} from "./gur_subuser_usage_history";
import {gur_survey_answers} from "./gur_survey_answers";
import {gur_teams} from "./gur_teams";
import {gur_training_camp_users_map} from "./gur_training_camp_users_map";
import {gur_user_aikido_profile} from "./gur_user_aikido_profile";
import {gur_user_air_racing_profile} from "./gur_user_air_racing_profile";
import {gur_user_albums} from "./gur_user_albums";
import {gur_user_alpine_skiing_profile} from "./gur_user_alpine_skiing_profile";
import {gur_user_american_handball_profile} from "./gur_user_american_handball_profile";
import {gur_user_american_rules_football_profile} from "./gur_user_american_rules_football_profile";
import {gur_user_archery_profile} from "./gur_user_archery_profile";
import {gur_user_archery_stats} from "./gur_user_archery_stats";
import {gur_user_artistic_roller_skating_profile} from "./gur_user_artistic_roller_skating_profile";
import {gur_user_athletics_profile} from "./gur_user_athletics_profile";
import {gur_user_athletics_stats} from "./gur_user_athletics_stats";
import {gur_user_australian_rules_football_profile} from "./gur_user_australian_rules_football_profile";
import {gur_user_awards_achievements} from "./gur_user_awards_achievements";
import {gur_user_badminton_profile} from "./gur_user_badminton_profile";
import {gur_user_badminton_stats} from "./gur_user_badminton_stats";
import {gur_user_bandy_profile} from "./gur_user_bandy_profile";
import {gur_user_baseball_profile} from "./gur_user_baseball_profile";
import {gur_user_basic_moderation} from "./gur_user_basic_moderation";
import {gur_user_basketball_profile} from "./gur_user_basketball_profile";
import {gur_user_basketball_stats} from "./gur_user_basketball_stats";
import {gur_user_basque_pelota_profile} from "./gur_user_basque_pelota_profile";
import {gur_user_beach_volleyball_profile} from "./gur_user_beach_volleyball_profile";
import {gur_user_biathlon_profile} from "./gur_user_biathlon_profile";
import {gur_user_billiards_profile} from "./gur_user_billiards_profile";
import {gur_user_blind_cricket_profile} from "./gur_user_blind_cricket_profile";
import {gur_user_bobsleigh_profile} from "./gur_user_bobsleigh_profile";
import {gur_user_bodybuilding_profile} from "./gur_user_bodybuilding_profile";
import {gur_user_boxing_profile} from "./gur_user_boxing_profile";
import {gur_user_boxing_stats} from "./gur_user_boxing_stats";
import {gur_user_canadian_football_profile} from "./gur_user_canadian_football_profile";
import {gur_user_canoe_kayak_profile} from "./gur_user_canoe_kayak_profile";
import {gur_user_carrom_profile} from "./gur_user_carrom_profile";
import {gur_user_chess_profile} from "./gur_user_chess_profile";
import {gur_user_chess_stats} from "./gur_user_chess_stats";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_user_cricket_profile} from "./gur_user_cricket_profile";
import {gur_user_cricket_stats} from "./gur_user_cricket_stats";
import {gur_user_croquet_profile} from "./gur_user_croquet_profile";
import {gur_user_cross_country_skiing_profile} from "./gur_user_cross_country_skiing_profile";
import {gur_user_curling_profile} from "./gur_user_curling_profile";
import {gur_user_cycling_profile} from "./gur_user_cycling_profile";
import {gur_user_cycling_stats} from "./gur_user_cycling_stats";
import {gur_user_darts_profile} from "./gur_user_darts_profile";
import {gur_user_diving_profile} from "./gur_user_diving_profile";
import {gur_user_diving_stats} from "./gur_user_diving_stats";
import {gur_user_dodgeball_profile} from "./gur_user_dodgeball_profile";
import {gur_user_equestrian_profile} from "./gur_user_equestrian_profile";
import {gur_user_eton_fives_profile} from "./gur_user_eton_fives_profile";
import {gur_user_fencing_profile} from "./gur_user_fencing_profile";
import {gur_user_field_hockey_profile} from "./gur_user_field_hockey_profile";
import {gur_user_field_hockey_stats} from "./gur_user_field_hockey_stats";
import {gur_user_figure_skating_profile} from "./gur_user_figure_skating_profile";
import {gur_user_figure_skating_stats} from "./gur_user_figure_skating_stats";
import {gur_user_floorball_profile} from "./gur_user_floorball_profile";
import {gur_user_formula_four_profile} from "./gur_user_formula_four_profile";
import {gur_user_formula_indy_profile} from "./gur_user_formula_indy_profile";
import {gur_user_formula_one_profile} from "./gur_user_formula_one_profile";
import {gur_user_formula_three_profile} from "./gur_user_formula_three_profile";
import {gur_user_freestyle_football_profile} from "./gur_user_freestyle_football_profile";
import {gur_user_freestyle_skiing_profile} from "./gur_user_freestyle_skiing_profile";
import {gur_user_futsal_profile} from "./gur_user_futsal_profile";
import {gur_user_gaelic_football_profile} from "./gur_user_gaelic_football_profile";
import {gur_user_gliding_profile} from "./gur_user_gliding_profile";
import {gur_user_golf_profile} from "./gur_user_golf_profile";
import {gur_user_golf_stats} from "./gur_user_golf_stats";
import {gur_user_gymnastics_profile} from "./gur_user_gymnastics_profile";
import {gur_user_gymnastics_stats} from "./gur_user_gymnastics_stats";
import {gur_user_hang_gliding_profile} from "./gur_user_hang_gliding_profile";
import {gur_user_horseball_profile} from "./gur_user_horseball_profile";
import {gur_user_horseshoe_pitching_profile} from "./gur_user_horseshoe_pitching_profile";
import {gur_user_hurling_profile} from "./gur_user_hurling_profile";
import {gur_user_ice_hockey_profile} from "./gur_user_ice_hockey_profile";
import {gur_user_inline_alpine_skating_profile} from "./gur_user_inline_alpine_skating_profile";
import {gur_user_inline_downhill_skating_profile} from "./gur_user_inline_downhill_skating_profile";
import {gur_user_inline_freestyle_skating_profile} from "./gur_user_inline_freestyle_skating_profile";
import {gur_user_inline_skating_profile} from "./gur_user_inline_skating_profile";
import {gur_user_jianzi_profile} from "./gur_user_jianzi_profile";
import {gur_user_judo_profile} from "./gur_user_judo_profile";
import {gur_user_judo_stats} from "./gur_user_judo_stats";
import {gur_user_jujitsu_profile} from "./gur_user_jujitsu_profile";
import {gur_user_kabaddi_profile} from "./gur_user_kabaddi_profile";
import {gur_user_kabaddi_stats} from "./gur_user_kabaddi_stats";
import {gur_user_karate_profile} from "./gur_user_karate_profile";
import {gur_user_karate_stats} from "./gur_user_karate_stats";
import {gur_user_karting_profile} from "./gur_user_karting_profile";
import {gur_user_kickboxing_profile} from "./gur_user_kickboxing_profile";
import {gur_user_kiteboarding_profile} from "./gur_user_kiteboarding_profile";
import {gur_user_korfball_profile} from "./gur_user_korfball_profile";
import {gur_user_krav_maga_profile} from "./gur_user_krav_maga_profile";
import {gur_user_lacrosse_profile} from "./gur_user_lacrosse_profile";
import {gur_user_lawn_bowling_profile} from "./gur_user_lawn_bowling_profile";
import {gur_user_leg_cricket_profile} from "./gur_user_leg_cricket_profile";
import {gur_user_luge_profile} from "./gur_user_luge_profile";
import {gur_user_media_gallery} from "./gur_user_media_gallery";
import {gur_user_modern_pentathlon_profile} from "./gur_user_modern_pentathlon_profile";
import {gur_user_modern_pentathlon_stats} from "./gur_user_modern_pentathlon_stats";
import {gur_user_motogp_profile} from "./gur_user_motogp_profile";
import {gur_user_netball_profile} from "./gur_user_netball_profile";
import {gur_user_netball_stats} from "./gur_user_netball_stats";
import {gur_user_openwater_swimming_profile} from "./gur_user_openwater_swimming_profile";
import {gur_user_orienteering_profile} from "./gur_user_orienteering_profile";
import {gur_user_paddleball_profile} from "./gur_user_paddleball_profile";
import {gur_user_para_alpine_skiing_profile} from "./gur_user_para_alpine_skiing_profile";
import {gur_user_para_archery_profile} from "./gur_user_para_archery_profile";
import {gur_user_para_athletics_profile} from "./gur_user_para_athletics_profile";
import {gur_user_para_badminton_profile} from "./gur_user_para_badminton_profile";
import {gur_user_para_beach_volleyball_profile} from "./gur_user_para_beach_volleyball_profile";
import {gur_user_para_biathlon_profile} from "./gur_user_para_biathlon_profile";
import {gur_user_para_boccia_profile} from "./gur_user_para_boccia_profile";
import {gur_user_para_canoe_profile} from "./gur_user_para_canoe_profile";
import {gur_user_para_cross_country_skiing_profile} from "./gur_user_para_cross_country_skiing_profile";
import {gur_user_para_curling_profile} from "./gur_user_para_curling_profile";
import {gur_user_para_cycling_profile} from "./gur_user_para_cycling_profile";
import {gur_user_para_dance_profile} from "./gur_user_para_dance_profile";
import {gur_user_para_equestrian_profile} from "./gur_user_para_equestrian_profile";
import {gur_user_para_goalball_profile} from "./gur_user_para_goalball_profile";
import {gur_user_para_ice_sledge_hockey_profile} from "./gur_user_para_ice_sledge_hockey_profile";
import {gur_user_para_judo_profile} from "./gur_user_para_judo_profile";
import {gur_user_para_powerlifting_profile} from "./gur_user_para_powerlifting_profile";
import {gur_user_para_rowing_profile} from "./gur_user_para_rowing_profile";
import {gur_user_para_sailing_profile} from "./gur_user_para_sailing_profile";
import {gur_user_para_shooting_profile} from "./gur_user_para_shooting_profile";
import {gur_user_para_snowboarding_profile} from "./gur_user_para_snowboarding_profile";
import {gur_user_para_soccer_profile} from "./gur_user_para_soccer_profile";
import {gur_user_para_swimming_profile} from "./gur_user_para_swimming_profile";
import {gur_user_para_table_tennis_profile} from "./gur_user_para_table_tennis_profile";
import {gur_user_para_taekwondo_profile} from "./gur_user_para_taekwondo_profile";
import {gur_user_para_triathlon_profile} from "./gur_user_para_triathlon_profile";
import {gur_user_para_volleyball_profile} from "./gur_user_para_volleyball_profile";
import {gur_user_para_wheelchair_basketball_profile} from "./gur_user_para_wheelchair_basketball_profile";
import {gur_user_para_wheelchair_fencing_profile} from "./gur_user_para_wheelchair_fencing_profile";
import {gur_user_para_wheelchair_rugby_profile} from "./gur_user_para_wheelchair_rugby_profile";
import {gur_user_para_wheelchair_tennis_profile} from "./gur_user_para_wheelchair_tennis_profile";
import {gur_user_paragliding_profile} from "./gur_user_paragliding_profile";
import {gur_user_pesapallo_profile} from "./gur_user_pesapallo_profile";
import {gur_user_petanque_profile} from "./gur_user_petanque_profile";
import {gur_user_pickleball_profile} from "./gur_user_pickleball_profile";
import {gur_user_polo_profile} from "./gur_user_polo_profile";
import {gur_user_pool_profile} from "./gur_user_pool_profile";
import {gur_user_powerboat_racing_profile} from "./gur_user_powerboat_racing_profile";
import {gur_user_powerlifting_profile} from "./gur_user_powerlifting_profile";
import {gur_user_powerlifting_stats} from "./gur_user_powerlifting_stats";
import {gur_user_profile_views} from "./gur_user_profile_views";
import {gur_user_qualifications} from "./gur_user_qualifications";
import {gur_user_racquetball_profile} from "./gur_user_racquetball_profile";
import {gur_user_rafting_profile} from "./gur_user_rafting_profile";
import {gur_user_real_tennis_profile} from "./gur_user_real_tennis_profile";
import {gur_user_rock_climbing_profile} from "./gur_user_rock_climbing_profile";
import {gur_user_rodeo_profile} from "./gur_user_rodeo_profile";
import {gur_user_roller_derby_profile} from "./gur_user_roller_derby_profile";
import {gur_user_roller_hockey_inline_profile} from "./gur_user_roller_hockey_inline_profile";
import {gur_user_roller_hockey_quad_profile} from "./gur_user_roller_hockey_quad_profile";
import {gur_user_rope_skipping_profile} from "./gur_user_rope_skipping_profile";
import {gur_user_rowing_profile} from "./gur_user_rowing_profile";
import {gur_user_rowing_stats} from "./gur_user_rowing_stats";
import {gur_user_rubik_profile} from "./gur_user_rubik_profile";
import {gur_user_rugby_league_profile} from "./gur_user_rugby_league_profile";
import {gur_user_rugby_sevens_profile} from "./gur_user_rugby_sevens_profile";
import {gur_user_rugby_sevens_stats} from "./gur_user_rugby_sevens_stats";
import {gur_user_rugby_union_profile} from "./gur_user_rugby_union_profile";
import {gur_user_rugby_union_stats} from "./gur_user_rugby_union_stats";
import {gur_user_sailing_profile} from "./gur_user_sailing_profile";
import {gur_user_sambo_profile} from "./gur_user_sambo_profile";
import {gur_user_sepak_takraw_profile} from "./gur_user_sepak_takraw_profile";
import {gur_user_shinty_profile} from "./gur_user_shinty_profile";
import {gur_user_shooting_profile} from "./gur_user_shooting_profile";
import {gur_user_shooting_stats} from "./gur_user_shooting_stats";
import {gur_user_skateboarding_profile} from "./gur_user_skateboarding_profile";
import {gur_user_skeleton_profile} from "./gur_user_skeleton_profile";
import {gur_user_ski_flying_profile} from "./gur_user_ski_flying_profile";
import {gur_user_ski_jumping_profile} from "./gur_user_ski_jumping_profile";
import {gur_user_skydiving_profile} from "./gur_user_skydiving_profile";
import {gur_user_snooker_profile} from "./gur_user_snooker_profile";
import {gur_user_snowboarding_profile} from "./gur_user_snowboarding_profile";
import {gur_user_snowmobiling_profile} from "./gur_user_snowmobiling_profile";
import {gur_user_soccer_profile} from "./gur_user_soccer_profile";
import {gur_user_soccer_stats} from "./gur_user_soccer_stats";
import {gur_user_soft_tennis_profile} from "./gur_user_soft_tennis_profile";
import {gur_user_softball_profile} from "./gur_user_softball_profile";
import {gur_user_speed_skating_profile} from "./gur_user_speed_skating_profile";
import {gur_user_speed_skiing_profile} from "./gur_user_speed_skiing_profile";
import {gur_user_sports_map} from "./gur_user_sports_map";
import {gur_user_squash_profile} from "./gur_user_squash_profile";
import {gur_user_squash_stats} from "./gur_user_squash_stats";
import {gur_user_sumo_wrestling_profile} from "./gur_user_sumo_wrestling_profile";
import {gur_user_surfing_profile} from "./gur_user_surfing_profile";
import {gur_user_swimming_profile} from "./gur_user_swimming_profile";
import {gur_user_swimming_stats} from "./gur_user_swimming_stats";
import {gur_user_synchronised_swimming_profile} from "./gur_user_synchronised_swimming_profile";
import {gur_user_table_tennis_profile} from "./gur_user_table_tennis_profile";
import {gur_user_table_tennis_stats} from "./gur_user_table_tennis_stats";
import {gur_user_taekwondo_profile} from "./gur_user_taekwondo_profile";
import {gur_user_taekwondo_stats} from "./gur_user_taekwondo_stats";
import {gur_user_team_handball_profile} from "./gur_user_team_handball_profile";
import {gur_user_ten_pin_bowling_profile} from "./gur_user_ten_pin_bowling_profile";
import {gur_user_tennis_profile} from "./gur_user_tennis_profile";
import {gur_user_tennis_stats} from "./gur_user_tennis_stats";
import {gur_user_touringcar_profile} from "./gur_user_touringcar_profile";
import {gur_user_triathlon_profile} from "./gur_user_triathlon_profile";
import {gur_user_triathlon_stats} from "./gur_user_triathlon_stats";
import {gur_user_tug_of_war_profile} from "./gur_user_tug_of_war_profile";
import {gur_user_ultimate_profile} from "./gur_user_ultimate_profile";
import {gur_user_ultra_running_profile} from "./gur_user_ultra_running_profile";
import {gur_user_volleyball_profile} from "./gur_user_volleyball_profile";
import {gur_user_volleyball_stats} from "./gur_user_volleyball_stats";
import {gur_user_water_polo_profile} from "./gur_user_water_polo_profile";
import {gur_user_water_polo_stats} from "./gur_user_water_polo_stats";
import {gur_user_water_skiing_profile} from "./gur_user_water_skiing_profile";
import {gur_user_weightlifting_profile} from "./gur_user_weightlifting_profile";
import {gur_user_weightlifting_stats} from "./gur_user_weightlifting_stats";
import {gur_user_wheelchair_cricket_profile} from "./gur_user_wheelchair_cricket_profile";
import {gur_user_windsurfing_profile} from "./gur_user_windsurfing_profile";
import {gur_user_work_details} from "./gur_user_work_details";
import {gur_user_wrestling_profile} from "./gur_user_wrestling_profile";
import {gur_user_wrestling_stats} from "./gur_user_wrestling_stats";
import {gur_user_wushu_profile} from "./gur_user_wushu_profile";
import {gur_user_yoyo_profile} from "./gur_user_yoyo_profile";
import {gur_users_contact_view_log} from "./gur_users_contact_view_log";
import {gur_users_permissions} from "./gur_users_permissions";


@Entity("gur_users" ,{schema:"sportsmatik_local" } )
@Index("usr_slug",["usr_slug","usrType",],{unique:true})
@Index("usr_country",["usrCountry",])
@Index("usr_type",["usrType",])
@Index("usr_dis_id",["usrDis",])
@Index("user_referred_by",["user_referred_by",])
@Index("user_referred_by_type",["userReferredByType",])
@Index("usr_aff_code",["usr_aff_code",])
@Index("usr_sts_id",["usrSts",])
@Index("usr_salutation",["usrSalutation",])
@Index("usr_email",["usr_email_old",])
@Index("usr_fname",["usr_fnam_old",])
export class gur_users {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"usr_id"
        })
    usr_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_slug"
        })
    usr_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_pin"
        })
    usr_pin:string | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usr_type'})
    usrType:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_additional"
        })
    usr_additional:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"user_referred_by"
        })
    user_referred_by:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_referred_by_type'})
    userReferredByType:gur_user_types | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"usr_aff_code"
        })
    usr_aff_code:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usr_salutation'})
    usrSalutation:gur_value_list | null;


    @Column("varchar",{ 
        nullable:true,
        name:"usr_fnam_old"
        })
    usr_fnam_old:string | null;
        

    @Column("varbinary",{ 
        nullable:false,
        length:100,
        name:"usr_fname"
        })
    usr_fname:Buffer;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_mname_old"
        })
    usr_mname_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"usr_mname"
        })
    usr_mname:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_lname_old"
        })
    usr_lname_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"usr_lname"
        })
    usr_lname:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_screen_name"
        })
    usr_screen_name:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","o"],
        name:"usr_sex"
        })
    usr_sex:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"usr_birthday_old"
        })
    usr_birthday_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"usr_birthday"
        })
    usr_birthday:Buffer | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_differently_abled"
        })
    usr_differently_abled:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_email_old"
        })
    usr_email_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        length:100,
        name:"usr_email"
        })
    usr_email:Buffer | null;
        

    @Column("text",{ 
        nullable:true,
        name:"usr_password"
        })
    usr_password:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_cookie_selector"
        })
    usr_cookie_selector:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_cookie_verifier"
        })
    usr_cookie_verifier:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"usr_email_verified"
        })
    usr_email_verified:boolean | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_email_verify_time"
        })
    usr_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"usr_phone1_old"
        })
    usr_phone1_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        name:"usr_phone1"
        })
    usr_phone1:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_phone1_otp"
        })
    usr_phone1_otp:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_phone1_otp_time"
        })
    usr_phone1_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"usr_phone1_verified"
        })
    usr_phone1_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"usr_phone2_old"
        })
    usr_phone2_old:string | null;
        

    @Column("varbinary",{ 
        nullable:true,
        name:"usr_phone2"
        })
    usr_phone2:Buffer | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_phone2_otp"
        })
    usr_phone2_otp:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_phone2_otp_time"
        })
    usr_phone2_otp_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"usr_phone2_verified"
        })
    usr_phone2_verified:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_address"
        })
    usr_address:string | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"usr_height"
        })
    usr_height:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"usr_weight"
        })
    usr_weight:number | null;
        

   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurUserss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usr_sts_id'})
    usrSts:gur_states | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurUserss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usr_dis_id'})
    usrDis:gur_districts | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurUserss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'usr_country'})
    usrCountry:gur_countries | null;


    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_pincode"
        })
    usr_pincode:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"usr_profile_summary"
        })
    usr_profile_summary:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Unverified'",
        enum:["Active","Inactive","Deleted","Unverified","Deactivated"],
        name:"usr_status"
        })
    usr_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_profile_pic"
        })
    usr_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_timeline_pic"
        })
    usr_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_intro_status"
        })
    usr_intro_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_view_contact_person"
        })
    usr_view_contact_person:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_last_login_date"
        })
    usr_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"usr_last_login_ip"
        })
    usr_last_login_ip:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_register_date"
        })
    usr_register_date:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"usr_intro_modifiedtime"
        })
    usr_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_profile_pic_modifiedtime"
        })
    usr_profile_pic_modifiedtime:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"usr_password_reset_pin"
        })
    usr_password_reset_pin:string | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"usr_password_reset_time"
        })
    usr_password_reset_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"usr_free_sports"
        })
    usr_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"usr_free_sub_users"
        })
    usr_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_consent_form"
        })
    usr_consent_form:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_viewable"
        })
    usr_viewable:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_google_id"
        })
    usr_google_id:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"usr_dob_change"
        })
    usr_dob_change:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_createdby"
        })
    usr_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"usr_modifiedby"
        })
    usr_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"usr_modifiedtime"
        })
    usr_modifiedtime:Date;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"usr_nationality"
        })
    usr_nationality:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"usr_team"
        })
    usr_team:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_invited"
        })
    usr_invited:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"usr_is_gdpr_notify"
        })
    usr_is_gdpr_notify:boolean;
        

   
    @OneToMany(()=>gur_academies, (gur_academies: gur_academies)=>gur_academies.acaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAcademiess:gur_academies[];
    

   
    @OneToMany(()=>gur_academies, (gur_academies: gur_academies)=>gur_academies.acaUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAcademiess2:gur_academies[];
    

   
    @OneToMany(()=>gur_blocked_users, (gur_blocked_users: gur_blocked_users)=>gur_blocked_users.blkCreatedby,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlockedUserss:gur_blocked_users[];
    

   
    @OneToMany(()=>gur_clubs, (gur_clubs: gur_clubs)=>gur_clubs.clbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurClubss:gur_clubs[];
    

   
    @OneToMany(()=>gur_clubs, (gur_clubs: gur_clubs)=>gur_clubs.clbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurClubss2:gur_clubs[];
    

   
    @OneToMany(()=>gur_colleges, (gur_colleges: gur_colleges)=>gur_colleges.clgUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCollegess:gur_colleges[];
    

   
    @OneToMany(()=>gur_colleges, (gur_colleges: gur_colleges)=>gur_colleges.clgCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCollegess2:gur_colleges[];
    

   
    @OneToMany(()=>gur_competition_sports_map, (gur_competition_sports_map: gur_competition_sports_map)=>gur_competition_sports_map.csmCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionSportsMaps:gur_competition_sports_map[];
    

   
    @OneToMany(()=>gur_competitions, (gur_competitions: gur_competitions)=>gur_competitions.comCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionss:gur_competitions[];
    

   
    @OneToMany(()=>gur_contact_view_history, (gur_contact_view_history: gur_contact_view_history)=>gur_contact_view_history.chrByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurContactViewHistorys:gur_contact_view_history[];
    

   
    @OneToMany(()=>gur_corporates, (gur_corporates: gur_corporates)=>gur_corporates.corUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatess:gur_corporates[];
    

   
    @OneToMany(()=>gur_corporates, (gur_corporates: gur_corporates)=>gur_corporates.corCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatess2:gur_corporates[];
    

   
    @OneToMany(()=>gur_educational_courses, (gur_educational_courses: gur_educational_courses)=>gur_educational_courses.corCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurEducationalCoursess:gur_educational_courses[];
    

   
    @OneToMany(()=>gur_educational_courses_specialization, (gur_educational_courses_specialization: gur_educational_courses_specialization)=>gur_educational_courses_specialization.cspCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEducationalCoursesSpecializations:gur_educational_courses_specialization[];
    

   
    @OneToMany(()=>gur_educational_institutions, (gur_educational_institutions: gur_educational_institutions)=>gur_educational_institutions.insCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEducationalInstitutionss:gur_educational_institutions[];
    

   
    @OneToMany(()=>gur_events, (gur_events: gur_events)=>gur_events.evtCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEventss:gur_events[];
    

   
    @OneToMany(()=>gur_facilities, (gur_facilities: gur_facilities)=>gur_facilities.facCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFacilitiess:gur_facilities[];
    

   
    @OneToMany(()=>gur_fitness_center, (gur_fitness_center: gur_fitness_center)=>gur_fitness_center.fitUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenters:gur_fitness_center[];
    

   
    @OneToMany(()=>gur_fitness_center, (gur_fitness_center: gur_fitness_center)=>gur_fitness_center.fitCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenters2:gur_fitness_center[];
    

   
    @OneToMany(()=>gur_fitness_center_services, (gur_fitness_center_services: gur_fitness_center_services)=>gur_fitness_center_services.fcsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFitnessCenterServicess:gur_fitness_center_services[];
    

   
    @OneToMany(()=>gur_hours_of_operation, (gur_hours_of_operation: gur_hours_of_operation)=>gur_hours_of_operation.hopCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHoursOfOperations:gur_hours_of_operation[];
    

   
    @OneToMany(()=>gur_infra_portfolio, (gur_infra_portfolio: gur_infra_portfolio)=>gur_infra_portfolio.ipoCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInfraPortfolios:gur_infra_portfolio[];
    

   
    @OneToMany(()=>gur_infra_user_products, (gur_infra_user_products: gur_infra_user_products)=>gur_infra_user_products.iupCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInfraUserProductss:gur_infra_user_products[];
    

   
    @OneToMany(()=>gur_institute_branches, (gur_institute_branches: gur_institute_branches)=>gur_institute_branches.ibrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteBranchess:gur_institute_branches[];
    

   
    @OneToMany(()=>gur_institute_course_and_fees, (gur_institute_course_and_fees: gur_institute_course_and_fees)=>gur_institute_course_and_fees.icfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstituteCourseAndFeess:gur_institute_course_and_fees[];
    

   
    @OneToMany(()=>gur_institutional_staff, (gur_institutional_staff: gur_institutional_staff)=>gur_institutional_staff.itsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalStaffs:gur_institutional_staff[];
    

   
    @OneToMany(()=>gur_institutional_teams, (gur_institutional_teams: gur_institutional_teams)=>gur_institutional_teams.itnCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTeamss:gur_institutional_teams[];
    

   
    @OneToMany(()=>gur_institutional_teams_contact_usage, (gur_institutional_teams_contact_usage: gur_institutional_teams_contact_usage)=>gur_institutional_teams_contact_usage.tcuSendByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTeamsContactUsages:gur_institutional_teams_contact_usage[];
    

   
    @OneToMany(()=>gur_institutional_trophies, (gur_institutional_trophies: gur_institutional_trophies)=>gur_institutional_trophies.intCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTrophiess:gur_institutional_trophies[];
    

   
    @OneToMany(()=>gur_journalist_writer_portfolio, (gur_journalist_writer_portfolio: gur_journalist_writer_portfolio)=>gur_journalist_writer_portfolio.jwpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJournalistWriterPortfolios:gur_journalist_writer_portfolio[];
    

   
    @OneToMany(()=>gur_law_ca_services_categories, (gur_law_ca_services_categories: gur_law_ca_services_categories)=>gur_law_ca_services_categories.lcsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawCaServicesCategoriess:gur_law_ca_services_categories[];
    

   
    @OneToMany(()=>gur_lawyer_ca_services, (gur_lawyer_ca_services: gur_lawyer_ca_services)=>gur_lawyer_ca_services.lwcCreatedByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawyerCaServicess:gur_lawyer_ca_services[];
    

   
    @OneToMany(()=>gur_lawyer_professional_details, (gur_lawyer_professional_details: gur_lawyer_professional_details)=>gur_lawyer_professional_details.lpfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawyerProfessionalDetailss:gur_lawyer_professional_details[];
    

   
    @OneToMany(()=>gur_logistics_services, (gur_logistics_services: gur_logistics_services)=>gur_logistics_services.lgsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLogisticsServicess:gur_logistics_services[];
    

   
    @OneToMany(()=>gur_managed_portfolio, (gur_managed_portfolio: gur_managed_portfolio)=>gur_managed_portfolio.mhtCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurManagedPortfolios:gur_managed_portfolio[];
    

   
    @OneToMany(()=>gur_matik_ads, (gur_matik_ads: gur_matik_ads)=>gur_matik_ads.adsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikAdss:gur_matik_ads[];
    

   
    @OneToMany(()=>gur_matik_tags, (gur_matik_tags: gur_matik_tags)=>gur_matik_tags.mtgByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikTagss:gur_matik_tags[];
    

   
    @OneToMany(()=>gur_membership_fees, (gur_membership_fees: gur_membership_fees)=>gur_membership_fees.fmfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMembershipFeess:gur_membership_fees[];
    

   
    @OneToMany(()=>gur_plan_usage_log, (gur_plan_usage_log: gur_plan_usage_log)=>gur_plan_usage_log.pulByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanUsageLogs:gur_plan_usage_log[];
    

   
    @OneToMany(()=>gur_recruitment_history, (gur_recruitment_history: gur_recruitment_history)=>gur_recruitment_history.rehCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRecruitmentHistorys:gur_recruitment_history[];
    

   
    @OneToMany(()=>gur_schools, (gur_schools: gur_schools)=>gur_schools.schUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSchoolss:gur_schools[];
    

   
    @OneToMany(()=>gur_schools, (gur_schools: gur_schools)=>gur_schools.schCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSchoolss2:gur_schools[];
    

   
    @OneToMany(()=>gur_sponsors, (gur_sponsors: gur_sponsors)=>gur_sponsors.spnUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSponsorss:gur_sponsors[];
    

   
    @OneToMany(()=>gur_sports_attributes, (gur_sports_attributes: gur_sports_attributes)=>gur_sports_attributes.sabCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsAttributess:gur_sports_attributes[];
    

   
    @OneToMany(()=>gur_sports_events_company, (gur_sports_events_company: gur_sports_events_company)=>gur_sports_events_company.secUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsEventsCompanys:gur_sports_events_company[];
    

   
    @OneToMany(()=>gur_sports_governing_body, (gur_sports_governing_body: gur_sports_governing_body)=>gur_sports_governing_body.sgbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodys:gur_sports_governing_body[];
    

   
    @OneToMany(()=>gur_sports_governing_body, (gur_sports_governing_body: gur_sports_governing_body)=>gur_sports_governing_body.sgbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsGoverningBodys2:gur_sports_governing_body[];
    

   
    @OneToMany(()=>gur_sports_infrastructure_company, (gur_sports_infrastructure_company: gur_sports_infrastructure_company)=>gur_sports_infrastructure_company.sicUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsInfrastructureCompanys:gur_sports_infrastructure_company[];
    

   
    @OneToMany(()=>gur_sports_infrastructure_company, (gur_sports_infrastructure_company: gur_sports_infrastructure_company)=>gur_sports_infrastructure_company.sicCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsInfrastructureCompanys2:gur_sports_infrastructure_company[];
    

   
    @OneToMany(()=>gur_sports_logistics, (gur_sports_logistics: gur_sports_logistics)=>gur_sports_logistics.slgUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticss:gur_sports_logistics[];
    

   
    @OneToMany(()=>gur_sports_logistics_venues, (gur_sports_logistics_venues: gur_sports_logistics_venues)=>gur_sports_logistics_venues.lvnCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLogisticsVenuess:gur_sports_logistics_venues[];
    

   
    @OneToMany(()=>gur_sports_marketing_company, (gur_sports_marketing_company: gur_sports_marketing_company)=>gur_sports_marketing_company.smcUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsMarketingCompanys:gur_sports_marketing_company[];
    

   
    @OneToMany(()=>gur_sports_marketing_company, (gur_sports_marketing_company: gur_sports_marketing_company)=>gur_sports_marketing_company.smcCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsMarketingCompanys2:gur_sports_marketing_company[];
    

   
    @OneToMany(()=>gur_sports_official_types, (gur_sports_official_types: gur_sports_official_types)=>gur_sports_official_types.sotCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialTypess:gur_sports_official_types[];
    

   
    @OneToMany(()=>gur_sports_official_user_profile, (gur_sports_official_user_profile: gur_sports_official_user_profile)=>gur_sports_official_user_profile.somUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialUserProfiles:gur_sports_official_user_profile[];
    

   
    @OneToMany(()=>gur_sports_official_user_profile, (gur_sports_official_user_profile: gur_sports_official_user_profile)=>gur_sports_official_user_profile.somCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialUserProfiles2:gur_sports_official_user_profile[];
    

   
    @OneToMany(()=>gur_sports_tech_company, (gur_sports_tech_company: gur_sports_tech_company)=>gur_sports_tech_company.stcUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsTechCompanys:gur_sports_tech_company[];
    

   
    @OneToMany(()=>gur_sports_vacancies_applicants, (gur_sports_vacancies_applicants: gur_sports_vacancies_applicants)=>gur_sports_vacancies_applicants.upaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciesApplicantss:gur_sports_vacancies_applicants[];
    

   
    @OneToMany(()=>gur_squads_group, (gur_squads_group: gur_squads_group)=>gur_squads_group.sqdCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSquadsGroups:gur_squads_group[];
    

   
    @OneToMany(()=>gur_squads_list, (gur_squads_list: gur_squads_list)=>gur_squads_list.sqlUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSquadsLists:gur_squads_list[];
    

   
    @OneToMany(()=>gur_sub_users, (gur_sub_users: gur_sub_users)=>gur_sub_users.msuSubUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSubUserss:gur_sub_users[];
    

   
    @OneToMany(()=>gur_subuser_usage_history, (gur_subuser_usage_history: gur_subuser_usage_history)=>gur_subuser_usage_history.suhSubuser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSubuserUsageHistorys:gur_subuser_usage_history[];
    

   
    @OneToMany(()=>gur_survey_answers, (gur_survey_answers: gur_survey_answers)=>gur_survey_answers.sanUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSurveyAnswerss:gur_survey_answers[];
    

   
    @OneToMany(()=>gur_teams, (gur_teams: gur_teams)=>gur_teams.tmsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTeamss:gur_teams[];
    

   
    @OneToMany(()=>gur_training_camp_users_map, (gur_training_camp_users_map: gur_training_camp_users_map)=>gur_training_camp_users_map.tcuUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTrainingCampUsersMaps:gur_training_camp_users_map[];
    

   
    @OneToMany(()=>gur_training_camp_users_map, (gur_training_camp_users_map: gur_training_camp_users_map)=>gur_training_camp_users_map.tcuCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurTrainingCampUsersMaps2:gur_training_camp_users_map[];
    

   
    @OneToMany(()=>gur_user_aikido_profile, (gur_user_aikido_profile: gur_user_aikido_profile)=>gur_user_aikido_profile.aikUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAikidoProfiles:gur_user_aikido_profile[];
    

   
    @OneToMany(()=>gur_user_aikido_profile, (gur_user_aikido_profile: gur_user_aikido_profile)=>gur_user_aikido_profile.aikCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAikidoProfiles2:gur_user_aikido_profile[];
    

   
    @OneToMany(()=>gur_user_air_racing_profile, (gur_user_air_racing_profile: gur_user_air_racing_profile)=>gur_user_air_racing_profile.airUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAirRacingProfiles:gur_user_air_racing_profile[];
    

   
    @OneToMany(()=>gur_user_air_racing_profile, (gur_user_air_racing_profile: gur_user_air_racing_profile)=>gur_user_air_racing_profile.airCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAirRacingProfiles2:gur_user_air_racing_profile[];
    

   
    @OneToMany(()=>gur_user_albums, (gur_user_albums: gur_user_albums)=>gur_user_albums.albCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAlbumss:gur_user_albums[];
    

   
    @OneToMany(()=>gur_user_alpine_skiing_profile, (gur_user_alpine_skiing_profile: gur_user_alpine_skiing_profile)=>gur_user_alpine_skiing_profile.alpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAlpineSkiingProfiles:gur_user_alpine_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_alpine_skiing_profile, (gur_user_alpine_skiing_profile: gur_user_alpine_skiing_profile)=>gur_user_alpine_skiing_profile.alpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAlpineSkiingProfiles2:gur_user_alpine_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_american_handball_profile, (gur_user_american_handball_profile: gur_user_american_handball_profile)=>gur_user_american_handball_profile.ahbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAmericanHandballProfiles:gur_user_american_handball_profile[];
    

   
    @OneToMany(()=>gur_user_american_handball_profile, (gur_user_american_handball_profile: gur_user_american_handball_profile)=>gur_user_american_handball_profile.ahbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAmericanHandballProfiles2:gur_user_american_handball_profile[];
    

   
    @OneToMany(()=>gur_user_american_rules_football_profile, (gur_user_american_rules_football_profile: gur_user_american_rules_football_profile)=>gur_user_american_rules_football_profile.arfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAmericanRulesFootballProfiles:gur_user_american_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_american_rules_football_profile, (gur_user_american_rules_football_profile: gur_user_american_rules_football_profile)=>gur_user_american_rules_football_profile.arfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAmericanRulesFootballProfiles2:gur_user_american_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_archery_profile, (gur_user_archery_profile: gur_user_archery_profile)=>gur_user_archery_profile.arcUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArcheryProfiles:gur_user_archery_profile[];
    

   
    @OneToMany(()=>gur_user_archery_profile, (gur_user_archery_profile: gur_user_archery_profile)=>gur_user_archery_profile.arcCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArcheryProfiles2:gur_user_archery_profile[];
    

   
    @OneToMany(()=>gur_user_archery_stats, (gur_user_archery_stats: gur_user_archery_stats)=>gur_user_archery_stats.arsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArcheryStatss:gur_user_archery_stats[];
    

   
    @OneToMany(()=>gur_user_artistic_roller_skating_profile, (gur_user_artistic_roller_skating_profile: gur_user_artistic_roller_skating_profile)=>gur_user_artistic_roller_skating_profile.arrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArtisticRollerSkatingProfiles:gur_user_artistic_roller_skating_profile[];
    

   
    @OneToMany(()=>gur_user_artistic_roller_skating_profile, (gur_user_artistic_roller_skating_profile: gur_user_artistic_roller_skating_profile)=>gur_user_artistic_roller_skating_profile.arrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArtisticRollerSkatingProfiles2:gur_user_artistic_roller_skating_profile[];
    

   
    @OneToMany(()=>gur_user_athletics_profile, (gur_user_athletics_profile: gur_user_athletics_profile)=>gur_user_athletics_profile.uapUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAthleticsProfiles:gur_user_athletics_profile[];
    

   
    @OneToMany(()=>gur_user_athletics_profile, (gur_user_athletics_profile: gur_user_athletics_profile)=>gur_user_athletics_profile.uapCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAthleticsProfiles2:gur_user_athletics_profile[];
    

   
    @OneToMany(()=>gur_user_athletics_stats, (gur_user_athletics_stats: gur_user_athletics_stats)=>gur_user_athletics_stats.atsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAthleticsStatss:gur_user_athletics_stats[];
    

   
    @OneToMany(()=>gur_user_australian_rules_football_profile, (gur_user_australian_rules_football_profile: gur_user_australian_rules_football_profile)=>gur_user_australian_rules_football_profile.arfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAustralianRulesFootballProfiles:gur_user_australian_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_australian_rules_football_profile, (gur_user_australian_rules_football_profile: gur_user_australian_rules_football_profile)=>gur_user_australian_rules_football_profile.arfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAustralianRulesFootballProfiles2:gur_user_australian_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_awards_achievements, (gur_user_awards_achievements: gur_user_awards_achievements)=>gur_user_awards_achievements.uaaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAwardsAchievementss:gur_user_awards_achievements[];
    

   
    @OneToMany(()=>gur_user_badminton_profile, (gur_user_badminton_profile: gur_user_badminton_profile)=>gur_user_badminton_profile.badUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBadmintonProfiles:gur_user_badminton_profile[];
    

   
    @OneToMany(()=>gur_user_badminton_profile, (gur_user_badminton_profile: gur_user_badminton_profile)=>gur_user_badminton_profile.badCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBadmintonProfiles2:gur_user_badminton_profile[];
    

   
    @OneToMany(()=>gur_user_badminton_stats, (gur_user_badminton_stats: gur_user_badminton_stats)=>gur_user_badminton_stats.bdsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBadmintonStatss:gur_user_badminton_stats[];
    

   
    @OneToMany(()=>gur_user_bandy_profile, (gur_user_bandy_profile: gur_user_bandy_profile)=>gur_user_bandy_profile.banUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBandyProfiles:gur_user_bandy_profile[];
    

   
    @OneToMany(()=>gur_user_bandy_profile, (gur_user_bandy_profile: gur_user_bandy_profile)=>gur_user_bandy_profile.banCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBandyProfiles2:gur_user_bandy_profile[];
    

   
    @OneToMany(()=>gur_user_baseball_profile, (gur_user_baseball_profile: gur_user_baseball_profile)=>gur_user_baseball_profile.basUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBaseballProfiles:gur_user_baseball_profile[];
    

   
    @OneToMany(()=>gur_user_baseball_profile, (gur_user_baseball_profile: gur_user_baseball_profile)=>gur_user_baseball_profile.basCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBaseballProfiles2:gur_user_baseball_profile[];
    

   
    @OneToMany(()=>gur_user_basic_moderation, (gur_user_basic_moderation: gur_user_basic_moderation)=>gur_user_basic_moderation.ubmEntityUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasicModerations:gur_user_basic_moderation[];
    

   
    @OneToMany(()=>gur_user_basketball_profile, (gur_user_basketball_profile: gur_user_basketball_profile)=>gur_user_basketball_profile.basUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballProfiles:gur_user_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_basketball_profile, (gur_user_basketball_profile: gur_user_basketball_profile)=>gur_user_basketball_profile.basCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballProfiles2:gur_user_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_basketball_stats, (gur_user_basketball_stats: gur_user_basketball_stats)=>gur_user_basketball_stats.basUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballStatss:gur_user_basketball_stats[];
    

   
    @OneToMany(()=>gur_user_basque_pelota_profile, (gur_user_basque_pelota_profile: gur_user_basque_pelota_profile)=>gur_user_basque_pelota_profile.bplUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasquePelotaProfiles:gur_user_basque_pelota_profile[];
    

   
    @OneToMany(()=>gur_user_basque_pelota_profile, (gur_user_basque_pelota_profile: gur_user_basque_pelota_profile)=>gur_user_basque_pelota_profile.bplCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasquePelotaProfiles2:gur_user_basque_pelota_profile[];
    

   
    @OneToMany(()=>gur_user_beach_volleyball_profile, (gur_user_beach_volleyball_profile: gur_user_beach_volleyball_profile)=>gur_user_beach_volleyball_profile.bvbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBeachVolleyballProfiles:gur_user_beach_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_beach_volleyball_profile, (gur_user_beach_volleyball_profile: gur_user_beach_volleyball_profile)=>gur_user_beach_volleyball_profile.bvbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBeachVolleyballProfiles2:gur_user_beach_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_biathlon_profile, (gur_user_biathlon_profile: gur_user_biathlon_profile)=>gur_user_biathlon_profile.biaUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBiathlonProfiles:gur_user_biathlon_profile[];
    

   
    @OneToMany(()=>gur_user_biathlon_profile, (gur_user_biathlon_profile: gur_user_biathlon_profile)=>gur_user_biathlon_profile.biaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBiathlonProfiles2:gur_user_biathlon_profile[];
    

   
    @OneToMany(()=>gur_user_billiards_profile, (gur_user_billiards_profile: gur_user_billiards_profile)=>gur_user_billiards_profile.bilUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBilliardsProfiles:gur_user_billiards_profile[];
    

   
    @OneToMany(()=>gur_user_billiards_profile, (gur_user_billiards_profile: gur_user_billiards_profile)=>gur_user_billiards_profile.bilCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBilliardsProfiles2:gur_user_billiards_profile[];
    

   
    @OneToMany(()=>gur_user_blind_cricket_profile, (gur_user_blind_cricket_profile: gur_user_blind_cricket_profile)=>gur_user_blind_cricket_profile.bcpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBlindCricketProfiles:gur_user_blind_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_blind_cricket_profile, (gur_user_blind_cricket_profile: gur_user_blind_cricket_profile)=>gur_user_blind_cricket_profile.bcpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBlindCricketProfiles2:gur_user_blind_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_bobsleigh_profile, (gur_user_bobsleigh_profile: gur_user_bobsleigh_profile)=>gur_user_bobsleigh_profile.bobUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBobsleighProfiles:gur_user_bobsleigh_profile[];
    

   
    @OneToMany(()=>gur_user_bobsleigh_profile, (gur_user_bobsleigh_profile: gur_user_bobsleigh_profile)=>gur_user_bobsleigh_profile.bobCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBobsleighProfiles2:gur_user_bobsleigh_profile[];
    

   
    @OneToMany(()=>gur_user_bodybuilding_profile, (gur_user_bodybuilding_profile: gur_user_bodybuilding_profile)=>gur_user_bodybuilding_profile.bodUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBodybuildingProfiles:gur_user_bodybuilding_profile[];
    

   
    @OneToMany(()=>gur_user_bodybuilding_profile, (gur_user_bodybuilding_profile: gur_user_bodybuilding_profile)=>gur_user_bodybuilding_profile.bodCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBodybuildingProfiles2:gur_user_bodybuilding_profile[];
    

   
    @OneToMany(()=>gur_user_boxing_profile, (gur_user_boxing_profile: gur_user_boxing_profile)=>gur_user_boxing_profile.boxUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBoxingProfiles:gur_user_boxing_profile[];
    

   
    @OneToMany(()=>gur_user_boxing_profile, (gur_user_boxing_profile: gur_user_boxing_profile)=>gur_user_boxing_profile.boxCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBoxingProfiles2:gur_user_boxing_profile[];
    

   
    @OneToMany(()=>gur_user_boxing_stats, (gur_user_boxing_stats: gur_user_boxing_stats)=>gur_user_boxing_stats.bosUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBoxingStatss:gur_user_boxing_stats[];
    

   
    @OneToMany(()=>gur_user_canadian_football_profile, (gur_user_canadian_football_profile: gur_user_canadian_football_profile)=>gur_user_canadian_football_profile.canUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCanadianFootballProfiles:gur_user_canadian_football_profile[];
    

   
    @OneToMany(()=>gur_user_canadian_football_profile, (gur_user_canadian_football_profile: gur_user_canadian_football_profile)=>gur_user_canadian_football_profile.canCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCanadianFootballProfiles2:gur_user_canadian_football_profile[];
    

   
    @OneToMany(()=>gur_user_canoe_kayak_profile, (gur_user_canoe_kayak_profile: gur_user_canoe_kayak_profile)=>gur_user_canoe_kayak_profile.ckpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCanoeKayakProfiles:gur_user_canoe_kayak_profile[];
    

   
    @OneToMany(()=>gur_user_canoe_kayak_profile, (gur_user_canoe_kayak_profile: gur_user_canoe_kayak_profile)=>gur_user_canoe_kayak_profile.ckpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCanoeKayakProfiles2:gur_user_canoe_kayak_profile[];
    

   
    @OneToMany(()=>gur_user_carrom_profile, (gur_user_carrom_profile: gur_user_carrom_profile)=>gur_user_carrom_profile.carUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCarromProfiles:gur_user_carrom_profile[];
    

   
    @OneToMany(()=>gur_user_carrom_profile, (gur_user_carrom_profile: gur_user_carrom_profile)=>gur_user_carrom_profile.carCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCarromProfiles2:gur_user_carrom_profile[];
    

   
    @OneToMany(()=>gur_user_chess_profile, (gur_user_chess_profile: gur_user_chess_profile)=>gur_user_chess_profile.ucpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserChessProfiles:gur_user_chess_profile[];
    

   
    @OneToMany(()=>gur_user_chess_profile, (gur_user_chess_profile: gur_user_chess_profile)=>gur_user_chess_profile.ucpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserChessProfiles2:gur_user_chess_profile[];
    

   
    @OneToMany(()=>gur_user_chess_stats, (gur_user_chess_stats: gur_user_chess_stats)=>gur_user_chess_stats.ucsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserChessStatss:gur_user_chess_stats[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds2:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_cricket_profile, (gur_user_cricket_profile: gur_user_cricket_profile)=>gur_user_cricket_profile.ucpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketProfiles:gur_user_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_cricket_profile, (gur_user_cricket_profile: gur_user_cricket_profile)=>gur_user_cricket_profile.ucpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketProfiles2:gur_user_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_cricket_stats, (gur_user_cricket_stats: gur_user_cricket_stats)=>gur_user_cricket_stats.ucsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketStatss:gur_user_cricket_stats[];
    

   
    @OneToMany(()=>gur_user_croquet_profile, (gur_user_croquet_profile: gur_user_croquet_profile)=>gur_user_croquet_profile.croUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCroquetProfiles:gur_user_croquet_profile[];
    

   
    @OneToMany(()=>gur_user_croquet_profile, (gur_user_croquet_profile: gur_user_croquet_profile)=>gur_user_croquet_profile.croCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCroquetProfiles2:gur_user_croquet_profile[];
    

   
    @OneToMany(()=>gur_user_cross_country_skiing_profile, (gur_user_cross_country_skiing_profile: gur_user_cross_country_skiing_profile)=>gur_user_cross_country_skiing_profile.ccsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCrossCountrySkiingProfiles:gur_user_cross_country_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_cross_country_skiing_profile, (gur_user_cross_country_skiing_profile: gur_user_cross_country_skiing_profile)=>gur_user_cross_country_skiing_profile.ccsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCrossCountrySkiingProfiles2:gur_user_cross_country_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_cross_country_skiing_profile, (gur_user_cross_country_skiing_profile: gur_user_cross_country_skiing_profile)=>gur_user_cross_country_skiing_profile.ccsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCrossCountrySkiingProfiles3:gur_user_cross_country_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_curling_profile, (gur_user_curling_profile: gur_user_curling_profile)=>gur_user_curling_profile.curUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCurlingProfiles:gur_user_curling_profile[];
    

   
    @OneToMany(()=>gur_user_curling_profile, (gur_user_curling_profile: gur_user_curling_profile)=>gur_user_curling_profile.curCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCurlingProfiles2:gur_user_curling_profile[];
    

   
    @OneToMany(()=>gur_user_cycling_profile, (gur_user_cycling_profile: gur_user_cycling_profile)=>gur_user_cycling_profile.cycUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCyclingProfiles:gur_user_cycling_profile[];
    

   
    @OneToMany(()=>gur_user_cycling_profile, (gur_user_cycling_profile: gur_user_cycling_profile)=>gur_user_cycling_profile.cycCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCyclingProfiles2:gur_user_cycling_profile[];
    

   
    @OneToMany(()=>gur_user_cycling_stats, (gur_user_cycling_stats: gur_user_cycling_stats)=>gur_user_cycling_stats.cysUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCyclingStatss:gur_user_cycling_stats[];
    

   
    @OneToMany(()=>gur_user_darts_profile, (gur_user_darts_profile: gur_user_darts_profile)=>gur_user_darts_profile.darUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDartsProfiles:gur_user_darts_profile[];
    

   
    @OneToMany(()=>gur_user_darts_profile, (gur_user_darts_profile: gur_user_darts_profile)=>gur_user_darts_profile.darCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDartsProfiles2:gur_user_darts_profile[];
    

   
    @OneToMany(()=>gur_user_diving_profile, (gur_user_diving_profile: gur_user_diving_profile)=>gur_user_diving_profile.divUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingProfiles:gur_user_diving_profile[];
    

   
    @OneToMany(()=>gur_user_diving_profile, (gur_user_diving_profile: gur_user_diving_profile)=>gur_user_diving_profile.divCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingProfiles2:gur_user_diving_profile[];
    

   
    @OneToMany(()=>gur_user_diving_stats, (gur_user_diving_stats: gur_user_diving_stats)=>gur_user_diving_stats.dvsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingStatss:gur_user_diving_stats[];
    

   
    @OneToMany(()=>gur_user_dodgeball_profile, (gur_user_dodgeball_profile: gur_user_dodgeball_profile)=>gur_user_dodgeball_profile.dodUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDodgeballProfiles:gur_user_dodgeball_profile[];
    

   
    @OneToMany(()=>gur_user_dodgeball_profile, (gur_user_dodgeball_profile: gur_user_dodgeball_profile)=>gur_user_dodgeball_profile.dodCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDodgeballProfiles2:gur_user_dodgeball_profile[];
    

   
    @OneToMany(()=>gur_user_equestrian_profile, (gur_user_equestrian_profile: gur_user_equestrian_profile)=>gur_user_equestrian_profile.equUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserEquestrianProfiles:gur_user_equestrian_profile[];
    

   
    @OneToMany(()=>gur_user_equestrian_profile, (gur_user_equestrian_profile: gur_user_equestrian_profile)=>gur_user_equestrian_profile.equCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserEquestrianProfiles2:gur_user_equestrian_profile[];
    

   
    @OneToMany(()=>gur_user_eton_fives_profile, (gur_user_eton_fives_profile: gur_user_eton_fives_profile)=>gur_user_eton_fives_profile.etfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserEtonFivesProfiles:gur_user_eton_fives_profile[];
    

   
    @OneToMany(()=>gur_user_eton_fives_profile, (gur_user_eton_fives_profile: gur_user_eton_fives_profile)=>gur_user_eton_fives_profile.etfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserEtonFivesProfiles2:gur_user_eton_fives_profile[];
    

   
    @OneToMany(()=>gur_user_fencing_profile, (gur_user_fencing_profile: gur_user_fencing_profile)=>gur_user_fencing_profile.fenUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFencingProfiles:gur_user_fencing_profile[];
    

   
    @OneToMany(()=>gur_user_fencing_profile, (gur_user_fencing_profile: gur_user_fencing_profile)=>gur_user_fencing_profile.fenCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFencingProfiles2:gur_user_fencing_profile[];
    

   
    @OneToMany(()=>gur_user_field_hockey_profile, (gur_user_field_hockey_profile: gur_user_field_hockey_profile)=>gur_user_field_hockey_profile.fhkUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFieldHockeyProfiles:gur_user_field_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_field_hockey_profile, (gur_user_field_hockey_profile: gur_user_field_hockey_profile)=>gur_user_field_hockey_profile.fhkCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFieldHockeyProfiles2:gur_user_field_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_field_hockey_stats, (gur_user_field_hockey_stats: gur_user_field_hockey_stats)=>gur_user_field_hockey_stats.fhsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFieldHockeyStatss:gur_user_field_hockey_stats[];
    

   
    @OneToMany(()=>gur_user_figure_skating_profile, (gur_user_figure_skating_profile: gur_user_figure_skating_profile)=>gur_user_figure_skating_profile.figUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFigureSkatingProfiles:gur_user_figure_skating_profile[];
    

   
    @OneToMany(()=>gur_user_figure_skating_profile, (gur_user_figure_skating_profile: gur_user_figure_skating_profile)=>gur_user_figure_skating_profile.figCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFigureSkatingProfiles2:gur_user_figure_skating_profile[];
    

   
    @OneToMany(()=>gur_user_figure_skating_stats, (gur_user_figure_skating_stats: gur_user_figure_skating_stats)=>gur_user_figure_skating_stats.fssUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFigureSkatingStatss:gur_user_figure_skating_stats[];
    

   
    @OneToMany(()=>gur_user_floorball_profile, (gur_user_floorball_profile: gur_user_floorball_profile)=>gur_user_floorball_profile.flrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFloorballProfiles:gur_user_floorball_profile[];
    

   
    @OneToMany(()=>gur_user_floorball_profile, (gur_user_floorball_profile: gur_user_floorball_profile)=>gur_user_floorball_profile.flrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFloorballProfiles2:gur_user_floorball_profile[];
    

   
    @OneToMany(()=>gur_user_formula_four_profile, (gur_user_formula_four_profile: gur_user_formula_four_profile)=>gur_user_formula_four_profile.froUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaFourProfiles:gur_user_formula_four_profile[];
    

   
    @OneToMany(()=>gur_user_formula_indy_profile, (gur_user_formula_indy_profile: gur_user_formula_indy_profile)=>gur_user_formula_indy_profile.froUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaIndyProfiles:gur_user_formula_indy_profile[];
    

   
    @OneToMany(()=>gur_user_formula_indy_profile, (gur_user_formula_indy_profile: gur_user_formula_indy_profile)=>gur_user_formula_indy_profile.froCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaIndyProfiles2:gur_user_formula_indy_profile[];
    

   
    @OneToMany(()=>gur_user_formula_one_profile, (gur_user_formula_one_profile: gur_user_formula_one_profile)=>gur_user_formula_one_profile.froUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaOneProfiles:gur_user_formula_one_profile[];
    

   
    @OneToMany(()=>gur_user_formula_one_profile, (gur_user_formula_one_profile: gur_user_formula_one_profile)=>gur_user_formula_one_profile.froCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaOneProfiles2:gur_user_formula_one_profile[];
    

   
    @OneToMany(()=>gur_user_formula_three_profile, (gur_user_formula_three_profile: gur_user_formula_three_profile)=>gur_user_formula_three_profile.froUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaThreeProfiles:gur_user_formula_three_profile[];
    

   
    @OneToMany(()=>gur_user_formula_three_profile, (gur_user_formula_three_profile: gur_user_formula_three_profile)=>gur_user_formula_three_profile.froCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFormulaThreeProfiles2:gur_user_formula_three_profile[];
    

   
    @OneToMany(()=>gur_user_freestyle_football_profile, (gur_user_freestyle_football_profile: gur_user_freestyle_football_profile)=>gur_user_freestyle_football_profile.fsfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFreestyleFootballProfiles:gur_user_freestyle_football_profile[];
    

   
    @OneToMany(()=>gur_user_freestyle_football_profile, (gur_user_freestyle_football_profile: gur_user_freestyle_football_profile)=>gur_user_freestyle_football_profile.fsfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFreestyleFootballProfiles2:gur_user_freestyle_football_profile[];
    

   
    @OneToMany(()=>gur_user_freestyle_skiing_profile, (gur_user_freestyle_skiing_profile: gur_user_freestyle_skiing_profile)=>gur_user_freestyle_skiing_profile.frsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFreestyleSkiingProfiles:gur_user_freestyle_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_freestyle_skiing_profile, (gur_user_freestyle_skiing_profile: gur_user_freestyle_skiing_profile)=>gur_user_freestyle_skiing_profile.frsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFreestyleSkiingProfiles2:gur_user_freestyle_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_futsal_profile, (gur_user_futsal_profile: gur_user_futsal_profile)=>gur_user_futsal_profile.futUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFutsalProfiles:gur_user_futsal_profile[];
    

   
    @OneToMany(()=>gur_user_futsal_profile, (gur_user_futsal_profile: gur_user_futsal_profile)=>gur_user_futsal_profile.futCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFutsalProfiles2:gur_user_futsal_profile[];
    

   
    @OneToMany(()=>gur_user_gaelic_football_profile, (gur_user_gaelic_football_profile: gur_user_gaelic_football_profile)=>gur_user_gaelic_football_profile.gafUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGaelicFootballProfiles:gur_user_gaelic_football_profile[];
    

   
    @OneToMany(()=>gur_user_gaelic_football_profile, (gur_user_gaelic_football_profile: gur_user_gaelic_football_profile)=>gur_user_gaelic_football_profile.gafCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGaelicFootballProfiles2:gur_user_gaelic_football_profile[];
    

   
    @OneToMany(()=>gur_user_gliding_profile, (gur_user_gliding_profile: gur_user_gliding_profile)=>gur_user_gliding_profile.gliUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGlidingProfiles:gur_user_gliding_profile[];
    

   
    @OneToMany(()=>gur_user_gliding_profile, (gur_user_gliding_profile: gur_user_gliding_profile)=>gur_user_gliding_profile.gliCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGlidingProfiles2:gur_user_gliding_profile[];
    

   
    @OneToMany(()=>gur_user_golf_profile, (gur_user_golf_profile: gur_user_golf_profile)=>gur_user_golf_profile.glfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGolfProfiles:gur_user_golf_profile[];
    

   
    @OneToMany(()=>gur_user_golf_profile, (gur_user_golf_profile: gur_user_golf_profile)=>gur_user_golf_profile.glfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGolfProfiles2:gur_user_golf_profile[];
    

   
    @OneToMany(()=>gur_user_golf_stats, (gur_user_golf_stats: gur_user_golf_stats)=>gur_user_golf_stats.glsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGolfStatss:gur_user_golf_stats[];
    

   
    @OneToMany(()=>gur_user_gymnastics_profile, (gur_user_gymnastics_profile: gur_user_gymnastics_profile)=>gur_user_gymnastics_profile.gymUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGymnasticsProfiles:gur_user_gymnastics_profile[];
    

   
    @OneToMany(()=>gur_user_gymnastics_profile, (gur_user_gymnastics_profile: gur_user_gymnastics_profile)=>gur_user_gymnastics_profile.gymCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGymnasticsProfiles2:gur_user_gymnastics_profile[];
    

   
    @OneToMany(()=>gur_user_gymnastics_stats, (gur_user_gymnastics_stats: gur_user_gymnastics_stats)=>gur_user_gymnastics_stats.gysUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGymnasticsStatss:gur_user_gymnastics_stats[];
    

   
    @OneToMany(()=>gur_user_hang_gliding_profile, (gur_user_hang_gliding_profile: gur_user_hang_gliding_profile)=>gur_user_hang_gliding_profile.hanUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHangGlidingProfiles:gur_user_hang_gliding_profile[];
    

   
    @OneToMany(()=>gur_user_hang_gliding_profile, (gur_user_hang_gliding_profile: gur_user_hang_gliding_profile)=>gur_user_hang_gliding_profile.hanCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHangGlidingProfiles2:gur_user_hang_gliding_profile[];
    

   
    @OneToMany(()=>gur_user_horseball_profile, (gur_user_horseball_profile: gur_user_horseball_profile)=>gur_user_horseball_profile.hrbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHorseballProfiles:gur_user_horseball_profile[];
    

   
    @OneToMany(()=>gur_user_horseball_profile, (gur_user_horseball_profile: gur_user_horseball_profile)=>gur_user_horseball_profile.hrbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHorseballProfiles2:gur_user_horseball_profile[];
    

   
    @OneToMany(()=>gur_user_horseshoe_pitching_profile, (gur_user_horseshoe_pitching_profile: gur_user_horseshoe_pitching_profile)=>gur_user_horseshoe_pitching_profile.hspUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHorseshoePitchingProfiles:gur_user_horseshoe_pitching_profile[];
    

   
    @OneToMany(()=>gur_user_horseshoe_pitching_profile, (gur_user_horseshoe_pitching_profile: gur_user_horseshoe_pitching_profile)=>gur_user_horseshoe_pitching_profile.hspCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHorseshoePitchingProfiles2:gur_user_horseshoe_pitching_profile[];
    

   
    @OneToMany(()=>gur_user_hurling_profile, (gur_user_hurling_profile: gur_user_hurling_profile)=>gur_user_hurling_profile.hurUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHurlingProfiles:gur_user_hurling_profile[];
    

   
    @OneToMany(()=>gur_user_hurling_profile, (gur_user_hurling_profile: gur_user_hurling_profile)=>gur_user_hurling_profile.hurCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHurlingProfiles2:gur_user_hurling_profile[];
    

   
    @OneToMany(()=>gur_user_ice_hockey_profile, (gur_user_ice_hockey_profile: gur_user_ice_hockey_profile)=>gur_user_ice_hockey_profile.ichUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserIceHockeyProfiles:gur_user_ice_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_ice_hockey_profile, (gur_user_ice_hockey_profile: gur_user_ice_hockey_profile)=>gur_user_ice_hockey_profile.ichCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserIceHockeyProfiles2:gur_user_ice_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_inline_alpine_skating_profile, (gur_user_inline_alpine_skating_profile: gur_user_inline_alpine_skating_profile)=>gur_user_inline_alpine_skating_profile.iasUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineAlpineSkatingProfiles:gur_user_inline_alpine_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_alpine_skating_profile, (gur_user_inline_alpine_skating_profile: gur_user_inline_alpine_skating_profile)=>gur_user_inline_alpine_skating_profile.iasCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineAlpineSkatingProfiles2:gur_user_inline_alpine_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_downhill_skating_profile, (gur_user_inline_downhill_skating_profile: gur_user_inline_downhill_skating_profile)=>gur_user_inline_downhill_skating_profile.idsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineDownhillSkatingProfiles:gur_user_inline_downhill_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_downhill_skating_profile, (gur_user_inline_downhill_skating_profile: gur_user_inline_downhill_skating_profile)=>gur_user_inline_downhill_skating_profile.idsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineDownhillSkatingProfiles2:gur_user_inline_downhill_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_freestyle_skating_profile, (gur_user_inline_freestyle_skating_profile: gur_user_inline_freestyle_skating_profile)=>gur_user_inline_freestyle_skating_profile.ifsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineFreestyleSkatingProfiles:gur_user_inline_freestyle_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_freestyle_skating_profile, (gur_user_inline_freestyle_skating_profile: gur_user_inline_freestyle_skating_profile)=>gur_user_inline_freestyle_skating_profile.ifsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineFreestyleSkatingProfiles2:gur_user_inline_freestyle_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_skating_profile, (gur_user_inline_skating_profile: gur_user_inline_skating_profile)=>gur_user_inline_skating_profile.insUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineSkatingProfiles:gur_user_inline_skating_profile[];
    

   
    @OneToMany(()=>gur_user_inline_skating_profile, (gur_user_inline_skating_profile: gur_user_inline_skating_profile)=>gur_user_inline_skating_profile.insCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserInlineSkatingProfiles2:gur_user_inline_skating_profile[];
    

   
    @OneToMany(()=>gur_user_jianzi_profile, (gur_user_jianzi_profile: gur_user_jianzi_profile)=>gur_user_jianzi_profile.jiaUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJianziProfiles:gur_user_jianzi_profile[];
    

   
    @OneToMany(()=>gur_user_jianzi_profile, (gur_user_jianzi_profile: gur_user_jianzi_profile)=>gur_user_jianzi_profile.jiaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJianziProfiles2:gur_user_jianzi_profile[];
    

   
    @OneToMany(()=>gur_user_judo_profile, (gur_user_judo_profile: gur_user_judo_profile)=>gur_user_judo_profile.judUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoProfiles:gur_user_judo_profile[];
    

   
    @OneToMany(()=>gur_user_judo_profile, (gur_user_judo_profile: gur_user_judo_profile)=>gur_user_judo_profile.judCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoProfiles2:gur_user_judo_profile[];
    

   
    @OneToMany(()=>gur_user_judo_stats, (gur_user_judo_stats: gur_user_judo_stats)=>gur_user_judo_stats.jusUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoStatss:gur_user_judo_stats[];
    

   
    @OneToMany(()=>gur_user_jujitsu_profile, (gur_user_jujitsu_profile: gur_user_jujitsu_profile)=>gur_user_jujitsu_profile.jujUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJujitsuProfiles:gur_user_jujitsu_profile[];
    

   
    @OneToMany(()=>gur_user_jujitsu_profile, (gur_user_jujitsu_profile: gur_user_jujitsu_profile)=>gur_user_jujitsu_profile.jujCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJujitsuProfiles2:gur_user_jujitsu_profile[];
    

   
    @OneToMany(()=>gur_user_kabaddi_profile, (gur_user_kabaddi_profile: gur_user_kabaddi_profile)=>gur_user_kabaddi_profile.kabUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKabaddiProfiles:gur_user_kabaddi_profile[];
    

   
    @OneToMany(()=>gur_user_kabaddi_profile, (gur_user_kabaddi_profile: gur_user_kabaddi_profile)=>gur_user_kabaddi_profile.kabCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKabaddiProfiles2:gur_user_kabaddi_profile[];
    

   
    @OneToMany(()=>gur_user_kabaddi_stats, (gur_user_kabaddi_stats: gur_user_kabaddi_stats)=>gur_user_kabaddi_stats.kbsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKabaddiStatss:gur_user_kabaddi_stats[];
    

   
    @OneToMany(()=>gur_user_karate_profile, (gur_user_karate_profile: gur_user_karate_profile)=>gur_user_karate_profile.karUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateProfiles:gur_user_karate_profile[];
    

   
    @OneToMany(()=>gur_user_karate_profile, (gur_user_karate_profile: gur_user_karate_profile)=>gur_user_karate_profile.karCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateProfiles2:gur_user_karate_profile[];
    

   
    @OneToMany(()=>gur_user_karate_stats, (gur_user_karate_stats: gur_user_karate_stats)=>gur_user_karate_stats.kasUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateStatss:gur_user_karate_stats[];
    

   
    @OneToMany(()=>gur_user_karting_profile, (gur_user_karting_profile: gur_user_karting_profile)=>gur_user_karting_profile.karUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKartingProfiles:gur_user_karting_profile[];
    

   
    @OneToMany(()=>gur_user_kickboxing_profile, (gur_user_kickboxing_profile: gur_user_kickboxing_profile)=>gur_user_kickboxing_profile.kcbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKickboxingProfiles:gur_user_kickboxing_profile[];
    

   
    @OneToMany(()=>gur_user_kickboxing_profile, (gur_user_kickboxing_profile: gur_user_kickboxing_profile)=>gur_user_kickboxing_profile.kcbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKickboxingProfiles2:gur_user_kickboxing_profile[];
    

   
    @OneToMany(()=>gur_user_kiteboarding_profile, (gur_user_kiteboarding_profile: gur_user_kiteboarding_profile)=>gur_user_kiteboarding_profile.kbrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKiteboardingProfiles:gur_user_kiteboarding_profile[];
    

   
    @OneToMany(()=>gur_user_kiteboarding_profile, (gur_user_kiteboarding_profile: gur_user_kiteboarding_profile)=>gur_user_kiteboarding_profile.kbrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKiteboardingProfiles2:gur_user_kiteboarding_profile[];
    

   
    @OneToMany(()=>gur_user_korfball_profile, (gur_user_korfball_profile: gur_user_korfball_profile)=>gur_user_korfball_profile.korUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKorfballProfiles:gur_user_korfball_profile[];
    

   
    @OneToMany(()=>gur_user_korfball_profile, (gur_user_korfball_profile: gur_user_korfball_profile)=>gur_user_korfball_profile.korCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKorfballProfiles2:gur_user_korfball_profile[];
    

   
    @OneToMany(()=>gur_user_krav_maga_profile, (gur_user_krav_maga_profile: gur_user_krav_maga_profile)=>gur_user_krav_maga_profile.krvUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKravMagaProfiles:gur_user_krav_maga_profile[];
    

   
    @OneToMany(()=>gur_user_krav_maga_profile, (gur_user_krav_maga_profile: gur_user_krav_maga_profile)=>gur_user_krav_maga_profile.krvCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKravMagaProfiles2:gur_user_krav_maga_profile[];
    

   
    @OneToMany(()=>gur_user_lacrosse_profile, (gur_user_lacrosse_profile: gur_user_lacrosse_profile)=>gur_user_lacrosse_profile.lacUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLacrosseProfiles:gur_user_lacrosse_profile[];
    

   
    @OneToMany(()=>gur_user_lacrosse_profile, (gur_user_lacrosse_profile: gur_user_lacrosse_profile)=>gur_user_lacrosse_profile.lacCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLacrosseProfiles2:gur_user_lacrosse_profile[];
    

   
    @OneToMany(()=>gur_user_lawn_bowling_profile, (gur_user_lawn_bowling_profile: gur_user_lawn_bowling_profile)=>gur_user_lawn_bowling_profile.lwbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLawnBowlingProfiles:gur_user_lawn_bowling_profile[];
    

   
    @OneToMany(()=>gur_user_lawn_bowling_profile, (gur_user_lawn_bowling_profile: gur_user_lawn_bowling_profile)=>gur_user_lawn_bowling_profile.lwbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLawnBowlingProfiles2:gur_user_lawn_bowling_profile[];
    

   
    @OneToMany(()=>gur_user_leg_cricket_profile, (gur_user_leg_cricket_profile: gur_user_leg_cricket_profile)=>gur_user_leg_cricket_profile.lgcUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLegCricketProfiles:gur_user_leg_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_leg_cricket_profile, (gur_user_leg_cricket_profile: gur_user_leg_cricket_profile)=>gur_user_leg_cricket_profile.lgcCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLegCricketProfiles2:gur_user_leg_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_luge_profile, (gur_user_luge_profile: gur_user_luge_profile)=>gur_user_luge_profile.lugUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLugeProfiles:gur_user_luge_profile[];
    

   
    @OneToMany(()=>gur_user_luge_profile, (gur_user_luge_profile: gur_user_luge_profile)=>gur_user_luge_profile.lugCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLugeProfiles2:gur_user_luge_profile[];
    

   
    @OneToMany(()=>gur_user_media_gallery, (gur_user_media_gallery: gur_user_media_gallery)=>gur_user_media_gallery.umgCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserMediaGallerys:gur_user_media_gallery[];
    

   
    @OneToMany(()=>gur_user_modern_pentathlon_profile, (gur_user_modern_pentathlon_profile: gur_user_modern_pentathlon_profile)=>gur_user_modern_pentathlon_profile.mopUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModernPentathlonProfiles:gur_user_modern_pentathlon_profile[];
    

   
    @OneToMany(()=>gur_user_modern_pentathlon_profile, (gur_user_modern_pentathlon_profile: gur_user_modern_pentathlon_profile)=>gur_user_modern_pentathlon_profile.mopCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModernPentathlonProfiles2:gur_user_modern_pentathlon_profile[];
    

   
    @OneToMany(()=>gur_user_modern_pentathlon_stats, (gur_user_modern_pentathlon_stats: gur_user_modern_pentathlon_stats)=>gur_user_modern_pentathlon_stats.mpsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModernPentathlonStatss:gur_user_modern_pentathlon_stats[];
    

   
    @OneToMany(()=>gur_user_motogp_profile, (gur_user_motogp_profile: gur_user_motogp_profile)=>gur_user_motogp_profile.mgpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserMotogpProfiles:gur_user_motogp_profile[];
    

   
    @OneToMany(()=>gur_user_motogp_profile, (gur_user_motogp_profile: gur_user_motogp_profile)=>gur_user_motogp_profile.mgpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserMotogpProfiles2:gur_user_motogp_profile[];
    

   
    @OneToMany(()=>gur_user_netball_profile, (gur_user_netball_profile: gur_user_netball_profile)=>gur_user_netball_profile.netUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserNetballProfiles:gur_user_netball_profile[];
    

   
    @OneToMany(()=>gur_user_netball_profile, (gur_user_netball_profile: gur_user_netball_profile)=>gur_user_netball_profile.netCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserNetballProfiles2:gur_user_netball_profile[];
    

   
    @OneToMany(()=>gur_user_netball_stats, (gur_user_netball_stats: gur_user_netball_stats)=>gur_user_netball_stats.ntsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserNetballStatss:gur_user_netball_stats[];
    

   
    @OneToMany(()=>gur_user_openwater_swimming_profile, (gur_user_openwater_swimming_profile: gur_user_openwater_swimming_profile)=>gur_user_openwater_swimming_profile.owsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserOpenwaterSwimmingProfiles:gur_user_openwater_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_openwater_swimming_profile, (gur_user_openwater_swimming_profile: gur_user_openwater_swimming_profile)=>gur_user_openwater_swimming_profile.owsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserOpenwaterSwimmingProfiles2:gur_user_openwater_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_orienteering_profile, (gur_user_orienteering_profile: gur_user_orienteering_profile)=>gur_user_orienteering_profile.oriUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserOrienteeringProfiles:gur_user_orienteering_profile[];
    

   
    @OneToMany(()=>gur_user_orienteering_profile, (gur_user_orienteering_profile: gur_user_orienteering_profile)=>gur_user_orienteering_profile.oriCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserOrienteeringProfiles2:gur_user_orienteering_profile[];
    

   
    @OneToMany(()=>gur_user_paddleball_profile, (gur_user_paddleball_profile: gur_user_paddleball_profile)=>gur_user_paddleball_profile.padUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPaddleballProfiles:gur_user_paddleball_profile[];
    

   
    @OneToMany(()=>gur_user_paddleball_profile, (gur_user_paddleball_profile: gur_user_paddleball_profile)=>gur_user_paddleball_profile.padCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPaddleballProfiles2:gur_user_paddleball_profile[];
    

   
    @OneToMany(()=>gur_user_para_alpine_skiing_profile, (gur_user_para_alpine_skiing_profile: gur_user_para_alpine_skiing_profile)=>gur_user_para_alpine_skiing_profile.pasUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaAlpineSkiingProfiles:gur_user_para_alpine_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_para_alpine_skiing_profile, (gur_user_para_alpine_skiing_profile: gur_user_para_alpine_skiing_profile)=>gur_user_para_alpine_skiing_profile.pasCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaAlpineSkiingProfiles2:gur_user_para_alpine_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_para_archery_profile, (gur_user_para_archery_profile: gur_user_para_archery_profile)=>gur_user_para_archery_profile.arcUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaArcheryProfiles:gur_user_para_archery_profile[];
    

   
    @OneToMany(()=>gur_user_para_archery_profile, (gur_user_para_archery_profile: gur_user_para_archery_profile)=>gur_user_para_archery_profile.arcCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaArcheryProfiles2:gur_user_para_archery_profile[];
    

   
    @OneToMany(()=>gur_user_para_athletics_profile, (gur_user_para_athletics_profile: gur_user_para_athletics_profile)=>gur_user_para_athletics_profile.uapUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaAthleticsProfiles:gur_user_para_athletics_profile[];
    

   
    @OneToMany(()=>gur_user_para_athletics_profile, (gur_user_para_athletics_profile: gur_user_para_athletics_profile)=>gur_user_para_athletics_profile.uapCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaAthleticsProfiles2:gur_user_para_athletics_profile[];
    

   
    @OneToMany(()=>gur_user_para_badminton_profile, (gur_user_para_badminton_profile: gur_user_para_badminton_profile)=>gur_user_para_badminton_profile.pbdUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBadmintonProfiles:gur_user_para_badminton_profile[];
    

   
    @OneToMany(()=>gur_user_para_badminton_profile, (gur_user_para_badminton_profile: gur_user_para_badminton_profile)=>gur_user_para_badminton_profile.pbdCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBadmintonProfiles2:gur_user_para_badminton_profile[];
    

   
    @OneToMany(()=>gur_user_para_beach_volleyball_profile, (gur_user_para_beach_volleyball_profile: gur_user_para_beach_volleyball_profile)=>gur_user_para_beach_volleyball_profile.pbvUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBeachVolleyballProfiles:gur_user_para_beach_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_para_beach_volleyball_profile, (gur_user_para_beach_volleyball_profile: gur_user_para_beach_volleyball_profile)=>gur_user_para_beach_volleyball_profile.pbvCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBeachVolleyballProfiles2:gur_user_para_beach_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_para_biathlon_profile, (gur_user_para_biathlon_profile: gur_user_para_biathlon_profile)=>gur_user_para_biathlon_profile.pbiUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBiathlonProfiles:gur_user_para_biathlon_profile[];
    

   
    @OneToMany(()=>gur_user_para_biathlon_profile, (gur_user_para_biathlon_profile: gur_user_para_biathlon_profile)=>gur_user_para_biathlon_profile.pbiCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBiathlonProfiles2:gur_user_para_biathlon_profile[];
    

   
    @OneToMany(()=>gur_user_para_boccia_profile, (gur_user_para_boccia_profile: gur_user_para_boccia_profile)=>gur_user_para_boccia_profile.bocUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBocciaProfiles:gur_user_para_boccia_profile[];
    

   
    @OneToMany(()=>gur_user_para_boccia_profile, (gur_user_para_boccia_profile: gur_user_para_boccia_profile)=>gur_user_para_boccia_profile.bocCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBocciaProfiles2:gur_user_para_boccia_profile[];
    

   
    @OneToMany(()=>gur_user_para_canoe_profile, (gur_user_para_canoe_profile: gur_user_para_canoe_profile)=>gur_user_para_canoe_profile.pcaUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCanoeProfiles:gur_user_para_canoe_profile[];
    

   
    @OneToMany(()=>gur_user_para_canoe_profile, (gur_user_para_canoe_profile: gur_user_para_canoe_profile)=>gur_user_para_canoe_profile.pcaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCanoeProfiles2:gur_user_para_canoe_profile[];
    

   
    @OneToMany(()=>gur_user_para_cross_country_skiing_profile, (gur_user_para_cross_country_skiing_profile: gur_user_para_cross_country_skiing_profile)=>gur_user_para_cross_country_skiing_profile.pccUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCrossCountrySkiingProfiles:gur_user_para_cross_country_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_para_cross_country_skiing_profile, (gur_user_para_cross_country_skiing_profile: gur_user_para_cross_country_skiing_profile)=>gur_user_para_cross_country_skiing_profile.pccCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCrossCountrySkiingProfiles2:gur_user_para_cross_country_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_para_curling_profile, (gur_user_para_curling_profile: gur_user_para_curling_profile)=>gur_user_para_curling_profile.pcrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCurlingProfiles:gur_user_para_curling_profile[];
    

   
    @OneToMany(()=>gur_user_para_curling_profile, (gur_user_para_curling_profile: gur_user_para_curling_profile)=>gur_user_para_curling_profile.pcrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCurlingProfiles2:gur_user_para_curling_profile[];
    

   
    @OneToMany(()=>gur_user_para_cycling_profile, (gur_user_para_cycling_profile: gur_user_para_cycling_profile)=>gur_user_para_cycling_profile.pcyUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCyclingProfiles:gur_user_para_cycling_profile[];
    

   
    @OneToMany(()=>gur_user_para_cycling_profile, (gur_user_para_cycling_profile: gur_user_para_cycling_profile)=>gur_user_para_cycling_profile.pcyCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCyclingProfiles2:gur_user_para_cycling_profile[];
    

   
    @OneToMany(()=>gur_user_para_dance_profile, (gur_user_para_dance_profile: gur_user_para_dance_profile)=>gur_user_para_dance_profile.pdsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaDanceProfiles:gur_user_para_dance_profile[];
    

   
    @OneToMany(()=>gur_user_para_dance_profile, (gur_user_para_dance_profile: gur_user_para_dance_profile)=>gur_user_para_dance_profile.pdsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaDanceProfiles2:gur_user_para_dance_profile[];
    

   
    @OneToMany(()=>gur_user_para_equestrian_profile, (gur_user_para_equestrian_profile: gur_user_para_equestrian_profile)=>gur_user_para_equestrian_profile.equUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaEquestrianProfiles:gur_user_para_equestrian_profile[];
    

   
    @OneToMany(()=>gur_user_para_equestrian_profile, (gur_user_para_equestrian_profile: gur_user_para_equestrian_profile)=>gur_user_para_equestrian_profile.equCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaEquestrianProfiles2:gur_user_para_equestrian_profile[];
    

   
    @OneToMany(()=>gur_user_para_goalball_profile, (gur_user_para_goalball_profile: gur_user_para_goalball_profile)=>gur_user_para_goalball_profile.pgbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaGoalballProfiles:gur_user_para_goalball_profile[];
    

   
    @OneToMany(()=>gur_user_para_goalball_profile, (gur_user_para_goalball_profile: gur_user_para_goalball_profile)=>gur_user_para_goalball_profile.pgbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaGoalballProfiles2:gur_user_para_goalball_profile[];
    

   
    @OneToMany(()=>gur_user_para_ice_sledge_hockey_profile, (gur_user_para_ice_sledge_hockey_profile: gur_user_para_ice_sledge_hockey_profile)=>gur_user_para_ice_sledge_hockey_profile.ishUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaIceSledgeHockeyProfiles:gur_user_para_ice_sledge_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_para_ice_sledge_hockey_profile, (gur_user_para_ice_sledge_hockey_profile: gur_user_para_ice_sledge_hockey_profile)=>gur_user_para_ice_sledge_hockey_profile.ishCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaIceSledgeHockeyProfiles2:gur_user_para_ice_sledge_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_para_judo_profile, (gur_user_para_judo_profile: gur_user_para_judo_profile)=>gur_user_para_judo_profile.pjdUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaJudoProfiles:gur_user_para_judo_profile[];
    

   
    @OneToMany(()=>gur_user_para_judo_profile, (gur_user_para_judo_profile: gur_user_para_judo_profile)=>gur_user_para_judo_profile.pjdCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaJudoProfiles2:gur_user_para_judo_profile[];
    

   
    @OneToMany(()=>gur_user_para_powerlifting_profile, (gur_user_para_powerlifting_profile: gur_user_para_powerlifting_profile)=>gur_user_para_powerlifting_profile.pplUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaPowerliftingProfiles:gur_user_para_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_para_powerlifting_profile, (gur_user_para_powerlifting_profile: gur_user_para_powerlifting_profile)=>gur_user_para_powerlifting_profile.pplCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaPowerliftingProfiles2:gur_user_para_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_para_rowing_profile, (gur_user_para_rowing_profile: gur_user_para_rowing_profile)=>gur_user_para_rowing_profile.prwUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaRowingProfiles:gur_user_para_rowing_profile[];
    

   
    @OneToMany(()=>gur_user_para_rowing_profile, (gur_user_para_rowing_profile: gur_user_para_rowing_profile)=>gur_user_para_rowing_profile.prwCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaRowingProfiles2:gur_user_para_rowing_profile[];
    

   
    @OneToMany(()=>gur_user_para_sailing_profile, (gur_user_para_sailing_profile: gur_user_para_sailing_profile)=>gur_user_para_sailing_profile.pslUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSailingProfiles:gur_user_para_sailing_profile[];
    

   
    @OneToMany(()=>gur_user_para_sailing_profile, (gur_user_para_sailing_profile: gur_user_para_sailing_profile)=>gur_user_para_sailing_profile.pslCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSailingProfiles2:gur_user_para_sailing_profile[];
    

   
    @OneToMany(()=>gur_user_para_shooting_profile, (gur_user_para_shooting_profile: gur_user_para_shooting_profile)=>gur_user_para_shooting_profile.pshUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaShootingProfiles:gur_user_para_shooting_profile[];
    

   
    @OneToMany(()=>gur_user_para_shooting_profile, (gur_user_para_shooting_profile: gur_user_para_shooting_profile)=>gur_user_para_shooting_profile.pshCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaShootingProfiles2:gur_user_para_shooting_profile[];
    

   
    @OneToMany(()=>gur_user_para_snowboarding_profile, (gur_user_para_snowboarding_profile: gur_user_para_snowboarding_profile)=>gur_user_para_snowboarding_profile.psbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSnowboardingProfiles:gur_user_para_snowboarding_profile[];
    

   
    @OneToMany(()=>gur_user_para_snowboarding_profile, (gur_user_para_snowboarding_profile: gur_user_para_snowboarding_profile)=>gur_user_para_snowboarding_profile.psbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSnowboardingProfiles2:gur_user_para_snowboarding_profile[];
    

   
    @OneToMany(()=>gur_user_para_soccer_profile, (gur_user_para_soccer_profile: gur_user_para_soccer_profile)=>gur_user_para_soccer_profile.uspUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSoccerProfiles:gur_user_para_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_para_soccer_profile, (gur_user_para_soccer_profile: gur_user_para_soccer_profile)=>gur_user_para_soccer_profile.uspCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSoccerProfiles2:gur_user_para_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_para_swimming_profile, (gur_user_para_swimming_profile: gur_user_para_swimming_profile)=>gur_user_para_swimming_profile.pswUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSwimmingProfiles:gur_user_para_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_para_swimming_profile, (gur_user_para_swimming_profile: gur_user_para_swimming_profile)=>gur_user_para_swimming_profile.pswCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSwimmingProfiles2:gur_user_para_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_para_table_tennis_profile, (gur_user_para_table_tennis_profile: gur_user_para_table_tennis_profile)=>gur_user_para_table_tennis_profile.pttUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTableTennisProfiles:gur_user_para_table_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_para_table_tennis_profile, (gur_user_para_table_tennis_profile: gur_user_para_table_tennis_profile)=>gur_user_para_table_tennis_profile.pttCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTableTennisProfiles2:gur_user_para_table_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_para_taekwondo_profile, (gur_user_para_taekwondo_profile: gur_user_para_taekwondo_profile)=>gur_user_para_taekwondo_profile.ptkUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTaekwondoProfiles:gur_user_para_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_para_taekwondo_profile, (gur_user_para_taekwondo_profile: gur_user_para_taekwondo_profile)=>gur_user_para_taekwondo_profile.ptkCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTaekwondoProfiles2:gur_user_para_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_para_triathlon_profile, (gur_user_para_triathlon_profile: gur_user_para_triathlon_profile)=>gur_user_para_triathlon_profile.ptrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTriathlonProfiles:gur_user_para_triathlon_profile[];
    

   
    @OneToMany(()=>gur_user_para_triathlon_profile, (gur_user_para_triathlon_profile: gur_user_para_triathlon_profile)=>gur_user_para_triathlon_profile.ptrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTriathlonProfiles2:gur_user_para_triathlon_profile[];
    

   
    @OneToMany(()=>gur_user_para_volleyball_profile, (gur_user_para_volleyball_profile: gur_user_para_volleyball_profile)=>gur_user_para_volleyball_profile.prwUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaVolleyballProfiles:gur_user_para_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_para_volleyball_profile, (gur_user_para_volleyball_profile: gur_user_para_volleyball_profile)=>gur_user_para_volleyball_profile.prwCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaVolleyballProfiles2:gur_user_para_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_basketball_profile, (gur_user_para_wheelchair_basketball_profile: gur_user_para_wheelchair_basketball_profile)=>gur_user_para_wheelchair_basketball_profile.pwbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairBasketballProfiles:gur_user_para_wheelchair_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_basketball_profile, (gur_user_para_wheelchair_basketball_profile: gur_user_para_wheelchair_basketball_profile)=>gur_user_para_wheelchair_basketball_profile.pwbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairBasketballProfiles2:gur_user_para_wheelchair_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_fencing_profile, (gur_user_para_wheelchair_fencing_profile: gur_user_para_wheelchair_fencing_profile)=>gur_user_para_wheelchair_fencing_profile.pwfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairFencingProfiles:gur_user_para_wheelchair_fencing_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_fencing_profile, (gur_user_para_wheelchair_fencing_profile: gur_user_para_wheelchair_fencing_profile)=>gur_user_para_wheelchair_fencing_profile.pwfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairFencingProfiles2:gur_user_para_wheelchair_fencing_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_rugby_profile, (gur_user_para_wheelchair_rugby_profile: gur_user_para_wheelchair_rugby_profile)=>gur_user_para_wheelchair_rugby_profile.pwrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairRugbyProfiles:gur_user_para_wheelchair_rugby_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_rugby_profile, (gur_user_para_wheelchair_rugby_profile: gur_user_para_wheelchair_rugby_profile)=>gur_user_para_wheelchair_rugby_profile.pwrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairRugbyProfiles2:gur_user_para_wheelchair_rugby_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_tennis_profile, (gur_user_para_wheelchair_tennis_profile: gur_user_para_wheelchair_tennis_profile)=>gur_user_para_wheelchair_tennis_profile.utpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairTennisProfiles:gur_user_para_wheelchair_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_tennis_profile, (gur_user_para_wheelchair_tennis_profile: gur_user_para_wheelchair_tennis_profile)=>gur_user_para_wheelchair_tennis_profile.utpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairTennisProfiles2:gur_user_para_wheelchair_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_paragliding_profile, (gur_user_paragliding_profile: gur_user_paragliding_profile)=>gur_user_paragliding_profile.pglUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaglidingProfiles:gur_user_paragliding_profile[];
    

   
    @OneToMany(()=>gur_user_paragliding_profile, (gur_user_paragliding_profile: gur_user_paragliding_profile)=>gur_user_paragliding_profile.pglCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaglidingProfiles2:gur_user_paragliding_profile[];
    

   
    @OneToMany(()=>gur_user_pesapallo_profile, (gur_user_pesapallo_profile: gur_user_pesapallo_profile)=>gur_user_pesapallo_profile.pesUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPesapalloProfiles:gur_user_pesapallo_profile[];
    

   
    @OneToMany(()=>gur_user_pesapallo_profile, (gur_user_pesapallo_profile: gur_user_pesapallo_profile)=>gur_user_pesapallo_profile.pesCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPesapalloProfiles2:gur_user_pesapallo_profile[];
    

   
    @OneToMany(()=>gur_user_petanque_profile, (gur_user_petanque_profile: gur_user_petanque_profile)=>gur_user_petanque_profile.penUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPetanqueProfiles:gur_user_petanque_profile[];
    

   
    @OneToMany(()=>gur_user_petanque_profile, (gur_user_petanque_profile: gur_user_petanque_profile)=>gur_user_petanque_profile.penCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPetanqueProfiles2:gur_user_petanque_profile[];
    

   
    @OneToMany(()=>gur_user_pickleball_profile, (gur_user_pickleball_profile: gur_user_pickleball_profile)=>gur_user_pickleball_profile.picUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPickleballProfiles:gur_user_pickleball_profile[];
    

   
    @OneToMany(()=>gur_user_pickleball_profile, (gur_user_pickleball_profile: gur_user_pickleball_profile)=>gur_user_pickleball_profile.picCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPickleballProfiles2:gur_user_pickleball_profile[];
    

   
    @OneToMany(()=>gur_user_polo_profile, (gur_user_polo_profile: gur_user_polo_profile)=>gur_user_polo_profile.polUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoloProfiles:gur_user_polo_profile[];
    

   
    @OneToMany(()=>gur_user_polo_profile, (gur_user_polo_profile: gur_user_polo_profile)=>gur_user_polo_profile.polCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoloProfiles2:gur_user_polo_profile[];
    

   
    @OneToMany(()=>gur_user_pool_profile, (gur_user_pool_profile: gur_user_pool_profile)=>gur_user_pool_profile.polUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoolProfiles:gur_user_pool_profile[];
    

   
    @OneToMany(()=>gur_user_pool_profile, (gur_user_pool_profile: gur_user_pool_profile)=>gur_user_pool_profile.polCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoolProfiles2:gur_user_pool_profile[];
    

   
    @OneToMany(()=>gur_user_powerboat_racing_profile, (gur_user_powerboat_racing_profile: gur_user_powerboat_racing_profile)=>gur_user_powerboat_racing_profile.powUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerboatRacingProfiles:gur_user_powerboat_racing_profile[];
    

   
    @OneToMany(()=>gur_user_powerboat_racing_profile, (gur_user_powerboat_racing_profile: gur_user_powerboat_racing_profile)=>gur_user_powerboat_racing_profile.powCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerboatRacingProfiles2:gur_user_powerboat_racing_profile[];
    

   
    @OneToMany(()=>gur_user_powerlifting_profile, (gur_user_powerlifting_profile: gur_user_powerlifting_profile)=>gur_user_powerlifting_profile.uppUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingProfiles:gur_user_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_powerlifting_profile, (gur_user_powerlifting_profile: gur_user_powerlifting_profile)=>gur_user_powerlifting_profile.uppCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingProfiles2:gur_user_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_powerlifting_stats, (gur_user_powerlifting_stats: gur_user_powerlifting_stats)=>gur_user_powerlifting_stats.upsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingStatss:gur_user_powerlifting_stats[];
    

   
    @OneToMany(()=>gur_user_profile_views, (gur_user_profile_views: gur_user_profile_views)=>gur_user_profile_views.upvByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserProfileViewss:gur_user_profile_views[];
    

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss2:gur_user_qualifications[];
    

   
    @OneToMany(()=>gur_user_racquetball_profile, (gur_user_racquetball_profile: gur_user_racquetball_profile)=>gur_user_racquetball_profile.raqUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRacquetballProfiles:gur_user_racquetball_profile[];
    

   
    @OneToMany(()=>gur_user_racquetball_profile, (gur_user_racquetball_profile: gur_user_racquetball_profile)=>gur_user_racquetball_profile.raqCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRacquetballProfiles2:gur_user_racquetball_profile[];
    

   
    @OneToMany(()=>gur_user_rafting_profile, (gur_user_rafting_profile: gur_user_rafting_profile)=>gur_user_rafting_profile.rafUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRaftingProfiles:gur_user_rafting_profile[];
    

   
    @OneToMany(()=>gur_user_rafting_profile, (gur_user_rafting_profile: gur_user_rafting_profile)=>gur_user_rafting_profile.rafCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRaftingProfiles2:gur_user_rafting_profile[];
    

   
    @OneToMany(()=>gur_user_real_tennis_profile, (gur_user_real_tennis_profile: gur_user_real_tennis_profile)=>gur_user_real_tennis_profile.retUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRealTennisProfiles:gur_user_real_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_real_tennis_profile, (gur_user_real_tennis_profile: gur_user_real_tennis_profile)=>gur_user_real_tennis_profile.retCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRealTennisProfiles2:gur_user_real_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_rock_climbing_profile, (gur_user_rock_climbing_profile: gur_user_rock_climbing_profile)=>gur_user_rock_climbing_profile.rcbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRockClimbingProfiles:gur_user_rock_climbing_profile[];
    

   
    @OneToMany(()=>gur_user_rock_climbing_profile, (gur_user_rock_climbing_profile: gur_user_rock_climbing_profile)=>gur_user_rock_climbing_profile.rcbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRockClimbingProfiles2:gur_user_rock_climbing_profile[];
    

   
    @OneToMany(()=>gur_user_rodeo_profile, (gur_user_rodeo_profile: gur_user_rodeo_profile)=>gur_user_rodeo_profile.rodUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRodeoProfiles:gur_user_rodeo_profile[];
    

   
    @OneToMany(()=>gur_user_rodeo_profile, (gur_user_rodeo_profile: gur_user_rodeo_profile)=>gur_user_rodeo_profile.rodCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRodeoProfiles2:gur_user_rodeo_profile[];
    

   
    @OneToMany(()=>gur_user_roller_derby_profile, (gur_user_roller_derby_profile: gur_user_roller_derby_profile)=>gur_user_roller_derby_profile.rodUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerDerbyProfiles:gur_user_roller_derby_profile[];
    

   
    @OneToMany(()=>gur_user_roller_derby_profile, (gur_user_roller_derby_profile: gur_user_roller_derby_profile)=>gur_user_roller_derby_profile.rodCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerDerbyProfiles2:gur_user_roller_derby_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_inline_profile, (gur_user_roller_hockey_inline_profile: gur_user_roller_hockey_inline_profile)=>gur_user_roller_hockey_inline_profile.rhiUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyInlineProfiles:gur_user_roller_hockey_inline_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_inline_profile, (gur_user_roller_hockey_inline_profile: gur_user_roller_hockey_inline_profile)=>gur_user_roller_hockey_inline_profile.rhiCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyInlineProfiles2:gur_user_roller_hockey_inline_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_quad_profile, (gur_user_roller_hockey_quad_profile: gur_user_roller_hockey_quad_profile)=>gur_user_roller_hockey_quad_profile.rhqUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyQuadProfiles:gur_user_roller_hockey_quad_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_quad_profile, (gur_user_roller_hockey_quad_profile: gur_user_roller_hockey_quad_profile)=>gur_user_roller_hockey_quad_profile.rhqCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyQuadProfiles2:gur_user_roller_hockey_quad_profile[];
    

   
    @OneToMany(()=>gur_user_rope_skipping_profile, (gur_user_rope_skipping_profile: gur_user_rope_skipping_profile)=>gur_user_rope_skipping_profile.rpsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRopeSkippingProfiles:gur_user_rope_skipping_profile[];
    

   
    @OneToMany(()=>gur_user_rowing_profile, (gur_user_rowing_profile: gur_user_rowing_profile)=>gur_user_rowing_profile.rowUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRowingProfiles:gur_user_rowing_profile[];
    

   
    @OneToMany(()=>gur_user_rowing_profile, (gur_user_rowing_profile: gur_user_rowing_profile)=>gur_user_rowing_profile.rowCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRowingProfiles2:gur_user_rowing_profile[];
    

   
    @OneToMany(()=>gur_user_rowing_stats, (gur_user_rowing_stats: gur_user_rowing_stats)=>gur_user_rowing_stats.rosUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRowingStatss:gur_user_rowing_stats[];
    

   
    @OneToMany(()=>gur_user_rubik_profile, (gur_user_rubik_profile: gur_user_rubik_profile)=>gur_user_rubik_profile.rubUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRubikProfiles:gur_user_rubik_profile[];
    

   
    @OneToMany(()=>gur_user_rubik_profile, (gur_user_rubik_profile: gur_user_rubik_profile)=>gur_user_rubik_profile.rubCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRubikProfiles2:gur_user_rubik_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_league_profile, (gur_user_rugby_league_profile: gur_user_rugby_league_profile)=>gur_user_rugby_league_profile.rglUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyLeagueProfiles:gur_user_rugby_league_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_league_profile, (gur_user_rugby_league_profile: gur_user_rugby_league_profile)=>gur_user_rugby_league_profile.rglCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyLeagueProfiles2:gur_user_rugby_league_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_sevens_profile, (gur_user_rugby_sevens_profile: gur_user_rugby_sevens_profile)=>gur_user_rugby_sevens_profile.rgsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbySevensProfiles:gur_user_rugby_sevens_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_sevens_profile, (gur_user_rugby_sevens_profile: gur_user_rugby_sevens_profile)=>gur_user_rugby_sevens_profile.rgsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbySevensProfiles2:gur_user_rugby_sevens_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_sevens_stats, (gur_user_rugby_sevens_stats: gur_user_rugby_sevens_stats)=>gur_user_rugby_sevens_stats.rgsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbySevensStatss:gur_user_rugby_sevens_stats[];
    

   
    @OneToMany(()=>gur_user_rugby_union_profile, (gur_user_rugby_union_profile: gur_user_rugby_union_profile)=>gur_user_rugby_union_profile.rguUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyUnionProfiles:gur_user_rugby_union_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_union_profile, (gur_user_rugby_union_profile: gur_user_rugby_union_profile)=>gur_user_rugby_union_profile.rguCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyUnionProfiles2:gur_user_rugby_union_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_union_stats, (gur_user_rugby_union_stats: gur_user_rugby_union_stats)=>gur_user_rugby_union_stats.rgsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyUnionStatss:gur_user_rugby_union_stats[];
    

   
    @OneToMany(()=>gur_user_sailing_profile, (gur_user_sailing_profile: gur_user_sailing_profile)=>gur_user_sailing_profile.saiUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSailingProfiles:gur_user_sailing_profile[];
    

   
    @OneToMany(()=>gur_user_sailing_profile, (gur_user_sailing_profile: gur_user_sailing_profile)=>gur_user_sailing_profile.saiCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurUserSailingProfiles2:gur_user_sailing_profile[];
    

   
    @OneToMany(()=>gur_user_sambo_profile, (gur_user_sambo_profile: gur_user_sambo_profile)=>gur_user_sambo_profile.samUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSamboProfiles:gur_user_sambo_profile[];
    

   
    @OneToMany(()=>gur_user_sambo_profile, (gur_user_sambo_profile: gur_user_sambo_profile)=>gur_user_sambo_profile.samCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSamboProfiles2:gur_user_sambo_profile[];
    

   
    @OneToMany(()=>gur_user_sepak_takraw_profile, (gur_user_sepak_takraw_profile: gur_user_sepak_takraw_profile)=>gur_user_sepak_takraw_profile.sepUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSepakTakrawProfiles:gur_user_sepak_takraw_profile[];
    

   
    @OneToMany(()=>gur_user_sepak_takraw_profile, (gur_user_sepak_takraw_profile: gur_user_sepak_takraw_profile)=>gur_user_sepak_takraw_profile.sepCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSepakTakrawProfiles2:gur_user_sepak_takraw_profile[];
    

   
    @OneToMany(()=>gur_user_shinty_profile, (gur_user_shinty_profile: gur_user_shinty_profile)=>gur_user_shinty_profile.shnUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShintyProfiles:gur_user_shinty_profile[];
    

   
    @OneToMany(()=>gur_user_shinty_profile, (gur_user_shinty_profile: gur_user_shinty_profile)=>gur_user_shinty_profile.shnCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShintyProfiles2:gur_user_shinty_profile[];
    

   
    @OneToMany(()=>gur_user_shooting_profile, (gur_user_shooting_profile: gur_user_shooting_profile)=>gur_user_shooting_profile.shoUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShootingProfiles:gur_user_shooting_profile[];
    

   
    @OneToMany(()=>gur_user_shooting_profile, (gur_user_shooting_profile: gur_user_shooting_profile)=>gur_user_shooting_profile.shoCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShootingProfiles2:gur_user_shooting_profile[];
    

   
    @OneToMany(()=>gur_user_shooting_stats, (gur_user_shooting_stats: gur_user_shooting_stats)=>gur_user_shooting_stats.shsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShootingStatss:gur_user_shooting_stats[];
    

   
    @OneToMany(()=>gur_user_skateboarding_profile, (gur_user_skateboarding_profile: gur_user_skateboarding_profile)=>gur_user_skateboarding_profile.skbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkateboardingProfiles:gur_user_skateboarding_profile[];
    

   
    @OneToMany(()=>gur_user_skateboarding_profile, (gur_user_skateboarding_profile: gur_user_skateboarding_profile)=>gur_user_skateboarding_profile.skbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkateboardingProfiles2:gur_user_skateboarding_profile[];
    

   
    @OneToMany(()=>gur_user_skeleton_profile, (gur_user_skeleton_profile: gur_user_skeleton_profile)=>gur_user_skeleton_profile.skeUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkeletonProfiles:gur_user_skeleton_profile[];
    

   
    @OneToMany(()=>gur_user_skeleton_profile, (gur_user_skeleton_profile: gur_user_skeleton_profile)=>gur_user_skeleton_profile.skeCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkeletonProfiles2:gur_user_skeleton_profile[];
    

   
    @OneToMany(()=>gur_user_ski_flying_profile, (gur_user_ski_flying_profile: gur_user_ski_flying_profile)=>gur_user_ski_flying_profile.skfUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkiFlyingProfiles:gur_user_ski_flying_profile[];
    

   
    @OneToMany(()=>gur_user_ski_flying_profile, (gur_user_ski_flying_profile: gur_user_ski_flying_profile)=>gur_user_ski_flying_profile.skfCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkiFlyingProfiles2:gur_user_ski_flying_profile[];
    

   
    @OneToMany(()=>gur_user_ski_jumping_profile, (gur_user_ski_jumping_profile: gur_user_ski_jumping_profile)=>gur_user_ski_jumping_profile.skjUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkiJumpingProfiles:gur_user_ski_jumping_profile[];
    

   
    @OneToMany(()=>gur_user_ski_jumping_profile, (gur_user_ski_jumping_profile: gur_user_ski_jumping_profile)=>gur_user_ski_jumping_profile.skjCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkiJumpingProfiles2:gur_user_ski_jumping_profile[];
    

   
    @OneToMany(()=>gur_user_skydiving_profile, (gur_user_skydiving_profile: gur_user_skydiving_profile)=>gur_user_skydiving_profile.skyUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkydivingProfiles:gur_user_skydiving_profile[];
    

   
    @OneToMany(()=>gur_user_skydiving_profile, (gur_user_skydiving_profile: gur_user_skydiving_profile)=>gur_user_skydiving_profile.skyCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSkydivingProfiles2:gur_user_skydiving_profile[];
    

   
    @OneToMany(()=>gur_user_snooker_profile, (gur_user_snooker_profile: gur_user_snooker_profile)=>gur_user_snooker_profile.snoUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSnookerProfiles:gur_user_snooker_profile[];
    

   
    @OneToMany(()=>gur_user_snooker_profile, (gur_user_snooker_profile: gur_user_snooker_profile)=>gur_user_snooker_profile.snoCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSnookerProfiles2:gur_user_snooker_profile[];
    

   
    @OneToMany(()=>gur_user_snowboarding_profile, (gur_user_snowboarding_profile: gur_user_snowboarding_profile)=>gur_user_snowboarding_profile.snoUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSnowboardingProfiles:gur_user_snowboarding_profile[];
    

   
    @OneToMany(()=>gur_user_snowboarding_profile, (gur_user_snowboarding_profile: gur_user_snowboarding_profile)=>gur_user_snowboarding_profile.snoCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSnowboardingProfiles2:gur_user_snowboarding_profile[];
    

   
    @OneToMany(()=>gur_user_snowmobiling_profile, (gur_user_snowmobiling_profile: gur_user_snowmobiling_profile)=>gur_user_snowmobiling_profile.snmUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSnowmobilingProfiles:gur_user_snowmobiling_profile[];
    

   
    @OneToMany(()=>gur_user_snowmobiling_profile, (gur_user_snowmobiling_profile: gur_user_snowmobiling_profile)=>gur_user_snowmobiling_profile.snmCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSnowmobilingProfiles2:gur_user_snowmobiling_profile[];
    

   
    @OneToMany(()=>gur_user_soccer_profile, (gur_user_soccer_profile: gur_user_soccer_profile)=>gur_user_soccer_profile.uspUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerProfiles:gur_user_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_soccer_profile, (gur_user_soccer_profile: gur_user_soccer_profile)=>gur_user_soccer_profile.uspCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerProfiles2:gur_user_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_soccer_stats, (gur_user_soccer_stats: gur_user_soccer_stats)=>gur_user_soccer_stats.ussUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerStatss:gur_user_soccer_stats[];
    

   
    @OneToMany(()=>gur_user_soft_tennis_profile, (gur_user_soft_tennis_profile: gur_user_soft_tennis_profile)=>gur_user_soft_tennis_profile.sotUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoftTennisProfiles:gur_user_soft_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_soft_tennis_profile, (gur_user_soft_tennis_profile: gur_user_soft_tennis_profile)=>gur_user_soft_tennis_profile.sotCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoftTennisProfiles2:gur_user_soft_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_softball_profile, (gur_user_softball_profile: gur_user_softball_profile)=>gur_user_softball_profile.sofUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoftballProfiles:gur_user_softball_profile[];
    

   
    @OneToMany(()=>gur_user_softball_profile, (gur_user_softball_profile: gur_user_softball_profile)=>gur_user_softball_profile.sofCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoftballProfiles2:gur_user_softball_profile[];
    

   
    @OneToMany(()=>gur_user_speed_skating_profile, (gur_user_speed_skating_profile: gur_user_speed_skating_profile)=>gur_user_speed_skating_profile.spsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSpeedSkatingProfiles:gur_user_speed_skating_profile[];
    

   
    @OneToMany(()=>gur_user_speed_skating_profile, (gur_user_speed_skating_profile: gur_user_speed_skating_profile)=>gur_user_speed_skating_profile.spsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSpeedSkatingProfiles2:gur_user_speed_skating_profile[];
    

   
    @OneToMany(()=>gur_user_speed_skiing_profile, (gur_user_speed_skiing_profile: gur_user_speed_skiing_profile)=>gur_user_speed_skiing_profile.spsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSpeedSkiingProfiles:gur_user_speed_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_speed_skiing_profile, (gur_user_speed_skiing_profile: gur_user_speed_skiing_profile)=>gur_user_speed_skiing_profile.spsCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSpeedSkiingProfiles2:gur_user_speed_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_sports_map, (gur_user_sports_map: gur_user_sports_map)=>gur_user_sports_map.upmUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSportsMaps:gur_user_sports_map[];
    

   
    @OneToMany(()=>gur_user_squash_profile, (gur_user_squash_profile: gur_user_squash_profile)=>gur_user_squash_profile.sqaUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashProfiles:gur_user_squash_profile[];
    

   
    @OneToMany(()=>gur_user_squash_profile, (gur_user_squash_profile: gur_user_squash_profile)=>gur_user_squash_profile.sqaCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashProfiles2:gur_user_squash_profile[];
    

   
    @OneToMany(()=>gur_user_squash_stats, (gur_user_squash_stats: gur_user_squash_stats)=>gur_user_squash_stats.sqsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashStatss:gur_user_squash_stats[];
    

   
    @OneToMany(()=>gur_user_sumo_wrestling_profile, (gur_user_sumo_wrestling_profile: gur_user_sumo_wrestling_profile)=>gur_user_sumo_wrestling_profile.sumUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSumoWrestlingProfiles:gur_user_sumo_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_sumo_wrestling_profile, (gur_user_sumo_wrestling_profile: gur_user_sumo_wrestling_profile)=>gur_user_sumo_wrestling_profile.sumCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSumoWrestlingProfiles2:gur_user_sumo_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_surfing_profile, (gur_user_surfing_profile: gur_user_surfing_profile)=>gur_user_surfing_profile.supUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSurfingProfiles:gur_user_surfing_profile[];
    

   
    @OneToMany(()=>gur_user_surfing_profile, (gur_user_surfing_profile: gur_user_surfing_profile)=>gur_user_surfing_profile.supCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSurfingProfiles2:gur_user_surfing_profile[];
    

   
    @OneToMany(()=>gur_user_swimming_profile, (gur_user_swimming_profile: gur_user_swimming_profile)=>gur_user_swimming_profile.swmUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSwimmingProfiles:gur_user_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_swimming_profile, (gur_user_swimming_profile: gur_user_swimming_profile)=>gur_user_swimming_profile.swmCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSwimmingProfiles2:gur_user_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_swimming_stats, (gur_user_swimming_stats: gur_user_swimming_stats)=>gur_user_swimming_stats.spsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSwimmingStatss:gur_user_swimming_stats[];
    

   
    @OneToMany(()=>gur_user_synchronised_swimming_profile, (gur_user_synchronised_swimming_profile: gur_user_synchronised_swimming_profile)=>gur_user_synchronised_swimming_profile.sysUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSynchronisedSwimmingProfiles:gur_user_synchronised_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_synchronised_swimming_profile, (gur_user_synchronised_swimming_profile: gur_user_synchronised_swimming_profile)=>gur_user_synchronised_swimming_profile.sysCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSynchronisedSwimmingProfiles2:gur_user_synchronised_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_table_tennis_profile, (gur_user_table_tennis_profile: gur_user_table_tennis_profile)=>gur_user_table_tennis_profile.tatUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisProfiles:gur_user_table_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_table_tennis_profile, (gur_user_table_tennis_profile: gur_user_table_tennis_profile)=>gur_user_table_tennis_profile.tatCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisProfiles2:gur_user_table_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_table_tennis_stats, (gur_user_table_tennis_stats: gur_user_table_tennis_stats)=>gur_user_table_tennis_stats.ttsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisStatss:gur_user_table_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_taekwondo_profile, (gur_user_taekwondo_profile: gur_user_taekwondo_profile)=>gur_user_taekwondo_profile.taeUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoProfiles:gur_user_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_taekwondo_profile, (gur_user_taekwondo_profile: gur_user_taekwondo_profile)=>gur_user_taekwondo_profile.taeCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoProfiles2:gur_user_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_taekwondo_stats, (gur_user_taekwondo_stats: gur_user_taekwondo_stats)=>gur_user_taekwondo_stats.tasUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoStatss:gur_user_taekwondo_stats[];
    

   
    @OneToMany(()=>gur_user_team_handball_profile, (gur_user_team_handball_profile: gur_user_team_handball_profile)=>gur_user_team_handball_profile.thbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTeamHandballProfiles:gur_user_team_handball_profile[];
    

   
    @OneToMany(()=>gur_user_team_handball_profile, (gur_user_team_handball_profile: gur_user_team_handball_profile)=>gur_user_team_handball_profile.thbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTeamHandballProfiles2:gur_user_team_handball_profile[];
    

   
    @OneToMany(()=>gur_user_ten_pin_bowling_profile, (gur_user_ten_pin_bowling_profile: gur_user_ten_pin_bowling_profile)=>gur_user_ten_pin_bowling_profile.tpbUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTenPinBowlingProfiles:gur_user_ten_pin_bowling_profile[];
    

   
    @OneToMany(()=>gur_user_ten_pin_bowling_profile, (gur_user_ten_pin_bowling_profile: gur_user_ten_pin_bowling_profile)=>gur_user_ten_pin_bowling_profile.tpbCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTenPinBowlingProfiles2:gur_user_ten_pin_bowling_profile[];
    

   
    @OneToMany(()=>gur_user_tennis_profile, (gur_user_tennis_profile: gur_user_tennis_profile)=>gur_user_tennis_profile.utpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisProfiles:gur_user_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_tennis_profile, (gur_user_tennis_profile: gur_user_tennis_profile)=>gur_user_tennis_profile.utpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisProfiles2:gur_user_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_tennis_stats, (gur_user_tennis_stats: gur_user_tennis_stats)=>gur_user_tennis_stats.utsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisStatss:gur_user_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_touringcar_profile, (gur_user_touringcar_profile: gur_user_touringcar_profile)=>gur_user_touringcar_profile.tcrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTouringcarProfiles:gur_user_touringcar_profile[];
    

   
    @OneToMany(()=>gur_user_touringcar_profile, (gur_user_touringcar_profile: gur_user_touringcar_profile)=>gur_user_touringcar_profile.tcrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTouringcarProfiles2:gur_user_touringcar_profile[];
    

   
    @OneToMany(()=>gur_user_triathlon_profile, (gur_user_triathlon_profile: gur_user_triathlon_profile)=>gur_user_triathlon_profile.triUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTriathlonProfiles:gur_user_triathlon_profile[];
    

   
    @OneToMany(()=>gur_user_triathlon_profile, (gur_user_triathlon_profile: gur_user_triathlon_profile)=>gur_user_triathlon_profile.triCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTriathlonProfiles2:gur_user_triathlon_profile[];
    

   
    @OneToMany(()=>gur_user_triathlon_stats, (gur_user_triathlon_stats: gur_user_triathlon_stats)=>gur_user_triathlon_stats.trsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTriathlonStatss:gur_user_triathlon_stats[];
    

   
    @OneToMany(()=>gur_user_tug_of_war_profile, (gur_user_tug_of_war_profile: gur_user_tug_of_war_profile)=>gur_user_tug_of_war_profile.towUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTugOfWarProfiles:gur_user_tug_of_war_profile[];
    

   
    @OneToMany(()=>gur_user_tug_of_war_profile, (gur_user_tug_of_war_profile: gur_user_tug_of_war_profile)=>gur_user_tug_of_war_profile.towCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTugOfWarProfiles2:gur_user_tug_of_war_profile[];
    

   
    @OneToMany(()=>gur_user_ultimate_profile, (gur_user_ultimate_profile: gur_user_ultimate_profile)=>gur_user_ultimate_profile.ultUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserUltimateProfiles:gur_user_ultimate_profile[];
    

   
    @OneToMany(()=>gur_user_ultimate_profile, (gur_user_ultimate_profile: gur_user_ultimate_profile)=>gur_user_ultimate_profile.ultCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserUltimateProfiles2:gur_user_ultimate_profile[];
    

   
    @OneToMany(()=>gur_user_ultra_running_profile, (gur_user_ultra_running_profile: gur_user_ultra_running_profile)=>gur_user_ultra_running_profile.utrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserUltraRunningProfiles:gur_user_ultra_running_profile[];
    

   
    @OneToMany(()=>gur_user_ultra_running_profile, (gur_user_ultra_running_profile: gur_user_ultra_running_profile)=>gur_user_ultra_running_profile.utrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserUltraRunningProfiles2:gur_user_ultra_running_profile[];
    

   
    @OneToMany(()=>gur_user_volleyball_profile, (gur_user_volleyball_profile: gur_user_volleyball_profile)=>gur_user_volleyball_profile.volUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserVolleyballProfiles:gur_user_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_volleyball_profile, (gur_user_volleyball_profile: gur_user_volleyball_profile)=>gur_user_volleyball_profile.volCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserVolleyballProfiles2:gur_user_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_volleyball_stats, (gur_user_volleyball_stats: gur_user_volleyball_stats)=>gur_user_volleyball_stats.vosUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserVolleyballStatss:gur_user_volleyball_stats[];
    

   
    @OneToMany(()=>gur_user_water_polo_profile, (gur_user_water_polo_profile: gur_user_water_polo_profile)=>gur_user_water_polo_profile.wapUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloProfiles:gur_user_water_polo_profile[];
    

   
    @OneToMany(()=>gur_user_water_polo_profile, (gur_user_water_polo_profile: gur_user_water_polo_profile)=>gur_user_water_polo_profile.wapCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloProfiles2:gur_user_water_polo_profile[];
    

   
    @OneToMany(()=>gur_user_water_polo_stats, (gur_user_water_polo_stats: gur_user_water_polo_stats)=>gur_user_water_polo_stats.wpsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloStatss:gur_user_water_polo_stats[];
    

   
    @OneToMany(()=>gur_user_water_skiing_profile, (gur_user_water_skiing_profile: gur_user_water_skiing_profile)=>gur_user_water_skiing_profile.wskUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterSkiingProfiles:gur_user_water_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_water_skiing_profile, (gur_user_water_skiing_profile: gur_user_water_skiing_profile)=>gur_user_water_skiing_profile.wskCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterSkiingProfiles2:gur_user_water_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_weightlifting_profile, (gur_user_weightlifting_profile: gur_user_weightlifting_profile)=>gur_user_weightlifting_profile.uwpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingProfiles:gur_user_weightlifting_profile[];
    

   
    @OneToMany(()=>gur_user_weightlifting_profile, (gur_user_weightlifting_profile: gur_user_weightlifting_profile)=>gur_user_weightlifting_profile.uwpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingProfiles2:gur_user_weightlifting_profile[];
    

   
    @OneToMany(()=>gur_user_weightlifting_stats, (gur_user_weightlifting_stats: gur_user_weightlifting_stats)=>gur_user_weightlifting_stats.uwsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingStatss:gur_user_weightlifting_stats[];
    

   
    @OneToMany(()=>gur_user_wheelchair_cricket_profile, (gur_user_wheelchair_cricket_profile: gur_user_wheelchair_cricket_profile)=>gur_user_wheelchair_cricket_profile.wcpUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWheelchairCricketProfiles:gur_user_wheelchair_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_wheelchair_cricket_profile, (gur_user_wheelchair_cricket_profile: gur_user_wheelchair_cricket_profile)=>gur_user_wheelchair_cricket_profile.wcpCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWheelchairCricketProfiles2:gur_user_wheelchair_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_windsurfing_profile, (gur_user_windsurfing_profile: gur_user_windsurfing_profile)=>gur_user_windsurfing_profile.winUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWindsurfingProfiles:gur_user_windsurfing_profile[];
    

   
    @OneToMany(()=>gur_user_windsurfing_profile, (gur_user_windsurfing_profile: gur_user_windsurfing_profile)=>gur_user_windsurfing_profile.winCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWindsurfingProfiles2:gur_user_windsurfing_profile[];
    

   
    @OneToMany(()=>gur_user_work_details, (gur_user_work_details: gur_user_work_details)=>gur_user_work_details.uwdUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWorkDetailss:gur_user_work_details[];
    

   
    @OneToMany(()=>gur_user_work_details, (gur_user_work_details: gur_user_work_details)=>gur_user_work_details.uwdCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWorkDetailss2:gur_user_work_details[];
    

   
    @OneToMany(()=>gur_user_wrestling_profile, (gur_user_wrestling_profile: gur_user_wrestling_profile)=>gur_user_wrestling_profile.uwrUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingProfiles:gur_user_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_wrestling_profile, (gur_user_wrestling_profile: gur_user_wrestling_profile)=>gur_user_wrestling_profile.uwrCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingProfiles2:gur_user_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_wrestling_stats, (gur_user_wrestling_stats: gur_user_wrestling_stats)=>gur_user_wrestling_stats.wrsUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingStatss:gur_user_wrestling_stats[];
    

   
    @OneToMany(()=>gur_user_wushu_profile, (gur_user_wushu_profile: gur_user_wushu_profile)=>gur_user_wushu_profile.wusUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWushuProfiles:gur_user_wushu_profile[];
    

   
    @OneToMany(()=>gur_user_wushu_profile, (gur_user_wushu_profile: gur_user_wushu_profile)=>gur_user_wushu_profile.wusCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWushuProfiles2:gur_user_wushu_profile[];
    

   
    @OneToMany(()=>gur_user_yoyo_profile, (gur_user_yoyo_profile: gur_user_yoyo_profile)=>gur_user_yoyo_profile.yoyUsr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserYoyoProfiles:gur_user_yoyo_profile[];
    

   
    @OneToMany(()=>gur_user_yoyo_profile, (gur_user_yoyo_profile: gur_user_yoyo_profile)=>gur_user_yoyo_profile.yoyCreatedbyUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserYoyoProfiles2:gur_user_yoyo_profile[];
    

   
    @OneToMany(()=>gur_users_contact_view_log, (gur_users_contact_view_log: gur_users_contact_view_log)=>gur_users_contact_view_log.cvlByUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUsersContactViewLogs:gur_users_contact_view_log[];
    

   
    @OneToMany(()=>gur_users_permissions, (gur_users_permissions: gur_users_permissions)=>gur_users_permissions.adrUser,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUsersPermissionss:gur_users_permissions[];
    
}
