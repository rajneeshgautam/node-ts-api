import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_awards_main} from "./gur_matik_knowhow_awards_main";


@Entity("gur_matik_knowhow_awards_main_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_awards_main_ls_ibfk_1",["wmlAwm",])
export class gur_matik_knowhow_awards_main_ls {

   
    @ManyToOne(()=>gur_matik_knowhow_awards_main, (gur_matik_knowhow_awards_main: gur_matik_knowhow_awards_main)=>gur_matik_knowhow_awards_main.gurMatikKnowhowAwardsMainLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wml_awm_id'})
    wmlAwm:gur_matik_knowhow_awards_main | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wml_title"
        })
    wml_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wml_description"
        })
    wml_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"wml_lng_code"
        })
    wml_lng_code:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wml_meta_title"
        })
    wml_meta_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wml_meta_description"
        })
    wml_meta_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"wml_meta_keywords"
        })
    wml_meta_keywords:string | null;
        
}
