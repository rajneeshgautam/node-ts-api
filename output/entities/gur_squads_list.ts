import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_squads_group} from "./gur_squads_group";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_squads_list" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["sqlUty",])
@Index("sqd_createdby_user",["sql_createdby_user",])
@Index("sql_sqd_id",["sqlSqd",])
@Index("sql_usr_id",["sqlUsr",])
export class gur_squads_list {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sql_id"
        })
    sql_id:number;
        

   
    @ManyToOne(()=>gur_squads_group, (gur_squads_group: gur_squads_group)=>gur_squads_group.gurSquadsLists,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sql_sqd_id'})
    sqlSqd:gur_squads_group | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSquadsLists,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sql_uty_id'})
    sqlUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSquadsLists,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sql_usr_id'})
    sqlUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"sql_desc"
        })
    sql_desc:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"sql_createdby_user"
        })
    sql_createdby_user:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sql_createdby"
        })
    sql_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sql_modifiedby"
        })
    sql_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sql_modifiedtime"
        })
    sql_modifiedtime:Date;
        
}
