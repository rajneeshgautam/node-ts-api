import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_gymnastics_stats" ,{schema:"sportsmatik_local" } )
@Index("ags_usr_id",["gysUsr",])
@Index("ags_ucp_id",["gysUcp",])
@Index("ags_event",["gysEvent",])
@Index("ags_rank",["gysRank",])
export class gur_user_gymnastics_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"gys_id"
        })
    gys_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGymnasticsStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gys_usr_id'})
    gysUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserGymnasticsStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gys_ucp_id'})
    gysUcp:gur_user_competition_played | null;


    @Column("enum",{ 
        nullable:false,
        enum:["artistics","rhythmic","trampoline","aerobics","acrobatics"],
        name:"gys_mode"
        })
    gys_mode:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserGymnasticsStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gys_event'})
    gysEvent:gur_value_list | null;


    @Column("double",{ 
        nullable:false,
        default: () => "'0'",
        precision:22,
        name:"gys_points"
        })
    gys_points:number;
        

    @Column("text",{ 
        nullable:true,
        name:"gys_all_round_breakdown_points"
        })
    gys_all_round_breakdown_points:string | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserGymnasticsStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gys_rank'})
    gysRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"gys_modifiedtime"
        })
    gys_modifiedtime:Date;
        
}
