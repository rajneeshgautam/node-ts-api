import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_plan_orders} from "./gur_plan_orders";
import {gur_plan_categories} from "./gur_plan_categories";
import {gur_plans} from "./gur_plans";
import {gur_corporates_sport_map} from "./gur_corporates_sport_map";
import {gur_plan_usage_log} from "./gur_plan_usage_log";
import {gur_sports_official_user_profile} from "./gur_sports_official_user_profile";
import {gur_user_plans_addons} from "./gur_user_plans_addons";
import {gur_user_sports_map} from "./gur_user_sports_map";


@Entity("gur_user_plans" ,{schema:"sportsmatik_local" } )
@Index("evt_uty_id",["pchUty",])
@Index("pch_cat_id",["pchPlc",])
@Index("pch_cat_id_2",["pchPlc",])
@Index("pch_prp_id",["pchPln",])
@Index("gur_user_plans_ibfk_2",["pchOrder",])
export class gur_user_plans {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"pch_id"
        })
    pch_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserPlanss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_uty_id'})
    pchUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"pch_user_id"
        })
    pch_user_id:number;
        

   
    @ManyToOne(()=>gur_plan_orders, (gur_plan_orders: gur_plan_orders)=>gur_plan_orders.gurUserPlanss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_order_id'})
    pchOrder:gur_plan_orders | null;


   
    @ManyToOne(()=>gur_plan_categories, (gur_plan_categories: gur_plan_categories)=>gur_plan_categories.gurUserPlanss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_plc_id'})
    pchPlc:gur_plan_categories | null;


   
    @ManyToOne(()=>gur_plans, (gur_plans: gur_plans)=>gur_plans.gurUserPlanss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pch_pln_id'})
    pchPln:gur_plans | null;


    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"pch_overwrite"
        })
    pch_overwrite:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"pch_credits_left"
        })
    pch_credits_left:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"pch_total_credits"
        })
    pch_total_credits:number;
        

    @Column("date",{ 
        nullable:false,
        name:"pch_start_date"
        })
    pch_start_date:string;
        

    @Column("date",{ 
        nullable:true,
        name:"pch_end_date"
        })
    pch_end_date:string | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'free'",
        enum:["free","buy"],
        name:"pch_avail_type"
        })
    pch_avail_type:string;
        

   
    @OneToMany(()=>gur_corporates_sport_map, (gur_corporates_sport_map: gur_corporates_sport_map)=>gur_corporates_sport_map.csmPch,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatesSportMaps:gur_corporates_sport_map[];
    

   
    @OneToMany(()=>gur_plan_usage_log, (gur_plan_usage_log: gur_plan_usage_log)=>gur_plan_usage_log.pulPch,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurPlanUsageLogs:gur_plan_usage_log[];
    

   
    @OneToMany(()=>gur_sports_official_user_profile, (gur_sports_official_user_profile: gur_sports_official_user_profile)=>gur_sports_official_user_profile.somPch,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurSportsOfficialUserProfiles:gur_sports_official_user_profile[];
    

   
    @OneToMany(()=>gur_user_plans_addons, (gur_user_plans_addons: gur_user_plans_addons)=>gur_user_plans_addons.upaPch,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPlansAddonss:gur_user_plans_addons[];
    

   
    @OneToMany(()=>gur_user_sports_map, (gur_user_sports_map: gur_user_sports_map)=>gur_user_sports_map.upmPch,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSportsMaps:gur_user_sports_map[];
    
}
