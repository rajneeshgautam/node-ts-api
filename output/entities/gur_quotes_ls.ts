import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quotes} from "./gur_quotes";
import {gur_languages} from "./gur_languages";


@Entity("gur_quotes_ls" ,{schema:"sportsmatik_local" } )
@Index("qol_quo_id",["qolQuo",])
@Index("qol_lng_code",["qolLngCode",])
export class gur_quotes_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"qol_id"
        })
    qol_id:number;
        

   
    @ManyToOne(()=>gur_quotes, (gur_quotes: gur_quotes)=>gur_quotes.gurQuotesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qol_quo_id'})
    qolQuo:gur_quotes | null;


    @Column("text",{ 
        nullable:true,
        name:"qol_description"
        })
    qol_description:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurQuotesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qol_lng_code'})
    qolLngCode:gur_languages | null;

}
