import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_competition_sports_map} from "./gur_competition_sports_map";
import {gur_corporates_sport_map} from "./gur_corporates_sport_map";
import {gur_did_you_know} from "./gur_did_you_know";
import {gur_institutional_staff_map} from "./gur_institutional_staff_map";
import {gur_institutional_teams} from "./gur_institutional_teams";
import {gur_institutional_trophies} from "./gur_institutional_trophies";
import {gur_managed_portfolio} from "./gur_managed_portfolio";
import {gur_recruitment_history} from "./gur_recruitment_history";
import {gur_sports_attributes_categories} from "./gur_sports_attributes_categories";
import {gur_sports_ls} from "./gur_sports_ls";
import {gur_sports_official_user_profile} from "./gur_sports_official_user_profile";
import {gur_sports_usage_history} from "./gur_sports_usage_history";
import {gur_sports_vacancies} from "./gur_sports_vacancies";
import {gur_squads_group} from "./gur_squads_group";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_user_sports_map} from "./gur_user_sports_map";
import {gur_vendors_sports_map} from "./gur_vendors_sports_map";
import {gur_sports_categories} from "./gur_sports_categories";


@Entity("gur_sports" ,{schema:"sportsmatik_local" } )
@Index("spo_slug",["spo_slug",],{unique:true})
export class gur_sports {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"spo_id"
        })
    spo_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"spo_slug"
        })
    spo_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_para"
        })
    spo_para:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_available_stats"
        })
    spo_available_stats:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"spo_sort_order"
        })
    spo_sort_order:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_active_recruitment"
        })
    spo_active_recruitment:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"spo_rank_taken_from"
        })
    spo_rank_taken_from:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"spo_is_popular"
        })
    spo_is_popular:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"spo_popular_in_cnt"
        })
    spo_popular_in_cnt:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_primary_photo"
        })
    spo_primary_photo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"spo_createdby"
        })
    spo_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"spo_modifiedby"
        })
    spo_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"spo_modifiedtime"
        })
    spo_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_competition_sports_map, (gur_competition_sports_map: gur_competition_sports_map)=>gur_competition_sports_map.csmSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionSportsMaps:gur_competition_sports_map[];
    

   
    @OneToMany(()=>gur_corporates_sport_map, (gur_corporates_sport_map: gur_corporates_sport_map)=>gur_corporates_sport_map.csmSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCorporatesSportMaps:gur_corporates_sport_map[];
    

   
    @OneToMany(()=>gur_did_you_know, (gur_did_you_know: gur_did_you_know)=>gur_did_you_know.dykSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurDidYouKnows:gur_did_you_know[];
    

   
    @OneToMany(()=>gur_institutional_staff_map, (gur_institutional_staff_map: gur_institutional_staff_map)=>gur_institutional_staff_map.ismSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalStaffMaps:gur_institutional_staff_map[];
    

   
    @OneToMany(()=>gur_institutional_teams, (gur_institutional_teams: gur_institutional_teams)=>gur_institutional_teams.itnSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTeamss:gur_institutional_teams[];
    

   
    @OneToMany(()=>gur_institutional_trophies, (gur_institutional_trophies: gur_institutional_trophies)=>gur_institutional_trophies.intSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInstitutionalTrophiess:gur_institutional_trophies[];
    

   
    @OneToMany(()=>gur_managed_portfolio, (gur_managed_portfolio: gur_managed_portfolio)=>gur_managed_portfolio.mhtSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurManagedPortfolios:gur_managed_portfolio[];
    

   
    @OneToMany(()=>gur_recruitment_history, (gur_recruitment_history: gur_recruitment_history)=>gur_recruitment_history.rehSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurRecruitmentHistorys:gur_recruitment_history[];
    

   
    @OneToMany(()=>gur_sports_attributes_categories, (gur_sports_attributes_categories: gur_sports_attributes_categories)=>gur_sports_attributes_categories.sacSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsAttributesCategoriess:gur_sports_attributes_categories[];
    

   
    @OneToMany(()=>gur_sports_ls, (gur_sports_ls: gur_sports_ls)=>gur_sports_ls.splSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsLss:gur_sports_ls[];
    

   
    @OneToMany(()=>gur_sports_official_user_profile, (gur_sports_official_user_profile: gur_sports_official_user_profile)=>gur_sports_official_user_profile.somSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsOfficialUserProfiles:gur_sports_official_user_profile[];
    

   
    @OneToMany(()=>gur_sports_usage_history, (gur_sports_usage_history: gur_sports_usage_history)=>gur_sports_usage_history.suhSport,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsUsageHistorys:gur_sports_usage_history[];
    

   
    @OneToMany(()=>gur_sports_vacancies, (gur_sports_vacancies: gur_sports_vacancies)=>gur_sports_vacancies.upvSport,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsVacanciess:gur_sports_vacancies[];
    

   
    @OneToMany(()=>gur_squads_group, (gur_squads_group: gur_squads_group)=>gur_squads_group.sqdSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSquadsGroups:gur_squads_group[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_sports_map, (gur_user_sports_map: gur_user_sports_map)=>gur_user_sports_map.upmSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSportsMaps:gur_user_sports_map[];
    

   
    @OneToMany(()=>gur_vendors_sports_map, (gur_vendors_sports_map: gur_vendors_sports_map)=>gur_vendors_sports_map.vsmSpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorsSportsMaps:gur_vendors_sports_map[];
    

   
    @ManyToMany(()=>gur_sports_categories, (gur_sports_categories: gur_sports_categories)=>gur_sports_categories.gurSportss)
    gurSportsCategoriess:gur_sports_categories[];
    
}
