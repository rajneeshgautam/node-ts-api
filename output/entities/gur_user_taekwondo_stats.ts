import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_taekwondo_stats" ,{schema:"sportsmatik_local" } )
@Index("tas_usr_id",["tasUsr",])
@Index("tas_ucp_id",["tasUcp",])
@Index("tas_weight_event",["tasWeightEvent",])
@Index("tas_rank",["tasRank",])
export class gur_user_taekwondo_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"tas_id"
        })
    tas_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTaekwondoStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tas_usr_id'})
    tasUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserTaekwondoStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tas_ucp_id'})
    tasUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTaekwondoStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tas_weight_event'})
    tasWeightEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"tas_matches_played"
        })
    tas_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"tas_matches_win"
        })
    tas_matches_win:number;
        

    @Column("int",{ 
        nullable:false,
        name:"tas_matches_loss"
        })
    tas_matches_loss:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTaekwondoStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tas_rank'})
    tasRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tas_modifiedtime"
        })
    tas_modifiedtime:Date;
        
}
