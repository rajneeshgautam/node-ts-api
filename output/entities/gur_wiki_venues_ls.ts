import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_languages} from "./gur_languages";


@Entity("gur_wiki_venues_ls" ,{schema:"sportsmatik_local" } )
@Index("wvl_wvn_id",["wvlWvn",])
@Index("wvl_lng_code",["wvlLngCode",])
export class gur_wiki_venues_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wvl_id"
        })
    wvl_id:string;
        

   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurWikiVenuesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvl_wvn_id'})
    wvlWvn:gur_wiki_venues | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wvl_name"
        })
    wvl_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wvl_intro_desc"
        })
    wvl_intro_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wvl_short_desc"
        })
    wvl_short_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvl_meta_title"
        })
    wvl_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvl_meta_desc"
        })
    wvl_meta_desc:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wvl_records"
        })
    wvl_records:string | null;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurWikiVenuesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvl_lng_code'})
    wvlLngCode:gur_languages | null;

}
