import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_karate_stats" ,{schema:"sportsmatik_local" } )
@Index("kas_usr_id",["kasUsr",])
@Index("kas_ucp_id",["kasUcp",])
@Index("kas_weight_event",["kasWeightEvent",])
@Index("kas_rank",["kasRank",])
export class gur_user_karate_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"kas_id"
        })
    kas_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKarateStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kas_usr_id'})
    kasUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserKarateStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kas_ucp_id'})
    kasUcp:gur_user_competition_played | null;


    @Column("enum",{ 
        nullable:false,
        enum:["kata","kumite"],
        name:"kas_type"
        })
    kas_type:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKarateStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kas_weight_event'})
    kasWeightEvent:gur_value_list | null;


    @Column("int",{ 
        nullable:false,
        name:"kas_matches_played"
        })
    kas_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"kas_matches_won"
        })
    kas_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"kas_matches_lost"
        })
    kas_matches_lost:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserKarateStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kas_rank'})
    kasRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kas_modifiedtime"
        })
    kas_modifiedtime:Date;
        
}
