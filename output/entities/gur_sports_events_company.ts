import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_sports_events_company" ,{schema:"sportsmatik_local" } )
@Index("clb_phone1",["sec_phone1",],{unique:true})
@Index("clb_phone2",["sec_phone2",],{unique:true})
@Index("smc_gst_number",["sec_gst_number",],{unique:true})
@Index("clb_createdby_user",["sec_createdby_user",])
@Index("clb_country",["sec_country",])
@Index("clb_usr_id",["secUsr",])
@Index("clb_dis_id",["sec_dis_id",])
@Index("clb_sts_id",["sec_sts_id",])
@Index("smc_name",["sec_name",])
export class gur_sports_events_company {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sec_id"
        })
    sec_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sec_slug"
        })
    sec_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sec_name"
        })
    sec_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsEventsCompanys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sec_usr_id'})
    secUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"sec_address"
        })
    sec_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sec_address2"
        })
    sec_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sec_pincode"
        })
    sec_pincode:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"sec_country"
        })
    sec_country:number;
        

    @Column("int",{ 
        nullable:true,
        name:"sec_dis_id"
        })
    sec_dis_id:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sec_sts_id"
        })
    sec_sts_id:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"sec_address_google_map"
        })
    sec_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sec_email"
        })
    sec_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sec_email_verified"
        })
    sec_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sec_email_verify_time"
        })
    sec_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"sec_phone1"
        })
    sec_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sec_phone1_verified"
        })
    sec_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sec_phone1_otp"
        })
    sec_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sec_phone1_otp_time"
        })
    sec_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"sec_phone2"
        })
    sec_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sec_phone2_verified"
        })
    sec_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sec_phone2_otp"
        })
    sec_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sec_phone2_otp_time"
        })
    sec_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"sec_status"
        })
    sec_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sec_profile_pic"
        })
    sec_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sec_timeline_pic"
        })
    sec_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sec_intro_status"
        })
    sec_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"sec_profile_summary"
        })
    sec_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sec_founded_year"
        })
    sec_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sec_founded_month"
        })
    sec_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sec_last_login_date"
        })
    sec_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sec_last_login_details"
        })
    sec_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"sec_intro_modifiedtime"
        })
    sec_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sec_profile_pic_modifiedtime"
        })
    sec_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sec_free_sports"
        })
    sec_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sec_free_sub_users"
        })
    sec_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"sec_gst_number"
        })
    sec_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sec_viewable"
        })
    sec_viewable:boolean;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"sec_createdby_user"
        })
    sec_createdby_user:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sec_createdby"
        })
    sec_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sec_modifiedby"
        })
    sec_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sec_modifiedtime"
        })
    sec_modifiedtime:Date;
        
}
