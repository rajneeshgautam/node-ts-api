import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list_master} from "./gur_value_list_master";
import {gur_competitions} from "./gur_competitions";
import {gur_contact_view_history} from "./gur_contact_view_history";
import {gur_educational_courses} from "./gur_educational_courses";
import {gur_feedback} from "./gur_feedback";
import {gur_health_injuries_titles} from "./gur_health_injuries_titles";
import {gur_journalist_writer_portfolio} from "./gur_journalist_writer_portfolio";
import {gur_law_ca_services_categories} from "./gur_law_ca_services_categories";
import {gur_notifications} from "./gur_notifications";
import {gur_sports_personalities} from "./gur_sports_personalities";
import {gur_user_aikido_profile} from "./gur_user_aikido_profile";
import {gur_user_air_racing_profile} from "./gur_user_air_racing_profile";
import {gur_user_american_rules_football_profile} from "./gur_user_american_rules_football_profile";
import {gur_user_archery_stats} from "./gur_user_archery_stats";
import {gur_user_athletics_stats} from "./gur_user_athletics_stats";
import {gur_user_australian_rules_football_profile} from "./gur_user_australian_rules_football_profile";
import {gur_user_badminton_stats} from "./gur_user_badminton_stats";
import {gur_user_bandy_profile} from "./gur_user_bandy_profile";
import {gur_user_baseball_profile} from "./gur_user_baseball_profile";
import {gur_user_basketball_profile} from "./gur_user_basketball_profile";
import {gur_user_basque_pelota_profile} from "./gur_user_basque_pelota_profile";
import {gur_user_blind_cricket_profile} from "./gur_user_blind_cricket_profile";
import {gur_user_boxing_stats} from "./gur_user_boxing_stats";
import {gur_user_canadian_football_profile} from "./gur_user_canadian_football_profile";
import {gur_user_chess_stats} from "./gur_user_chess_stats";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_user_cricket_profile} from "./gur_user_cricket_profile";
import {gur_user_curling_profile} from "./gur_user_curling_profile";
import {gur_user_cycling_stats} from "./gur_user_cycling_stats";
import {gur_user_diving_profile} from "./gur_user_diving_profile";
import {gur_user_diving_stats} from "./gur_user_diving_stats";
import {gur_user_field_hockey_profile} from "./gur_user_field_hockey_profile";
import {gur_user_figure_skating_stats} from "./gur_user_figure_skating_stats";
import {gur_user_floorball_profile} from "./gur_user_floorball_profile";
import {gur_user_futsal_profile} from "./gur_user_futsal_profile";
import {gur_user_gaelic_football_profile} from "./gur_user_gaelic_football_profile";
import {gur_user_golf_stats} from "./gur_user_golf_stats";
import {gur_user_gymnastics_stats} from "./gur_user_gymnastics_stats";
import {gur_user_hurling_profile} from "./gur_user_hurling_profile";
import {gur_user_ice_hockey_profile} from "./gur_user_ice_hockey_profile";
import {gur_user_judo_profile} from "./gur_user_judo_profile";
import {gur_user_judo_stats} from "./gur_user_judo_stats";
import {gur_user_jujitsu_profile} from "./gur_user_jujitsu_profile";
import {gur_user_kabaddi_profile} from "./gur_user_kabaddi_profile";
import {gur_user_karate_profile} from "./gur_user_karate_profile";
import {gur_user_karate_stats} from "./gur_user_karate_stats";
import {gur_user_kickboxing_profile} from "./gur_user_kickboxing_profile";
import {gur_user_kiteboarding_profile} from "./gur_user_kiteboarding_profile";
import {gur_user_korfball_profile} from "./gur_user_korfball_profile";
import {gur_user_lacrosse_profile} from "./gur_user_lacrosse_profile";
import {gur_user_modern_pentathlon_stats} from "./gur_user_modern_pentathlon_stats";
import {gur_user_netball_profile} from "./gur_user_netball_profile";
import {gur_user_para_alpine_skiing_profile} from "./gur_user_para_alpine_skiing_profile";
import {gur_user_para_archery_profile} from "./gur_user_para_archery_profile";
import {gur_user_para_athletics_profile} from "./gur_user_para_athletics_profile";
import {gur_user_para_badminton_profile} from "./gur_user_para_badminton_profile";
import {gur_user_para_beach_volleyball_profile} from "./gur_user_para_beach_volleyball_profile";
import {gur_user_para_biathlon_profile} from "./gur_user_para_biathlon_profile";
import {gur_user_para_boccia_profile} from "./gur_user_para_boccia_profile";
import {gur_user_para_canoe_profile} from "./gur_user_para_canoe_profile";
import {gur_user_para_cross_country_skiing_profile} from "./gur_user_para_cross_country_skiing_profile";
import {gur_user_para_curling_profile} from "./gur_user_para_curling_profile";
import {gur_user_para_cycling_profile} from "./gur_user_para_cycling_profile";
import {gur_user_para_dance_profile} from "./gur_user_para_dance_profile";
import {gur_user_para_equestrian_profile} from "./gur_user_para_equestrian_profile";
import {gur_user_para_goalball_profile} from "./gur_user_para_goalball_profile";
import {gur_user_para_ice_sledge_hockey_profile} from "./gur_user_para_ice_sledge_hockey_profile";
import {gur_user_para_judo_profile} from "./gur_user_para_judo_profile";
import {gur_user_para_powerlifting_profile} from "./gur_user_para_powerlifting_profile";
import {gur_user_para_rowing_profile} from "./gur_user_para_rowing_profile";
import {gur_user_para_snowboarding_profile} from "./gur_user_para_snowboarding_profile";
import {gur_user_para_soccer_profile} from "./gur_user_para_soccer_profile";
import {gur_user_para_swimming_profile} from "./gur_user_para_swimming_profile";
import {gur_user_para_table_tennis_profile} from "./gur_user_para_table_tennis_profile";
import {gur_user_para_taekwondo_profile} from "./gur_user_para_taekwondo_profile";
import {gur_user_para_triathlon_profile} from "./gur_user_para_triathlon_profile";
import {gur_user_para_volleyball_profile} from "./gur_user_para_volleyball_profile";
import {gur_user_para_wheelchair_basketball_profile} from "./gur_user_para_wheelchair_basketball_profile";
import {gur_user_para_wheelchair_fencing_profile} from "./gur_user_para_wheelchair_fencing_profile";
import {gur_user_para_wheelchair_rugby_profile} from "./gur_user_para_wheelchair_rugby_profile";
import {gur_user_para_wheelchair_tennis_profile} from "./gur_user_para_wheelchair_tennis_profile";
import {gur_user_pesapallo_profile} from "./gur_user_pesapallo_profile";
import {gur_user_polo_profile} from "./gur_user_polo_profile";
import {gur_user_powerlifting_profile} from "./gur_user_powerlifting_profile";
import {gur_user_powerlifting_stats} from "./gur_user_powerlifting_stats";
import {gur_user_qualifications} from "./gur_user_qualifications";
import {gur_user_rafting_profile} from "./gur_user_rafting_profile";
import {gur_user_roller_derby_profile} from "./gur_user_roller_derby_profile";
import {gur_user_roller_hockey_inline_profile} from "./gur_user_roller_hockey_inline_profile";
import {gur_user_roller_hockey_quad_profile} from "./gur_user_roller_hockey_quad_profile";
import {gur_user_rowing_stats} from "./gur_user_rowing_stats";
import {gur_user_rugby_league_profile} from "./gur_user_rugby_league_profile";
import {gur_user_rugby_sevens_profile} from "./gur_user_rugby_sevens_profile";
import {gur_user_rugby_union_profile} from "./gur_user_rugby_union_profile";
import {gur_user_sambo_profile} from "./gur_user_sambo_profile";
import {gur_user_sepak_takraw_profile} from "./gur_user_sepak_takraw_profile";
import {gur_user_shinty_profile} from "./gur_user_shinty_profile";
import {gur_user_shooting_stats} from "./gur_user_shooting_stats";
import {gur_user_soccer_profile} from "./gur_user_soccer_profile";
import {gur_user_softball_profile} from "./gur_user_softball_profile";
import {gur_user_squash_profile} from "./gur_user_squash_profile";
import {gur_user_squash_stats} from "./gur_user_squash_stats";
import {gur_user_sumo_wrestling_profile} from "./gur_user_sumo_wrestling_profile";
import {gur_user_surfing_profile} from "./gur_user_surfing_profile";
import {gur_user_swimming_stats} from "./gur_user_swimming_stats";
import {gur_user_table_tennis_profile} from "./gur_user_table_tennis_profile";
import {gur_user_table_tennis_stats} from "./gur_user_table_tennis_stats";
import {gur_user_taekwondo_profile} from "./gur_user_taekwondo_profile";
import {gur_user_taekwondo_stats} from "./gur_user_taekwondo_stats";
import {gur_user_team_handball_profile} from "./gur_user_team_handball_profile";
import {gur_user_tennis_profile} from "./gur_user_tennis_profile";
import {gur_user_tennis_stats} from "./gur_user_tennis_stats";
import {gur_user_triathlon_stats} from "./gur_user_triathlon_stats";
import {gur_user_ultimate_profile} from "./gur_user_ultimate_profile";
import {gur_user_volleyball_profile} from "./gur_user_volleyball_profile";
import {gur_user_water_polo_profile} from "./gur_user_water_polo_profile";
import {gur_user_weightlifting_profile} from "./gur_user_weightlifting_profile";
import {gur_user_weightlifting_stats} from "./gur_user_weightlifting_stats";
import {gur_user_wheelchair_cricket_profile} from "./gur_user_wheelchair_cricket_profile";
import {gur_user_wrestling_profile} from "./gur_user_wrestling_profile";
import {gur_user_wrestling_stats} from "./gur_user_wrestling_stats";
import {gur_user_wushu_profile} from "./gur_user_wushu_profile";
import {gur_users} from "./gur_users";
import {gur_vendors} from "./gur_vendors";
import {gur_world_events_players_bkup} from "./gur_world_events_players_bkup";


@Entity("gur_value_list" ,{schema:"sportsmatik_local" } )
@Index("vlc_vlm_id",["vlcVlm",])
export class gur_value_list {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"vlc_id"
        })
    vlc_id:number;
        

   
    @ManyToOne(()=>gur_value_list_master, (gur_value_list_master: gur_value_list_master)=>gur_value_list_master.gurValueLists,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'vlc_vlm_id'})
    vlcVlm:gur_value_list_master | null;


    @Column("varchar",{ 
        nullable:false,
        name:"vlc_value"
        })
    vlc_value:string;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"vlc_sort_order"
        })
    vlc_sort_order:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vlc_abbreviation_value"
        })
    vlc_abbreviation_value:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vlc_image"
        })
    vlc_image:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"vlc_team_event"
        })
    vlc_team_event:boolean;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","b"],
        name:"vlc_gender"
        })
    vlc_gender:string | null;
        

    @Column("longtext",{ 
        nullable:true,
        name:"vlc_description"
        })
    vlc_description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"vlc_createdby"
        })
    vlc_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"vlc_modifiedby"
        })
    vlc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"vlc_modifiedtime"
        })
    vlc_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_competitions, (gur_competitions: gur_competitions)=>gur_competitions.comCricketFormat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionss:gur_competitions[];
    

   
    @OneToMany(()=>gur_contact_view_history, (gur_contact_view_history: gur_contact_view_history)=>gur_contact_view_history.chrContactVia,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurContactViewHistorys:gur_contact_view_history[];
    

   
    @OneToMany(()=>gur_educational_courses, (gur_educational_courses: gur_educational_courses)=>gur_educational_courses.corEduType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurEducationalCoursess:gur_educational_courses[];
    

   
    @OneToMany(()=>gur_feedback, (gur_feedback: gur_feedback)=>gur_feedback.fedMode,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurFeedbacks:gur_feedback[];
    

   
    @OneToMany(()=>gur_health_injuries_titles, (gur_health_injuries_titles: gur_health_injuries_titles)=>gur_health_injuries_titles.hwtBodyPart,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurHealthInjuriesTitless:gur_health_injuries_titles[];
    

   
    @OneToMany(()=>gur_journalist_writer_portfolio, (gur_journalist_writer_portfolio: gur_journalist_writer_portfolio)=>gur_journalist_writer_portfolio.jwpType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJournalistWriterPortfolios:gur_journalist_writer_portfolio[];
    

   
    @OneToMany(()=>gur_law_ca_services_categories, (gur_law_ca_services_categories: gur_law_ca_services_categories)=>gur_law_ca_services_categories.lcsType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurLawCaServicesCategoriess:gur_law_ca_services_categories[];
    

   
    @OneToMany(()=>gur_notifications, (gur_notifications: gur_notifications)=>gur_notifications.notCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurNotificationss:gur_notifications[];
    

   
    @OneToMany(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.wepGrade,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiess:gur_sports_personalities[];
    

   
    @OneToMany(()=>gur_user_aikido_profile, (gur_user_aikido_profile: gur_user_aikido_profile)=>gur_user_aikido_profile.aikDegree,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAikidoProfiles:gur_user_aikido_profile[];
    

   
    @OneToMany(()=>gur_user_aikido_profile, (gur_user_aikido_profile: gur_user_aikido_profile)=>gur_user_aikido_profile.aikDegreeBelt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAikidoProfiles2:gur_user_aikido_profile[];
    

   
    @OneToMany(()=>gur_user_air_racing_profile, (gur_user_air_racing_profile: gur_user_air_racing_profile)=>gur_user_air_racing_profile.airClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAirRacingProfiles:gur_user_air_racing_profile[];
    

   
    @OneToMany(()=>gur_user_american_rules_football_profile, (gur_user_american_rules_football_profile: gur_user_american_rules_football_profile)=>gur_user_american_rules_football_profile.arfPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAmericanRulesFootballProfiles:gur_user_american_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_american_rules_football_profile, (gur_user_american_rules_football_profile: gur_user_american_rules_football_profile)=>gur_user_american_rules_football_profile.arfPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAmericanRulesFootballProfiles2:gur_user_american_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_archery_stats, (gur_user_archery_stats: gur_user_archery_stats)=>gur_user_archery_stats.arsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArcheryStatss:gur_user_archery_stats[];
    

   
    @OneToMany(()=>gur_user_archery_stats, (gur_user_archery_stats: gur_user_archery_stats)=>gur_user_archery_stats.arsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArcheryStatss2:gur_user_archery_stats[];
    

   
    @OneToMany(()=>gur_user_athletics_stats, (gur_user_athletics_stats: gur_user_athletics_stats)=>gur_user_athletics_stats.atsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAthleticsStatss:gur_user_athletics_stats[];
    

   
    @OneToMany(()=>gur_user_athletics_stats, (gur_user_athletics_stats: gur_user_athletics_stats)=>gur_user_athletics_stats.atsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAthleticsStatss2:gur_user_athletics_stats[];
    

   
    @OneToMany(()=>gur_user_australian_rules_football_profile, (gur_user_australian_rules_football_profile: gur_user_australian_rules_football_profile)=>gur_user_australian_rules_football_profile.arfPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAustralianRulesFootballProfiles:gur_user_australian_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_australian_rules_football_profile, (gur_user_australian_rules_football_profile: gur_user_australian_rules_football_profile)=>gur_user_australian_rules_football_profile.arfPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAustralianRulesFootballProfiles2:gur_user_australian_rules_football_profile[];
    

   
    @OneToMany(()=>gur_user_badminton_stats, (gur_user_badminton_stats: gur_user_badminton_stats)=>gur_user_badminton_stats.bdsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBadmintonStatss:gur_user_badminton_stats[];
    

   
    @OneToMany(()=>gur_user_badminton_stats, (gur_user_badminton_stats: gur_user_badminton_stats)=>gur_user_badminton_stats.bdsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBadmintonStatss2:gur_user_badminton_stats[];
    

   
    @OneToMany(()=>gur_user_bandy_profile, (gur_user_bandy_profile: gur_user_bandy_profile)=>gur_user_bandy_profile.banPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBandyProfiles:gur_user_bandy_profile[];
    

   
    @OneToMany(()=>gur_user_bandy_profile, (gur_user_bandy_profile: gur_user_bandy_profile)=>gur_user_bandy_profile.banPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBandyProfiles2:gur_user_bandy_profile[];
    

   
    @OneToMany(()=>gur_user_baseball_profile, (gur_user_baseball_profile: gur_user_baseball_profile)=>gur_user_baseball_profile.basPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBaseballProfiles:gur_user_baseball_profile[];
    

   
    @OneToMany(()=>gur_user_baseball_profile, (gur_user_baseball_profile: gur_user_baseball_profile)=>gur_user_baseball_profile.basPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBaseballProfiles2:gur_user_baseball_profile[];
    

   
    @OneToMany(()=>gur_user_basketball_profile, (gur_user_basketball_profile: gur_user_basketball_profile)=>gur_user_basketball_profile.basPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballProfiles:gur_user_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_basketball_profile, (gur_user_basketball_profile: gur_user_basketball_profile)=>gur_user_basketball_profile.basPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballProfiles2:gur_user_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_basketball_profile, (gur_user_basketball_profile: gur_user_basketball_profile)=>gur_user_basketball_profile.basShoots,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballProfiles3:gur_user_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_basque_pelota_profile, (gur_user_basque_pelota_profile: gur_user_basque_pelota_profile)=>gur_user_basque_pelota_profile.bplPositions,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasquePelotaProfiles:gur_user_basque_pelota_profile[];
    

   
    @OneToMany(()=>gur_user_blind_cricket_profile, (gur_user_blind_cricket_profile: gur_user_blind_cricket_profile)=>gur_user_blind_cricket_profile.bcpPlayerType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBlindCricketProfiles:gur_user_blind_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_boxing_stats, (gur_user_boxing_stats: gur_user_boxing_stats)=>gur_user_boxing_stats.bosWeightCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBoxingStatss:gur_user_boxing_stats[];
    

   
    @OneToMany(()=>gur_user_boxing_stats, (gur_user_boxing_stats: gur_user_boxing_stats)=>gur_user_boxing_stats.bosRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBoxingStatss2:gur_user_boxing_stats[];
    

   
    @OneToMany(()=>gur_user_canadian_football_profile, (gur_user_canadian_football_profile: gur_user_canadian_football_profile)=>gur_user_canadian_football_profile.canPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCanadianFootballProfiles:gur_user_canadian_football_profile[];
    

   
    @OneToMany(()=>gur_user_canadian_football_profile, (gur_user_canadian_football_profile: gur_user_canadian_football_profile)=>gur_user_canadian_football_profile.canPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCanadianFootballProfiles2:gur_user_canadian_football_profile[];
    

   
    @OneToMany(()=>gur_user_chess_stats, (gur_user_chess_stats: gur_user_chess_stats)=>gur_user_chess_stats.ucsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserChessStatss:gur_user_chess_stats[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpLevel,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds2:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpCricketFormat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds3:gur_user_competition_played[];
    

   
    @OneToMany(()=>gur_user_cricket_profile, (gur_user_cricket_profile: gur_user_cricket_profile)=>gur_user_cricket_profile.ucpPlayerType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketProfiles:gur_user_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_cricket_profile, (gur_user_cricket_profile: gur_user_cricket_profile)=>gur_user_cricket_profile.ucpBattingPositions,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketProfiles2:gur_user_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_cricket_profile, (gur_user_cricket_profile: gur_user_cricket_profile)=>gur_user_cricket_profile.ucpPlayingNature,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketProfiles3:gur_user_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_cricket_profile, (gur_user_cricket_profile: gur_user_cricket_profile)=>gur_user_cricket_profile.ucpBowlingStyle,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketProfiles4:gur_user_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_curling_profile, (gur_user_curling_profile: gur_user_curling_profile)=>gur_user_curling_profile.curPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCurlingProfiles:gur_user_curling_profile[];
    

   
    @OneToMany(()=>gur_user_curling_profile, (gur_user_curling_profile: gur_user_curling_profile)=>gur_user_curling_profile.curPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCurlingProfiles2:gur_user_curling_profile[];
    

   
    @OneToMany(()=>gur_user_cycling_stats, (gur_user_cycling_stats: gur_user_cycling_stats)=>gur_user_cycling_stats.cysEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCyclingStatss:gur_user_cycling_stats[];
    

   
    @OneToMany(()=>gur_user_cycling_stats, (gur_user_cycling_stats: gur_user_cycling_stats)=>gur_user_cycling_stats.cysRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCyclingStatss2:gur_user_cycling_stats[];
    

   
    @OneToMany(()=>gur_user_diving_profile, (gur_user_diving_profile: gur_user_diving_profile)=>gur_user_diving_profile.divPrimaryEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingProfiles:gur_user_diving_profile[];
    

   
    @OneToMany(()=>gur_user_diving_profile, (gur_user_diving_profile: gur_user_diving_profile)=>gur_user_diving_profile.divSecondaryEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingProfiles2:gur_user_diving_profile[];
    

   
    @OneToMany(()=>gur_user_diving_stats, (gur_user_diving_stats: gur_user_diving_stats)=>gur_user_diving_stats.dvsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingStatss:gur_user_diving_stats[];
    

   
    @OneToMany(()=>gur_user_field_hockey_profile, (gur_user_field_hockey_profile: gur_user_field_hockey_profile)=>gur_user_field_hockey_profile.fhkPrimaryPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFieldHockeyProfiles:gur_user_field_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_field_hockey_profile, (gur_user_field_hockey_profile: gur_user_field_hockey_profile)=>gur_user_field_hockey_profile.fhkAlternatePosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFieldHockeyProfiles2:gur_user_field_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_figure_skating_stats, (gur_user_figure_skating_stats: gur_user_figure_skating_stats)=>gur_user_figure_skating_stats.fssEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFigureSkatingStatss:gur_user_figure_skating_stats[];
    

   
    @OneToMany(()=>gur_user_figure_skating_stats, (gur_user_figure_skating_stats: gur_user_figure_skating_stats)=>gur_user_figure_skating_stats.fssRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFigureSkatingStatss2:gur_user_figure_skating_stats[];
    

   
    @OneToMany(()=>gur_user_floorball_profile, (gur_user_floorball_profile: gur_user_floorball_profile)=>gur_user_floorball_profile.flrPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFloorballProfiles:gur_user_floorball_profile[];
    

   
    @OneToMany(()=>gur_user_floorball_profile, (gur_user_floorball_profile: gur_user_floorball_profile)=>gur_user_floorball_profile.flrPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFloorballProfiles2:gur_user_floorball_profile[];
    

   
    @OneToMany(()=>gur_user_futsal_profile, (gur_user_futsal_profile: gur_user_futsal_profile)=>gur_user_futsal_profile.futPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFutsalProfiles:gur_user_futsal_profile[];
    

   
    @OneToMany(()=>gur_user_futsal_profile, (gur_user_futsal_profile: gur_user_futsal_profile)=>gur_user_futsal_profile.futPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFutsalProfiles2:gur_user_futsal_profile[];
    

   
    @OneToMany(()=>gur_user_gaelic_football_profile, (gur_user_gaelic_football_profile: gur_user_gaelic_football_profile)=>gur_user_gaelic_football_profile.gafPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGaelicFootballProfiles:gur_user_gaelic_football_profile[];
    

   
    @OneToMany(()=>gur_user_gaelic_football_profile, (gur_user_gaelic_football_profile: gur_user_gaelic_football_profile)=>gur_user_gaelic_football_profile.gafPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGaelicFootballProfiles2:gur_user_gaelic_football_profile[];
    

   
    @OneToMany(()=>gur_user_golf_stats, (gur_user_golf_stats: gur_user_golf_stats)=>gur_user_golf_stats.glsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGolfStatss:gur_user_golf_stats[];
    

   
    @OneToMany(()=>gur_user_gymnastics_stats, (gur_user_gymnastics_stats: gur_user_gymnastics_stats)=>gur_user_gymnastics_stats.gysEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGymnasticsStatss:gur_user_gymnastics_stats[];
    

   
    @OneToMany(()=>gur_user_gymnastics_stats, (gur_user_gymnastics_stats: gur_user_gymnastics_stats)=>gur_user_gymnastics_stats.gysRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGymnasticsStatss2:gur_user_gymnastics_stats[];
    

   
    @OneToMany(()=>gur_user_hurling_profile, (gur_user_hurling_profile: gur_user_hurling_profile)=>gur_user_hurling_profile.hurPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHurlingProfiles:gur_user_hurling_profile[];
    

   
    @OneToMany(()=>gur_user_hurling_profile, (gur_user_hurling_profile: gur_user_hurling_profile)=>gur_user_hurling_profile.hurPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserHurlingProfiles2:gur_user_hurling_profile[];
    

   
    @OneToMany(()=>gur_user_ice_hockey_profile, (gur_user_ice_hockey_profile: gur_user_ice_hockey_profile)=>gur_user_ice_hockey_profile.ichPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserIceHockeyProfiles:gur_user_ice_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_ice_hockey_profile, (gur_user_ice_hockey_profile: gur_user_ice_hockey_profile)=>gur_user_ice_hockey_profile.ichPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserIceHockeyProfiles2:gur_user_ice_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_ice_hockey_profile, (gur_user_ice_hockey_profile: gur_user_ice_hockey_profile)=>gur_user_ice_hockey_profile.ichShot,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserIceHockeyProfiles3:gur_user_ice_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_judo_profile, (gur_user_judo_profile: gur_user_judo_profile)=>gur_user_judo_profile.judWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoProfiles:gur_user_judo_profile[];
    

   
    @OneToMany(()=>gur_user_judo_stats, (gur_user_judo_stats: gur_user_judo_stats)=>gur_user_judo_stats.jusWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoStatss:gur_user_judo_stats[];
    

   
    @OneToMany(()=>gur_user_judo_stats, (gur_user_judo_stats: gur_user_judo_stats)=>gur_user_judo_stats.jusRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoStatss2:gur_user_judo_stats[];
    

   
    @OneToMany(()=>gur_user_jujitsu_profile, (gur_user_jujitsu_profile: gur_user_jujitsu_profile)=>gur_user_jujitsu_profile.jujWeightClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJujitsuProfiles:gur_user_jujitsu_profile[];
    

   
    @OneToMany(()=>gur_user_jujitsu_profile, (gur_user_jujitsu_profile: gur_user_jujitsu_profile)=>gur_user_jujitsu_profile.jujDegree,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJujitsuProfiles2:gur_user_jujitsu_profile[];
    

   
    @OneToMany(()=>gur_user_jujitsu_profile, (gur_user_jujitsu_profile: gur_user_jujitsu_profile)=>gur_user_jujitsu_profile.jujBelt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJujitsuProfiles3:gur_user_jujitsu_profile[];
    

   
    @OneToMany(()=>gur_user_kabaddi_profile, (gur_user_kabaddi_profile: gur_user_kabaddi_profile)=>gur_user_kabaddi_profile.kabPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKabaddiProfiles:gur_user_kabaddi_profile[];
    

   
    @OneToMany(()=>gur_user_kabaddi_profile, (gur_user_kabaddi_profile: gur_user_kabaddi_profile)=>gur_user_kabaddi_profile.kabPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKabaddiProfiles2:gur_user_kabaddi_profile[];
    

   
    @OneToMany(()=>gur_user_karate_profile, (gur_user_karate_profile: gur_user_karate_profile)=>gur_user_karate_profile.karKumiteEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateProfiles:gur_user_karate_profile[];
    

   
    @OneToMany(()=>gur_user_karate_stats, (gur_user_karate_stats: gur_user_karate_stats)=>gur_user_karate_stats.kasWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateStatss:gur_user_karate_stats[];
    

   
    @OneToMany(()=>gur_user_karate_stats, (gur_user_karate_stats: gur_user_karate_stats)=>gur_user_karate_stats.kasRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateStatss2:gur_user_karate_stats[];
    

   
    @OneToMany(()=>gur_user_kickboxing_profile, (gur_user_kickboxing_profile: gur_user_kickboxing_profile)=>gur_user_kickboxing_profile.kcbEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKickboxingProfiles:gur_user_kickboxing_profile[];
    

   
    @OneToMany(()=>gur_user_kiteboarding_profile, (gur_user_kiteboarding_profile: gur_user_kiteboarding_profile)=>gur_user_kiteboarding_profile.kbrStance,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKiteboardingProfiles:gur_user_kiteboarding_profile[];
    

   
    @OneToMany(()=>gur_user_korfball_profile, (gur_user_korfball_profile: gur_user_korfball_profile)=>gur_user_korfball_profile.korPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKorfballProfiles:gur_user_korfball_profile[];
    

   
    @OneToMany(()=>gur_user_korfball_profile, (gur_user_korfball_profile: gur_user_korfball_profile)=>gur_user_korfball_profile.korPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKorfballProfiles2:gur_user_korfball_profile[];
    

   
    @OneToMany(()=>gur_user_lacrosse_profile, (gur_user_lacrosse_profile: gur_user_lacrosse_profile)=>gur_user_lacrosse_profile.lacPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLacrosseProfiles:gur_user_lacrosse_profile[];
    

   
    @OneToMany(()=>gur_user_lacrosse_profile, (gur_user_lacrosse_profile: gur_user_lacrosse_profile)=>gur_user_lacrosse_profile.lacPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLacrosseProfiles2:gur_user_lacrosse_profile[];
    

   
    @OneToMany(()=>gur_user_lacrosse_profile, (gur_user_lacrosse_profile: gur_user_lacrosse_profile)=>gur_user_lacrosse_profile.lacShoots,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserLacrosseProfiles3:gur_user_lacrosse_profile[];
    

   
    @OneToMany(()=>gur_user_modern_pentathlon_stats, (gur_user_modern_pentathlon_stats: gur_user_modern_pentathlon_stats)=>gur_user_modern_pentathlon_stats.mpsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModernPentathlonStatss:gur_user_modern_pentathlon_stats[];
    

   
    @OneToMany(()=>gur_user_netball_profile, (gur_user_netball_profile: gur_user_netball_profile)=>gur_user_netball_profile.netPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserNetballProfiles:gur_user_netball_profile[];
    

   
    @OneToMany(()=>gur_user_netball_profile, (gur_user_netball_profile: gur_user_netball_profile)=>gur_user_netball_profile.netPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserNetballProfiles2:gur_user_netball_profile[];
    

   
    @OneToMany(()=>gur_user_para_alpine_skiing_profile, (gur_user_para_alpine_skiing_profile: gur_user_para_alpine_skiing_profile)=>gur_user_para_alpine_skiing_profile.pasDisability,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaAlpineSkiingProfiles:gur_user_para_alpine_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_para_archery_profile, (gur_user_para_archery_profile: gur_user_para_archery_profile)=>gur_user_para_archery_profile.arcDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaArcheryProfiles:gur_user_para_archery_profile[];
    

   
    @OneToMany(()=>gur_user_para_athletics_profile, (gur_user_para_athletics_profile: gur_user_para_athletics_profile)=>gur_user_para_athletics_profile.uapDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaAthleticsProfiles:gur_user_para_athletics_profile[];
    

   
    @OneToMany(()=>gur_user_para_badminton_profile, (gur_user_para_badminton_profile: gur_user_para_badminton_profile)=>gur_user_para_badminton_profile.pbdDisability,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBadmintonProfiles:gur_user_para_badminton_profile[];
    

   
    @OneToMany(()=>gur_user_para_beach_volleyball_profile, (gur_user_para_beach_volleyball_profile: gur_user_para_beach_volleyball_profile)=>gur_user_para_beach_volleyball_profile.pbvDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBeachVolleyballProfiles:gur_user_para_beach_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_para_biathlon_profile, (gur_user_para_biathlon_profile: gur_user_para_biathlon_profile)=>gur_user_para_biathlon_profile.pbiDisablilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBiathlonProfiles:gur_user_para_biathlon_profile[];
    

   
    @OneToMany(()=>gur_user_para_boccia_profile, (gur_user_para_boccia_profile: gur_user_para_boccia_profile)=>gur_user_para_boccia_profile.bocDisabilityClasses,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaBocciaProfiles:gur_user_para_boccia_profile[];
    

   
    @OneToMany(()=>gur_user_para_canoe_profile, (gur_user_para_canoe_profile: gur_user_para_canoe_profile)=>gur_user_para_canoe_profile.pcaDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCanoeProfiles:gur_user_para_canoe_profile[];
    

   
    @OneToMany(()=>gur_user_para_cross_country_skiing_profile, (gur_user_para_cross_country_skiing_profile: gur_user_para_cross_country_skiing_profile)=>gur_user_para_cross_country_skiing_profile.pccDisablilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCrossCountrySkiingProfiles:gur_user_para_cross_country_skiing_profile[];
    

   
    @OneToMany(()=>gur_user_para_curling_profile, (gur_user_para_curling_profile: gur_user_para_curling_profile)=>gur_user_para_curling_profile.pcrPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCurlingProfiles:gur_user_para_curling_profile[];
    

   
    @OneToMany(()=>gur_user_para_curling_profile, (gur_user_para_curling_profile: gur_user_para_curling_profile)=>gur_user_para_curling_profile.pcrPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCurlingProfiles2:gur_user_para_curling_profile[];
    

   
    @OneToMany(()=>gur_user_para_cycling_profile, (gur_user_para_cycling_profile: gur_user_para_cycling_profile)=>gur_user_para_cycling_profile.pcyDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaCyclingProfiles:gur_user_para_cycling_profile[];
    

   
    @OneToMany(()=>gur_user_para_dance_profile, (gur_user_para_dance_profile: gur_user_para_dance_profile)=>gur_user_para_dance_profile.pdsDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaDanceProfiles:gur_user_para_dance_profile[];
    

   
    @OneToMany(()=>gur_user_para_equestrian_profile, (gur_user_para_equestrian_profile: gur_user_para_equestrian_profile)=>gur_user_para_equestrian_profile.equParaDressageDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaEquestrianProfiles:gur_user_para_equestrian_profile[];
    

   
    @OneToMany(()=>gur_user_para_equestrian_profile, (gur_user_para_equestrian_profile: gur_user_para_equestrian_profile)=>gur_user_para_equestrian_profile.equParaDrivingDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaEquestrianProfiles2:gur_user_para_equestrian_profile[];
    

   
    @OneToMany(()=>gur_user_para_goalball_profile, (gur_user_para_goalball_profile: gur_user_para_goalball_profile)=>gur_user_para_goalball_profile.pgbPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaGoalballProfiles:gur_user_para_goalball_profile[];
    

   
    @OneToMany(()=>gur_user_para_goalball_profile, (gur_user_para_goalball_profile: gur_user_para_goalball_profile)=>gur_user_para_goalball_profile.pgbPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaGoalballProfiles2:gur_user_para_goalball_profile[];
    

   
    @OneToMany(()=>gur_user_para_goalball_profile, (gur_user_para_goalball_profile: gur_user_para_goalball_profile)=>gur_user_para_goalball_profile.pgbDisablilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaGoalballProfiles3:gur_user_para_goalball_profile[];
    

   
    @OneToMany(()=>gur_user_para_ice_sledge_hockey_profile, (gur_user_para_ice_sledge_hockey_profile: gur_user_para_ice_sledge_hockey_profile)=>gur_user_para_ice_sledge_hockey_profile.ishDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaIceSledgeHockeyProfiles:gur_user_para_ice_sledge_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_para_ice_sledge_hockey_profile, (gur_user_para_ice_sledge_hockey_profile: gur_user_para_ice_sledge_hockey_profile)=>gur_user_para_ice_sledge_hockey_profile.ishPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaIceSledgeHockeyProfiles2:gur_user_para_ice_sledge_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_para_ice_sledge_hockey_profile, (gur_user_para_ice_sledge_hockey_profile: gur_user_para_ice_sledge_hockey_profile)=>gur_user_para_ice_sledge_hockey_profile.ishPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaIceSledgeHockeyProfiles3:gur_user_para_ice_sledge_hockey_profile[];
    

   
    @OneToMany(()=>gur_user_para_judo_profile, (gur_user_para_judo_profile: gur_user_para_judo_profile)=>gur_user_para_judo_profile.pjdWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaJudoProfiles:gur_user_para_judo_profile[];
    

   
    @OneToMany(()=>gur_user_para_judo_profile, (gur_user_para_judo_profile: gur_user_para_judo_profile)=>gur_user_para_judo_profile.pjdDisabilityClasses,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaJudoProfiles2:gur_user_para_judo_profile[];
    

   
    @OneToMany(()=>gur_user_para_powerlifting_profile, (gur_user_para_powerlifting_profile: gur_user_para_powerlifting_profile)=>gur_user_para_powerlifting_profile.pplSportCardStatus,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaPowerliftingProfiles:gur_user_para_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_para_powerlifting_profile, (gur_user_para_powerlifting_profile: gur_user_para_powerlifting_profile)=>gur_user_para_powerlifting_profile.pplWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaPowerliftingProfiles2:gur_user_para_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_para_powerlifting_profile, (gur_user_para_powerlifting_profile: gur_user_para_powerlifting_profile)=>gur_user_para_powerlifting_profile.pplDisabilityClasses,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaPowerliftingProfiles3:gur_user_para_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_para_rowing_profile, (gur_user_para_rowing_profile: gur_user_para_rowing_profile)=>gur_user_para_rowing_profile.prwDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaRowingProfiles:gur_user_para_rowing_profile[];
    

   
    @OneToMany(()=>gur_user_para_snowboarding_profile, (gur_user_para_snowboarding_profile: gur_user_para_snowboarding_profile)=>gur_user_para_snowboarding_profile.psbDisability,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSnowboardingProfiles:gur_user_para_snowboarding_profile[];
    

   
    @OneToMany(()=>gur_user_para_soccer_profile, (gur_user_para_soccer_profile: gur_user_para_soccer_profile)=>gur_user_para_soccer_profile.uspPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSoccerProfiles:gur_user_para_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_para_soccer_profile, (gur_user_para_soccer_profile: gur_user_para_soccer_profile)=>gur_user_para_soccer_profile.uspPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSoccerProfiles2:gur_user_para_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_para_soccer_profile, (gur_user_para_soccer_profile: gur_user_para_soccer_profile)=>gur_user_para_soccer_profile.uspDisablityClasses,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSoccerProfiles3:gur_user_para_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_para_swimming_profile, (gur_user_para_swimming_profile: gur_user_para_swimming_profile)=>gur_user_para_swimming_profile.pswSDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSwimmingProfiles:gur_user_para_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_para_swimming_profile, (gur_user_para_swimming_profile: gur_user_para_swimming_profile)=>gur_user_para_swimming_profile.pswSbDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaSwimmingProfiles2:gur_user_para_swimming_profile[];
    

   
    @OneToMany(()=>gur_user_para_table_tennis_profile, (gur_user_para_table_tennis_profile: gur_user_para_table_tennis_profile)=>gur_user_para_table_tennis_profile.pttDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTableTennisProfiles:gur_user_para_table_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_para_taekwondo_profile, (gur_user_para_taekwondo_profile: gur_user_para_taekwondo_profile)=>gur_user_para_taekwondo_profile.ptkWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTaekwondoProfiles:gur_user_para_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_para_taekwondo_profile, (gur_user_para_taekwondo_profile: gur_user_para_taekwondo_profile)=>gur_user_para_taekwondo_profile.ptkDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTaekwondoProfiles2:gur_user_para_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_para_triathlon_profile, (gur_user_para_triathlon_profile: gur_user_para_triathlon_profile)=>gur_user_para_triathlon_profile.ptrDisabilityClasses,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaTriathlonProfiles:gur_user_para_triathlon_profile[];
    

   
    @OneToMany(()=>gur_user_para_volleyball_profile, (gur_user_para_volleyball_profile: gur_user_para_volleyball_profile)=>gur_user_para_volleyball_profile.prwDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaVolleyballProfiles:gur_user_para_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_basketball_profile, (gur_user_para_wheelchair_basketball_profile: gur_user_para_wheelchair_basketball_profile)=>gur_user_para_wheelchair_basketball_profile.pwbPrimaryPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairBasketballProfiles:gur_user_para_wheelchair_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_basketball_profile, (gur_user_para_wheelchair_basketball_profile: gur_user_para_wheelchair_basketball_profile)=>gur_user_para_wheelchair_basketball_profile.pwbAlternatePosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairBasketballProfiles2:gur_user_para_wheelchair_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_basketball_profile, (gur_user_para_wheelchair_basketball_profile: gur_user_para_wheelchair_basketball_profile)=>gur_user_para_wheelchair_basketball_profile.pwbDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairBasketballProfiles3:gur_user_para_wheelchair_basketball_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_fencing_profile, (gur_user_para_wheelchair_fencing_profile: gur_user_para_wheelchair_fencing_profile)=>gur_user_para_wheelchair_fencing_profile.pwfDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairFencingProfiles:gur_user_para_wheelchair_fencing_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_rugby_profile, (gur_user_para_wheelchair_rugby_profile: gur_user_para_wheelchair_rugby_profile)=>gur_user_para_wheelchair_rugby_profile.pwrDisabilityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairRugbyProfiles:gur_user_para_wheelchair_rugby_profile[];
    

   
    @OneToMany(()=>gur_user_para_wheelchair_tennis_profile, (gur_user_para_wheelchair_tennis_profile: gur_user_para_wheelchair_tennis_profile)=>gur_user_para_wheelchair_tennis_profile.utpDisablityClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserParaWheelchairTennisProfiles:gur_user_para_wheelchair_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_pesapallo_profile, (gur_user_pesapallo_profile: gur_user_pesapallo_profile)=>gur_user_pesapallo_profile.pesPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPesapalloProfiles:gur_user_pesapallo_profile[];
    

   
    @OneToMany(()=>gur_user_pesapallo_profile, (gur_user_pesapallo_profile: gur_user_pesapallo_profile)=>gur_user_pesapallo_profile.pesPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPesapalloProfiles2:gur_user_pesapallo_profile[];
    

   
    @OneToMany(()=>gur_user_polo_profile, (gur_user_polo_profile: gur_user_polo_profile)=>gur_user_polo_profile.polHandicap,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoloProfiles:gur_user_polo_profile[];
    

   
    @OneToMany(()=>gur_user_polo_profile, (gur_user_polo_profile: gur_user_polo_profile)=>gur_user_polo_profile.polPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoloProfiles2:gur_user_polo_profile[];
    

   
    @OneToMany(()=>gur_user_polo_profile, (gur_user_polo_profile: gur_user_polo_profile)=>gur_user_polo_profile.polPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPoloProfiles3:gur_user_polo_profile[];
    

   
    @OneToMany(()=>gur_user_powerlifting_profile, (gur_user_powerlifting_profile: gur_user_powerlifting_profile)=>gur_user_powerlifting_profile.uppEventWeight,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingProfiles:gur_user_powerlifting_profile[];
    

   
    @OneToMany(()=>gur_user_powerlifting_stats, (gur_user_powerlifting_stats: gur_user_powerlifting_stats)=>gur_user_powerlifting_stats.upsEventWeight,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingStatss:gur_user_powerlifting_stats[];
    

   
    @OneToMany(()=>gur_user_powerlifting_stats, (gur_user_powerlifting_stats: gur_user_powerlifting_stats)=>gur_user_powerlifting_stats.upsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingStatss2:gur_user_powerlifting_stats[];
    

   
    @OneToMany(()=>gur_user_qualifications, (gur_user_qualifications: gur_user_qualifications)=>gur_user_qualifications.uqfType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserQualificationss:gur_user_qualifications[];
    

   
    @OneToMany(()=>gur_user_rafting_profile, (gur_user_rafting_profile: gur_user_rafting_profile)=>gur_user_rafting_profile.rafSkillLevel,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRaftingProfiles:gur_user_rafting_profile[];
    

   
    @OneToMany(()=>gur_user_roller_derby_profile, (gur_user_roller_derby_profile: gur_user_roller_derby_profile)=>gur_user_roller_derby_profile.rodPositions,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerDerbyProfiles:gur_user_roller_derby_profile[];
    

   
    @OneToMany(()=>gur_user_roller_derby_profile, (gur_user_roller_derby_profile: gur_user_roller_derby_profile)=>gur_user_roller_derby_profile.rodPositions2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerDerbyProfiles2:gur_user_roller_derby_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_inline_profile, (gur_user_roller_hockey_inline_profile: gur_user_roller_hockey_inline_profile)=>gur_user_roller_hockey_inline_profile.rhiPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyInlineProfiles:gur_user_roller_hockey_inline_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_inline_profile, (gur_user_roller_hockey_inline_profile: gur_user_roller_hockey_inline_profile)=>gur_user_roller_hockey_inline_profile.rhiPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyInlineProfiles2:gur_user_roller_hockey_inline_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_quad_profile, (gur_user_roller_hockey_quad_profile: gur_user_roller_hockey_quad_profile)=>gur_user_roller_hockey_quad_profile.rhqPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyQuadProfiles:gur_user_roller_hockey_quad_profile[];
    

   
    @OneToMany(()=>gur_user_roller_hockey_quad_profile, (gur_user_roller_hockey_quad_profile: gur_user_roller_hockey_quad_profile)=>gur_user_roller_hockey_quad_profile.rhqPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRollerHockeyQuadProfiles2:gur_user_roller_hockey_quad_profile[];
    

   
    @OneToMany(()=>gur_user_rowing_stats, (gur_user_rowing_stats: gur_user_rowing_stats)=>gur_user_rowing_stats.rosEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRowingStatss:gur_user_rowing_stats[];
    

   
    @OneToMany(()=>gur_user_rowing_stats, (gur_user_rowing_stats: gur_user_rowing_stats)=>gur_user_rowing_stats.rosRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRowingStatss2:gur_user_rowing_stats[];
    

   
    @OneToMany(()=>gur_user_rugby_league_profile, (gur_user_rugby_league_profile: gur_user_rugby_league_profile)=>gur_user_rugby_league_profile.rglPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyLeagueProfiles:gur_user_rugby_league_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_league_profile, (gur_user_rugby_league_profile: gur_user_rugby_league_profile)=>gur_user_rugby_league_profile.rglPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyLeagueProfiles2:gur_user_rugby_league_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_sevens_profile, (gur_user_rugby_sevens_profile: gur_user_rugby_sevens_profile)=>gur_user_rugby_sevens_profile.rgsPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbySevensProfiles:gur_user_rugby_sevens_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_sevens_profile, (gur_user_rugby_sevens_profile: gur_user_rugby_sevens_profile)=>gur_user_rugby_sevens_profile.rgsPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbySevensProfiles2:gur_user_rugby_sevens_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_union_profile, (gur_user_rugby_union_profile: gur_user_rugby_union_profile)=>gur_user_rugby_union_profile.rguPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyUnionProfiles:gur_user_rugby_union_profile[];
    

   
    @OneToMany(()=>gur_user_rugby_union_profile, (gur_user_rugby_union_profile: gur_user_rugby_union_profile)=>gur_user_rugby_union_profile.rguPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyUnionProfiles2:gur_user_rugby_union_profile[];
    

   
    @OneToMany(()=>gur_user_sambo_profile, (gur_user_sambo_profile: gur_user_sambo_profile)=>gur_user_sambo_profile.samWeightClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSamboProfiles:gur_user_sambo_profile[];
    

   
    @OneToMany(()=>gur_user_sepak_takraw_profile, (gur_user_sepak_takraw_profile: gur_user_sepak_takraw_profile)=>gur_user_sepak_takraw_profile.sepPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSepakTakrawProfiles:gur_user_sepak_takraw_profile[];
    

   
    @OneToMany(()=>gur_user_sepak_takraw_profile, (gur_user_sepak_takraw_profile: gur_user_sepak_takraw_profile)=>gur_user_sepak_takraw_profile.sepPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSepakTakrawProfiles2:gur_user_sepak_takraw_profile[];
    

   
    @OneToMany(()=>gur_user_shinty_profile, (gur_user_shinty_profile: gur_user_shinty_profile)=>gur_user_shinty_profile.shnPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShintyProfiles:gur_user_shinty_profile[];
    

   
    @OneToMany(()=>gur_user_shinty_profile, (gur_user_shinty_profile: gur_user_shinty_profile)=>gur_user_shinty_profile.shnPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShintyProfiles2:gur_user_shinty_profile[];
    

   
    @OneToMany(()=>gur_user_shooting_stats, (gur_user_shooting_stats: gur_user_shooting_stats)=>gur_user_shooting_stats.shsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShootingStatss:gur_user_shooting_stats[];
    

   
    @OneToMany(()=>gur_user_shooting_stats, (gur_user_shooting_stats: gur_user_shooting_stats)=>gur_user_shooting_stats.shsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShootingStatss2:gur_user_shooting_stats[];
    

   
    @OneToMany(()=>gur_user_soccer_profile, (gur_user_soccer_profile: gur_user_soccer_profile)=>gur_user_soccer_profile.uspPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerProfiles:gur_user_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_soccer_profile, (gur_user_soccer_profile: gur_user_soccer_profile)=>gur_user_soccer_profile.uspPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerProfiles2:gur_user_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_soccer_profile, (gur_user_soccer_profile: gur_user_soccer_profile)=>gur_user_soccer_profile.uspTemperament,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerProfiles3:gur_user_soccer_profile[];
    

   
    @OneToMany(()=>gur_user_softball_profile, (gur_user_softball_profile: gur_user_softball_profile)=>gur_user_softball_profile.sofPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoftballProfiles:gur_user_softball_profile[];
    

   
    @OneToMany(()=>gur_user_softball_profile, (gur_user_softball_profile: gur_user_softball_profile)=>gur_user_softball_profile.sofPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoftballProfiles2:gur_user_softball_profile[];
    

   
    @OneToMany(()=>gur_user_squash_profile, (gur_user_squash_profile: gur_user_squash_profile)=>gur_user_squash_profile.sqaPlayingStyle,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashProfiles:gur_user_squash_profile[];
    

   
    @OneToMany(()=>gur_user_squash_stats, (gur_user_squash_stats: gur_user_squash_stats)=>gur_user_squash_stats.sqsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashStatss:gur_user_squash_stats[];
    

   
    @OneToMany(()=>gur_user_squash_stats, (gur_user_squash_stats: gur_user_squash_stats)=>gur_user_squash_stats.sqsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashStatss2:gur_user_squash_stats[];
    

   
    @OneToMany(()=>gur_user_sumo_wrestling_profile, (gur_user_sumo_wrestling_profile: gur_user_sumo_wrestling_profile)=>gur_user_sumo_wrestling_profile.sumWeightClass,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSumoWrestlingProfiles:gur_user_sumo_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_surfing_profile, (gur_user_surfing_profile: gur_user_surfing_profile)=>gur_user_surfing_profile.supStance,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSurfingProfiles:gur_user_surfing_profile[];
    

   
    @OneToMany(()=>gur_user_swimming_stats, (gur_user_swimming_stats: gur_user_swimming_stats)=>gur_user_swimming_stats.spsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSwimmingStatss:gur_user_swimming_stats[];
    

   
    @OneToMany(()=>gur_user_swimming_stats, (gur_user_swimming_stats: gur_user_swimming_stats)=>gur_user_swimming_stats.spsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSwimmingStatss2:gur_user_swimming_stats[];
    

   
    @OneToMany(()=>gur_user_table_tennis_profile, (gur_user_table_tennis_profile: gur_user_table_tennis_profile)=>gur_user_table_tennis_profile.tatPlayStyle,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisProfiles:gur_user_table_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_table_tennis_stats, (gur_user_table_tennis_stats: gur_user_table_tennis_stats)=>gur_user_table_tennis_stats.ttsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisStatss:gur_user_table_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_table_tennis_stats, (gur_user_table_tennis_stats: gur_user_table_tennis_stats)=>gur_user_table_tennis_stats.ttsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisStatss2:gur_user_table_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_taekwondo_profile, (gur_user_taekwondo_profile: gur_user_taekwondo_profile)=>gur_user_taekwondo_profile.taeWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoProfiles:gur_user_taekwondo_profile[];
    

   
    @OneToMany(()=>gur_user_taekwondo_stats, (gur_user_taekwondo_stats: gur_user_taekwondo_stats)=>gur_user_taekwondo_stats.tasWeightEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoStatss:gur_user_taekwondo_stats[];
    

   
    @OneToMany(()=>gur_user_taekwondo_stats, (gur_user_taekwondo_stats: gur_user_taekwondo_stats)=>gur_user_taekwondo_stats.tasRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoStatss2:gur_user_taekwondo_stats[];
    

   
    @OneToMany(()=>gur_user_team_handball_profile, (gur_user_team_handball_profile: gur_user_team_handball_profile)=>gur_user_team_handball_profile.thbPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTeamHandballProfiles:gur_user_team_handball_profile[];
    

   
    @OneToMany(()=>gur_user_team_handball_profile, (gur_user_team_handball_profile: gur_user_team_handball_profile)=>gur_user_team_handball_profile.thbPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTeamHandballProfiles2:gur_user_team_handball_profile[];
    

   
    @OneToMany(()=>gur_user_tennis_profile, (gur_user_tennis_profile: gur_user_tennis_profile)=>gur_user_tennis_profile.utpPlayingStyle,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisProfiles:gur_user_tennis_profile[];
    

   
    @OneToMany(()=>gur_user_tennis_stats, (gur_user_tennis_stats: gur_user_tennis_stats)=>gur_user_tennis_stats.utsEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisStatss:gur_user_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_tennis_stats, (gur_user_tennis_stats: gur_user_tennis_stats)=>gur_user_tennis_stats.utsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisStatss2:gur_user_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_triathlon_stats, (gur_user_triathlon_stats: gur_user_triathlon_stats)=>gur_user_triathlon_stats.trsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTriathlonStatss:gur_user_triathlon_stats[];
    

   
    @OneToMany(()=>gur_user_ultimate_profile, (gur_user_ultimate_profile: gur_user_ultimate_profile)=>gur_user_ultimate_profile.ultPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserUltimateProfiles:gur_user_ultimate_profile[];
    

   
    @OneToMany(()=>gur_user_ultimate_profile, (gur_user_ultimate_profile: gur_user_ultimate_profile)=>gur_user_ultimate_profile.ultPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserUltimateProfiles2:gur_user_ultimate_profile[];
    

   
    @OneToMany(()=>gur_user_volleyball_profile, (gur_user_volleyball_profile: gur_user_volleyball_profile)=>gur_user_volleyball_profile.volPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserVolleyballProfiles:gur_user_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_volleyball_profile, (gur_user_volleyball_profile: gur_user_volleyball_profile)=>gur_user_volleyball_profile.volPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserVolleyballProfiles2:gur_user_volleyball_profile[];
    

   
    @OneToMany(()=>gur_user_water_polo_profile, (gur_user_water_polo_profile: gur_user_water_polo_profile)=>gur_user_water_polo_profile.wapPosition,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloProfiles:gur_user_water_polo_profile[];
    

   
    @OneToMany(()=>gur_user_water_polo_profile, (gur_user_water_polo_profile: gur_user_water_polo_profile)=>gur_user_water_polo_profile.wapPlayingNature,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloProfiles2:gur_user_water_polo_profile[];
    

   
    @OneToMany(()=>gur_user_water_polo_profile, (gur_user_water_polo_profile: gur_user_water_polo_profile)=>gur_user_water_polo_profile.wapPosition2,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloProfiles3:gur_user_water_polo_profile[];
    

   
    @OneToMany(()=>gur_user_weightlifting_profile, (gur_user_weightlifting_profile: gur_user_weightlifting_profile)=>gur_user_weightlifting_profile.uwpEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingProfiles:gur_user_weightlifting_profile[];
    

   
    @OneToMany(()=>gur_user_weightlifting_stats, (gur_user_weightlifting_stats: gur_user_weightlifting_stats)=>gur_user_weightlifting_stats.uwsEventPlayed,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingStatss:gur_user_weightlifting_stats[];
    

   
    @OneToMany(()=>gur_user_weightlifting_stats, (gur_user_weightlifting_stats: gur_user_weightlifting_stats)=>gur_user_weightlifting_stats.uwsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingStatss2:gur_user_weightlifting_stats[];
    

   
    @OneToMany(()=>gur_user_wheelchair_cricket_profile, (gur_user_wheelchair_cricket_profile: gur_user_wheelchair_cricket_profile)=>gur_user_wheelchair_cricket_profile.wcpPlayerType,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWheelchairCricketProfiles:gur_user_wheelchair_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_wheelchair_cricket_profile, (gur_user_wheelchair_cricket_profile: gur_user_wheelchair_cricket_profile)=>gur_user_wheelchair_cricket_profile.wcpBattingPositions,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWheelchairCricketProfiles2:gur_user_wheelchair_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_wheelchair_cricket_profile, (gur_user_wheelchair_cricket_profile: gur_user_wheelchair_cricket_profile)=>gur_user_wheelchair_cricket_profile.wcpPlayingNature,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWheelchairCricketProfiles3:gur_user_wheelchair_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_wheelchair_cricket_profile, (gur_user_wheelchair_cricket_profile: gur_user_wheelchair_cricket_profile)=>gur_user_wheelchair_cricket_profile.wcpBowlingStyle,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWheelchairCricketProfiles4:gur_user_wheelchair_cricket_profile[];
    

   
    @OneToMany(()=>gur_user_wrestling_profile, (gur_user_wrestling_profile: gur_user_wrestling_profile)=>gur_user_wrestling_profile.uwrFreestyleWeightCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingProfiles:gur_user_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_wrestling_profile, (gur_user_wrestling_profile: gur_user_wrestling_profile)=>gur_user_wrestling_profile.uwrGrecoRomanWeightCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingProfiles2:gur_user_wrestling_profile[];
    

   
    @OneToMany(()=>gur_user_wrestling_stats, (gur_user_wrestling_stats: gur_user_wrestling_stats)=>gur_user_wrestling_stats.wrsWeightCategory,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingStatss:gur_user_wrestling_stats[];
    

   
    @OneToMany(()=>gur_user_wrestling_stats, (gur_user_wrestling_stats: gur_user_wrestling_stats)=>gur_user_wrestling_stats.wrsRank,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingStatss2:gur_user_wrestling_stats[];
    

   
    @OneToMany(()=>gur_user_wushu_profile, (gur_user_wushu_profile: gur_user_wushu_profile)=>gur_user_wushu_profile.wusSanshouEvent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWushuProfiles:gur_user_wushu_profile[];
    

   
    @OneToMany(()=>gur_users, (gur_users: gur_users)=>gur_users.usrSalutation,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserss:gur_users[];
    

   
    @OneToMany(()=>gur_vendors, (gur_vendors: gur_vendors)=>gur_vendors.venSalutation,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurVendorss:gur_vendors[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepGrade,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups:gur_world_events_players_bkup[];
    
}
