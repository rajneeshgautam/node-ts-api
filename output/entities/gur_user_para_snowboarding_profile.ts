import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_snowboarding_profile" ,{schema:"sportsmatik_local" } )
@Index("psb_usr_id",["psbUsr",])
@Index("psb_disability",["psbDisability",])
@Index("psb_createdby_user",["psbCreatedbyUser",])
export class gur_user_para_snowboarding_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"psb_id"
        })
    psb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSnowboardingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psb_usr_id'})
    psbUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"psb_discipline"
        })
    psb_discipline:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaSnowboardingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psb_disability'})
    psbDisability:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"psb_modifiedtime"
        })
    psb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaSnowboardingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'psb_createdby_user'})
    psbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"psb_modifiedby"
        })
    psb_modifiedby:string | null;
        
}
