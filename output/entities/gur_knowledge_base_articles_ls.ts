import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_knowledge_base_articles} from "./gur_knowledge_base_articles";


@Entity("gur_knowledge_base_articles_ls" ,{schema:"sportsmatik_local" } )
@Index("kbl_kba_id",["kalKba",])
export class gur_knowledge_base_articles_ls {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"kal_id"
        })
    kal_id:string;
        

   
    @ManyToOne(()=>gur_knowledge_base_articles, (gur_knowledge_base_articles: gur_knowledge_base_articles)=>gur_knowledge_base_articles.gurKnowledgeBaseArticlesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kal_kba_id'})
    kalKba:gur_knowledge_base_articles | null;


    @Column("varchar",{ 
        nullable:true,
        name:"kal_title"
        })
    kal_title:string | null;
        

    @Column("mediumtext",{ 
        nullable:true,
        name:"kal_description"
        })
    kal_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"kal_lng_code"
        })
    kal_lng_code:string;
        
}
