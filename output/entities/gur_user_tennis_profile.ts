import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_tennis_profile" ,{schema:"sportsmatik_local" } )
@Index("utp_usr_id",["utpUsr",])
@Index("utp_playing_style",["utpPlayingStyle",])
@Index("utp_createdby_user",["utpCreatedbyUser",])
export class gur_user_tennis_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"utp_id"
        })
    utp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTennisProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'utp_usr_id'})
    utpUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"utp_primary_hand"
        })
    utp_primary_hand:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["one-handed","two-handed"],
        name:"utp_back_hand"
        })
    utp_back_hand:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTennisProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'utp_playing_style'})
    utpPlayingStyle:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"utp_modifiedtime"
        })
    utp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTennisProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'utp_createdby_user'})
    utpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"utp_modifiedby"
        })
    utp_modifiedby:string | null;
        
}
