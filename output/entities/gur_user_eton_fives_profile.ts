import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_eton_fives_profile" ,{schema:"sportsmatik_local" } )
@Index("etf_usr_id",["etfUsr",])
@Index("etf_createdby_user",["etfCreatedbyUser",])
export class gur_user_eton_fives_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"etf_id"
        })
    etf_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserEtonFivesProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'etf_usr_id'})
    etfUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"etf_playing_hand"
        })
    etf_playing_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"etf_modifiedtime"
        })
    etf_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserEtonFivesProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'etf_createdby_user'})
    etfCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"etf_modifiedby"
        })
    etf_modifiedby:string | null;
        
}
