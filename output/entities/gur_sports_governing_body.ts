import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_sports_governing_body" ,{schema:"sportsmatik_local" } )
@Index("sgb_gst_number",["sgb_gst_number",],{unique:true})
@Index("sgb_usr_id",["sgbUsr",])
@Index("sgb_createdby_user",["sgbCreatedbyUser",])
@Index("sgb_country",["sgbCountry",])
@Index("sgb_dis_id",["sgbDis",])
@Index("sgb_sts_id",["sgbSts",])
@Index("sgb_name",["sgb_name",])
export class gur_sports_governing_body {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sgb_id"
        })
    sgb_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_slug"
        })
    sgb_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sgb_name"
        })
    sgb_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsGoverningBodys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_usr_id'})
    sgbUsr:gur_users | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurSportsGoverningBodys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_country'})
    sgbCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurSportsGoverningBodys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_dis_id'})
    sgbDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurSportsGoverningBodys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_sts_id'})
    sgbSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"sgb_address_google_map"
        })
    sgb_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_address"
        })
    sgb_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_address2"
        })
    sgb_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"sgb_pincode"
        })
    sgb_pincode:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_email"
        })
    sgb_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sgb_email_verified"
        })
    sgb_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sgb_email_verify_time"
        })
    sgb_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sgb_phone1"
        })
    sgb_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sgb_phone1_verified"
        })
    sgb_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sgb_phone1_otp"
        })
    sgb_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sgb_phone1_otp_time"
        })
    sgb_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"sgb_phone2"
        })
    sgb_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sgb_phone2_verified"
        })
    sgb_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"sgb_phone2_otp"
        })
    sgb_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sgb_phone2_otp_time"
        })
    sgb_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"sgb_status"
        })
    sgb_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_profile_pic"
        })
    sgb_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sgb_intro_status"
        })
    sgb_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"sgb_profile_summary"
        })
    sgb_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sgb_founded_year"
        })
    sgb_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"sgb_founded_month"
        })
    sgb_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sgb_last_login_date"
        })
    sgb_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"sgb_last_login_details"
        })
    sgb_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"sgb_intro_modifiedtime"
        })
    sgb_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"sgb_profile_pic_modifiedtime"
        })
    sgb_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sgb_free_sports"
        })
    sgb_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"sgb_free_sub_users"
        })
    sgb_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"sgb_gst_number"
        })
    sgb_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sgb_viewable"
        })
    sgb_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsGoverningBodys2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sgb_createdby_user'})
    sgbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"sgb_createdby"
        })
    sgb_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"sgb_modifiedby"
        })
    sgb_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sgb_modifiedtime"
        })
    sgb_modifiedtime:Date;
        
}
