import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_triathlon_stats" ,{schema:"sportsmatik_local" } )
@Index("trs_usr_id",["trsUsr",])
@Index("trs_ucp_id",["trsUcp",])
@Index("trs_rank",["trsRank",])
export class gur_user_triathlon_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"trs_id"
        })
    trs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTriathlonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'trs_usr_id'})
    trsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserTriathlonStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'trs_ucp_id'})
    trsUcp:gur_user_competition_played | null;


    @Column("time",{ 
        nullable:false,
        name:"trs_time_completed"
        })
    trs_time_completed:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTriathlonStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'trs_rank'})
    trsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"trs_modifiedtime"
        })
    trs_modifiedtime:Date;
        
}
