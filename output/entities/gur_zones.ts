import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_zones" ,{schema:"sportsmatik_local" } )
@Index("zon_state",["zon_state",])
export class gur_zones {

    @Column("int",{ 
        nullable:false,
        primary:true,
        name:"zon_id"
        })
    zon_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"zon_name"
        })
    zon_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"zon_pincode"
        })
    zon_pincode:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"zon_district"
        })
    zon_district:string | null;
        

    @Column("int",{ 
        nullable:false,
        name:"zon_state"
        })
    zon_state:number;
        
}
