import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_orienteering_profile" ,{schema:"sportsmatik_local" } )
@Index("ori_usr_id",["oriUsr",])
@Index("ori_createdby_user",["oriCreatedbyUser",])
export class gur_user_orienteering_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ori_id"
        })
    ori_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserOrienteeringProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_usr_id'})
    oriUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"ori_discipline"
        })
    ori_discipline:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ori_modifiedtime"
        })
    ori_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserOrienteeringProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ori_createdby_user'})
    oriCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ori_modifiedby"
        })
    ori_modifiedby:string | null;
        
}
