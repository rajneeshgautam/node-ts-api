import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_cms_pages_ls} from "./gur_cms_pages_ls";


@Entity("gur_cms_pages" ,{schema:"sportsmatik_local" } )
@Index("cms_page_slug",["cms_page_slug",],{unique:true})
export class gur_cms_pages {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cms_id"
        })
    cms_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"cms_page_slug"
        })
    cms_page_slug:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cms_photo"
        })
    cms_photo:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cms_createdby"
        })
    cms_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cms_modifiedby"
        })
    cms_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cms_modifiedtime"
        })
    cms_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_cms_pages_ls, (gur_cms_pages_ls: gur_cms_pages_ls)=>gur_cms_pages_ls.cmlCms,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCmsPagesLss:gur_cms_pages_ls[];
    
}
