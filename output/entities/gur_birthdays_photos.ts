import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthdays} from "./gur_birthdays";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_countries} from "./gur_countries";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_birthdays_ls} from "./gur_birthdays_ls";


@Entity("gur_birthdays_photos" ,{schema:"sportsmatik_local" } )
@Index("gur_birthdays_photos_ibfk_4",["brpType",])
@Index("gur_birthdays_photos_old_ibfk_1",["brpBir",])
@Index("gur_birthdays_photos_old_ibfk_2",["brpSpo",])
@Index("gur_birthdays_photos_old_ibfk_3",["brpCnt",])
export class gur_birthdays_photos {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"brp_id"
        })
    brp_id:number;
        

   
    @ManyToOne(()=>gur_birthdays, (gur_birthdays: gur_birthdays)=>gur_birthdays.gurBirthdaysPhotoss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'brp_bir_id'})
    brpBir:gur_birthdays | null;


    @Column("varchar",{ 
        nullable:true,
        name:"brp_photo"
        })
    brp_photo:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["m","f","o"],
        name:"brp_gender"
        })
    brp_gender:string | null;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurBirthdaysPhotoss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'brp_spo_id'})
    brpSpo:gur_wiki_sports | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurBirthdaysPhotoss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'brp_cnt_id'})
    brpCnt:gur_countries | null;


    @Column("varchar",{ 
        nullable:true,
        name:"brp_external_link"
        })
    brp_external_link:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"brp_image_source"
        })
    brp_image_source:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"brp_sort_order"
        })
    brp_sort_order:number;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurBirthdaysPhotoss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'brp_type'})
    brpType:gur_matik_knowhow_tab_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"brp_only_knowhow"
        })
    brp_only_knowhow:boolean;
        

   
    @OneToMany(()=>gur_birthdays_ls, (gur_birthdays_ls: gur_birthdays_ls)=>gur_birthdays_ls.bilBrp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdaysLss:gur_birthdays_ls[];
    
}
