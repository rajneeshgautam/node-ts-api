import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_job} from "./gur_job";
import {gur_countries} from "./gur_countries";
import {gur_states} from "./gur_states";
import {gur_districts} from "./gur_districts";
import {gur_job_education} from "./gur_job_education";
import {gur_job_experience} from "./gur_job_experience";


@Entity("gur_job_application" ,{schema:"sportsmatik_local" } )
@Index("joa_sts_id",["joaSts",])
@Index("joa_dis_id",["joaDis",])
@Index("joa_job_id",["joaJob",])
@Index("joa_cnt_id",["joaCnt",])
export class gur_job_application {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"joa_id"
        })
    joa_id:number;
        

   
    @ManyToOne(()=>gur_job, (gur_job: gur_job)=>gur_job.gurJobApplications,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joa_job_id'})
    joaJob:gur_job | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"joa_first_name"
        })
    joa_first_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_last_name"
        })
    joa_last_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"joa_email"
        })
    joa_email:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joe_alternate_email"
        })
    joe_alternate_email:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"joa_phone"
        })
    joa_phone:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"joa_alternate_phone"
        })
    joa_alternate_phone:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurJobApplications,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joa_cnt_id'})
    joaCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurJobApplications,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joa_sts_id'})
    joaSts:gur_states | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurJobApplications,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joa_dis_id'})
    joaDis:gur_districts | null;


    @Column("varchar",{ 
        nullable:false,
        name:"joa_address"
        })
    joa_address:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_picture"
        })
    joa_picture:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_resume"
        })
    joa_resume:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_cover_letter"
        })
    joa_cover_letter:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"joa_comments"
        })
    joa_comments:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"joa_why_join_us"
        })
    joa_why_join_us:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"joa_crime_reason"
        })
    joa_crime_reason:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"joa_case_reason"
        })
    joa_case_reason:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_certificates"
        })
    joa_certificates:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_address_proof"
        })
    joa_address_proof:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"joa_income_proof"
        })
    joa_income_proof:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"joa_linked_in"
        })
    joa_linked_in:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"joa_facebook"
        })
    joa_facebook:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"joa_twitter"
        })
    joa_twitter:string | null;
        

    @Column("tinytext",{ 
        nullable:true,
        name:"joa_more_social_media"
        })
    joa_more_social_media:string | null;
        

    @Column("date",{ 
        nullable:false,
        name:"joa_created_at"
        })
    joa_created_at:string;
        

    @Column("date",{ 
        nullable:true,
        name:"joa_updated_at"
        })
    joa_updated_at:string | null;
        

   
    @OneToMany(()=>gur_job_education, (gur_job_education: gur_job_education)=>gur_job_education.jedJoa,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobEducations:gur_job_education[];
    

   
    @OneToMany(()=>gur_job_experience, (gur_job_experience: gur_job_experience)=>gur_job_experience.joeJoa,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurJobExperiences:gur_job_experience[];
    
}
