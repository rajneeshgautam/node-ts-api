import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_competitions} from "./gur_competitions";
import {gur_sports} from "./gur_sports";
import {gur_value_list} from "./gur_value_list";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_teams} from "./gur_teams";
import {gur_user_archery_stats} from "./gur_user_archery_stats";
import {gur_user_athletics_stats} from "./gur_user_athletics_stats";
import {gur_user_badminton_stats} from "./gur_user_badminton_stats";
import {gur_user_basketball_stats} from "./gur_user_basketball_stats";
import {gur_user_boxing_stats} from "./gur_user_boxing_stats";
import {gur_user_chess_stats} from "./gur_user_chess_stats";
import {gur_user_cricket_stats} from "./gur_user_cricket_stats";
import {gur_user_cycling_stats} from "./gur_user_cycling_stats";
import {gur_user_diving_stats} from "./gur_user_diving_stats";
import {gur_user_field_hockey_stats} from "./gur_user_field_hockey_stats";
import {gur_user_figure_skating_stats} from "./gur_user_figure_skating_stats";
import {gur_user_golf_stats} from "./gur_user_golf_stats";
import {gur_user_gymnastics_stats} from "./gur_user_gymnastics_stats";
import {gur_user_judo_stats} from "./gur_user_judo_stats";
import {gur_user_kabaddi_stats} from "./gur_user_kabaddi_stats";
import {gur_user_karate_stats} from "./gur_user_karate_stats";
import {gur_user_modern_pentathlon_stats} from "./gur_user_modern_pentathlon_stats";
import {gur_user_netball_stats} from "./gur_user_netball_stats";
import {gur_user_powerlifting_stats} from "./gur_user_powerlifting_stats";
import {gur_user_rowing_stats} from "./gur_user_rowing_stats";
import {gur_user_rugby_sevens_stats} from "./gur_user_rugby_sevens_stats";
import {gur_user_rugby_union_stats} from "./gur_user_rugby_union_stats";
import {gur_user_shooting_stats} from "./gur_user_shooting_stats";
import {gur_user_soccer_stats} from "./gur_user_soccer_stats";
import {gur_user_squash_stats} from "./gur_user_squash_stats";
import {gur_user_swimming_stats} from "./gur_user_swimming_stats";
import {gur_user_table_tennis_stats} from "./gur_user_table_tennis_stats";
import {gur_user_taekwondo_stats} from "./gur_user_taekwondo_stats";
import {gur_user_tennis_stats} from "./gur_user_tennis_stats";
import {gur_user_triathlon_stats} from "./gur_user_triathlon_stats";
import {gur_user_volleyball_stats} from "./gur_user_volleyball_stats";
import {gur_user_water_polo_stats} from "./gur_user_water_polo_stats";
import {gur_user_weightlifting_stats} from "./gur_user_weightlifting_stats";
import {gur_user_wrestling_stats} from "./gur_user_wrestling_stats";


@Entity("gur_user_competition_played" ,{schema:"sportsmatik_local" } )
@Index("ucp_usr_id",["ucpUsr",])
@Index("ucp_com_id",["ucpCom",])
@Index("ucp_com_played_team",["ucpComPlayedTeam",])
@Index("ucp_venue_cnt_id",["ucpVenueCnt",])
@Index("ucp_spo_id",["ucpSpo",])
@Index("ucp_venue_city",["ucpVenueCity",])
@Index("ucp_cricket_format",["ucpCricketFormat",])
@Index("ucp_createdby_user",["ucpCreatedbyUser",])
@Index("ucp_level",["ucpLevel",])
@Index("ucp_rank",["ucpRank",])
export class gur_user_competition_played {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ucp_id"
        })
    ucp_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCompetitionPlayeds,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_usr_id'})
    ucpUsr:gur_users | null;


   
    @ManyToOne(()=>gur_competitions, (gur_competitions: gur_competitions)=>gur_competitions.gurUserCompetitionPlayeds,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_com_id'})
    ucpCom:gur_competitions | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurUserCompetitionPlayeds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_spo_id'})
    ucpSpo:gur_sports | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCompetitionPlayeds3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_cricket_format'})
    ucpCricketFormat:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCompetitionPlayeds,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_level'})
    ucpLevel:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ucp_season_based"
        })
    ucp_season_based:boolean;
        

    @Column("date",{ 
        nullable:false,
        name:"ucp_year_start"
        })
    ucp_year_start:string;
        

    @Column("date",{ 
        nullable:true,
        name:"ucp_year_end"
        })
    ucp_year_end:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurUserCompetitionPlayeds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_venue_cnt_id'})
    ucpVenueCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurUserCompetitionPlayeds,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_venue_city'})
    ucpVenueCity:gur_all_cities | null;


   
    @ManyToOne(()=>gur_teams, (gur_teams: gur_teams)=>gur_teams.gurUserCompetitionPlayeds,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_com_played_team'})
    ucpComPlayedTeam:gur_teams | null;


    @Column("text",{ 
        nullable:true,
        name:"ucp_description"
        })
    ucp_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ucp_status"
        })
    ucp_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ucp_admin_review"
        })
    ucp_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserCompetitionPlayeds2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_rank'})
    ucpRank:gur_value_list | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCompetitionPlayeds2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_createdby_user'})
    ucpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ucp_createdby"
        })
    ucp_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"ucp_modifiedby"
        })
    ucp_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ucp_modifiedtime"
        })
    ucp_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_archery_stats, (gur_user_archery_stats: gur_user_archery_stats)=>gur_user_archery_stats.arsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserArcheryStatss:gur_user_archery_stats[];
    

   
    @OneToMany(()=>gur_user_athletics_stats, (gur_user_athletics_stats: gur_user_athletics_stats)=>gur_user_athletics_stats.atsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserAthleticsStatss:gur_user_athletics_stats[];
    

   
    @OneToMany(()=>gur_user_badminton_stats, (gur_user_badminton_stats: gur_user_badminton_stats)=>gur_user_badminton_stats.bdsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBadmintonStatss:gur_user_badminton_stats[];
    

   
    @OneToMany(()=>gur_user_basketball_stats, (gur_user_basketball_stats: gur_user_basketball_stats)=>gur_user_basketball_stats.basUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBasketballStatss:gur_user_basketball_stats[];
    

   
    @OneToMany(()=>gur_user_boxing_stats, (gur_user_boxing_stats: gur_user_boxing_stats)=>gur_user_boxing_stats.bosUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserBoxingStatss:gur_user_boxing_stats[];
    

   
    @OneToMany(()=>gur_user_chess_stats, (gur_user_chess_stats: gur_user_chess_stats)=>gur_user_chess_stats.ucsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserChessStatss:gur_user_chess_stats[];
    

   
    @OneToMany(()=>gur_user_cricket_stats, (gur_user_cricket_stats: gur_user_cricket_stats)=>gur_user_cricket_stats.ucsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCricketStatss:gur_user_cricket_stats[];
    

   
    @OneToMany(()=>gur_user_cycling_stats, (gur_user_cycling_stats: gur_user_cycling_stats)=>gur_user_cycling_stats.cysUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCyclingStatss:gur_user_cycling_stats[];
    

   
    @OneToMany(()=>gur_user_diving_stats, (gur_user_diving_stats: gur_user_diving_stats)=>gur_user_diving_stats.dvsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserDivingStatss:gur_user_diving_stats[];
    

   
    @OneToMany(()=>gur_user_field_hockey_stats, (gur_user_field_hockey_stats: gur_user_field_hockey_stats)=>gur_user_field_hockey_stats.fhsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFieldHockeyStatss:gur_user_field_hockey_stats[];
    

   
    @OneToMany(()=>gur_user_figure_skating_stats, (gur_user_figure_skating_stats: gur_user_figure_skating_stats)=>gur_user_figure_skating_stats.fssUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserFigureSkatingStatss:gur_user_figure_skating_stats[];
    

   
    @OneToMany(()=>gur_user_golf_stats, (gur_user_golf_stats: gur_user_golf_stats)=>gur_user_golf_stats.glsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGolfStatss:gur_user_golf_stats[];
    

   
    @OneToMany(()=>gur_user_gymnastics_stats, (gur_user_gymnastics_stats: gur_user_gymnastics_stats)=>gur_user_gymnastics_stats.gysUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserGymnasticsStatss:gur_user_gymnastics_stats[];
    

   
    @OneToMany(()=>gur_user_judo_stats, (gur_user_judo_stats: gur_user_judo_stats)=>gur_user_judo_stats.jusUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserJudoStatss:gur_user_judo_stats[];
    

   
    @OneToMany(()=>gur_user_kabaddi_stats, (gur_user_kabaddi_stats: gur_user_kabaddi_stats)=>gur_user_kabaddi_stats.kbsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKabaddiStatss:gur_user_kabaddi_stats[];
    

   
    @OneToMany(()=>gur_user_karate_stats, (gur_user_karate_stats: gur_user_karate_stats)=>gur_user_karate_stats.kasUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserKarateStatss:gur_user_karate_stats[];
    

   
    @OneToMany(()=>gur_user_modern_pentathlon_stats, (gur_user_modern_pentathlon_stats: gur_user_modern_pentathlon_stats)=>gur_user_modern_pentathlon_stats.mpsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserModernPentathlonStatss:gur_user_modern_pentathlon_stats[];
    

   
    @OneToMany(()=>gur_user_netball_stats, (gur_user_netball_stats: gur_user_netball_stats)=>gur_user_netball_stats.ntsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserNetballStatss:gur_user_netball_stats[];
    

   
    @OneToMany(()=>gur_user_powerlifting_stats, (gur_user_powerlifting_stats: gur_user_powerlifting_stats)=>gur_user_powerlifting_stats.upsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserPowerliftingStatss:gur_user_powerlifting_stats[];
    

   
    @OneToMany(()=>gur_user_rowing_stats, (gur_user_rowing_stats: gur_user_rowing_stats)=>gur_user_rowing_stats.rosUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRowingStatss:gur_user_rowing_stats[];
    

   
    @OneToMany(()=>gur_user_rugby_sevens_stats, (gur_user_rugby_sevens_stats: gur_user_rugby_sevens_stats)=>gur_user_rugby_sevens_stats.rgsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbySevensStatss:gur_user_rugby_sevens_stats[];
    

   
    @OneToMany(()=>gur_user_rugby_union_stats, (gur_user_rugby_union_stats: gur_user_rugby_union_stats)=>gur_user_rugby_union_stats.rgsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserRugbyUnionStatss:gur_user_rugby_union_stats[];
    

   
    @OneToMany(()=>gur_user_shooting_stats, (gur_user_shooting_stats: gur_user_shooting_stats)=>gur_user_shooting_stats.shsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserShootingStatss:gur_user_shooting_stats[];
    

   
    @OneToMany(()=>gur_user_soccer_stats, (gur_user_soccer_stats: gur_user_soccer_stats)=>gur_user_soccer_stats.ussUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSoccerStatss:gur_user_soccer_stats[];
    

   
    @OneToMany(()=>gur_user_squash_stats, (gur_user_squash_stats: gur_user_squash_stats)=>gur_user_squash_stats.sqsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSquashStatss:gur_user_squash_stats[];
    

   
    @OneToMany(()=>gur_user_swimming_stats, (gur_user_swimming_stats: gur_user_swimming_stats)=>gur_user_swimming_stats.spsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserSwimmingStatss:gur_user_swimming_stats[];
    

   
    @OneToMany(()=>gur_user_table_tennis_stats, (gur_user_table_tennis_stats: gur_user_table_tennis_stats)=>gur_user_table_tennis_stats.ttsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTableTennisStatss:gur_user_table_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_taekwondo_stats, (gur_user_taekwondo_stats: gur_user_taekwondo_stats)=>gur_user_taekwondo_stats.tasUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTaekwondoStatss:gur_user_taekwondo_stats[];
    

   
    @OneToMany(()=>gur_user_tennis_stats, (gur_user_tennis_stats: gur_user_tennis_stats)=>gur_user_tennis_stats.utsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTennisStatss:gur_user_tennis_stats[];
    

   
    @OneToMany(()=>gur_user_triathlon_stats, (gur_user_triathlon_stats: gur_user_triathlon_stats)=>gur_user_triathlon_stats.trsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserTriathlonStatss:gur_user_triathlon_stats[];
    

   
    @OneToMany(()=>gur_user_volleyball_stats, (gur_user_volleyball_stats: gur_user_volleyball_stats)=>gur_user_volleyball_stats.vosUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserVolleyballStatss:gur_user_volleyball_stats[];
    

   
    @OneToMany(()=>gur_user_water_polo_stats, (gur_user_water_polo_stats: gur_user_water_polo_stats)=>gur_user_water_polo_stats.wpsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWaterPoloStatss:gur_user_water_polo_stats[];
    

   
    @OneToMany(()=>gur_user_weightlifting_stats, (gur_user_weightlifting_stats: gur_user_weightlifting_stats)=>gur_user_weightlifting_stats.uwsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWeightliftingStatss:gur_user_weightlifting_stats[];
    

   
    @OneToMany(()=>gur_user_wrestling_stats, (gur_user_wrestling_stats: gur_user_wrestling_stats)=>gur_user_wrestling_stats.wrsUcp,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserWrestlingStatss:gur_user_wrestling_stats[];
    
}
