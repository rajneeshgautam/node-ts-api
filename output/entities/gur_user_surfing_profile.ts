import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_surfing_profile" ,{schema:"sportsmatik_local" } )
@Index("sup_usr_id",["supUsr",])
@Index("sup_createdby_user",["supCreatedbyUser",])
@Index("sup_stance",["supStance",])
export class gur_user_surfing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sup_id"
        })
    sup_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSurfingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sup_usr_id'})
    supUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSurfingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sup_stance'})
    supStance:gur_value_list | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"sup_disciplines"
        })
    sup_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sup_modifiedtime"
        })
    sup_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSurfingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sup_createdby_user'})
    supCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sup_modifiedby"
        })
    sup_modifiedby:string | null;
        
}
