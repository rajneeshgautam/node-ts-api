import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_birthday_wishes_ls} from "./gur_birthday_wishes_ls";


@Entity("gur_birthday_wishes" ,{schema:"sportsmatik_local" } )
export class gur_birthday_wishes {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bws_id"
        })
    bws_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bws_createdby"
        })
    bws_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"bws_modifiedby"
        })
    bws_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bws_modifiedtime"
        })
    bws_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_birthday_wishes_ls, (gur_birthday_wishes_ls: gur_birthday_wishes_ls)=>gur_birthday_wishes_ls.bwlBws,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBirthdayWishesLss:gur_birthday_wishes_ls[];
    
}
