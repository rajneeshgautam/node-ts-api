import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_health_exercise_treatments} from "./gur_health_exercise_treatments";


@Entity("gur_health_exercise_treatments_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_health_exercise_treatments_ls_ibfk_1",["htlHet",])
export class gur_health_exercise_treatments_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"htl_id"
        })
    htl_id:number;
        

   
    @ManyToOne(()=>gur_health_exercise_treatments, (gur_health_exercise_treatments: gur_health_exercise_treatments)=>gur_health_exercise_treatments.gurHealthExerciseTreatmentsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'htl_het_id'})
    htlHet:gur_health_exercise_treatments | null;


    @Column("varchar",{ 
        nullable:true,
        name:"htl_title"
        })
    htl_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"htl_description"
        })
    htl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"htl_lng_code"
        })
    htl_lng_code:string;
        
}
