import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_user_login_tokens" ,{schema:"sportsmatik_local" } )
export class gur_user_login_tokens {

    @Column("bigint",{ 
        nullable:false,
        name:"ltk_id"
        })
    ltk_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ltk_usr_id"
        })
    ltk_usr_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"ltk_usr_type"
        })
    ltk_usr_type:number;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ltk_created_time"
        })
    ltk_created_time:Date;
        
}
