import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_orders_old} from "./gur_orders_old";
import {gur_plan_categories} from "./gur_plan_categories";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_user_plans_details" ,{schema:"sportsmatik_local" } )
@Index("upd_pln_id",["upd_pln_id",])
@Index("upd_user_id",["upd_user_id",])
@Index("gur_user_plans_details_ibfk_1",["updOrd",])
@Index("gur_user_plans_details_ibfk_2",["updCat",])
@Index("gur_user_plans_details_ibfk_3",["updUty",])
export class gur_user_plans_details {

    @Column("bigint",{ 
        nullable:false,
        name:"upd_id"
        })
    upd_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"upd_pln_id"
        })
    upd_pln_id:number;
        

    @Column("date",{ 
        nullable:false,
        name:"upd_start_date"
        })
    upd_start_date:string;
        

    @Column("date",{ 
        nullable:false,
        name:"upd_end_date"
        })
    upd_end_date:string;
        

   
    @ManyToOne(()=>gur_orders_old, (gur_orders_old: gur_orders_old)=>gur_orders_old.gurUserPlansDetailss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upd_ord_id'})
    updOrd:gur_orders_old | null;


   
    @ManyToOne(()=>gur_plan_categories, (gur_plan_categories: gur_plan_categories)=>gur_plan_categories.gurUserPlansDetailss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upd_cat_id'})
    updCat:gur_plan_categories | null;


    @Column("int",{ 
        nullable:false,
        name:"upd_credits"
        })
    upd_credits:number;
        

    @Column("int",{ 
        nullable:false,
        name:"upd_credits_left"
        })
    upd_credits_left:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"upd_user_id"
        })
    upd_user_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserPlansDetailss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upd_uty_id'})
    updUty:gur_user_types | null;

}
