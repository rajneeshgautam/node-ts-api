import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_colleges" ,{schema:"sportsmatik_local" } )
@Index("clg_gst_number",["clg_gst_number",],{unique:true})
@Index("clg_usr_id",["clgUsr",])
@Index("clg_createdby_user",["clgCreatedbyUser",])
@Index("clg_country",["clgCountry",])
@Index("clg_sts_id",["clgSts",])
@Index("clg_dis_id",["clgDis",])
@Index("clg_name",["clg_name",])
@Index("clg_name_2",["clg_name",])
export class gur_colleges {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"clg_id"
        })
    clg_id:number;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"clg_type"
        })
    clg_type:boolean | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clg_slug"
        })
    clg_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"clg_name"
        })
    clg_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurCollegess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clg_usr_id'})
    clgUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"clg_address"
        })
    clg_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clg_address2"
        })
    clg_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"clg_pincode"
        })
    clg_pincode:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurCollegess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clg_country'})
    clgCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurCollegess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clg_dis_id'})
    clgDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurCollegess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clg_sts_id'})
    clgSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"clg_address_google_map"
        })
    clg_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clg_email"
        })
    clg_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clg_email_verified"
        })
    clg_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clg_email_verify_time"
        })
    clg_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"clg_phone1"
        })
    clg_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clg_phone1_verified"
        })
    clg_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"clg_phone1_otp"
        })
    clg_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clg_phone1_otp_time"
        })
    clg_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:25,
        name:"clg_phone2"
        })
    clg_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clg_phone2_verified"
        })
    clg_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"clg_phone2_otp"
        })
    clg_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clg_phone2_otp_time"
        })
    clg_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"clg_status"
        })
    clg_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clg_profile_pic"
        })
    clg_profile_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clg_intro_status"
        })
    clg_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"clg_profile_summary"
        })
    clg_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"clg_founded_year"
        })
    clg_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"clg_founded_month"
        })
    clg_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clg_last_login_date"
        })
    clg_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"clg_last_login_details"
        })
    clg_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"clg_intro_modifiedtime"
        })
    clg_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"clg_profile_pic_modifiedtime"
        })
    clg_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"clg_free_sports"
        })
    clg_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"clg_free_sub_users"
        })
    clg_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"clg_gst_number"
        })
    clg_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"clg_viewable"
        })
    clg_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurCollegess2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'clg_createdby_user'})
    clgCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"clg_createdby"
        })
    clg_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"clg_modifiedby"
        })
    clg_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"clg_modifiedtime"
        })
    clg_modifiedtime:Date;
        
}
