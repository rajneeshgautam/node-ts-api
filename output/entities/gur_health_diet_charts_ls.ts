import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_health_diet_charts_ls" ,{schema:"sportsmatik_local" } )
@Index("htl_htc_id",["hdl_htc_id",])
export class gur_health_diet_charts_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"hdl_id"
        })
    hdl_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"hdl_htc_id"
        })
    hdl_htc_id:number;
        

    @Column("text",{ 
        nullable:true,
        name:"hdl_description"
        })
    hdl_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"hdl_lng_code"
        })
    hdl_lng_code:string;
        
}
