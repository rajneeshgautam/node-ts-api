import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_plan_features} from "./gur_plan_features";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_plan_features_map" ,{schema:"sportsmatik_local" } )
@Index("user_for_feature",["pfmPlf","pfmUty",],{unique:true})
@Index("gur_plan_features_map_ibfk_3",["pfmUty",])
export class gur_plan_features_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"pfm_id"
        })
    pfm_id:string;
        

   
    @ManyToOne(()=>gur_plan_features, (gur_plan_features: gur_plan_features)=>gur_plan_features.gurPlanFeaturesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pfm_plf_id'})
    pfmPlf:gur_plan_features | null;


    @Column("int",{ 
        nullable:true,
        name:"pfm_credits"
        })
    pfm_credits:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"pfm_credits_unlist"
        })
    pfm_credits_unlist:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurPlanFeaturesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pfm_uty_id'})
    pfmUty:gur_user_types | null;


    @Column("enum",{ 
        nullable:false,
        enum:["Individual","Corporate","Merchant"],
        name:"pfm_utype_label"
        })
    pfm_utype_label:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"pfm_sort_order"
        })
    pfm_sort_order:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"pfm_limit_nonpremium"
        })
    pfm_limit_nonpremium:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"pfm_limit_label_nonpremium"
        })
    pfm_limit_label_nonpremium:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"pfm_limit_premium"
        })
    pfm_limit_premium:number | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"pfm_limit_label_premium"
        })
    pfm_limit_label_premium:number | null;
        
}
