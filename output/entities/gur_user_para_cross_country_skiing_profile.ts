import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_cross_country_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("pcc_usr_id",["pccUsr",])
@Index("pcc_disablility_class",["pccDisablilityClass",])
@Index("pcc_createdby_user",["pccCreatedbyUser",])
export class gur_user_para_cross_country_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pcc_id"
        })
    pcc_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCrossCountrySkiingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcc_usr_id'})
    pccUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaCrossCountrySkiingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcc_disablility_class'})
    pccDisablilityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pcc_modifiedtime"
        })
    pcc_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCrossCountrySkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pcc_createdby_user'})
    pccCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pcc_modifiedby"
        })
    pcc_modifiedby:string | null;
        
}
