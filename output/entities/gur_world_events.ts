import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_matik_knowhow_gallery_media} from "./gur_matik_knowhow_gallery_media";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";
import {gur_world_events_ls} from "./gur_world_events_ls";
import {gur_world_events_records} from "./gur_world_events_records";
import {gur_world_events_schedules} from "./gur_world_events_schedules";


@Entity("gur_world_events" ,{schema:"sportsmatik_local" } )
@Index("venue",["evt_venue",])
@Index("gur_world_events_ibfk_1",["evtKnc",])
export class gur_world_events {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"evt_id"
        })
    evt_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_photo"
        })
    evt_photo:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"evt_photo_data"
        })
    evt_photo_data:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_logo"
        })
    evt_logo:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"evt_logo_data"
        })
    evt_logo_data:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_mascot"
        })
    evt_mascot:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"evt_mascot_data"
        })
    evt_mascot_data:string;
        

    @Column("date",{ 
        nullable:false,
        name:"evt_date"
        })
    evt_date:string;
        

    @Column("date",{ 
        nullable:true,
        name:"evt_end_date"
        })
    evt_end_date:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"evt_status"
        })
    evt_status:boolean;
        

    @Column("text",{ 
        nullable:false,
        name:"evt_spo_id"
        })
    evt_spo_id:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"evt_single_sport"
        })
    evt_single_sport:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_organizer"
        })
    evt_organizer:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_contact"
        })
    evt_contact:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_email"
        })
    evt_email:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_address"
        })
    evt_address:string | null;
        

    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"evt_venue"
        })
    evt_venue:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_link"
        })
    evt_link:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"evt_cnt_id"
        })
    evt_cnt_id:string | null;
        

    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"evt_city"
        })
    evt_city:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"evt_contacts"
        })
    evt_contacts:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_createdby"
        })
    evt_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"evt_modifiedby"
        })
    evt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"evt_modifiedtime"
        })
    evt_modifiedtime:Date;
        

    @Column("text",{ 
        nullable:true,
        name:"evt_downloads"
        })
    evt_downloads:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"evt_links"
        })
    evt_links:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"evt_participate"
        })
    evt_participate:string | null;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWorldEventss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evt_knc_id'})
    evtKnc:gur_matik_knowhow_tab_categories | null;


    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"evt_only_knowhow"
        })
    evt_only_knowhow:number;
        

   
    @OneToMany(()=>gur_matik_knowhow_gallery_media, (gur_matik_knowhow_gallery_media: gur_matik_knowhow_gallery_media)=>gur_matik_knowhow_gallery_media.kgmEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowGalleryMedias:gur_matik_knowhow_gallery_media[];
    

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    

   
    @OneToMany(()=>gur_world_events_ls, (gur_world_events_ls: gur_world_events_ls)=>gur_world_events_ls.evlEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsLss:gur_world_events_ls[];
    

   
    @OneToMany(()=>gur_world_events_records, (gur_world_events_records: gur_world_events_records)=>gur_world_events_records.werEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurWorldEventsRecordss:gur_world_events_records[];
    

   
    @OneToMany(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.evsEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsScheduless:gur_world_events_schedules[];
    
}
