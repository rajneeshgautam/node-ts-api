import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_matik_knowhow_gallery_media} from "./gur_matik_knowhow_gallery_media";


@Entity("gur_matik_knowhow_gallery" ,{schema:"sportsmatik_local" } )
@Index("khl_wkh_id",["khgMkh",])
@Index("gur_matik_knowhow_gallery_ibfk_4",["khgCategory",])
export class gur_matik_knowhow_gallery {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"khg_id"
        })
    khg_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowGallerys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khg_mkh_id'})
    khgMkh:gur_matik_knowhow | null;


    @Column("varchar",{ 
        nullable:true,
        name:"khg_title"
        })
    khg_title:string | null;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowGallerys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khg_category'})
    khgCategory:gur_matik_knowhow_tab_categories | null;


    @Column("varchar",{ 
        nullable:true,
        name:"khg_cover"
        })
    khg_cover:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khg_createdby"
        })
    khg_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khg_modifiedby"
        })
    khg_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khg_modifiedtime"
        })
    khg_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_gallery_media, (gur_matik_knowhow_gallery_media: gur_matik_knowhow_gallery_media)=>gur_matik_knowhow_gallery_media.kgmKhg,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowGalleryMedias:gur_matik_knowhow_gallery_media[];
    
}
