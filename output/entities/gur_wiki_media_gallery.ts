import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_terminologies} from "./gur_wiki_terminologies";
import {gur_wiki_sports_equipage} from "./gur_wiki_sports_equipage";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_wiki_technologies} from "./gur_wiki_technologies";
import {gur_wiki_competitions} from "./gur_wiki_competitions";
import {gur_wiki_knowhow} from "./gur_wiki_knowhow";
import {gur_faq_lists} from "./gur_faq_lists";
import {gur_world_events} from "./gur_world_events";


@Entity("gur_wiki_media_gallery" ,{schema:"sportsmatik_local" } )
@Index("wmg_ter_id",["wmgTer",])
@Index("wmg_uae_id",["wmgUae",])
@Index("wmg_wvn_id",["wmgWvn",])
@Index("wmg_tec_id",["wmgTec",])
@Index("wmg_wcm_id",["wmgWcm",])
@Index("wmg_wkh_id",["wmgWkh",])
@Index("wmg_fll_id",["wmgFll",])
@Index("gur_wiki_media_gallery_ibfk_8",["wmgEvt",])
export class gur_wiki_media_gallery {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wmg_id"
        })
    wmg_id:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["terminology","venues","equipments","technology","competition","knowhow","faq","event"],
        name:"wmg_mode"
        })
    wmg_mode:string;
        

   
    @ManyToOne(()=>gur_wiki_terminologies, (gur_wiki_terminologies: gur_wiki_terminologies)=>gur_wiki_terminologies.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_ter_id'})
    wmgTer:gur_wiki_terminologies | null;


   
    @ManyToOne(()=>gur_wiki_sports_equipage, (gur_wiki_sports_equipage: gur_wiki_sports_equipage)=>gur_wiki_sports_equipage.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_uae_id'})
    wmgUae:gur_wiki_sports_equipage | null;


   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_wvn_id'})
    wmgWvn:gur_wiki_venues | null;


   
    @ManyToOne(()=>gur_wiki_technologies, (gur_wiki_technologies: gur_wiki_technologies)=>gur_wiki_technologies.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_tec_id'})
    wmgTec:gur_wiki_technologies | null;


   
    @ManyToOne(()=>gur_wiki_competitions, (gur_wiki_competitions: gur_wiki_competitions)=>gur_wiki_competitions.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_wcm_id'})
    wmgWcm:gur_wiki_competitions | null;


   
    @ManyToOne(()=>gur_wiki_knowhow, (gur_wiki_knowhow: gur_wiki_knowhow)=>gur_wiki_knowhow.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_wkh_id'})
    wmgWkh:gur_wiki_knowhow | null;


   
    @ManyToOne(()=>gur_faq_lists, (gur_faq_lists: gur_faq_lists)=>gur_faq_lists.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_fll_id'})
    wmgFll:gur_faq_lists | null;


   
    @ManyToOne(()=>gur_world_events, (gur_world_events: gur_world_events)=>gur_world_events.gurWikiMediaGallerys,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wmg_evt_id'})
    wmgEvt:gur_world_events | null;


    @Column("tinyint",{ 
        nullable:true,
        default: () => "'3'",
        name:"wmg_sort_order"
        })
    wmg_sort_order:number | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["audio","video","photo"],
        name:"wmg_type"
        })
    wmg_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wmg_file"
        })
    wmg_file:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wmg_file_alt"
        })
    wmg_file_alt:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wmg_file_title"
        })
    wmg_file_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wmg_file_source"
        })
    wmg_file_source:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"wmg_file_source_url"
        })
    wmg_file_source_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wmg_video_url"
        })
    wmg_video_url:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wmg_createdby"
        })
    wmg_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wmg_modifiedby"
        })
    wmg_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wmg_modifiedtime"
        })
    wmg_modifiedtime:Date;
        
}
