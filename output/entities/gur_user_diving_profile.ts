import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_diving_profile" ,{schema:"sportsmatik_local" } )
@Index("div_usr_id",["divUsr",])
@Index("div_primary_event",["divPrimaryEvent",])
@Index("div_secondary_event",["divSecondaryEvent",])
@Index("div_createdby_user",["divCreatedbyUser",])
export class gur_user_diving_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"div_id"
        })
    div_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDivingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'div_usr_id'})
    divUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserDivingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'div_primary_event'})
    divPrimaryEvent:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserDivingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'div_secondary_event'})
    divSecondaryEvent:gur_value_list | null;


    @Column("varchar",{ 
        nullable:true,
        name:"div_types"
        })
    div_types:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"div_modifiedtime"
        })
    div_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserDivingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'div_createdby_user'})
    divCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"div_modifiedby"
        })
    div_modifiedby:string | null;
        
}
