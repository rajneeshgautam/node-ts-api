import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_modules} from "./gur_user_modules";
import {gur_user_types} from "./gur_user_types";
import {gur_profile_completeness_modules} from "./gur_profile_completeness_modules";


@Entity("gur_user_modules_map" ,{schema:"sportsmatik_local" } )
@Index("umm_amd_id",["ummAmd","ummUty",],{unique:true})
@Index("umm_uty_id",["ummUty",])
@Index("umm_pcm_id",["ummPcm",])
export class gur_user_modules_map {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"umm_id"
        })
    umm_id:number;
        

   
    @ManyToOne(()=>gur_user_modules, (gur_user_modules: gur_user_modules)=>gur_user_modules.gurUserModulesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'umm_amd_id'})
    ummAmd:gur_user_modules | null;


   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurUserModulesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'umm_uty_id'})
    ummUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_profile_completeness_modules, (gur_profile_completeness_modules: gur_profile_completeness_modules)=>gur_profile_completeness_modules.gurUserModulesMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'umm_pcm_id'})
    ummPcm:gur_profile_completeness_modules | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'-1'",
        name:"umm_sort_order"
        })
    umm_sort_order:boolean;
        
}
