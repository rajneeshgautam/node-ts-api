import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_archery_stats" ,{schema:"sportsmatik_local" } )
@Index("ars_usr_id",["arsUsr",])
@Index("ars_ucp_id",["arsUcp",])
@Index("ars_event",["arsEvent",])
@Index("ars_rank",["arsRank",])
export class gur_user_archery_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"ars_id"
        })
    ars_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserArcheryStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ars_usr_id'})
    arsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserArcheryStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ars_ucp_id'})
    arsUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserArcheryStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ars_event'})
    arsEvent:gur_value_list | null;


    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"ars_total_score"
        })
    ars_total_score:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"ars_10s"
        })
    ars_10s:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"ars_xs"
        })
    ars_xs:number;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserArcheryStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ars_rank'})
    arsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ars_modifiedtime"
        })
    ars_modifiedtime:Date;
        
}
