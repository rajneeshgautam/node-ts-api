import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_softball_profile" ,{schema:"sportsmatik_local" } )
@Index("sof_usr_id",["sofUsr",])
@Index("sof_position1",["sofPosition",])
@Index("sof_position2",["sofPosition2",])
@Index("sof_createdby_user",["sofCreatedbyUser",])
export class gur_user_softball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"sof_id"
        })
    sof_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSoftballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sof_usr_id'})
    sofUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSoftballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sof_position1'})
    sofPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserSoftballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sof_position2'})
    sofPosition2:gur_value_list | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"sof_bat_hand"
        })
    sof_bat_hand:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"sof_throw_hand"
        })
    sof_throw_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"sof_modifiedtime"
        })
    sof_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSoftballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sof_createdby_user'})
    sofCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"sof_modifiedby"
        })
    sof_modifiedby:string | null;
        
}
