import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_postcodes" ,{schema:"sportsmatik_local" } )
@Index("pos_dis_id",["pos_dis_id",])
@Index("pos_name",["pos_name",])
@Index("pos_pin",["pos_pin",])
export class gur_postcodes {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pos_id"
        })
    pos_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"pos_dis_id"
        })
    pos_dis_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pos_name"
        })
    pos_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"pos_pin"
        })
    pos_pin:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pos_lat"
        })
    pos_lat:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pos_long"
        })
    pos_long:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"pos_createdby"
        })
    pos_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"pos_modifiedby"
        })
    pos_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pos_modifiedtime"
        })
    pos_modifiedtime:Date;
        
}
