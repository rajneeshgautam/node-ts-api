import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_team_handball_profile" ,{schema:"sportsmatik_local" } )
@Index("thb_usr_id",["thbUsr",])
@Index("thb_position1",["thbPosition",])
@Index("thb_position2",["thbPosition2",])
@Index("thb_createdby_user",["thbCreatedbyUser",])
export class gur_user_team_handball_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"thb_id"
        })
    thb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTeamHandballProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'thb_usr_id'})
    thbUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTeamHandballProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'thb_position1'})
    thbPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTeamHandballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'thb_position2'})
    thbPosition2:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"thb_modifiedtime"
        })
    thb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTeamHandballProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'thb_createdby_user'})
    thbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"thb_modifiedby"
        })
    thb_modifiedby:string | null;
        
}
