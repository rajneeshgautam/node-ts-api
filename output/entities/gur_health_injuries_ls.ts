import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_health_injuries} from "./gur_health_injuries";


@Entity("gur_health_injuries_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_health_injuries_ls_ibfk_1",["wilHwi",])
export class gur_health_injuries_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wil_id"
        })
    wil_id:number;
        

   
    @ManyToOne(()=>gur_health_injuries, (gur_health_injuries: gur_health_injuries)=>gur_health_injuries.gurHealthInjuriesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wil_hwi_id'})
    wilHwi:gur_health_injuries | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wil_title"
        })
    wil_title:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wil_description"
        })
    wil_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wil_symptoms"
        })
    wil_symptoms:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wil_prevention"
        })
    wil_prevention:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wil_treatment"
        })
    wil_treatment:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wil_referrals"
        })
    wil_referrals:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:2,
        name:"wil_lng_code"
        })
    wil_lng_code:string;
        
}
