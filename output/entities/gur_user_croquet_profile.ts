import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_croquet_profile" ,{schema:"sportsmatik_local" } )
@Index("cro_usr_id",["croUsr",])
@Index("cro_createdby_user",["croCreatedbyUser",])
export class gur_user_croquet_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"cro_id"
        })
    cro_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCroquetProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cro_usr_id'})
    croUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"cro_primary_hand"
        })
    cro_primary_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cro_modifiedtime"
        })
    cro_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCroquetProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cro_createdby_user'})
    croCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"cro_modifiedby"
        })
    cro_modifiedby:string | null;
        
}
