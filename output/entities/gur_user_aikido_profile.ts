import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_aikido_profile" ,{schema:"sportsmatik_local" } )
@Index("aik_usr_id",["aikUsr",])
@Index("aik_degree",["aikDegree",])
@Index("aik_degree_belt",["aikDegreeBelt",])
@Index("aik_createdby_user",["aikCreatedbyUser",])
export class gur_user_aikido_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"aik_id"
        })
    aik_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAikidoProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aik_usr_id'})
    aikUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"aik_style"
        })
    aik_style:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAikidoProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aik_degree'})
    aikDegree:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserAikidoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aik_degree_belt'})
    aikDegreeBelt:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"aik_modifiedtime"
        })
    aik_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserAikidoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aik_createdby_user'})
    aikCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"aik_modifiedby"
        })
    aik_modifiedby:string | null;
        
}
