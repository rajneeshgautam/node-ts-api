import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";


@Entity("gur_matik_knowhow_career_blogs" ,{schema:"sportsmatik_local" } )
@Index("khl_spo_id",["khbSpo",])
export class gur_matik_knowhow_career_blogs {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"khb_id"
        })
    khb_id:string;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowCareerBlogss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'khb_spo_id'})
    khbSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"khb_title"
        })
    khb_title:string;
        

    @Column("text",{ 
        nullable:true,
        name:"khb_photos"
        })
    khb_photos:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"khb_description"
        })
    khb_description:string;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"khb_sort_order"
        })
    khb_sort_order:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"khb_createdby"
        })
    khb_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"khb_modifiedby"
        })
    khb_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"khb_modifiedtime"
        })
    khb_modifiedtime:Date;
        
}
