import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_world_events_schedules} from "./gur_world_events_schedules";


@Entity("gur_world_events_schedules_map" ,{schema:"sportsmatik_local" } )
@Index("gur_world_events_schedules_map_ibfk_1",["esmEvs",])
export class gur_world_events_schedules_map {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"esm_id"
        })
    esm_id:string;
        

   
    @ManyToOne(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.gurWorldEventsSchedulesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'esm_evs_id'})
    esmEvs:gur_world_events_schedules | null;


    @Column("bigint",{ 
        nullable:false,
        name:"esm_participant"
        })
    esm_participant:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"esm_score"
        })
    esm_score:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"esm_result"
        })
    esm_result:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"esm_members"
        })
    esm_members:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"esm_member_cnt"
        })
    esm_member_cnt:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"esm_member_score"
        })
    esm_member_score:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"esm_member_result"
        })
    esm_member_result:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"esm_sort_order"
        })
    esm_sort_order:number;
        
}
