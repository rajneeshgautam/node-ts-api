import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_infra_portfolio_media} from "./gur_infra_portfolio_media";


@Entity("gur_infra_portfolio" ,{schema:"sportsmatik_local" } )
@Index("ipo_uty_id",["ipoUty",])
@Index("ipo_createdby_user",["ipoCreatedbyUser",])
@Index("ipo_user_id",["ipo_user_id",])
export class gur_infra_portfolio {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ipo_id"
        })
    ipo_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInfraPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ipo_uty_id'})
    ipoUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ipo_user_id"
        })
    ipo_user_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ipo_client"
        })
    ipo_client:string;
        

    @Column("text",{ 
        nullable:false,
        name:"ipo_products"
        })
    ipo_products:string;
        

    @Column("text",{ 
        nullable:false,
        name:"ipo_sports"
        })
    ipo_sports:string;
        

    @Column("date",{ 
        nullable:false,
        name:"ipo_installation"
        })
    ipo_installation:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ipo_image"
        })
    ipo_image:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:1000,
        name:"ipo_description"
        })
    ipo_description:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ipo_status"
        })
    ipo_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ipo_admin_review"
        })
    ipo_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInfraPortfolios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ipo_createdby_user'})
    ipoCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"ipo_createdby"
        })
    ipo_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"ipo_modifiedby"
        })
    ipo_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ipo_modified_time"
        })
    ipo_modified_time:Date;
        

   
    @OneToMany(()=>gur_infra_portfolio_media, (gur_infra_portfolio_media: gur_infra_portfolio_media)=>gur_infra_portfolio_media.ipmIpo,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInfraPortfolioMedias:gur_infra_portfolio_media[];
    
}
