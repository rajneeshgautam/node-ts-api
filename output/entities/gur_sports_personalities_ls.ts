import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_personalities} from "./gur_sports_personalities";


@Entity("gur_sports_personalities_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_sports_personalities_ls_ibfk_1",["wplWep",])
export class gur_sports_personalities_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"wpl_id"
        })
    wpl_id:number;
        

   
    @ManyToOne(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.gurSportsPersonalitiesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wpl_wep_id'})
    wplWep:gur_sports_personalities | null;


    @Column("varchar",{ 
        nullable:true,
        name:"wpl_name"
        })
    wpl_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wpl_full_name"
        })
    wpl_full_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_description"
        })
    wpl_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_records"
        })
    wpl_records:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_key_points"
        })
    wpl_key_points:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wpl_designation"
        })
    wpl_designation:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"wpl_profession"
        })
    wpl_profession:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_halloffame"
        })
    wpl_halloffame:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_hero_story"
        })
    wpl_hero_story:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_references"
        })
    wpl_references:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_stats"
        })
    wpl_stats:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wpl_fun_facts"
        })
    wpl_fun_facts:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"wpl_lng_code"
        })
    wpl_lng_code:string;
        
}
