import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_cross_country_skiing_profile" ,{schema:"sportsmatik_local" } )
@Index("ccs_usr_id",["ccsUsr",])
@Index("ccs_createdby_user",["ccsCreatedbyUser",])
export class gur_user_cross_country_skiing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ccs_id"
        })
    ccs_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCrossCountrySkiingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ccs_usr_id'})
    ccsUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ccs_modifiedtime"
        })
    ccs_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCrossCountrySkiingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ccs_createdby_user'})
    ccsCreatedbyUser:gur_users | null;

    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserCrossCountrySkiingProfiles3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ccs_createdby_user'})
    ccsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ccs_modifiedby"
        })
    ccs_modifiedby:string | null;
        
}
