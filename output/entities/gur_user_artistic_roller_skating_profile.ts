import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_artistic_roller_skating_profile" ,{schema:"sportsmatik_local" } )
@Index("arr_usr_id",["arrUsr",])
@Index("arr_createdby_user",["arrCreatedbyUser",])
export class gur_user_artistic_roller_skating_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"arr_id"
        })
    arr_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserArtisticRollerSkatingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arr_usr_id'})
    arrUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"arr_disciplines"
        })
    arr_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"arr_modifiedtime"
        })
    arr_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserArtisticRollerSkatingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'arr_createdby_user'})
    arrCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"arr_modifiedby"
        })
    arr_modifiedby:string | null;
        
}
