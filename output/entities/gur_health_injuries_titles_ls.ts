import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_health_injuries_titles} from "./gur_health_injuries_titles";


@Entity("gur_health_injuries_titles_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_health_injuries_titles_ls_ibfk_1",["htlHwt",])
export class gur_health_injuries_titles_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"htl_id"
        })
    htl_id:number;
        

   
    @ManyToOne(()=>gur_health_injuries_titles, (gur_health_injuries_titles: gur_health_injuries_titles)=>gur_health_injuries_titles.gurHealthInjuriesTitlesLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'htl_hwt_id'})
    htlHwt:gur_health_injuries_titles | null;


    @Column("varchar",{ 
        nullable:true,
        name:"htl_name"
        })
    htl_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"htl_lng_code"
        })
    htl_lng_code:string;
        
}
