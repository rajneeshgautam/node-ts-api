import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_lawn_bowling_profile" ,{schema:"sportsmatik_local" } )
@Index("lwb_usr_id",["lwbUsr",])
@Index("lwb_createdby_user",["lwbCreatedbyUser",])
export class gur_user_lawn_bowling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"lwb_id"
        })
    lwb_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserLawnBowlingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwb_usr_id'})
    lwbUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"lwb_hand"
        })
    lwb_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lwb_modifiedtime"
        })
    lwb_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserLawnBowlingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lwb_createdby_user'})
    lwbCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"lwb_modifiedby"
        })
    lwb_modifiedby:string | null;
        
}
