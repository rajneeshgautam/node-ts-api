import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_job_application} from "./gur_job_application";
import {gur_countries} from "./gur_countries";
import {gur_states} from "./gur_states";
import {gur_districts} from "./gur_districts";


@Entity("gur_job_experience" ,{schema:"sportsmatik_local" } )
@Index("joe_joa_id",["joeJoa",])
@Index("joe_sts_id",["joeSts",])
@Index("joe_dis_id",["joeDis",])
@Index("joe_cnt_id",["joeCnt",])
export class gur_job_experience {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"joe_id"
        })
    joe_id:number;
        

   
    @ManyToOne(()=>gur_job_application, (gur_job_application: gur_job_application)=>gur_job_application.gurJobExperiences,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joe_joa_id'})
    joeJoa:gur_job_application | null;


    @Column("varchar",{ 
        nullable:false,
        length:200,
        name:"joe_occupation"
        })
    joe_occupation:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"joe_company_name"
        })
    joe_company_name:string;
        

    @Column("date",{ 
        nullable:false,
        name:"joe_from"
        })
    joe_from:string;
        

    @Column("date",{ 
        nullable:true,
        name:"joe_to"
        })
    joe_to:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurJobExperiences,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joe_cnt_id'})
    joeCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurJobExperiences,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joe_sts_id'})
    joeSts:gur_states | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"joe_state_name"
        })
    joe_state_name:string;
        

   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurJobExperiences,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'joe_dis_id'})
    joeDis:gur_districts | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"joe_district_name"
        })
    joe_district_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"joe_is_working_here"
        })
    joe_is_working_here:boolean;
        

    @Column("float",{ 
        nullable:false,
        precision:12,
        name:"joe_gross_salary"
        })
    joe_gross_salary:number;
        

    @Column("float",{ 
        nullable:false,
        precision:12,
        name:"joe_net_salary"
        })
    joe_net_salary:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"joe_income_proof"
        })
    joe_income_proof:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"joe_description"
        })
    joe_description:string | null;
        
}
