import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_banners} from "./gur_banners";
import {gur_languages} from "./gur_languages";


@Entity("gur_banners_ls" ,{schema:"sportsmatik_local" } )
@Index("bnl_ban_id",["bnlBan",])
@Index("bnl_lng_code",["bnlLngCode",])
export class gur_banners_ls {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bnl_id"
        })
    bnl_id:number;
        

   
    @ManyToOne(()=>gur_banners, (gur_banners: gur_banners)=>gur_banners.gurBannersLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bnl_ban_id'})
    bnlBan:gur_banners | null;


    @Column("varchar",{ 
        nullable:false,
        name:"bnl_title"
        })
    bnl_title:string;
        

   
    @ManyToOne(()=>gur_languages, (gur_languages: gur_languages)=>gur_languages.gurBannersLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'bnl_lng_code'})
    bnlLngCode:gur_languages | null;

}
