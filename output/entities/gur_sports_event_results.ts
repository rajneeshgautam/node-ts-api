import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_event_schedule} from "./gur_sports_event_schedule";


@Entity("gur_sports_event_results" ,{schema:"sportsmatik_local" } )
@Index("gur_sports_event_results_ibfk_1",["evrEvs",])
export class gur_sports_event_results {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"evr_id"
        })
    evr_id:string;
        

   
    @ManyToOne(()=>gur_sports_event_schedule, (gur_sports_event_schedule: gur_sports_event_schedule)=>gur_sports_event_schedule.gurSportsEventResultss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evr_evs_id'})
    evrEvs:gur_sports_event_schedule | null;


    @Column("text",{ 
        nullable:false,
        name:"evr_result"
        })
    evr_result:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"evr_createdby"
        })
    evr_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evr_modifiedby"
        })
    evr_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"evr_modifiedtime"
        })
    evr_modifiedtime:Date;
        
}
