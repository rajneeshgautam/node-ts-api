import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_water_polo_stats" ,{schema:"sportsmatik_local" } )
@Index("wps_usr_id",["wpsUsr",])
@Index("wps_ucp_id",["wpsUcp",])
export class gur_user_water_polo_stats {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"wps_id"
        })
    wps_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWaterPoloStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wps_usr_id'})
    wpsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserWaterPoloStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wps_ucp_id'})
    wpsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"wps_matches_played"
        })
    wps_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wps_games_starts"
        })
    wps_games_starts:number;
        

    @Column("int",{ 
        nullable:false,
        name:"wps_total_minutes_played"
        })
    wps_total_minutes_played:number;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_matches_won"
        })
    wps_matches_won:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_matches_draw"
        })
    wps_matches_draw:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_matches_lose"
        })
    wps_matches_lose:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_scored"
        })
    wps_goals_scored:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_shots_attempted"
        })
    wps_shots_attempted:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_assist"
        })
    wps_goals_assist:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_steals"
        })
    wps_goals_steals:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_turnover"
        })
    wps_goals_turnover:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_blocks"
        })
    wps_goals_blocks:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_disqualifications"
        })
    wps_disqualifications:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_brutalities"
        })
    wps_brutalities:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_saves"
        })
    wps_goals_saves:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_goals_against"
        })
    wps_goals_against:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"wps_goals_against_average"
        })
    wps_goals_against_average:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"wps_shutouts"
        })
    wps_shutouts:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wps_modifiedtime"
        })
    wps_modifiedtime:Date;
        
}
