import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_personalities} from "./gur_sports_personalities";
import {gur_world_events_players_bkup} from "./gur_world_events_players_bkup";
import {gur_world_events_schedules} from "./gur_world_events_schedules";


@Entity("gur_world_events_sports_categories" ,{schema:"sportsmatik_local" } )
export class gur_world_events_sports_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cat_id"
        })
    cat_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"cat_name"
        })
    cat_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cat_createdby"
        })
    cat_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"cat_modifiedby"
        })
    cat_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cat_modifiedtime"
        })
    cat_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.wepCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsPersonalitiess:gur_sports_personalities[];
    

   
    @OneToMany(()=>gur_world_events_players_bkup, (gur_world_events_players_bkup: gur_world_events_players_bkup)=>gur_world_events_players_bkup.wepCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsPlayersBkups:gur_world_events_players_bkup[];
    

   
    @OneToMany(()=>gur_world_events_schedules, (gur_world_events_schedules: gur_world_events_schedules)=>gur_world_events_schedules.evsCat,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsScheduless:gur_world_events_schedules[];
    
}
