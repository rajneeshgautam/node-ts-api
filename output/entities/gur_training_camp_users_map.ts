import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";


@Entity("gur_training_camp_users_map" ,{schema:"sportsmatik_local" } )
@Index("tcu_usr_id",["tcuUsr",])
@Index("tcu_cnt_id",["tcuCnt",])
@Index("tcu_city",["tcu_city",])
@Index("tcu_createdby",["tcuCreatedbyUser",])
@Index("tcu_uty_id",["tcuUty",])
export class gur_training_camp_users_map {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"tcu_id"
        })
    tcu_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurTrainingCampUsersMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcu_uty_id'})
    tcuUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurTrainingCampUsersMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcu_usr_id'})
    tcuUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"tcu_name"
        })
    tcu_name:string;
        

    @Column("bigint",{ 
        nullable:true,
        name:"tcu_city"
        })
    tcu_city:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurTrainingCampUsersMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcu_cnt_id'})
    tcuCnt:gur_countries | null;


    @Column("date",{ 
        nullable:false,
        name:"tcu_year_start"
        })
    tcu_year_start:string;
        

    @Column("date",{ 
        nullable:false,
        name:"tcu_year_end"
        })
    tcu_year_end:string;
        

    @Column("text",{ 
        nullable:true,
        name:"tcu_experience"
        })
    tcu_experience:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"tcu_image"
        })
    tcu_image:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"tcu_status"
        })
    tcu_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"tcu_admin_review"
        })
    tcu_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurTrainingCampUsersMaps2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tcu_createdby_user'})
    tcuCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"tcu_createdby"
        })
    tcu_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"tcu_modifiedby"
        })
    tcu_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tcu_modifiedtime"
        })
    tcu_modifiedtime:Date;
        
}
