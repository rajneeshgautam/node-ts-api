import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_blog_categories_ls} from "./gur_blog_categories_ls";
import {gur_blog_posts} from "./gur_blog_posts";


@Entity("gur_blog_categories" ,{schema:"sportsmatik_local" } )
export class gur_blog_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"bct_id"
        })
    bct_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bct_photo"
        })
    bct_photo:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bct_slug"
        })
    bct_slug:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"bct_status"
        })
    bct_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"bct_is_featured"
        })
    bct_is_featured:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"bct_createdby"
        })
    bct_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"bct_modifiedby"
        })
    bct_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"bct_modifiedtime"
        })
    bct_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_blog_categories_ls, (gur_blog_categories_ls: gur_blog_categories_ls)=>gur_blog_categories_ls.bclBct,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurBlogCategoriesLss:gur_blog_categories_ls[];
    

   
    @ManyToMany(()=>gur_blog_posts, (gur_blog_posts: gur_blog_posts)=>gur_blog_posts.gurBlogCategoriess)
    gurBlogPostss:gur_blog_posts[];
    
}
