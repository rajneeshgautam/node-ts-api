import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_surveys} from "./gur_surveys";


@Entity("gur_survey_questions" ,{schema:"sportsmatik_local" } )
@Index("sqt_sur_id",["sqtSur",])
export class gur_survey_questions {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"sqt_id"
        })
    sqt_id:number;
        

   
    @ManyToOne(()=>gur_surveys, (gur_surveys: gur_surveys)=>gur_surveys.gurSurveyQuestionss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sqt_sur_id'})
    sqtSur:gur_surveys | null;


    @Column("enum",{ 
        nullable:false,
        enum:["subjective","objective"],
        name:"sqt_qtn_type"
        })
    sqt_qtn_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"sqt_qtn"
        })
    sqt_qtn:string;
        

    @Column("text",{ 
        nullable:true,
        name:"sqt_options"
        })
    sqt_options:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"sqt_option_count"
        })
    sqt_option_count:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"sqt_sort_order"
        })
    sqt_sort_order:boolean | null;
        
}
