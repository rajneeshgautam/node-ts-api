import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_equestrian_profile" ,{schema:"sportsmatik_local" } )
@Index("equ_usr_id",["equUsr",])
@Index("equ_para_dressage_disablity_class",["equParaDressageDisablityClass",])
@Index("equ_para_driving_disablity_class",["equParaDrivingDisablityClass",])
@Index("equ_createdby_user",["equCreatedbyUser",])
export class gur_user_para_equestrian_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"equ_id"
        })
    equ_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaEquestrianProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'equ_usr_id'})
    equUsr:gur_users | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"equ_para_dressage"
        })
    equ_para_dressage:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaEquestrianProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'equ_para_dressage_disablity_class'})
    equParaDressageDisablityClass:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"equ_para_driving"
        })
    equ_para_driving:boolean | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaEquestrianProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'equ_para_driving_disablity_class'})
    equParaDrivingDisablityClass:gur_value_list | null;


    @Column("varchar",{ 
        nullable:true,
        name:"equ_horse_name"
        })
    equ_horse_name:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"equ_modifiedtime"
        })
    equ_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaEquestrianProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'equ_createdby_user'})
    equCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"equ_modifiedby"
        })
    equ_modifiedby:string | null;
        
}
