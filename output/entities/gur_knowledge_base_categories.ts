import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_knowledge_base_articles} from "./gur_knowledge_base_articles";
import {gur_knowledge_base_categories_ls} from "./gur_knowledge_base_categories_ls";


@Entity("gur_knowledge_base_categories" ,{schema:"sportsmatik_local" } )
@Index("kbc_parent",["kbcParent",])
export class gur_knowledge_base_categories {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kbc_id"
        })
    kbc_id:number;
        

   
    @ManyToOne(()=>gur_knowledge_base_categories, (gur_knowledge_base_categories: gur_knowledge_base_categories)=>gur_knowledge_base_categories.gurKnowledgeBaseCategoriess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbc_parent'})
    kbcParent:gur_knowledge_base_categories | null;


    @Column("varchar",{ 
        nullable:false,
        name:"kbc_createdby"
        })
    kbc_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kbc_modifiedby"
        })
    kbc_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kbc_modifiedtime"
        })
    kbc_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_knowledge_base_articles, (gur_knowledge_base_articles: gur_knowledge_base_articles)=>gur_knowledge_base_articles.kbaKbc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurKnowledgeBaseArticless:gur_knowledge_base_articles[];
    

   
    @OneToMany(()=>gur_knowledge_base_categories, (gur_knowledge_base_categories: gur_knowledge_base_categories)=>gur_knowledge_base_categories.kbcParent,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurKnowledgeBaseCategoriess:gur_knowledge_base_categories[];
    

   
    @OneToMany(()=>gur_knowledge_base_categories_ls, (gur_knowledge_base_categories_ls: gur_knowledge_base_categories_ls)=>gur_knowledge_base_categories_ls.kblKbc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurKnowledgeBaseCategoriesLss:gur_knowledge_base_categories_ls[];
    
}
