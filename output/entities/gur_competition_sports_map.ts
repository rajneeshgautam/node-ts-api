import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_competitions} from "./gur_competitions";
import {gur_sports} from "./gur_sports";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";


@Entity("gur_competition_sports_map" ,{schema:"sportsmatik_local" } )
@Index("csm_unique",["csmCom","csmSpo",],{unique:true})
@Index("csm_usr_id",["csmCreatedbyUser",])
@Index("csm_spo_id",["csmSpo",])
@Index("csm_createdby_admin",["csmCreatedbyAdmin",])
export class gur_competition_sports_map {

   
    @ManyToOne(()=>gur_competitions, (gur_competitions: gur_competitions)=>gur_competitions.gurCompetitionSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_com_id'})
    csmCom:gur_competitions | null;


   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurCompetitionSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_spo_id'})
    csmSpo:gur_sports | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'2'",
        name:"csm_status"
        })
    csm_status:boolean | null;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurCompetitionSportsMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_createdby_user'})
    csmCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurCompetitionSportsMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'csm_createdby_admin'})
    csmCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"csm_createdby"
        })
    csm_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"csm_modifiedby"
        })
    csm_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"csm_modifiedtime"
        })
    csm_modifiedtime:Date;
        
}
