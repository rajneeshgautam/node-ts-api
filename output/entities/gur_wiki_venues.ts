import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_all_cities} from "./gur_all_cities";
import {gur_inhouse_events} from "./gur_inhouse_events";
import {gur_matik_knowhow_calendar} from "./gur_matik_knowhow_calendar";
import {gur_matik_play_contest} from "./gur_matik_play_contest";
import {gur_sports_event_schedule} from "./gur_sports_event_schedule";
import {gur_wiki_features} from "./gur_wiki_features";
import {gur_wiki_media_gallery} from "./gur_wiki_media_gallery";
import {gur_wiki_venues_ls} from "./gur_wiki_venues_ls";
import {gur_wiki_venues_sports_map} from "./gur_wiki_venues_sports_map";
import {gur_world_events_venues} from "./gur_world_events_venues";


@Entity("gur_wiki_venues" ,{schema:"sportsmatik_local" } )
@Index("wvn_cnt_id",["wvnCnt",])
@Index("wvn_cit_id",["wvnCit",])
export class gur_wiki_venues {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"wvn_id"
        })
    wvn_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"wvn_slug"
        })
    wvn_slug:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_primary_photo"
        })
    wvn_primary_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"wvn_source_detail"
        })
    wvn_source_detail:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wvn_status"
        })
    wvn_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"wvn_teams"
        })
    wvn_teams:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurWikiVenuess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvn_cnt_id'})
    wvnCnt:gur_countries | null;


   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurWikiVenuess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wvn_cit_id'})
    wvnCit:gur_all_cities | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wvn_multisports"
        })
    wvn_multisports:boolean;
        

    @Column("enum",{ 
        nullable:true,
        enum:["indoor","outdoor","both"],
        name:"wvn_ven_type"
        })
    wvn_ven_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_circuit_direction"
        })
    wvn_circuit_direction:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_circuit_type"
        })
    wvn_circuit_type:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_surface_type"
        })
    wvn_surface_type:string | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"wvn_length"
        })
    wvn_length:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_length_str"
        })
    wvn_length_str:string | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"wvn_capacity"
        })
    wvn_capacity:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_capacity_str"
        })
    wvn_capacity_str:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"wvn_is_featured"
        })
    wvn_is_featured:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wvn_createdby"
        })
    wvn_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"wvn_modifiedby"
        })
    wvn_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"wvn_modifiedtime"
        })
    wvn_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_inhouse_events, (gur_inhouse_events: gur_inhouse_events)=>gur_inhouse_events.evtVenue,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInhouseEventss:gur_inhouse_events[];
    

   
    @OneToMany(()=>gur_matik_knowhow_calendar, (gur_matik_knowhow_calendar: gur_matik_knowhow_calendar)=>gur_matik_knowhow_calendar.khcVenue,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowCalendars:gur_matik_knowhow_calendar[];
    

   
    @OneToMany(()=>gur_matik_play_contest, (gur_matik_play_contest: gur_matik_play_contest)=>gur_matik_play_contest.mpcMatchVenue,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayContests:gur_matik_play_contest[];
    

   
    @OneToMany(()=>gur_sports_event_schedule, (gur_sports_event_schedule: gur_sports_event_schedule)=>gur_sports_event_schedule.evsVenue,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurSportsEventSchedules:gur_sports_event_schedule[];
    

   
    @OneToMany(()=>gur_wiki_features, (gur_wiki_features: gur_wiki_features)=>gur_wiki_features.wvfWvn,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiFeaturess:gur_wiki_features[];
    

   
    @OneToMany(()=>gur_wiki_media_gallery, (gur_wiki_media_gallery: gur_wiki_media_gallery)=>gur_wiki_media_gallery.wmgWvn,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiMediaGallerys:gur_wiki_media_gallery[];
    

   
    @OneToMany(()=>gur_wiki_venues_ls, (gur_wiki_venues_ls: gur_wiki_venues_ls)=>gur_wiki_venues_ls.wvlWvn,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiVenuesLss:gur_wiki_venues_ls[];
    

   
    @OneToMany(()=>gur_wiki_venues_sports_map, (gur_wiki_venues_sports_map: gur_wiki_venues_sports_map)=>gur_wiki_venues_sports_map.wsmWvn,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWikiVenuesSportsMaps:gur_wiki_venues_sports_map[];
    

   
    @OneToOne(()=>gur_world_events_venues, (gur_world_events_venues: gur_world_events_venues)=>gur_world_events_venues.wevWvn,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurWorldEventsVenues:gur_world_events_venues | null;

}
