import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_canoe_profile" ,{schema:"sportsmatik_local" } )
@Index("pca_usr_id",["pcaUsr",])
@Index("pca_disablity_class",["pcaDisablityClass",])
@Index("pca_createdby_user",["pcaCreatedbyUser",])
export class gur_user_para_canoe_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pca_id"
        })
    pca_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCanoeProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pca_usr_id'})
    pcaUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaCanoeProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pca_disablity_class'})
    pcaDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pca_modifiedtime"
        })
    pca_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaCanoeProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pca_createdby_user'})
    pcaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pca_modifiedby"
        })
    pca_modifiedby:string | null;
        
}
