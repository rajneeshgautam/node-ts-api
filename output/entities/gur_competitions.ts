import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_value_list} from "./gur_value_list";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_competition_sports_map} from "./gur_competition_sports_map";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_competitions" ,{schema:"sportsmatik_local" } )
@Index("com_name",["com_name",],{unique:true})
@Index("com_createdby_user",["comCreatedbyUser",])
@Index("com_createdby_admin",["comCreatedbyAdmin",])
@Index("com_cricket_format",["comCricketFormat",])
export class gur_competitions {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"com_id"
        })
    com_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"com_name"
        })
    com_name:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurCompetitionss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'com_cricket_format'})
    comCricketFormat:gur_value_list | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'2'",
        name:"com_status"
        })
    com_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"com_is_authentic"
        })
    com_is_authentic:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"com_level"
        })
    com_level:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"com_desc"
        })
    com_desc:string | null;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurCompetitionss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'com_createdby_user'})
    comCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurCompetitionss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'com_createdby_admin'})
    comCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"com_createdby"
        })
    com_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"com_modifiedby"
        })
    com_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"com_modifiedtime"
        })
    com_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_competition_sports_map, (gur_competition_sports_map: gur_competition_sports_map)=>gur_competition_sports_map.csmCom,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurCompetitionSportsMaps:gur_competition_sports_map[];
    

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpCom,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    
}
