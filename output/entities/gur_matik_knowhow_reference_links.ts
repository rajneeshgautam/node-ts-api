import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";
import {gur_wiki_sports} from "./gur_wiki_sports";


@Entity("gur_matik_knowhow_reference_links" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_reference_links_ibfk_1",["krlSpo",])
@Index("gur_matik_knowhow_reference_links_ibfk_2",["krlKnc",])
export class gur_matik_knowhow_reference_links {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"krl_id"
        })
    krl_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"krl_title"
        })
    krl_title:string;
        

    @Column("text",{ 
        nullable:true,
        name:"krl_description"
        })
    krl_description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"krl_links"
        })
    krl_links:string | null;
        

   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurMatikKnowhowReferenceLinkss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'krl_knc_id'})
    krlKnc:gur_matik_knowhow_tab_categories | null;


   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowReferenceLinkss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'krl_spo_id'})
    krlSpo:gur_wiki_sports | null;


    @Column("varchar",{ 
        nullable:true,
        name:"krl_photo"
        })
    krl_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"krl_photo_data"
        })
    krl_photo_data:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"krl_createdby"
        })
    krl_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"krl_modifiedby"
        })
    krl_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"krl_modifiedtime"
        })
    krl_modifiedtime:Date;
        
}
