import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_golf_stats" ,{schema:"sportsmatik_local" } )
@Index("gls_usr_id",["glsUsr",])
@Index("gls_ucp_id",["glsUcp",])
@Index("gls_rank",["glsRank",])
export class gur_user_golf_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"gls_id"
        })
    gls_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserGolfStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gls_usr_id'})
    glsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserGolfStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gls_ucp_id'})
    glsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"gls_holes"
        })
    gls_holes:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"gls_holes_done"
        })
    gls_holes_done:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"gls_total_strokes_taken"
        })
    gls_total_strokes_taken:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"gls_rounds_played"
        })
    gls_rounds_played:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"gls_birdies"
        })
    gls_birdies:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"gls_fairways_hits"
        })
    gls_fairways_hits:number;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"gls_driving_distance"
        })
    gls_driving_distance:number | null;
        

    @Column("double",{ 
        nullable:true,
        default: () => "'0'",
        precision:22,
        name:"gls_total_points"
        })
    gls_total_points:number | null;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserGolfStatss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'gls_rank'})
    glsRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"gls_modifiedtime"
        })
    gls_modifiedtime:Date;
        
}
