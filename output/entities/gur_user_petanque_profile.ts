import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_petanque_profile" ,{schema:"sportsmatik_local" } )
@Index("pen_usr_id",["penUsr",])
@Index("pen_createdby_user",["penCreatedbyUser",])
export class gur_user_petanque_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pen_id"
        })
    pen_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPetanqueProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pen_usr_id'})
    penUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"pen_hand"
        })
    pen_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pen_modifiedtime"
        })
    pen_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPetanqueProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pen_createdby_user'})
    penCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pen_modifiedby"
        })
    pen_modifiedby:string | null;
        
}
