import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_taekwondo_profile" ,{schema:"sportsmatik_local" } )
@Index("tae_usr_id",["taeUsr",])
@Index("tae_weight_event",["taeWeightEvent",])
@Index("tae_createdby_user",["taeCreatedbyUser",])
export class gur_user_taekwondo_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"tae_id"
        })
    tae_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTaekwondoProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tae_usr_id'})
    taeUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserTaekwondoProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tae_weight_event'})
    taeWeightEvent:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tae_modifiedtime"
        })
    tae_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserTaekwondoProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tae_createdby_user'})
    taeCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"tae_modifiedby"
        })
    tae_modifiedby:string | null;
        
}
