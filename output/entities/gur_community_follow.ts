import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_community_question} from "./gur_community_question";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_community_follow" ,{schema:"sportsmatik_local" } )
@Index("cfo_com_id",["cfoCom","cfo_user_id","cfoUty",],{unique:true})
@Index("cfo_uty_id",["cfoUty",])
export class gur_community_follow {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"cfo_id"
        })
    cfo_id:number;
        

   
    @ManyToOne(()=>gur_community_question, (gur_community_question: gur_community_question)=>gur_community_question.gurCommunityFollows,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cfo_com_id'})
    cfoCom:gur_community_question | null;


    @Column("bigint",{ 
        nullable:false,
        name:"cfo_user_id"
        })
    cfo_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCommunityFollows,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cfo_uty_id'})
    cfoUty:gur_user_types | null;

}
