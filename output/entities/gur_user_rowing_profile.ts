import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_rowing_profile" ,{schema:"sportsmatik_local" } )
@Index("row_usr_id",["rowUsr",])
@Index("row_createdby_user",["rowCreatedbyUser",])
export class gur_user_rowing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"row_id"
        })
    row_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRowingProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'row_usr_id'})
    rowUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"row_modifiedtime"
        })
    row_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRowingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'row_createdby_user'})
    rowCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"row_modifiedby"
        })
    row_modifiedby:string | null;
        
}
