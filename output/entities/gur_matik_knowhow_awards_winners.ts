import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_matik_knowhow_awards_yearly} from "./gur_matik_knowhow_awards_yearly";
import {gur_matik_knowhow_awards_titles} from "./gur_matik_knowhow_awards_titles";
import {gur_sports_personalities} from "./gur_sports_personalities";


@Entity("gur_matik_knowhow_awards_winners" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_awards_winners_ibfk_1",["awwAwt",])
@Index("gur_matik_knowhow_awards_winners_ibfk_2",["awwWinner",])
@Index("gur_matik_knowhow_awards_winners_ibfk_3",["awwAwy",])
@Index("gur_matik_knowhow_awards_winners_ibfk_4",["awwSpo",])
export class gur_matik_knowhow_awards_winners {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"aww_id"
        })
    aww_id:number;
        

   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurMatikKnowhowAwardsWinnerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aww_spo_id'})
    awwSpo:gur_wiki_sports | null;


   
    @ManyToOne(()=>gur_matik_knowhow_awards_yearly, (gur_matik_knowhow_awards_yearly: gur_matik_knowhow_awards_yearly)=>gur_matik_knowhow_awards_yearly.gurMatikKnowhowAwardsWinnerss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aww_awy_id'})
    awwAwy:gur_matik_knowhow_awards_yearly | null;


   
    @ManyToOne(()=>gur_matik_knowhow_awards_titles, (gur_matik_knowhow_awards_titles: gur_matik_knowhow_awards_titles)=>gur_matik_knowhow_awards_titles.gurMatikKnowhowAwardsWinnerss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aww_awt_id'})
    awwAwt:gur_matik_knowhow_awards_titles | null;


   
    @ManyToOne(()=>gur_sports_personalities, (gur_sports_personalities: gur_sports_personalities)=>gur_sports_personalities.gurMatikKnowhowAwardsWinnerss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aww_winner'})
    awwWinner:gur_sports_personalities | null;


    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"aww_designation"
        })
    aww_designation:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"aww_description"
        })
    aww_description:string | null;
        
}
