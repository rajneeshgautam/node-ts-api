import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_order_items} from "./gur_order_items";


@Entity("gur_coupons" ,{schema:"sportsmatik_local" } )
@Index("cou_name",["cou_name",],{unique:true})
@Index("cou_usr_type",["couUsrType",])
@Index("cou_usr_id",["cou_usr_id",])
@Index("cou_plc_id",["cou_plc_id",])
export class gur_coupons {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"cou_id"
        })
    cou_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"cou_name"
        })
    cou_name:string;
        

    @Column("int",{ 
        nullable:true,
        name:"cou_max_redemption"
        })
    cou_max_redemption:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cou_discount_percentage"
        })
    cou_discount_percentage:number | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cou_status"
        })
    cou_status:boolean;
        

    @Column("date",{ 
        nullable:true,
        name:"cou_start_date"
        })
    cou_start_date:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"cou_end_date"
        })
    cou_end_date:string | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["flat","plan-wise"],
        name:"cou_type"
        })
    cou_type:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"cou_is_specific"
        })
    cou_is_specific:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"cou_desc"
        })
    cou_desc:string | null;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"cou_plc_id"
        })
    cou_plc_id:number | null;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurCouponss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cou_usr_type'})
    couUsrType:gur_user_types | null;


    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"cou_usr_id"
        })
    cou_usr_id:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"cou_createdby"
        })
    cou_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"cou_modifiedby"
        })
    cou_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"cou_modifiedtime"
        })
    cou_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_order_items, (gur_order_items: gur_order_items)=>gur_order_items.oriCoupon,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurOrderItemss:gur_order_items[];
    
}
