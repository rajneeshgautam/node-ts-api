import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";
import {gur_matik_knowhow_academies} from "./gur_matik_knowhow_academies";


@Entity("gur_academies" ,{schema:"sportsmatik_local" } )
@Index("aca_phone1",["aca_phone1",],{unique:true})
@Index("aca_phone2",["aca_phone2",],{unique:true})
@Index("aca_gst_number",["aca_gst_number",],{unique:true})
@Index("aca_country",["acaCountry",])
@Index("aca_createdby_user",["acaCreatedbyUser",])
@Index("aca_usr_id",["acaUsr",])
@Index("aca_sts_id",["acaSts",])
@Index("aca_dis_id",["acaDis",])
@Index("aca_name",["aca_name",])
export class gur_academies {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"aca_id"
        })
    aca_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aca_slug"
        })
    aca_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"aca_name"
        })
    aca_name:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurAcademiess2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aca_usr_id'})
    acaUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"aca_address"
        })
    aca_address:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aca_address2"
        })
    aca_address2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"aca_pincode"
        })
    aca_pincode:string | null;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurAcademiess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aca_country'})
    acaCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurAcademiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aca_dis_id'})
    acaDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurAcademiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aca_sts_id'})
    acaSts:gur_states | null;


    @Column("text",{ 
        nullable:true,
        name:"aca_address_google_map"
        })
    aca_address_google_map:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aca_email"
        })
    aca_email:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aca_email_verified"
        })
    aca_email_verified:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"aca_email_verify_time"
        })
    aca_email_verify_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"aca_phone1"
        })
    aca_phone1:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aca_phone1_verified"
        })
    aca_phone1_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"aca_phone1_otp"
        })
    aca_phone1_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"aca_phone1_otp_time"
        })
    aca_phone1_otp_time:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:25,
        name:"aca_phone2"
        })
    aca_phone2:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aca_phone2_verified"
        })
    aca_phone2_verified:boolean;
        

    @Column("int",{ 
        nullable:true,
        name:"aca_phone2_otp"
        })
    aca_phone2_otp:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"aca_phone2_otp_time"
        })
    aca_phone2_otp_time:Date | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Active'",
        enum:["Active","Inactive","Deleted","Deactivated"],
        name:"aca_status"
        })
    aca_status:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aca_profile_pic"
        })
    aca_profile_pic:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aca_timeline_pic"
        })
    aca_timeline_pic:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aca_intro_status"
        })
    aca_intro_status:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"aca_profile_summary"
        })
    aca_profile_summary:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"aca_founded_year"
        })
    aca_founded_year:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"aca_founded_month"
        })
    aca_founded_month:number | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"aca_last_login_date"
        })
    aca_last_login_date:Date | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"aca_last_login_details"
        })
    aca_last_login_details:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"aca_intro_modifiedtime"
        })
    aca_intro_modifiedtime:Date | null;
        

    @Column("datetime",{ 
        nullable:true,
        name:"aca_profile_pic_modifiedtime"
        })
    aca_profile_pic_modifiedtime:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"aca_free_sports"
        })
    aca_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"aca_free_sub_users"
        })
    aca_free_sub_users:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"aca_gst_number"
        })
    aca_gst_number:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"aca_viewable"
        })
    aca_viewable:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurAcademiess,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'aca_createdby_user'})
    acaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"aca_createdby"
        })
    aca_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"aca_modifiedby"
        })
    aca_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"aca_modifiedtime"
        })
    aca_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_academies, (gur_matik_knowhow_academies: gur_matik_knowhow_academies)=>gur_matik_knowhow_academies.kacAca,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAcademiess:gur_matik_knowhow_academies[];
    
}
