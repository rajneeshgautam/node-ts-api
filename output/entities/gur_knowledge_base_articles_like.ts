import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_knowledge_base_articles} from "./gur_knowledge_base_articles";
import {gur_user_types} from "./gur_user_types";


@Entity("gur_knowledge_base_articles_like" ,{schema:"sportsmatik_local" } )
@Index("kbl_uty_id",["kblUty",])
@Index("kbl_kba_id",["kblKba",])
export class gur_knowledge_base_articles_like {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"kbl_id"
        })
    kbl_id:number;
        

   
    @ManyToOne(()=>gur_knowledge_base_articles, (gur_knowledge_base_articles: gur_knowledge_base_articles)=>gur_knowledge_base_articles.gurKnowledgeBaseArticlesLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'kbl_kba_id'})
    kblKba:gur_knowledge_base_articles | null;


    @Column("bigint",{ 
        nullable:false,
        name:"kbl_user_id"
        })
    kbl_user_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurKnowledgeBaseArticlesLikes,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'kbl_uty_id'})
    kblUty:gur_user_types | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"kbl_is_liked"
        })
    kbl_is_liked:boolean;
        
}
