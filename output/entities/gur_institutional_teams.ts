import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports} from "./gur_sports";
import {gur_user_types} from "./gur_user_types";
import {gur_users} from "./gur_users";


@Entity("gur_institutional_teams" ,{schema:"sportsmatik_local" } )
@Index("itn_createdby_user",["itnCreatedbyUser",])
@Index("itn_spo_id",["itnSpo",])
@Index("itn_user_type",["itnUserType",])
export class gur_institutional_teams {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"itn_id"
        })
    itn_id:number;
        

   
    @ManyToOne(()=>gur_sports, (gur_sports: gur_sports)=>gur_sports.gurInstitutionalTeamss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'itn_spo_id'})
    itnSpo:gur_sports | null;


    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"itn_name"
        })
    itn_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"itn_status"
        })
    itn_status:boolean;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurInstitutionalTeamss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'itn_user_type'})
    itnUserType:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"itn_user_id"
        })
    itn_user_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"itn_admin_review"
        })
    itn_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurInstitutionalTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'itn_createdby_user'})
    itnCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"itn_createdby"
        })
    itn_createdby:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"itn_modifiedby"
        })
    itn_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"itn_modifiedtime"
        })
    itn_modifiedtime:Date;
        
}
