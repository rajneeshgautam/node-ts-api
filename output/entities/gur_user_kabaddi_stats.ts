import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_user_kabaddi_stats" ,{schema:"sportsmatik_local" } )
@Index("kbs_usr_id",["kbsUsr",])
@Index("kbs_ucp_id",["kbsUcp",])
export class gur_user_kabaddi_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"kbs_id"
        })
    kbs_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserKabaddiStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbs_usr_id'})
    kbsUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserKabaddiStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kbs_ucp_id'})
    kbsUcp:gur_user_competition_played | null;


    @Column("int",{ 
        nullable:false,
        name:"kbs_matches_played"
        })
    kbs_matches_played:number;
        

    @Column("int",{ 
        nullable:false,
        name:"kbs_matches_won"
        })
    kbs_matches_won:number;
        

    @Column("int",{ 
        nullable:false,
        name:"kbs_matches_loss"
        })
    kbs_matches_loss:number;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_successfull_raid"
        })
    kbs_successfull_raid:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_unsuccessfull_raid"
        })
    kbs_unsuccessfull_raid:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_empty_raid"
        })
    kbs_empty_raid:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_total_raid"
        })
    kbs_total_raid:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_total_raid_points"
        })
    kbs_total_raid_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_successfull_tackles"
        })
    kbs_successfull_tackles:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_unsuccessfull_tackles"
        })
    kbs_unsuccessfull_tackles:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_total_tackles"
        })
    kbs_total_tackles:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_total_defence_points"
        })
    kbs_total_defence_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_total_points"
        })
    kbs_total_points:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_green_card"
        })
    kbs_green_card:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_red_card"
        })
    kbs_red_card:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"kbs_yellow_card"
        })
    kbs_yellow_card:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kbs_modifiedtime"
        })
    kbs_modifiedtime:Date;
        
}
