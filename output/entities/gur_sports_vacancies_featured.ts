import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_sports_vacancies} from "./gur_sports_vacancies";


@Entity("gur_sports_vacancies_featured" ,{schema:"sportsmatik_local" } )
@Index("svf_upv_id",["svfUpv",])
export class gur_sports_vacancies_featured {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"svf_id"
        })
    svf_id:string;
        

   
    @ManyToOne(()=>gur_sports_vacancies, (gur_sports_vacancies: gur_sports_vacancies)=>gur_sports_vacancies.gurSportsVacanciesFeatureds,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'svf_upv_id'})
    svfUpv:gur_sports_vacancies | null;


    @Column("datetime",{ 
        nullable:false,
        name:"svf_start"
        })
    svf_start:Date;
        

    @Column("datetime",{ 
        nullable:false,
        name:"svf_end"
        })
    svf_end:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"svf_status"
        })
    svf_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"svf_is_top"
        })
    svf_is_top:number;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"svf_is_priority"
        })
    svf_is_priority:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"svf_createdby"
        })
    svf_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"svf_modifiedby"
        })
    svf_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"svf_modifiedtime"
        })
    svf_modifiedtime:Date;
        
}
