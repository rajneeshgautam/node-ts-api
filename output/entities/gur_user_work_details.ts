import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_all_cities} from "./gur_all_cities";


@Entity("gur_user_work_details" ,{schema:"sportsmatik_local" } )
@Index("uwd_usr_id",["uwdUsr",])
@Index("uwd_zone",["uwdZone",])
@Index("uwd_createdby_user",["uwdCreatedbyUser",])
export class gur_user_work_details {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"uwd_id"
        })
    uwd_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWorkDetailss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwd_usr_id'})
    uwdUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"uwd_company"
        })
    uwd_company:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"uwd_designation"
        })
    uwd_designation:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'99'",
        name:"uwd_country"
        })
    uwd_country:number;
        

   
    @ManyToOne(()=>gur_all_cities, (gur_all_cities: gur_all_cities)=>gur_all_cities.gurUserWorkDetailss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwd_zone'})
    uwdZone:gur_all_cities | null;


    @Column("date",{ 
        nullable:false,
        name:"uwd_from"
        })
    uwd_from:string;
        

    @Column("date",{ 
        nullable:true,
        name:"uwd_till"
        })
    uwd_till:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"uwd_description"
        })
    uwd_description:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"uwd_currently_working"
        })
    uwd_currently_working:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uwd_status"
        })
    uwd_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uwd_admin_review"
        })
    uwd_admin_review:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWorkDetailss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwd_createdby_user'})
    uwdCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"uwd_createdby"
        })
    uwd_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"uwd_modifiedby"
        })
    uwd_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uwd_modifiedtime"
        })
    uwd_modifiedtime:Date;
        
}
