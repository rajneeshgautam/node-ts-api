import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_matik_play_contest_questions} from "./gur_matik_play_contest_questions";
import {gur_matik_play_contest_user_answers} from "./gur_matik_play_contest_user_answers";
import {gur_matik_play_contest_winners} from "./gur_matik_play_contest_winners";


@Entity("gur_matik_play_contest" ,{schema:"sportsmatik_local" } )
@Index("mpc_slug",["mpc_slug",],{unique:true})
@Index("mpc_match_venue",["mpcMatchVenue",])
export class gur_matik_play_contest {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"mpc_id"
        })
    mpc_id:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"mpc_type"
        })
    mpc_type:number;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:100,
        name:"mpc_slug"
        })
    mpc_slug:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpc_name"
        })
    mpc_name:string;
        

    @Column("text",{ 
        nullable:false,
        name:"mpc_desc"
        })
    mpc_desc:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"mpc_short_desc"
        })
    mpc_short_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpc_image"
        })
    mpc_image:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpc_status"
        })
    mpc_status:boolean;
        

    @Column("datetime",{ 
        nullable:false,
        name:"mpc_start"
        })
    mpc_start:Date;
        

    @Column("datetime",{ 
        nullable:false,
        name:"mpc_end"
        })
    mpc_end:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpc_show_time"
        })
    mpc_show_time:boolean;
        

   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurMatikPlayContests,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mpc_match_venue'})
    mpcMatchVenue:gur_wiki_venues | null;


    @Column("datetime",{ 
        nullable:true,
        name:"mpc_match_time"
        })
    mpc_match_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"mpc_need_to_ship"
        })
    mpc_need_to_ship:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mpc_meta_title"
        })
    mpc_meta_title:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"mpc_meta_description"
        })
    mpc_meta_description:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mpc_createdby"
        })
    mpc_createdby:string;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"mpc_modifiedby"
        })
    mpc_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mpc_modified_time"
        })
    mpc_modified_time:Date;
        

   
    @OneToMany(()=>gur_matik_play_contest_questions, (gur_matik_play_contest_questions: gur_matik_play_contest_questions)=>gur_matik_play_contest_questions.mpqMpc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayContestQuestionss:gur_matik_play_contest_questions[];
    

   
    @OneToMany(()=>gur_matik_play_contest_user_answers, (gur_matik_play_contest_user_answers: gur_matik_play_contest_user_answers)=>gur_matik_play_contest_user_answers.mpaMpc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayContestUserAnswerss:gur_matik_play_contest_user_answers[];
    

   
    @OneToMany(()=>gur_matik_play_contest_winners, (gur_matik_play_contest_winners: gur_matik_play_contest_winners)=>gur_matik_play_contest_winners.mpwMpc,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikPlayContestWinnerss:gur_matik_play_contest_winners[];
    
}
