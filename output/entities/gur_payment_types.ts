import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_payment_types" ,{schema:"sportsmatik_local" } )
@Index("spt_name",["pty_name",],{unique:true})
export class gur_payment_types {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"pty_id"
        })
    pty_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"pty_name"
        })
    pty_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"pty_status"
        })
    pty_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"pty_for_production"
        })
    pty_for_production:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pty_createdby"
        })
    pty_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"pty_modifiedby"
        })
    pty_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pty_modifiedtime"
        })
    pty_modifiedtime:Date;
        
}
