import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_translation" ,{schema:"sportsmatik_local" } )
export class gur_translation {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        name:"trl_label"
        })
    trl_label:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'0'",
        name:"trl_global"
        })
    trl_global:boolean | null;
        

    @Column("text",{ 
        nullable:true,
        name:"trl_page"
        })
    trl_page:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"trl_value_en"
        })
    trl_value_en:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"trl_value_hi"
        })
    trl_value_hi:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"trl_value_ur"
        })
    trl_value_ur:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"trl_value_es"
        })
    trl_value_es:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"trl_value_bn"
        })
    trl_value_bn:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"trl_createdby"
        })
    trl_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"trl_modifiedby"
        })
    trl_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"trl_modifiedtime"
        })
    trl_modifiedtime:Date;
        
}
