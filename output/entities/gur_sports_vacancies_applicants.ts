import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_user_types} from "./gur_user_types";
import {gur_sports_vacancies} from "./gur_sports_vacancies";
import {gur_users} from "./gur_users";


@Entity("gur_sports_vacancies_applicants" ,{schema:"sportsmatik_local" } )
@Index("upv_uty_id",["upaUty",])
@Index("gur_user_posted_vacancies_applicants_ibfk_1",["upaUpv",])
@Index("upa_createdby_user",["upaCreatedbyUser",])
export class gur_sports_vacancies_applicants {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"upa_id"
        })
    upa_id:string;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurSportsVacanciesApplicantss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upa_uty_id'})
    upaUty:gur_user_types | null;


    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"upa_user_id"
        })
    upa_user_id:number;
        

   
    @ManyToOne(()=>gur_sports_vacancies, (gur_sports_vacancies: gur_sports_vacancies)=>gur_sports_vacancies.gurSportsVacanciesApplicantss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upa_upv_id'})
    upaUpv:gur_sports_vacancies | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upa_status"
        })
    upa_status:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upa_favorite"
        })
    upa_favorite:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upa_applied"
        })
    upa_applied:boolean;
        

    @Column("datetime",{ 
        nullable:true,
        name:"upa_applied_time"
        })
    upa_applied_time:Date | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"upa_seen"
        })
    upa_seen:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurSportsVacanciesApplicantss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'upa_createdby_user'})
    upaCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"upa_createdby"
        })
    upa_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"upa_modifiedby"
        })
    upa_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"upa_modifiedtime"
        })
    upa_modifiedtime:Date;
        
}
