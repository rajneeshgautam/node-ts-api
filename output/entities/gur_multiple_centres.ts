import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_districts} from "./gur_districts";
import {gur_states} from "./gur_states";


@Entity("gur_multiple_centres" ,{schema:"sportsmatik_local" } )
@Index("mlc_country",["mlcCountry",])
@Index("mlc_dis_id",["mlcDis",])
@Index("mlc_sts_id",["mlcSts",])
export class gur_multiple_centres {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"mlc_id"
        })
    mlc_id:string;
        

    @Column("int",{ 
        nullable:false,
        name:"mlc_uty_id"
        })
    mlc_uty_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"mlc_usr_id"
        })
    mlc_usr_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"mlc_address"
        })
    mlc_address:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMultipleCentress,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mlc_country'})
    mlcCountry:gur_countries | null;


   
    @ManyToOne(()=>gur_districts, (gur_districts: gur_districts)=>gur_districts.gurMultipleCentress,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mlc_dis_id'})
    mlcDis:gur_districts | null;


   
    @ManyToOne(()=>gur_states, (gur_states: gur_states)=>gur_states.gurMultipleCentress,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'mlc_sts_id'})
    mlcSts:gur_states | null;


    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"mlc_pincode"
        })
    mlc_pincode:string;
        

    @Column("text",{ 
        nullable:true,
        name:"mlc_desc"
        })
    mlc_desc:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"mlc_photo"
        })
    mlc_photo:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"mlc_modifiedtime"
        })
    mlc_modifiedtime:Date;
        
}
