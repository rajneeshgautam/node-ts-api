import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_competitions} from "./gur_wiki_competitions";
import {gur_wiki_sports} from "./gur_wiki_sports";
import {gur_matik_knowhow_tab_categories} from "./gur_matik_knowhow_tab_categories";


@Entity("gur_wiki_competitions_sports_map" ,{schema:"sportsmatik_local" } )
@Index("wsm_wcm_id",["wsmWcm",])
@Index("wsm_spo_id",["wsmSpo",])
@Index("gur_wiki_competitions_sports_map_ibfk_3",["wsmKnc",])
export class gur_wiki_competitions_sports_map {

   
    @ManyToOne(()=>gur_wiki_competitions, (gur_wiki_competitions: gur_wiki_competitions)=>gur_wiki_competitions.gurWikiCompetitionsSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsm_wcm_id'})
    wsmWcm:gur_wiki_competitions | null;


   
    @ManyToOne(()=>gur_wiki_sports, (gur_wiki_sports: gur_wiki_sports)=>gur_wiki_sports.gurWikiCompetitionsSportsMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsm_spo_id'})
    wsmSpo:gur_wiki_sports | null;


   
    @ManyToOne(()=>gur_matik_knowhow_tab_categories, (gur_matik_knowhow_tab_categories: gur_matik_knowhow_tab_categories)=>gur_matik_knowhow_tab_categories.gurWikiCompetitionsSportsMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'wsm_knc_id'})
    wsmKnc:gur_matik_knowhow_tab_categories | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"wsm_teams_count"
        })
    wsm_teams_count:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"wsm_event_count"
        })
    wsm_event_count:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"wsm_first_event"
        })
    wsm_first_event:string | null;
        
}
