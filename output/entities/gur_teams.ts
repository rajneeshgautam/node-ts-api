import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_countries} from "./gur_countries";
import {gur_users} from "./gur_users";
import {gur_admin_users} from "./gur_admin_users";
import {gur_user_competition_played} from "./gur_user_competition_played";


@Entity("gur_teams" ,{schema:"sportsmatik_local" } )
@Index("tms_name",["tms_name",],{unique:true})
@Index("tms_createdby_user",["tmsCreatedbyUser",])
@Index("tms_cnt_id",["tmsCnt",])
@Index("tms_createdby_admin",["tmsCreatedbyAdmin",])
export class gur_teams {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"tms_id"
        })
    tms_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"tms_name"
        })
    tms_name:string;
        

   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurTeamss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tms_cnt_id'})
    tmsCnt:gur_countries | null;


    @Column("tinyint",{ 
        nullable:true,
        width:1,
        default: () => "'2'",
        name:"tms_status"
        })
    tms_status:boolean | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"tms_authentic"
        })
    tms_authentic:boolean;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tms_createdby_user'})
    tmsCreatedbyUser:gur_users | null;


   
    @ManyToOne(()=>gur_admin_users, (gur_admin_users: gur_admin_users)=>gur_admin_users.gurTeamss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'tms_createdby_admin'})
    tmsCreatedbyAdmin:gur_admin_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"tms_createdby"
        })
    tms_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"tms_modifiedby"
        })
    tms_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tms_modifiedtime"
        })
    tms_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.ucpComPlayedTeam,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurUserCompetitionPlayeds:gur_user_competition_played[];
    
}
