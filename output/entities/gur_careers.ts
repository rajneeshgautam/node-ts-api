import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_careers" ,{schema:"sportsmatik_local" } )
@Index("car_name",["car_name",])
export class gur_careers {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"car_id"
        })
    car_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"car_name"
        })
    car_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"car_email"
        })
    car_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"car_resume"
        })
    car_resume:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"car_position"
        })
    car_position:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:25,
        name:"car_phone"
        })
    car_phone:string;
        

    @Column("text",{ 
        nullable:true,
        name:"car_message"
        })
    car_message:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"car_ip_address"
        })
    car_ip_address:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"car_sent_on"
        })
    car_sent_on:Date;
        
}
