import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_snowmobiling_profile" ,{schema:"sportsmatik_local" } )
@Index("snm_usr_id",["snmUsr",])
@Index("snm_createdby_user",["snmCreatedbyUser",])
export class gur_user_snowmobiling_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"snm_id"
        })
    snm_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSnowmobilingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'snm_usr_id'})
    snmUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:false,
        length:500,
        name:"snm_disciplines"
        })
    snm_disciplines:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"snm_modifiedtime"
        })
    snm_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserSnowmobilingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'snm_createdby_user'})
    snmCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"snm_modifiedby"
        })
    snm_modifiedby:string | null;
        
}
