import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_formula_one_profile" ,{schema:"sportsmatik_local" } )
@Index("fro_usr_id",["froUsr",])
@Index("fro_createdby_user",["froCreatedbyUser",])
export class gur_user_formula_one_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"fro_id"
        })
    fro_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFormulaOneProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fro_usr_id'})
    froUsr:gur_users | null;


    @Column("year",{ 
        nullable:true,
        name:"fro_debut_date"
        })
    fro_debut_date:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fro_debut_competition"
        })
    fro_debut_competition:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"fro_current_team"
        })
    fro_current_team:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fro_modifiedtime"
        })
    fro_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserFormulaOneProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'fro_createdby_user'})
    froCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"fro_modifiedby"
        })
    fro_modifiedby:string | null;
        
}
