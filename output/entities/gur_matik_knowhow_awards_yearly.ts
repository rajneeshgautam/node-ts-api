import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_awards_main} from "./gur_matik_knowhow_awards_main";
import {gur_matik_knowhow_awards_winners} from "./gur_matik_knowhow_awards_winners";
import {gur_matik_knowhow_awards_yearly_ls} from "./gur_matik_knowhow_awards_yearly_ls";


@Entity("gur_matik_knowhow_awards_yearly" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_awards_yearly_ibfk_1",["awyAwm",])
export class gur_matik_knowhow_awards_yearly {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"awy_id"
        })
    awy_id:number;
        

   
    @ManyToOne(()=>gur_matik_knowhow_awards_main, (gur_matik_knowhow_awards_main: gur_matik_knowhow_awards_main)=>gur_matik_knowhow_awards_main.gurMatikKnowhowAwardsYearlys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'awy_awm_id'})
    awyAwm:gur_matik_knowhow_awards_main | null;


    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"awy_year"
        })
    awy_year:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awy_pic"
        })
    awy_pic:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"awy_pic_data"
        })
    awy_pic_data:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"awy_sel_committee"
        })
    awy_sel_committee:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"awy_createdby"
        })
    awy_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"awy_modifiedby"
        })
    awy_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"awy_modifiedtime"
        })
    awy_modifiedtime:Date;
        

    @Column("text",{ 
        nullable:true,
        name:"awy_awards"
        })
    awy_awards:string | null;
        

   
    @OneToMany(()=>gur_matik_knowhow_awards_winners, (gur_matik_knowhow_awards_winners: gur_matik_knowhow_awards_winners)=>gur_matik_knowhow_awards_winners.awwAwy,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsWinnerss:gur_matik_knowhow_awards_winners[];
    

   
    @OneToMany(()=>gur_matik_knowhow_awards_yearly_ls, (gur_matik_knowhow_awards_yearly_ls: gur_matik_knowhow_awards_yearly_ls)=>gur_matik_knowhow_awards_yearly_ls.wylAwy,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowAwardsYearlyLss:gur_matik_knowhow_awards_yearly_ls[];
    
}
