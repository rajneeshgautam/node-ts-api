import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow} from "./gur_matik_knowhow";
import {gur_wiki_competitions} from "./gur_wiki_competitions";
import {gur_countries} from "./gur_countries";
import {gur_matik_knowhow_history_timeline_ls} from "./gur_matik_knowhow_history_timeline_ls";


@Entity("gur_matik_knowhow_history_timeline" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_history_timeline_ibfk_1",["khtMkh",])
@Index("gur_matik_knowhow_history_timeline_ibfk_2",["khtWcm",])
@Index("gur_matik_knowhow_history_timeline_ibfk_3",["khtCnt",])
export class gur_matik_knowhow_history_timeline {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"kht_id"
        })
    kht_id:string;
        

   
    @ManyToOne(()=>gur_matik_knowhow, (gur_matik_knowhow: gur_matik_knowhow)=>gur_matik_knowhow.gurMatikKnowhowHistoryTimelines,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_mkh_id'})
    khtMkh:gur_matik_knowhow | null;


    @Column("tinyint",{ 
        nullable:true,
        name:"kht_day"
        })
    kht_day:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"kht_month"
        })
    kht_month:number;
        

    @Column("int",{ 
        nullable:false,
        name:"kht_year"
        })
    kht_year:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kht_photo"
        })
    kht_photo:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"kht_photo_data"
        })
    kht_photo_data:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["f","m","o"],
        name:"kht_gender"
        })
    kht_gender:string | null;
        

   
    @ManyToOne(()=>gur_wiki_competitions, (gur_wiki_competitions: gur_wiki_competitions)=>gur_wiki_competitions.gurMatikKnowhowHistoryTimelines,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_wcm_id'})
    khtWcm:gur_wiki_competitions | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurMatikKnowhowHistoryTimelines,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kht_cnt_id'})
    khtCnt:gur_countries | null;


    @Column("tinyint",{ 
        nullable:true,
        name:"kht_type"
        })
    kht_type:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"kht_createdby"
        })
    kht_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"kht_modifiedby"
        })
    kht_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"kht_modifiedtime"
        })
    kht_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_matik_knowhow_history_timeline_ls, (gur_matik_knowhow_history_timeline_ls: gur_matik_knowhow_history_timeline_ls)=>gur_matik_knowhow_history_timeline_ls.htlKht,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurMatikKnowhowHistoryTimelineLss:gur_matik_knowhow_history_timeline_ls[];
    
}
