import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz} from "./gur_quiz";
import {gur_user_types} from "./gur_user_types";
import {gur_quiz_question_bank} from "./gur_quiz_question_bank";
import {gur_quiz_result_detail} from "./gur_quiz_result_detail";


@Entity("gur_quiz_result" ,{schema:"sportsmatik_local" } )
@Index("qrs_uty_id",["qrsUty",])
@Index("qrs_usr_id",["qrs_usr_id",])
@Index("qrs_qiz_id",["qrsQiz",])
@Index("qrs_active_qbk_id",["qrsActiveQbk",])
export class gur_quiz_result {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"qrs_id"
        })
    qrs_id:string;
        

   
    @ManyToOne(()=>gur_quiz, (gur_quiz: gur_quiz)=>gur_quiz.gurQuizResults,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrs_qiz_id'})
    qrsQiz:gur_quiz | null;


    @Column("int",{ 
        nullable:false,
        name:"qrs_usr_id"
        })
    qrs_usr_id:number;
        

   
    @ManyToOne(()=>gur_user_types, (gur_user_types: gur_user_types)=>gur_user_types.gurQuizResults,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrs_uty_id'})
    qrsUty:gur_user_types | null;


   
    @ManyToOne(()=>gur_quiz_question_bank, (gur_quiz_question_bank: gur_quiz_question_bank)=>gur_quiz_question_bank.gurQuizResults,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrs_active_qbk_id'})
    qrsActiveQbk:gur_quiz_question_bank | null;


    @Column("time",{ 
        nullable:true,
        name:"qrs_time_consumed"
        })
    qrs_time_consumed:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"qrs_is_submitted"
        })
    qrs_is_submitted:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        length:20,
        name:"qrs_ip_address"
        })
    qrs_ip_address:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"qrs_user_agent"
        })
    qrs_user_agent:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"qrs_result_json"
        })
    qrs_result_json:number | null;
        

   
    @OneToMany(()=>gur_quiz_result_detail, (gur_quiz_result_detail: gur_quiz_result_detail)=>gur_quiz_result_detail.qrdQrs,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResultDetails:gur_quiz_result_detail[];
    
}
