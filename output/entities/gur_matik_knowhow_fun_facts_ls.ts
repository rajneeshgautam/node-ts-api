import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_matik_knowhow_fun_facts} from "./gur_matik_knowhow_fun_facts";


@Entity("gur_matik_knowhow_fun_facts_ls" ,{schema:"sportsmatik_local" } )
@Index("gur_matik_knowhow_fun_facts_ls_ibfk_1",["kflKhf",])
export class gur_matik_knowhow_fun_facts_ls {

   
    @ManyToOne(()=>gur_matik_knowhow_fun_facts, (gur_matik_knowhow_fun_facts: gur_matik_knowhow_fun_facts)=>gur_matik_knowhow_fun_facts.gurMatikKnowhowFunFactsLss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'kfl_khf_id'})
    kflKhf:gur_matik_knowhow_fun_facts | null;


    @Column("text",{ 
        nullable:false,
        name:"kfl_description"
        })
    kfl_description:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:4,
        default: () => "'en'",
        name:"kfl_lng_code"
        })
    kfl_lng_code:string;
        
}
