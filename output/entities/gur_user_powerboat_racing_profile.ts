import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_powerboat_racing_profile" ,{schema:"sportsmatik_local" } )
@Index("pow_usr_id",["powUsr",])
@Index("pow_createdby_user",["powCreatedbyUser",])
export class gur_user_powerboat_racing_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"pow_id"
        })
    pow_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPowerboatRacingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pow_usr_id'})
    powUsr:gur_users | null;


    @Column("year",{ 
        nullable:true,
        name:"pow_debut_date"
        })
    pow_debut_date:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pow_debut_competition"
        })
    pow_debut_competition:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"pow_current_team"
        })
    pow_current_team:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"pow_modifiedtime"
        })
    pow_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserPowerboatRacingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'pow_createdby_user'})
    powCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"pow_modifiedby"
        })
    pow_modifiedby:string | null;
        
}
