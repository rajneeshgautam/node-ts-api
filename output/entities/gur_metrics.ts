import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_metrics" ,{schema:"sportsmatik_local" } )
@Index("met_name",["met_name",],{unique:true})
export class gur_metrics {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:100,
        name:"met_id"
        })
    met_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"met_name"
        })
    met_name:string;
        

    @Column("tinyint",{ 
        nullable:true,
        default: () => "'1'",
        name:"met_sort_order"
        })
    met_sort_order:number | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["weight","height"],
        name:"met_type"
        })
    met_type:string;
        
}
