import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_infra_portfolio} from "./gur_infra_portfolio";


@Entity("gur_infra_portfolio_media" ,{schema:"sportsmatik_local" } )
@Index("psm_stp_id",["ipmIpo",])
export class gur_infra_portfolio_media {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ipm_id"
        })
    ipm_id:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["photo","video","url"],
        name:"ipm_type"
        })
    ipm_type:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ipm_file"
        })
    ipm_file:string;
        

   
    @ManyToOne(()=>gur_infra_portfolio, (gur_infra_portfolio: gur_infra_portfolio)=>gur_infra_portfolio.gurInfraPortfolioMedias,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ipm_ipo_id'})
    ipmIpo:gur_infra_portfolio | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ipm_admin_review"
        })
    ipm_admin_review:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ipm_status"
        })
    ipm_status:boolean;
        
}
