import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_user_competition_played} from "./gur_user_competition_played";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_rowing_stats" ,{schema:"sportsmatik_local" } )
@Index("ros_usr_id",["rosUsr",])
@Index("ros_ucp_id",["rosUcp",])
@Index("ros_event",["rosEvent",])
@Index("ros_rank",["rosRank",])
export class gur_user_rowing_stats {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"ros_id"
        })
    ros_id:string;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserRowingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ros_usr_id'})
    rosUsr:gur_users | null;


   
    @ManyToOne(()=>gur_user_competition_played, (gur_user_competition_played: gur_user_competition_played)=>gur_user_competition_played.gurUserRowingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ros_ucp_id'})
    rosUcp:gur_user_competition_played | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRowingStatss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ros_event'})
    rosEvent:gur_value_list | null;


    @Column("double",{ 
        nullable:true,
        precision:22,
        name:"ros_distance"
        })
    ros_distance:number | null;
        

    @Column("time",{ 
        nullable:false,
        name:"ros_time"
        })
    ros_time:string;
        

   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserRowingStatss2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ros_rank'})
    rosRank:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ros_modifiedtime"
        })
    ros_modifiedtime:Date;
        
}
