import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_wiki_venues} from "./gur_wiki_venues";
import {gur_inhouse_events_ls} from "./gur_inhouse_events_ls";


@Entity("gur_inhouse_events" ,{schema:"sportsmatik_local" } )
@Index("venue",["evtVenue",])
export class gur_inhouse_events {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"evt_id"
        })
    evt_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_photo"
        })
    evt_photo:string | null;
        

    @Column("date",{ 
        nullable:false,
        name:"evt_date"
        })
    evt_date:string;
        

    @Column("date",{ 
        nullable:true,
        name:"evt_end_date"
        })
    evt_end_date:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"evt_status"
        })
    evt_status:boolean;
        

    @Column("text",{ 
        nullable:false,
        name:"evt_spo_id"
        })
    evt_spo_id:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_organizer"
        })
    evt_organizer:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_contact"
        })
    evt_contact:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_email"
        })
    evt_email:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_address"
        })
    evt_address:string | null;
        

   
    @ManyToOne(()=>gur_wiki_venues, (gur_wiki_venues: gur_wiki_venues)=>gur_wiki_venues.gurInhouseEventss,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'evt_venue'})
    evtVenue:gur_wiki_venues | null;


    @Column("varchar",{ 
        nullable:true,
        name:"evt_link"
        })
    evt_link:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"evt_cnt_id"
        })
    evt_cnt_id:number | null;
        

    @Column("bigint",{ 
        nullable:true,
        unsigned: true,
        name:"evt_city"
        })
    evt_city:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"evt_createdby"
        })
    evt_createdby:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"evt_modifiedby"
        })
    evt_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"evt_modifiedtime"
        })
    evt_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_inhouse_events_ls, (gur_inhouse_events_ls: gur_inhouse_events_ls)=>gur_inhouse_events_ls.evlEvt,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInhouseEventsLss:gur_inhouse_events_ls[];
    
}
