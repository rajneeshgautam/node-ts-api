import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_infra_user_products} from "./gur_infra_user_products";


@Entity("gur_infra_products" ,{schema:"sportsmatik_local" } )
@Index("ipr_name",["ipr_name",],{unique:true})
export class gur_infra_products {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ipr_id"
        })
    ipr_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"ipr_name"
        })
    ipr_name:string;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'0'",
        name:"ipr_sort_order"
        })
    ipr_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"ipr_status"
        })
    ipr_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"ipr_createdby"
        })
    ipr_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"ipr_modifiedby"
        })
    ipr_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ipr_modifiedtime"
        })
    ipr_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_infra_user_products, (gur_infra_user_products: gur_infra_user_products)=>gur_infra_user_products.iupIpr,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurInfraUserProductss:gur_infra_user_products[];
    
}
