import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_admin_users_rights} from "./gur_admin_users_rights";


@Entity("gur_modules" ,{schema:"sportsmatik_local" } )
@Index("amd_code",["amd_code",],{unique:true})
export class gur_modules {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"amd_id"
        })
    amd_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"amd_code"
        })
    amd_code:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"amd_name"
        })
    amd_name:string;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"amd_parent"
        })
    amd_parent:number | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'1'",
        name:"amd_sort_order"
        })
    amd_sort_order:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"amd_pagename"
        })
    amd_pagename:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"amd_icon"
        })
    amd_icon:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'superadmin'",
        name:"amd_createdby"
        })
    amd_createdby:string;
        

    @Column("varchar",{ 
        nullable:true,
        default: () => "'-'",
        name:"amd_modifiedby"
        })
    amd_modifiedby:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"amd_modifiedtime"
        })
    amd_modifiedtime:Date;
        

   
    @OneToMany(()=>gur_admin_users_rights, (gur_admin_users_rights: gur_admin_users_rights)=>gur_admin_users_rights.adrModule,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurAdminUsersRightss:gur_admin_users_rights[];
    
}
