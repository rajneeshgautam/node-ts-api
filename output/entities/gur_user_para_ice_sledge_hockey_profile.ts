import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_para_ice_sledge_hockey_profile" ,{schema:"sportsmatik_local" } )
@Index("ish_usr_id",["ishUsr",])
@Index("ish_disablity_class",["ishDisablityClass",])
@Index("ish_position",["ishPosition",])
@Index("ish_createdby_user",["ishCreatedbyUser",])
@Index("ish_position2",["ishPosition2",])
export class gur_user_para_ice_sledge_hockey_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ish_id"
        })
    ish_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaIceSledgeHockeyProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ish_usr_id'})
    ishUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaIceSledgeHockeyProfiles2,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ish_position1'})
    ishPosition:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaIceSledgeHockeyProfiles3,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ish_position2'})
    ishPosition2:gur_value_list | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserParaIceSledgeHockeyProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ish_disablity_class'})
    ishDisablityClass:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ish_modifiedtime"
        })
    ish_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserParaIceSledgeHockeyProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ish_createdby_user'})
    ishCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ish_modifiedby"
        })
    ish_modifiedby:string | null;
        
}
