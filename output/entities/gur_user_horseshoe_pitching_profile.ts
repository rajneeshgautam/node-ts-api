import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_horseshoe_pitching_profile" ,{schema:"sportsmatik_local" } )
@Index("hsp_usr_id",["hspUsr",])
@Index("hsp_createdby_user",["hspCreatedbyUser",])
export class gur_user_horseshoe_pitching_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"hsp_id"
        })
    hsp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserHorseshoePitchingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hsp_usr_id'})
    hspUsr:gur_users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["left","right"],
        name:"hsp_hand"
        })
    hsp_hand:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"hsp_modifiedtime"
        })
    hsp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserHorseshoePitchingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'hsp_createdby_user'})
    hspCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"hsp_modifiedby"
        })
    hsp_modifiedby:string | null;
        
}
