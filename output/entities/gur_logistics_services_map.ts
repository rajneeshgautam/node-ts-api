import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_logistics_services} from "./gur_logistics_services";
import {gur_countries} from "./gur_countries";


@Entity("gur_logistics_services_map" ,{schema:"sportsmatik_local" } )
@Index("lsm_lgs_id",["lsmLgs",])
@Index("lsm_cnt_id",["lsmCnt",])
export class gur_logistics_services_map {

   
    @ManyToOne(()=>gur_logistics_services, (gur_logistics_services: gur_logistics_services)=>gur_logistics_services.gurLogisticsServicesMaps,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lsm_lgs_id'})
    lsmLgs:gur_logistics_services | null;


   
    @ManyToOne(()=>gur_countries, (gur_countries: gur_countries)=>gur_countries.gurLogisticsServicesMaps,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lsm_cnt_id'})
    lsmCnt:gur_countries | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lsm_all_over_cnt"
        })
    lsm_all_over_cnt:boolean;
        

    @Column("text",{ 
        nullable:true,
        name:"lsm_region"
        })
    lsm_region:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"lsm_all_over"
        })
    lsm_all_over:boolean;
        
}
