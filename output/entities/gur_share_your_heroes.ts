import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("gur_share_your_heroes" ,{schema:"sportsmatik_local" } )
export class gur_share_your_heroes {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"syh_id"
        })
    syh_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"syh_name"
        })
    syh_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"syh_hero_name"
        })
    syh_hero_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"syh_hero_occupation"
        })
    syh_hero_occupation:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"syh_email"
        })
    syh_email:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"syh_phone"
        })
    syh_phone:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"syh_occupation"
        })
    syh_occupation:string;
        

    @Column("text",{ 
        nullable:false,
        name:"syh_message"
        })
    syh_message:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"syh_user_pic"
        })
    syh_user_pic:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"syh_other_pic"
        })
    syh_other_pic:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"syh_status"
        })
    syh_status:boolean;
        

    @Column("varchar",{ 
        nullable:false,
        name:"syh_createdby_ip"
        })
    syh_createdby_ip:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"syh_created_time"
        })
    syh_created_time:Date;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"syh_admin_review"
        })
    syh_admin_review:boolean;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"syh_approved_time"
        })
    syh_approved_time:Date | null;
        

    @Column("varchar",{ 
        nullable:false,
        default: () => "'-'",
        name:"syh_modifiedby"
        })
    syh_modifiedby:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"syh_modified_time"
        })
    syh_modified_time:Date | null;
        
}
