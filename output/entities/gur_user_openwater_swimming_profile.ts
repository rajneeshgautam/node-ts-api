import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_openwater_swimming_profile" ,{schema:"sportsmatik_local" } )
@Index("ows_usr_id",["owsUsr",])
@Index("ows_createdby_user",["owsCreatedbyUser",])
export class gur_user_openwater_swimming_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"ows_id"
        })
    ows_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserOpenwaterSwimmingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ows_usr_id'})
    owsUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ows_modifiedtime"
        })
    ows_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserOpenwaterSwimmingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ows_createdby_user'})
    owsCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ows_modifiedby"
        })
    ows_modifiedby:string | null;
        
}
