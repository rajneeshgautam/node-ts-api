import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_luge_profile" ,{schema:"sportsmatik_local" } )
@Index("lug_usr_id",["lugUsr",])
@Index("lug_createdby_user",["lugCreatedbyUser",])
export class gur_user_luge_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"lug_id"
        })
    lug_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserLugeProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lug_usr_id'})
    lugUsr:gur_users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"lug_modifiedtime"
        })
    lug_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserLugeProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'lug_createdby_user'})
    lugCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"lug_modifiedby"
        })
    lug_modifiedby:string | null;
        
}
