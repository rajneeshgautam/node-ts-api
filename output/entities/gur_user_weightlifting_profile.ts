import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";
import {gur_value_list} from "./gur_value_list";


@Entity("gur_user_weightlifting_profile" ,{schema:"sportsmatik_local" } )
@Index("uwp_usr_id",["uwpUsr",])
@Index("uwp_event",["uwpEvent",])
@Index("uwp_createdby_user",["uwpCreatedbyUser",])
export class gur_user_weightlifting_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"uwp_id"
        })
    uwp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWeightliftingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwp_usr_id'})
    uwpUsr:gur_users | null;


   
    @ManyToOne(()=>gur_value_list, (gur_value_list: gur_value_list)=>gur_value_list.gurUserWeightliftingProfiles,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwp_event'})
    uwpEvent:gur_value_list | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"uwp_modifiedtime"
        })
    uwp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserWeightliftingProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'uwp_createdby_user'})
    uwpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"uwp_modifiedby"
        })
    uwp_modifiedby:string | null;
        
}
