import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_order_items_temp} from "./gur_order_items_temp";


@Entity("gur_plan_orders_temp" ,{schema:"sportsmatik_local" } )
@Index("gur_order_history_ibfk_2",["ord_user_id",])
@Index("gur_order_history_ibfk_4",["ord_done_by",])
@Index("ord_ors_id",["ord_status",])
@Index("ord_usr_type",["ord_uty_id",])
export class gur_plan_orders_temp {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        unsigned: true,
        name:"ord_id"
        })
    ord_id:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ord_status"
        })
    ord_status:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"ord_deleted"
        })
    ord_deleted:boolean;
        

    @Column("int",{ 
        nullable:false,
        name:"ord_uty_id"
        })
    ord_uty_id:number;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"ord_user_id"
        })
    ord_user_id:number;
        

    @Column("char",{ 
        nullable:false,
        length:3,
        name:"ord_currency_code"
        })
    ord_currency_code:string;
        

    @Column("int",{ 
        nullable:true,
        unsigned: true,
        name:"ord_done_by"
        })
    ord_done_by:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"ord_session_id"
        })
    ord_session_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"ord_client_ip"
        })
    ord_client_ip:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ord_createdon"
        })
    ord_createdon:Date;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"ord_modifiedon"
        })
    ord_modifiedon:Date | null;
        

   
    @OneToMany(()=>gur_order_items_temp, (gur_order_items_temp: gur_order_items_temp)=>gur_order_items_temp.oriOrd,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    gurOrderItemsTemps:gur_order_items_temp[];
    
}
