import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_users} from "./gur_users";


@Entity("gur_user_chess_profile" ,{schema:"sportsmatik_local" } )
@Index("ucp_usr_id",["ucpUsr",])
@Index("ucp_fide_title",["ucp_fide_title",])
@Index("ucp_createdby_user",["ucpCreatedbyUser",])
export class gur_user_chess_profile {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"ucp_id"
        })
    ucp_id:number;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserChessProfiles,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_usr_id'})
    ucpUsr:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        name:"ucp_fide_id"
        })
    ucp_fide_id:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ucp_fide_title"
        })
    ucp_fide_title:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ucp_peak_rating"
        })
    ucp_peak_rating:number | null;
        

    @Column("date",{ 
        nullable:true,
        name:"ucp_peak_rating_date"
        })
    ucp_peak_rating_date:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ucp_rating_std"
        })
    ucp_rating_std:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ucp_rating_rapid"
        })
    ucp_rating_rapid:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ucp_rating_blitz"
        })
    ucp_rating_blitz:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"ucp_latest_rank"
        })
    ucp_latest_rank:number | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"ucp_modifiedtime"
        })
    ucp_modifiedtime:Date;
        

   
    @ManyToOne(()=>gur_users, (gur_users: gur_users)=>gur_users.gurUserChessProfiles2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'ucp_createdby_user'})
    ucpCreatedbyUser:gur_users | null;


    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"ucp_modifiedby"
        })
    ucp_modifiedby:string | null;
        
}
