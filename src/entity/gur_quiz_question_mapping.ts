import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz_question_bank} from "./gur_quiz_question_bank";
import { gur_quiz } from "./gur_quiz";


@Entity("gur_quiz_question_mapping" ,{schema:"sportsmatik_local" } )
@Index("qqm_qiz_id",["qqmQiz",])
@Index("qqm_qbk_id",["qqmQbk",])
export class gur_quiz_question_mapping {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"qqm_id"
        })
    qqm_id:number;
        
    @Column("tinyint",{ 
        nullable:true,
        width:2,
        name:"qqm_sort_order"
        })
        qqm_sort_order:number;
   
    @ManyToOne(()=>gur_quiz, (gur_quiz: gur_quiz)=>gur_quiz.gurQuizQuestionMappings,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qqm_qiz_id'})
    qqmQiz:gur_quiz | null;


   
    @ManyToOne(()=>gur_quiz_question_bank, (gur_quiz_question_bank: gur_quiz_question_bank)=>gur_quiz_question_bank.gurQuizQuestionMappings,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qqm_qbk_id'})
    qqmQbk:gur_quiz_question_bank | null;

}
