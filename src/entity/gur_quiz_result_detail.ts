import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz_result} from "./gur_quiz_result";
import {gur_quiz_question_bank} from "./gur_quiz_question_bank";
import {gur_quiz_question_option} from "./gur_quiz_question_option";


@Entity("gur_quiz_result_detail" ,{schema:"sportsmatik_local" } )
@Index("qbo_qbk_id",["qrdQbk",])
@Index("qrd_qrs_id",["qrdQrs",])
@Index("qrd_given_ans",["qrdGivenAns",])
@Index("qrd_correct_ans",["qrdCorrectAns",])
export class gur_quiz_result_detail {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"qrd_id"
        })
    qrd_id:string;
        
    @Column("bigint",{ 
        width:20,
        })
    qrd_qrs_id:number;
   
    @Column("bigint",{ 
        width:20,
        })
    qrd_qbk_id:number;

    @Column("bigint",{ 
        width:20,
        })
    qrd_given_ans:number;

    @Column("bigint",{ 
        width:20,
        })
    qrd_correct_ans:number;

    @Column("time")
    qrd_time_consumed:string;
    

    @ManyToOne(()=>gur_quiz_result, (gur_quiz_result: gur_quiz_result)=>gur_quiz_result.gurQuizResultDetails,{ nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrd_qrs_id'})
    qrdQrs:gur_quiz_result | null;


   
    @ManyToOne(()=>gur_quiz_question_bank, (gur_quiz_question_bank: gur_quiz_question_bank)=>gur_quiz_question_bank.gurQuizResultDetails,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrd_qbk_id'})
    qrdQbk:gur_quiz_question_bank | null;


   
    @ManyToOne(()=>gur_quiz_question_option, (gur_quiz_question_option: gur_quiz_question_option)=>gur_quiz_question_option.gurQuizResultDetails,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrd_given_ans'})
    qrdGivenAns:gur_quiz_question_option | null;


   
    @ManyToOne(()=>gur_quiz_question_option, (gur_quiz_question_option: gur_quiz_question_option)=>gur_quiz_question_option.gurQuizResultDetails2,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qrd_correct_ans'})
    qrdCorrectAns:gur_quiz_question_option | null;

}
