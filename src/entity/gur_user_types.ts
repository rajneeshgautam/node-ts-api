import {gur_quiz_result} from "./gur_quiz_result";
import { Entity, Index, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";

@Entity("gur_user_types" ,{schema:"sportsmatik_local" } )
@Index("uty_name",["uty_name",],{unique:true})
@Index("uty_constant",["uty_constant",],{unique:true})
@Index("uty_slug",["uty_slug",],{unique:true})
@Index("uty_code",["uty_code",])
export class gur_user_types {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"uty_id"
        })
    uty_id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:6,
        name:"uty_code_label"
        })
    uty_code_label:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uty_type"
        })
    uty_type:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"uty_code"
        })
    uty_code:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"uty_name"
        })
    uty_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:50,
        name:"uty_constant"
        })
    uty_constant:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:50,
        name:"uty_slug"
        })
    uty_slug:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"uty_show_label"
        })
    uty_show_label:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"uty_sort_order"
        })
    uty_sort_order:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"uty_free_sports"
        })
    uty_free_sports:number;
        

    @Column("tinyint",{ 
        nullable:false,
        unsigned: true,
        default: () => "'0'",
        name:"uty_free_sub_users"
        })
    uty_free_sub_users:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_applicable_for_sub_users"
        })
    uty_applicable_for_sub_users:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_applicable_for_modules"
        })
    uty_applicable_for_modules:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_show_on_registration"
        })
    uty_show_on_registration:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uty_show_on_report"
        })
    uty_show_on_report:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"uty_show_on_user_search"
        })
    uty_show_on_user_search:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'1'",
        name:"uty_show_on_pricing"
        })
    uty_show_on_pricing:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"uty_show_on_matik_tags"
        })
    uty_show_on_matik_tags:boolean;
        

    @Column("varchar",{ 
        nullable:true,
        length:300,
        name:"uty_short_desc"
        })
    uty_short_desc:string | null;
        

    @OneToMany(()=>gur_quiz_result, (gur_quiz_result: gur_quiz_result)=>gur_quiz_result.qrsUty,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResults:gur_quiz_result[];
    
}
