import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz_question_bank} from "./gur_quiz_question_bank";
import {gur_quiz_result_detail} from "./gur_quiz_result_detail";


@Entity("gur_quiz_question_option" ,{schema:"sportsmatik_local" } )
@Index("qbo_qbk_id",["qboQbk",])
export class gur_quiz_question_option {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"qbo_id"
        })
    qbo_id:string;
        

   
    @ManyToOne(()=>gur_quiz_question_bank, (gur_quiz_question_bank: gur_quiz_question_bank)=>gur_quiz_question_bank.gurQuizQuestionOptions,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'qbo_qbk_id'})
    qboQbk:gur_quiz_question_bank | null;


    @Column("text",{ 
        nullable:false,
        name:"qbo_option"
        })
    qbo_option:string;
        

    @Column("tinyint",{ 
        nullable:true,
        width:1,
        name:"qbo_is_correct"
        })
    qbo_is_correct:boolean | null;
        
    @Column("tinyint",{ 
        nullable:true,
        width:2,
        name:"qbo_sort_order"
        })
        qbo_sort_order:number;
    
   
    @OneToMany(()=>gur_quiz_result_detail, (gur_quiz_result_detail: gur_quiz_result_detail)=>gur_quiz_result_detail.qrdGivenAns,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResultDetails:gur_quiz_result_detail[];
    

   
    @OneToMany(()=>gur_quiz_result_detail, (gur_quiz_result_detail: gur_quiz_result_detail)=>gur_quiz_result_detail.qrdCorrectAns,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResultDetails2:gur_quiz_result_detail[];
    
}
