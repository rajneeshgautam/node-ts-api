import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gur_quiz_category} from "./gur_quiz_category";
import {gur_quiz_question_mapping} from "./gur_quiz_question_mapping";
import {gur_quiz_result} from "./gur_quiz_result";


@Entity("gur_quiz" ,{schema:"sportsmatik_local" } )
@Index("qiz_qct_id",["qizQct",])
export class gur_quiz {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"qiz_id"
        })
    qiz_id:number;
        

   
    @ManyToOne(()=>gur_quiz_category, (gur_quiz_category: gur_quiz_category)=>gur_quiz_category.gurQuizs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'qiz_qct_id'})
    qizQct:gur_quiz_category | null;


    @Column("varchar",{ 
        nullable:false,
        name:"qiz_name"
        })
    qiz_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"qiz_slug"
        })
    qiz_slug:string;
        

    @Column("text",{ 
        nullable:false,
        name:"qiz_description"
        })
    qiz_description:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:200,
        name:"qiz_image"
        })
    qiz_image:string | null;
        

    @Column("int",{ 
        nullable:false,
        name:"qiz_total_questions"
        })
    qiz_total_questions:number;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"qiz_quiz_level"
        })
    qiz_quiz_level:number;
        

    @Column("time",{ 
        nullable:false,
        name:"qiz_duration"
        })
    qiz_duration:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"qiz_show_palette"
        })
    qiz_show_palette:boolean;
        

    @Column("time",{ 
        nullable:true,
        name:"qiz_each_question_time_limit"
        })
    qiz_each_question_time_limit:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"qiz_auto_move"
        })
    qiz_auto_move:boolean;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"qiz_instant_answer"
        })
    qiz_instant_answer:boolean;
        

    @Column("int",{ 
        nullable:false,
        name:"qiz_passing_score"
        })
    qiz_passing_score:number;
        

    @Column("text",{ 
        nullable:true,
        name:"qiz_instruction"
        })
    qiz_instruction:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"qiz_is_active"
        })
    qiz_is_active:boolean;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"qiz_sort_order"
        })
    qiz_sort_order:number | null;
        

    @Column("datetime",{ 
        nullable:false,
        name:"qiz_created_at"
        })
    qiz_created_at:Date;
        

    @Column("datetime",{ 
        nullable:true,
        name:"qiz_updated_at"
        })
    qiz_updated_at:Date | null;
        

    @Column("int",{ 
        nullable:false,
        name:"qiz_created_by"
        })
    qiz_created_by:number;
        

    @Column("int",{ 
        nullable:true,
        name:"qiz_updated_by"
        })
    qiz_updated_by:number | null;
        

   
    @OneToMany(()=>gur_quiz_question_mapping, (gur_quiz_question_mapping: gur_quiz_question_mapping)=>gur_quiz_question_mapping.qqmQiz,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizQuestionMappings:gur_quiz_question_mapping[];
    

   
    @OneToMany(()=>gur_quiz_result, (gur_quiz_result: gur_quiz_result)=>gur_quiz_result.qrsQiz,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    gurQuizResults:gur_quiz_result[];
    
}
