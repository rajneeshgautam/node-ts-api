import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes/index";
import config from "./env/configs/main";
//console.log(config.HTTP_PORT);

//Connects to the Database -> then starts the express
createConnection()
  .then(async connection => {
    // Create a new express application instance
    const app = express();
    const API_URL = 'http://103.53.41.248:5000'

    //options for cors midddleware
    const options:cors.CorsOptions = {
      allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
      credentials: true,
      methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
      origin: API_URL,
      preflightContinue: false
    };

    // Call midlewares
    app.use(cors());
    app.use(helmet());
    app.use(bodyParser.json());

    //Set all routes from routes folder
    app.use("/", routes);

    app.listen(config.HTTP_PORT, () => {
      console.log(`Server started on port number: ${config.HTTP_PORT}.`);
    });
  })
  .catch(error => console.log(error));