import { Entity, PrimaryGeneratedColumn, Column, Timestamp, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Length, IsNotEmpty, IsInt, Max } from 'class-validator';
import { GurQuiz } from './GurQuiz';

@Entity()

export class GurQuizCategory {

    @PrimaryGeneratedColumn()
    qct_id: number;

    @Column('varchar', {length: 100})
    @Length(2,100)
    qct_slug: string;

    @Column('varchar', {
        length: 100
    })
    @IsNotEmpty()
    @Length(2,100)
    qct_name: string;

    @Column()
    @Length(2, 255)
    qct_image: string;

    @Column('varchar', {length: 1000})
    @Length(2, 1000)
    qct_description: string;
    
    @Column('tinyint', {width: 2})
    @IsInt()
    qct_sort_order: number;

    @Column('tinyint', {default: 1, width: 1})
    @Max(3)
    @IsInt()
    qct_is_active: number;

    @Column('integer')
    qct_created_by: number;

    @Column('integer', {nullable: true})
    qct_updated_by: number;

    @Column('datetime', {default: Timestamp})
    qct_created_at: Timestamp;

    @Column('datetime', {nullable: true})
    qct_updated_at: Timestamp;
    
    @OneToMany(type => GurQuiz, q => q.quizCategory)
    @JoinColumn({ name: "qiz_qct_id" })
    quizzes: GurQuiz[];

    //@OneToMany(() => GurQuiz, q => q.quiz, { primary: true })
    //quizCatConnection: Promise<GurQuiz[]>;
}