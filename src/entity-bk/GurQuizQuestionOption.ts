import { Entity, PrimaryGeneratedColumn, Column, Timestamp, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Length, IsNotEmpty, IsInt, Max } from 'class-validator';
import { GurQuiz } from './GurQuiz';

@Entity()

export class GurQuizQuestionOption {

    @PrimaryGeneratedColumn('increment', {type: 'bigint'})
    qbo_id: number;

    @Column({type: 'bigint', length: 20})
    qbo_qbk_id: number;

    @Column('varchar', { length: 1000 })
    @Length(2,1000)
    qbo_option: string;

    @Column('tinyint', {width: 1})
    @IsInt()
    qbo_is_correct: number;

    //@OneToMany(() => GurQuiz, q => q.quiz, { primary: true })
    //quizCatConnection: Promise<GurQuiz[]>;
}