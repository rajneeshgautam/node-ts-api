import { Entity, Unique, PrimaryGeneratedColumn, Column, Timestamp, OneToMany, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Length, IsNotEmpty, MaxLength, Max, IsInt, IsIn } from 'class-validator';
import { GurQuizCategory } from './GurQuizCategory';

@Entity()

export class GurQuiz{
    
    @PrimaryGeneratedColumn()
    qiz_id: number;

    @Column('integer')
    @IsInt()
    qiz_qct_id: number;

    @Column('varchar', {length: 255})
    @MaxLength(100)
    @Length(2, 100)
    qiz_name: string;

    @Column('varchar', {length: 100})
    @Length(2, 100)
    qiz_slug: string;

    @Column('text')
    @Length(2, 100)
    qiz_description: string;

    @Column('varchar', {length: 100})
    @Length(2, 100)
    qiz_image: string;

    @Column('integer')
    @IsInt()
    qiz_total_questions: number;

    @Column('tinyint', {width: 2})
    @IsInt()
    qiz_quiz_level: number;

    @Column('time')
    qiz_duration: string;

    @Column('tinyint', {width: 1})
    @IsInt()
    qiz_show_palette: number;
    
    @Column('time', {nullable: true})
    qiz_each_question_time_limit: string;

    @Column('tinyint', {width: 1})
    @IsInt()
    qiz_auto_move: number;

    @Column('tinyint', {width: 1})
    @IsInt()
    qiz_instant_answer: number;

    @Column('integer')
    @IsInt()
    qiz_passing_score: number;

    @Column('text')
    @Length(2, 1000)
    qiz_instruction: string;

    @Column('tinyint', {default: 1, width: 1, select: false})
    @IsInt()
    qiz_is_active: number;

    @Column('tinyint', {nullable: true,  width: 2})
    @IsInt()
    qiz_sort_order: string;

    @Column('tinyint', {default: 0, nullable: true, width: 1, select: false})
    @IsInt()
    qiz_is_deleted: number;

    @Column('integer', {select: false})
    qiz_created_by: number;

    @Column('integer', {nullable: true, select: false})
    qiz_updated_by: number;

    @Column('datetime', {default: Timestamp, select: false})
    qiz_created_at: Timestamp;

    @Column('datetime', {nullable: true, select: false})
    qiz_updated_at: Timestamp;

    @ManyToOne(type => GurQuizCategory, qc => qc.quizzes)
    @JoinColumn({ name: "qiz_qct_id" })
    public quizCategory: GurQuizCategory;

    // @OneToOne(() => GurQuizCategory, quizCat => quizCat.quizCatConnection, { primary: true })
    // @JoinColumn({ name: "qiz_qct_id" })
    // quiz: Promise<GurQuizCategory>;

    /*@ManyToOne(() => GurQuizCategory, quizCat => quizCat.quizCatConnection, { primary: true })
    @JoinColumn({ name: "bookId" })
    book: Promise<GurQuizCategory>;*/

}
