import { Entity, PrimaryGeneratedColumn, Column, Timestamp, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Length, IsNotEmpty, IsInt, Max, IsEnum } from 'class-validator';
import { GurQuiz } from './GurQuiz';

enum QuestionType {
    'boolean', 'mcq', 'msq'
}

@Entity()

export class GurQuizQuestionBank {

    @PrimaryGeneratedColumn('increment', {type: 'bigint'})
    qbk_id: number;

    @Column('tinyint', {width: 2})
    @IsInt()
    qbk_difficulty_level: number;

    @Column('enum', {enum: QuestionType})
    qbk_type: string;

    @Column('varchar', { length: 1000 })
    @Length(2,1000)
    qbk_question: string;

    @Column('varchar', {length: 1000})
    @Length(2, 1000)
    qbk_question_hint: string;

    @Column('tinyint', {default: 1, width: 1})
    @IsInt()
    qbk_is_active: number;

    @Column('integer')
    @IsInt()
    qbk_created_by: number;

    @Column('integer', {nullable: true})
    @IsInt()
    qbk_updated_by: number;

    @Column('datetime', {default: Timestamp})
    qbk_created_at: Timestamp;

    @Column('datetime', {nullable: true})
    qbk_updated_at: Timestamp;
    
    // @OneToMany(type => GurQuiz, q => q.quizCategory)
    // @JoinColumn({ name: "qiz_	qbk_id" })
    // quizzes: GurQuiz[];

    //@OneToMany(() => GurQuiz, q => q.quiz, { primary: true })
    //quizCatConnection: Promise<GurQuiz[]>;
}