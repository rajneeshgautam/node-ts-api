import { Entity, PrimaryGeneratedColumn, Column, Timestamp, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Length, IsNotEmpty, IsInt, Max, IsIn } from 'class-validator';
import { GurQuiz } from './GurQuiz';
import { GurQuizQuestionBank } from './GurQuizQuestionBank';

@Entity()

export class GurQuizQuestionMapping {

    // @Column({type: 'integer', length: 11})
    // @IsInt()
    // qqm_qiz_id: number;

    // @Column({type: 'bigint', length: 20})
    // @IsInt()
    // qqm_qbk_id: number;

    @ManyToOne((type) => GurQuiz, {
        primary: true,
    })
    @JoinColumn({ name: 'qqm_qiz_id' })
    public account: GurQuiz

    @ManyToOne((type) => GurQuizQuestionBank, (question) => question.quizzes, {
        primary: true,
    })
    @JoinColumn({ name: 'qqm_qbk_id' })
    public question: GurQuizQuestionBank

    //@OneToMany(() => GurQuiz, q => q.quiz, { primary: true })
    //quizCatConnection: Promise<GurQuiz[]>;
}