import { Router } from "express";
import QuestionController from "../controllers/quiz/QuestionController";


  const router = Router();

  //Get all quiz categories
  router.get("/", QuestionController.getList);
  
  router.get("/:id([0-9]+)", QuestionController.getOne);

  export default router;
