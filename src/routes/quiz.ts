import { Router } from "express";
import QuizController from "../controllers/quiz/QuizController";



  const router = Router();

  //Get all quiz categories
  router.get("/", QuizController.getList);
  // Get one user
  router.get( "/:id([0-9]+)", QuizController.getOne);

  export default router;
