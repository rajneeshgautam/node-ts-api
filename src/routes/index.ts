import { Router, Request, Response } from "express";
import auth from "./auth";
import user from "./user";
import quiz from "./quiz";
import question from "./question";
import quizCategory from "./quiz-category";

const routes = Router();

//routes.use("/auth", auth);
//routes.use("/user", user);
routes.use("/quiz", quiz);
routes.use("/question", question);
routes.use("/quiz-category", quizCategory);

export default routes;