import { Router } from "express";
import QuizCategoryController from "../controllers/quiz/QuizCategoryController";


const router = Router();

//Get all quiz categories
router.get("/", QuizCategoryController.listAll);

export default router;
