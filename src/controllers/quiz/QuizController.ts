import { Request, Response } from "express";
import { getRepository, LessThan, MoreThanOrEqual, MoreThan } from "typeorm";


import { QuizConfig } from "../../response/quiz/QuizConfig";
import { Option } from "../../response/quiz/Option";
import { Question } from "../../response/quiz/Question";
import { gur_quiz } from "../../entity/gur_quiz";
//import { Config } from "../response/quiz/Config";


class QuizController{

    static getList = async (req: Request, res: Response) => {
        //Get quizs from database
        const quizRepository = getRepository(gur_quiz);
        
        const quiz = await quizRepository.createQueryBuilder()
                        //.select(['q.qiz_name', 'category.qct_id', 'category.qct_name'])
                        .select(['q.qiz_name', 'q.qiz_slug', 'q.qiz_description', 'q.qiz_total_questions', 'q.qiz_quiz_level', 'q.qiz_duration', 'q.qiz_show_palette', 'q.qiz_each_question_time_limit', 'q.qiz_auto_move', 'q.qiz_instant_answer', 'q.qiz_passing_score', 'q.qiz_instruction', 'category.qct_id', 'category.qct_name'])
                        .from(gur_quiz, "q")
                        .leftJoin('q.qizQct', 'category')
                        .leftJoinAndSelect('q.gurQuizQuestionMappings', 'qqm')
                        .leftJoinAndSelect('qqm.qqmQbk', 'qbk')
                        .leftJoinAndSelect('qbk.gurQuizQuestionOptions', 'qbo')
                        .getOne();
                    
        //console.log(quiz.gurQuizQuestionMappings);

        let conf = new QuizConfig();
        if(quiz){
            conf.name = quiz.qiz_name;
            conf.slug = quiz.qiz_slug;
            conf.desc = quiz.qiz_description;
            conf.total_que = quiz.qiz_total_questions;
            conf.dur = quiz.qiz_duration;
            conf.show_palette = Boolean(quiz.qiz_show_palette);
            conf.eqtl = quiz.qiz_each_question_time_limit;
            conf.auto_move = Boolean(quiz.qiz_auto_move);
            conf.instant_answer = Boolean(quiz.qiz_instant_answer);
            conf.passing_score = quiz.qiz_passing_score;
            conf.instruction = quiz.qiz_instruction;
            
            quiz.gurQuizQuestionMappings.forEach(question => {
                let que = new Question();
                que.id = Number(question.qqmQbk.qbk_id);
                que.que = question.qqmQbk.qbk_question;
                que.hint = question.qqmQbk.qbk_question_hint;
                
                question.qqmQbk.gurQuizQuestionOptions.forEach(option => {
                    let opt = new Option();
                    opt.id = Number(option.qbo_id);
                    opt.opt = option.qbo_option;
                    opt.ic = Boolean(option.qbo_is_correct);
                    que.options.push(opt);
                });
                conf.questions.push(que);
            });
        }

        //Send the quizs object
        //res.status(200).send(conf);
        res.status(200).send({
            status_code: 200, 
            status: 'success', 
            message: 'Data successfully fetched.', 
            data: conf
        });
    };

    static getOne = async (req: Request, res: Response) => {
        //Get the ID from the url
        const qid: string = req.params.id;
        
        //Get quizs from database
        const quizRepository = getRepository(gur_quiz);
        
        const quiz = await quizRepository.createQueryBuilder()
                        //.select(['q.qiz_name', 'category.qct_id', 'category.qct_name'])
                        .select(['q.qiz_name', 'q.qiz_slug', 'q.qiz_description', 'q.qiz_total_questions', 'q.qiz_quiz_level', 'q.qiz_duration', 'q.qiz_show_palette', 'q.qiz_each_question_time_limit', 'q.qiz_auto_move', 'q.qiz_instant_answer', 'q.qiz_passing_score', 'q.qiz_instruction', 'category.qct_id', 'category.qct_name'])
                        .from(gur_quiz, "q")
                        .where('q.qiz_id = :qiz_id', {qiz_id: qid})
                        .leftJoin('q.qizQct', 'category')
                        .leftJoinAndSelect('q.gurQuizQuestionMappings', 'qqm')
                        .leftJoinAndSelect('qqm.qqmQbk', 'qbk')
                        .leftJoinAndSelect('qbk.gurQuizQuestionOptions', 'qbo')
                        .getOne();
                    
        //console.log(quiz.gurQuizQuestionMappings);

        let conf = new QuizConfig();
        if(quiz){
            conf.name = quiz.qiz_name;
            conf.slug = quiz.qiz_slug;
            conf.desc = quiz.qiz_description;
            conf.total_que = quiz.qiz_total_questions;
            conf.dur = quiz.qiz_duration;
            conf.show_palette = Boolean(quiz.qiz_show_palette);
            conf.eqtl = quiz.qiz_each_question_time_limit;
            conf.auto_move = Boolean(quiz.qiz_auto_move);
            conf.instant_answer = Boolean(quiz.qiz_instant_answer);
            conf.passing_score = quiz.qiz_passing_score;
            conf.instruction = quiz.qiz_instruction;
            
            quiz.gurQuizQuestionMappings.forEach(question => {
                let que = new Question();
                que.id = Number(question.qqmQbk.qbk_id);
                que.que = question.qqmQbk.qbk_question;
                que.hint = question.qqmQbk.qbk_question_hint;
                
                question.qqmQbk.gurQuizQuestionOptions.forEach(option => {
                    let opt = new Option();
                    opt.id = Number(option.qbo_id);
                    opt.opt = option.qbo_option;
                    opt.ic = Boolean(option.qbo_is_correct);
                    que.options.push(opt);
                });
                conf.questions.push(que);
            });
        }

        //Send the quizs object
        //res.status(200).send(conf);
        res.status(200).send({
            status_code: 200, 
            status: 'success', 
            message: 'Data successfully fetched.', 
            data: conf
        });
    };
};

export default QuizController;