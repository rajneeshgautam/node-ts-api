import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { gur_quiz_category } from "../../entity/gur_quiz_category";

class QuizCategoryController{

    static listAll = async (req: Request, res: Response) => {
    //Get quizs from database
    const quizCatRepository = getRepository(gur_quiz_category);
    /*const quizCategories = await quizCatRepository.find({ 
        order: {'qct_sort_order': "DESC"},
        select: ["qct_id", "qct_slug", "qct_name", "qct_image", "qct_description"],
        relations: [
            "quizzes"
        ] 
    });*/

    /*const quizCount = await quizCatRepository.createQueryBuilder()
                                .subQuery()
                                .from(GurQuiz, "q")
                                .select("COUNT(q.qiz_id)", "count")
                                .from(GurQuiz, "quiz")
                                .where("gur_quiz.qiz_qct_id = qc.qct_id").getQuery();

    const quizCategories = await quizCatRepository.createQueryBuilder()
                                .from(GurQuizCategory, "qc")
                                .select([
                                    'qc.qct_id', 'qc.qct_name', 'qc.qct_description'
                                ])
                                .addSelect(quizCount => {
                                    return quizCount
                                            .select("COUNT(quiz.qiz_id)")
                                            .from(GurQuiz, "quiz")
                                            .where("gur_quiz.qiz_qct_id = qc.qct_id");
                                }, 'total')
                                //.leftJoinAndSelect(GurQuiz, "gur_quiz", "gur_quiz.qiz_qct_id = qc.qct_id")
                                .leftJoinAndSelect("qc.quizzes", "gur_quiz", "gur_quiz.qiz_is_active = :qiz_is_active AND gur_quiz.qiz_is_deleted = :qiz_is_deleted", { 
                                    qiz_is_active: 1, 
                                    qiz_is_deleted: 0
                                })
                                .andWhere("qc.qct_is_active = :is_active", { is_active: 1 })
                                .andWhere("qc.qct_is_deleted = :is_deleted", { is_deleted: 0 })
                                .skip(0)
                                .take(3)
                                .getManyAndCount();*/

    /*const quizCategories = await quizCatRepository.createQueryBuilder("gur_quiz_category")
                                .leftJoinAndSelect(GurQuiz, "gur_quiz", "gur_quiz.qiz_qct_id = gur_quiz_category.qct_id")
                                .where("gur_quiz_category.qct_is_active = :is_active", { is_active: 1 })
                                .andWhere("gur_quiz_category.qct_is_deleted = :is_deleted", { is_deleted: 0 })
                                .getMany();*/

    //console.log(quizCategories);
    //Send the quizCategories object

        const quizCategories = await quizCatRepository.find({ 
            //select: ["qct_id", "qct_slug", "qct_name", "qct_image", "qct_description", "qct_sort_order"],
            relations: [
                "gurQuizs"
            ],
        });

        res.send(quizCategories);

    };


};

export default QuizCategoryController;