import { Request, Response } from "express";
import { getRepository, LessThan, MoreThanOrEqual, MoreThan } from "typeorm";


import { QuizConfig } from "../../response/quiz/QuizConfig";
import { Option } from "../../response/quiz/Option";
import { Question } from "../../response/quiz/Question";
import { gur_quiz } from "../../entity/gur_quiz";
import { gur_quiz_result } from "../../entity/gur_quiz_result";
import { gur_quiz_result_detail } from "../../entity/gur_quiz_result_detail";
//import { Config } from "../response/quiz/Config";


class QuestionController{
    static getList = async (req: Request, res: Response) => {
        
        
        //Get the ID from the url
        const qid: string = req.params.id;

        //Get quizs from database
        const quizRepository = getRepository(gur_quiz);
        
        const quiz = await quizRepository.createQueryBuilder()
                        .select(['q.qiz_name', 'q.qiz_slug', 'q.qiz_description', 'q.qiz_total_questions', 'q.qiz_quiz_level', 'q.qiz_duration', 'q.qiz_show_palette', 'q.qiz_each_question_time_limit', 'q.qiz_auto_move', 'q.qiz_instant_answer', 'q.qiz_passing_score', 'q.qiz_instruction', 'category.qct_id', 'category.qct_name'])
                        .from(gur_quiz, "q")
                        .where('q.qiz_id = :qiz_id', {qiz_id: qid})
                        .leftJoin('q.qizQct', 'category')
                        .leftJoinAndSelect('q.gurQuizQuestionMappings', 'qqm')
                        .leftJoinAndSelect('qqm.qqmQbk', 'qbk')
                        .leftJoinAndSelect('qbk.gurQuizQuestionOptions', 'qbo')
                        .getOne();
                    
        let conf = new QuizConfig();
        if(quiz){
            conf.name = quiz.qiz_name;
            conf.slug = quiz.qiz_slug;
            conf.desc = quiz.qiz_description;
            conf.total_que = quiz.qiz_total_questions;
            conf.dur = quiz.qiz_duration;
            conf.show_palette = Boolean(quiz.qiz_show_palette);
            conf.eqtl = quiz.qiz_each_question_time_limit;
            conf.auto_move = Boolean(quiz.qiz_auto_move);
            conf.instant_answer = Boolean(quiz.qiz_instant_answer);
            conf.passing_score = quiz.qiz_passing_score;
            conf.instruction = quiz.qiz_instruction;
            
            quiz.gurQuizQuestionMappings.forEach(question => {
                let que = new Question();
                que.id = Number(question.qqmQbk.qbk_id);
                que.que = question.qqmQbk.qbk_question;
                que.hint = question.qqmQbk.qbk_question_hint;
                
                question.qqmQbk.gurQuizQuestionOptions.forEach(option => {
                    let opt = new Option();
                    opt.id = Number(option.qbo_id);
                    opt.opt = option.qbo_option;
                    opt.ic = Boolean(option.qbo_is_correct);
                    que.options.push(opt);
                    //console.log(option);
                });
                conf.questions.push(que);
            });
        }

        //Send the quizs object
        res.status(200).send({
            status_code: 200, 
            status: 'success', 
            message: 'Data successfully fetched.', 
            data: conf
        });
    };

    static getOne = async (req: Request, res: Response) => {
        //Get the jwt token from the head
        const token = <string>req.headers["user_detail"];
        //var user_detail = Buffer.from(token, 'base64').toString().split("|");
        var user_detail = token.split("|");
        
        //Get the ID from the url
        const qid: string = req.params.id;

        //Get quizs from database
        const quizRepository = getRepository(gur_quiz);
        
        const quiz = await quizRepository.createQueryBuilder()
                        .select(['q.qiz_id', 'q.qiz_name', 'q.qiz_slug', 'q.qiz_description', 'q.qiz_total_questions', 'q.qiz_quiz_level', 'q.qiz_duration', 'q.qiz_show_palette', 'q.qiz_each_question_time_limit', 'q.qiz_auto_move', 'q.qiz_instant_answer', 'q.qiz_passing_score', 'q.qiz_instruction', 'category.qct_id', 'category.qct_name', 'qbk.qbk_id', 'qbk.qbk_difficulty_level', 'qbk.qbk_type', 'qbk.qbk_question', 'qbk.qbk_question_hint', 'qrd.qrd_qbk_id', 'qr.qrs_active_qbk_id', 'qr.qrs_usr_id', 'qr.qrs_time_consumed'])
                        .from(gur_quiz, "q")
                        .where('q.qiz_id = :qiz_id', {qiz_id: qid})
                        .andWhere('qr.qrs_usr_id = :qrs_usr_id', { qrs_usr_id: user_detail[0] })
                        .andWhere('qr.qrs_uty_id = :qrs_uty_id', { qrs_uty_id: user_detail[1] })
                        .andWhere(qb => {
                            const excludeQuestion = qb.subQuery()
                                .select("qrd.qrd_qbk_id")
                                .from(gur_quiz_result_detail, "qrd")
                                .innerJoin('qrd.qrdQrs', 'qrc')
                                .where("qrd.qrd_qbk_id = qbk.qbk_id")
                                .andWhere("qrc.qrs_qiz_id = :qiz_id", {qiz_id: qid})
                                .andWhere('qrc.qrs_usr_id = :qrs_usr_id', { qrs_usr_id: user_detail[0] })
                                .andWhere('qrc.qrs_uty_id = :qrs_uty_id', {qrs_uty_id: user_detail[1]})
                                .getQuery();
                            //console.log(excludeQuestion);
                            const activeQuestion = qb.subQuery()
                                .select("qr.qrs_active_qbk_id")
                                .from(gur_quiz_result, "qc")
                                .where("qc.qrs_active_qbk_id = qqm.qqm_qbk_id")
                                .andWhere("qc.qrs_qiz_id = :qiz_id", {qiz_id: qid})
                                .andWhere('qc.qrs_usr_id = :qrs_usr_id', { qrs_usr_id: user_detail[0] })
                                .andWhere('qc.qrs_uty_id = :qrs_uty_id', {qrs_uty_id: user_detail[1]})
                                .getQuery();
                            return `(CASE 
                            WHEN qr.qrs_active_qbk_id IS NOT NULL THEN qbk.qbk_id = ` + activeQuestion+` ELSE qbk.qbk_id NOT IN ` + excludeQuestion +
                            `END)`;
                        })
                        .leftJoin('q.qizQct', 'category')
                        .leftJoinAndSelect('q.gurQuizQuestionMappings', 'qqm')
                        .leftJoin('qqm.qqmQbk', 'qbk')
                        .leftJoinAndSelect('qbk.gurQuizQuestionOptions', 'qbo')
                        .leftJoinAndSelect('q.gurQuizResults', 'qr')
                        .leftJoinAndSelect('qr.gurQuizResultDetails', 'qrd')
                        //.limit(1)
                        .orderBy('RAND()')
                        .getOne();
                    
        //console.log(quiz);
        
        let conf = new QuizConfig();
        if(quiz){
            conf.name = quiz.qiz_name;
            conf.slug = quiz.qiz_slug;
            conf.desc = quiz.qiz_description;
            conf.total_que = quiz.qiz_total_questions;
            conf.answered_que = quiz.gurQuizResults[0].gurQuizResultDetails.length;
            conf.dur = quiz.qiz_duration;
            conf.time_consumed = quiz.gurQuizResults[0].qrs_time_consumed;
            conf.show_palette = Boolean(quiz.qiz_show_palette);
            conf.eqtl = quiz.qiz_each_question_time_limit;
            conf.auto_move = Boolean(quiz.qiz_auto_move);
            conf.instant_answer = Boolean(quiz.qiz_instant_answer);
            conf.passing_score = quiz.qiz_passing_score;
            conf.instruction = quiz.qiz_instruction;
            
            let que = new Question();
                que.id = Number(quiz.gurQuizQuestionMappings[0].qqmQbk.qbk_id);
                que.que = quiz.gurQuizQuestionMappings[0].qqmQbk.qbk_question;
                que.hint = quiz.gurQuizQuestionMappings[0].qqmQbk.qbk_question_hint;
                
                quiz.gurQuizQuestionMappings[0].qqmQbk.gurQuizQuestionOptions.forEach(option => {
                    let opt = new Option();
                    opt.id = Number(option.qbo_id);
                    opt.opt = option.qbo_option;
                    opt.ic = Boolean(option.qbo_is_correct);
                    que.options.push(opt);
                });
                conf.questions.push(que);

        }

        //Send the quizs object
        res.status(200).send({
            status_code: 200, 
            status: 'success', 
            message: 'Data successfully fetched.', 
            data: conf
        });
    };


    static test = async (req: Request, res: Response) => {
        //Get the jwt token from the head
        const token = <string>req.headers["user_detail"];
        var user_detail = Buffer.from(token, 'base64').toString().split("|");
        console.log(user_detail);
        //Get the ID from the url
        const qid: string = req.params.id;

        //Get quizs from database
        const quizRepository = getRepository(gur_quiz);
        
        const quiz = await quizRepository.createQueryBuilder()
                        .select(['q.qiz_id', 'q.qiz_name', 'q.qiz_slug', 'q.qiz_description', 'q.qiz_total_questions', 'q.qiz_quiz_level', 'q.qiz_duration', 'q.qiz_show_palette', 'q.qiz_each_question_time_limit', 'q.qiz_auto_move', 'q.qiz_instant_answer', 'q.qiz_passing_score', 'q.qiz_instruction', 'category.qct_id', 'category.qct_name', 'qbk.qbk_id', 'qbk.qbk_difficulty_level', 'qbk.qbk_type', 'qbk.qbk_question', 'qbk.qbk_question_hint', 'qrd.qrd_qbk_id', 'qr.qrs_active_qbk_id', 'qr.qrs_usr_id'])
                        .from(gur_quiz, "q")
                        .where('q.qiz_id = :qiz_id', {qiz_id: qid})
                        .leftJoinAndSelect('q.gurQuizResults', 'qr')
                        .orderBy('RAND()')
                        //.limit(1)
                        .getOne();
                    
        console.log(quiz);
        
        let conf = new QuizConfig();
        if(quiz){
            conf.name = quiz.qiz_name;
            conf.slug = quiz.qiz_slug;
            conf.desc = quiz.qiz_description;
            conf.total_que = quiz.qiz_total_questions;
            conf.dur = quiz.qiz_duration;
            conf.show_palette = Boolean(quiz.qiz_show_palette);
            conf.eqtl = quiz.qiz_each_question_time_limit;
            conf.auto_move = Boolean(quiz.qiz_auto_move);
            conf.instant_answer = Boolean(quiz.qiz_instant_answer);
            conf.passing_score = quiz.qiz_passing_score;
            conf.instruction = quiz.qiz_instruction;
            
            let que = new Question();
                que.id = Number(quiz.gurQuizQuestionMappings[0].qqmQbk.qbk_id);
                que.que = quiz.gurQuizQuestionMappings[0].qqmQbk.qbk_question;
                que.hint = quiz.gurQuizQuestionMappings[0].qqmQbk.qbk_question_hint;
                
                quiz.gurQuizQuestionMappings[0].qqmQbk.gurQuizQuestionOptions.forEach(option => {
                    let opt = new Option();
                    opt.id = Number(option.qbo_id);
                    opt.opt = option.qbo_option;
                    opt.ic = Boolean(option.qbo_is_correct);
                    que.options.push(opt);
                });
                conf.questions.push(que);

        }

        //Send the quizs object
        res.status(200).send({
            status_code: 200, 
            status: 'success', 
            message: 'Data successfully fetched.', 
            data: conf
        });
    };

};

export default QuestionController;