import { Question } from "./Question";

export class QuizConfig {
    name: string;
    slug: string;
    desc: string;
    q_level: string;
    total_que: number;
    answered_que: number;
    dur: string;
    time_consumed:string;
    show_palette: boolean;
    eqtl:string;
    auto_move: boolean;
    instant_answer:boolean;
    passing_score: number;
    instruction: string;
    //questions = [];
    questions: Array<Question> = [];
}