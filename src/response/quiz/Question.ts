import { Option } from "./Option";

export class Question {
    id: number;
    que: string;
    hint: string;
    options: Array<Option> = [];
}